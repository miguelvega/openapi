package org.openlogics.security.https;

import com.google.common.io.ByteStreams;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.security.HttpsException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * @author Miguel Vega
 * @version $Id: HttpsTest.java 0, 2013-11-04 11:53 AM mvega $
 */
public class HttpsTest {
    @Test
    public void testGoogleHTTPS() throws IOException {
        Https https = new Https(new URL("https://www.google.com/"));
        ByteArrayOutputStream to = new ByteArrayOutputStream();
        ByteStreams.copy(https.openStream(), to);

        System.out.println("*********");
        System.out.println(new String(to.toByteArray()));
    }

    @Test
    @Ignore("Server not found")
    public void testLocalHTTPS() throws IOException, KeyManagementException, NoSuchAlgorithmException {
        Https https = new Https(new URL("https://localhost:8444/tigomoneyws?wsdl"));

//        PEMReader reader = new PEMReader();

        X509Certificate certificate = null;

        try {
            CertificateFactory x509 = CertificateFactory.getInstance("X509");
            certificate = (X509Certificate) x509.generateCertificate(getClass().getResourceAsStream("/simplecert.pem"));
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        https.enableSSL(certificate);

        ByteArrayOutputStream to = new ByteArrayOutputStream();
        ByteStreams.copy(https.openStream(), to);

        System.out.println("*********");
        System.out.println(new String(to.toByteArray()));
    }

    @Test
    @Ignore("not working")
    public void testRetrieveCertificate() throws IOException, HttpsException {
        System.setProperty( "sun.security.ssl.allowUnsafeRenegotiation", "true" );

        Https https = new Https(new URL("https://localhost:8444/tigomoneyws?wsdl"));

        https.a();
      /*
        Certificate[] certificates = https.retrieveCertificates();

        for (Certificate cert : certificates) {
            System.out.println("Cert: "+cert.getClass());
        }*/
    }
}