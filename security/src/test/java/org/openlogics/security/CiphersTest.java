package org.openlogics.security;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

public class CiphersTest {

    static final Logger LOGGER = LoggerFactory.getLogger(CiphersTest.class);

    private final String keyStoreAlias = "mykeystore";

    //when not using -name "My Alias" in OPENSSL, the alias is randomically generated
    //private final String keyStoreAlias = "b40f9435f0581ad647fd2fc67e361a9a2b1163fd";//for testing: ca_no_alias.p12

    private final String pkcs12Passwd = "poipoipoi";

    private KeyStore keyStore;

    @Before
    public void init() throws NoSuchProviderException, KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        Security.addProvider(new
                org.bouncycastle.jce.provider.BouncyCastleProvider());

        this.keyStore = KeyStore.getInstance("PKCS12", "BC");

        keyStore.load(getClass().getResourceAsStream("/ca.p12"), pkcs12Passwd.toCharArray());
    }

    @Test
    @Ignore("can not run from mvn test")
    public void testDoEncrypt() throws Exception, SecurityError {

        KeyPair keyPair = getKeyPair(keyStore, keyStoreAlias, pkcs12Passwd.toCharArray());
        PublicKey publicKey = keyPair.getPublic();

        CharSequence charSequence = Ciphers.doEncrypt("Miguel Vega", publicKey);

        LOGGER.info("Encrypted: {}", charSequence);
    }

    @Test
    @Ignore("can not run from mvn test")
    public void testDoDecrypt() throws Exception, SecurityError {
        String encText = "ERfU4O+cmSxjX15I7fqRGf30RZoYE5aVzDMIxcHHAjl1fr03bZ43YdkW+XnPlVgiY7MyW6HtxxL0clB/1kX3vaqLCKITNvkeXUaDDYwgLiAhf+iKAxiSk/XvPAun8HQDC8UHeURk0SMBpGURsEJJ5QCajyJwROl6r8FntUVPKekPiraz7VaRW5PvsOw3ejrV23/8mpZ1AqM2m1xs7EFqZlFR3AK4g5Kz0adV05D/9S6CV6Xq5DS0/EdgcNt42Jx5vBczzkxRR662nWiqlRi1WzX6My6GIrZLdBpO+gbbEJ09Jp9JkwmjCtsJki0CBXyQFh9xn2iQ16Tg2I0pdSKep+f7O1SJL/GEmioY3OuvFdgecB/kF23MVn/UAWUe7SjixnpnUz2Z7pqjp/1849fGjGBHWODO3w1zrCikze0m9sqhdOezgeoW+FVbUD/SdI2ADKSX8ncQraD6Tx9M3PTqoxOpTMbhlr8zzapOf0EqvhFIBz5xPXMBecXOmfYIodZaLsQtIdURw1wn6CoxYPrG6rpT2/vqcvJrkogRZifMgX1L4wzAz4ebJg35+VZROp1wHs9wg+9bYBzqrJJMgeXc6EO3cGV84JlZfOlaIt5hlPBIBHN/dAgjkyUpY/qAJKwUQlr661GWqElc+S8QrW/4xLdGajp7IhLG7rtt9i16EcQ=";

        KeyPair keyPair = getKeyPair(keyStore, keyStoreAlias, pkcs12Passwd.toCharArray());
        PrivateKey privateKey = keyPair.getPrivate();

        CharSequence charSequence = Ciphers.doDecrypt(encText, privateKey);

        LOGGER.info("Decrypted: {}", charSequence);
    }

    private KeyPair getKeyPair(KeyStore keystore, String alias,
                               char[] password) {
        try {
            Key key = keystore.getKey(alias, password);
            if (key instanceof PrivateKey) {
                java.security.cert.Certificate cert = keystore
                        .getCertificate(alias);
                PublicKey publicKey = cert.getPublicKey();
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (UnrecoverableKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (KeyStoreException e) {
        }
        return null;
    }
}