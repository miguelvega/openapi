package org.openlogics.security;

import com.google.common.io.ByteStreams;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 * @author Miguel Vega
 * @version $Id: PKCS12Test.java 0, 2013-09-25 6:33 PM mvega $
 */
public class PKCS12Test {

    private final String keyStoreAlias = "mykeystore";

    //when not using -name "My Alias" in OPENSSL, the alias is randomically generated
    //private final String keyStoreAlias = "b40f9435f0581ad647fd2fc67e361a9a2b1163fd";//for testing: ca_no_alias.p12

    private final String pkcs12Passwd = "poipoipoi";

    File srcFile = new File("/tmp/testfile");
    File encFile = new File("/tmp/testfile_enc");
    File decFile = new File("/tmp/testfile_dec");

    private Logger LOGGER = LoggerFactory.getLogger(PKCS12Test.class);
    private InputStream pkcs12Stream;
    private String privatePemFile = "/tmp/private.pem";

    @Test
    @Ignore("can not run from mvn test")
    public void simpleCipher() throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        byte[] input = " www.java2s.com ".getBytes();
        byte[] keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03,
                0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b,
                0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13,
                0x14, 0x15, 0x16, 0x17 };

        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "BC");
        System.out.println("input text : " + new String(input));

        // encryption pass

        byte[] cipherText = new byte[input.length];
        cipher.init(Cipher.ENCRYPT_MODE, key);
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        ctLength += cipher.doFinal(cipherText, ctLength);
        System.out.println("cipher text: " + new String(cipherText) + " bytes: " + ctLength);

        // decryption pass

        byte[] plainText = new byte[ctLength];
        cipher.init(Cipher.DECRYPT_MODE, key);
        int ptLength = cipher.update(cipherText, 0, ctLength, plainText, 0);
        ptLength += cipher.doFinal(plainText, ptLength);
        System.out.println("plain text : " + new String(plainText) + " bytes: " + ptLength);
    }

    private static byte[] hex2String(String ss) {
        byte digest[] = new byte[ss.length() / 2];
        for (int i = 0; i < digest.length; i++) {
            String byteString = ss.substring(2 * i, 2 * i + 2);
            int byteValue = Integer.parseInt(byteString, 16);
            digest[i] = (byte) byteValue;
        }
        return digest;
    }

    private String toHexString(byte b[]) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            String plainText = Integer.toHexString(0xff & b[i]);
            if (plainText.length() < 2)
                plainText = "0" + plainText;
            hexString.append(plainText);
        }
        return hexString.toString();
    }

    /**
     * Extract the Private Key from the Keystore.
     *
     * @param keystore
     * @param alias
     * @param password
     * @return
     */
    private KeyPair getPrivateKey(KeyStore keystore, String alias,
                                  char[] password) {
        try {
            Key key = keystore.getKey(alias, password);
            if (key instanceof PrivateKey) {
                java.security.cert.Certificate cert = keystore
                        .getCertificate(alias);
                PublicKey publicKey = cert.getPublicKey();
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (UnrecoverableKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (KeyStoreException e) {
        }
        return null;
    }

    @Before
    public void init() {
        pkcs12Stream = getClass().getResourceAsStream("/ca.p12");
    }

    @Test
    public void testEncrypt() throws IOException, CertificateException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        if (!srcFile.exists()) {
            new FileOutputStream(srcFile).write("hola!".getBytes());
        }

        PublicKey publicKey = getPublicKey();

        encrypt(srcFile, publicKey, "RSA");
    }

    @Test
    @Ignore("can not run from mvn test")
    public void testDecrypt() throws Exception {
        if (!srcFile.exists()) {
            testEncrypt();
        }

        //generate private key from pem
        exportPrivateKey();

        Key privateKey = getPrivateKeyFromPem();

        decrypt(encFile, decFile, privateKey, "RSA");
    }

    /**
     * Encrypt the contents of a File.
     *
     * @param file
     * @param key            public KEY for encryption
     * @param transformation
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public void encrypt(File file, Key key, String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IOException {

        Cipher c = Cipher.getInstance(transformation);

        c.init(Cipher.ENCRYPT_MODE, key);

        FileInputStream is = new FileInputStream(file);

        CipherOutputStream os = new CipherOutputStream(new FileOutputStream(encFile), c);

        ByteStreams.copy(is, os);

        os.close();

        os = new CipherOutputStream(System.out, c);
        ByteStreams.copy(is, os);

        is.close();
    }

    /**
     * Decrypt the contents of a file.
     *
     * @param encryptedFile
     * @param decryptedFile
     * @param privateKey
     * @param transformation
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public void decrypt(File encryptedFile, File decryptedFile,
                        Key privateKey, String transformation)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IOException {
        Cipher c = Cipher.getInstance(transformation);

        c.init(Cipher.DECRYPT_MODE, privateKey);

        CipherInputStream is = new CipherInputStream(new FileInputStream(
                encryptedFile), c);
        FileOutputStream os = new FileOutputStream(decryptedFile);

        ByteStreams.copy(is, os);

        is.close();
        os.close();
    }

    /**
     * Reads and converts the private key from the Private key PEM.
     *
     * @return
     * @throws IOException
     */
    private Key getPrivateKeyFromPem() throws IOException {

        PEMReader pemReader = new PEMReader(new InputStreamReader(
                new FileInputStream(privatePemFile)));

        Object obj;
        while ((obj = pemReader.readObject()) != null) {
            if (obj instanceof X509Certificate) {
                System.out.println("X509Certificate found");
            } else if (obj instanceof PrivateKey) {
                PrivateKey key = (PrivateKey) obj;
                System.out.println("Private Key found");
            } else if (obj instanceof KeyPair) {
                KeyPair keyPair = (KeyPair) obj;
                System.out.println("KeyPair found");
                return keyPair.getPrivate();
            } else {
                System.out.println(" SOMETING ELSE FOUND");
            }
        }

        return null;

    }

    /**
     * Export Private Key to File
     *
     * @throws Exception
     */
    public void exportPrivateKey() throws Exception {
        Security.addProvider(new
                org.bouncycastle.jce.provider.BouncyCastleProvider());

        KeyStore keystore = KeyStore.getInstance("PKCS12", "BC");

        keystore.load(pkcs12Stream, pkcs12Passwd.toCharArray());

        Enumeration<String> aliases = keystore.aliases();
        while(aliases.hasMoreElements()){
            System.err.println("ALIAS:"+aliases.nextElement());
        }

        KeyPair keyPair = getPrivateKey(keystore, keyStoreAlias, pkcs12Passwd.toCharArray());
        PrivateKey privateKey = keyPair.getPrivate();

        /**
         * THIS DOES NOT WORK, GENERATES INCORRECT SEQUENCE
         *
         * String encoded = new String(Base64.encode(privateKey.getEncoded()));
         *
         * FileWriter fw = new FileWriter("private.key");
         * fw.write("-----BEGIN RSA PRIVATE KEY-----\n"); fw.write(encoded);
         * fw.write("\n"); fw.write("-----END RSA PRIVATE KEY-----");
         * fw.close();
         **/

        FileWriter fw = new FileWriter(privatePemFile);
        PEMWriter writer = new PEMWriter(fw);
        writer.writeObject(privateKey);
        writer.close();
    }

    /**
     * Get Public Key from Certificate.
     *
     * @return
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    private PublicKey getPublicKey() throws CertificateException {

        InputStream certFileIs = getClass().getResourceAsStream("/ca.pem");

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) cf.generateCertificate(certFileIs);

        return cert.getPublicKey();
    }


}
