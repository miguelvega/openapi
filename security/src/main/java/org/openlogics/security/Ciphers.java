package org.openlogics.security;

import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.Key;

import static com.google.common.base.Charsets.UTF_8;

/**
 * In the source for ACH-ACCL, cipher instantiation is not created with the BC provider
 *
 * @author miguel
 * @version $Id: Ciphers.java 0, 9/26/13 11:42 AM mvega $
 */
public class Ciphers {

    static final String ENCRYPT_ERROR = "ERROR-CC01";
    static final String DECRYPT_ERROR = "ERROR-CD01";

    final static Logger LOGGER = LoggerFactory.getLogger(Ciphers.class);

    public static final CharSequence doEncrypt(String plainText, Key key) throws SecurityError {

        assert plainText != null && key != null;

        try {
            byte[] bytesEncriptados;

            Cipher cipher = Cipher.getInstance(key.getAlgorithm(), "BC");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            bytesEncriptados = cipher.doFinal(plainText.getBytes());
            byte[] encoded = Base64.encode(bytesEncriptados);
            String encryptedText = new String(encoded, 0, encoded.length, UTF_8.name());

            LOGGER.debug("Text '{}', encrypted successfully to '{}'", plainText, encryptedText);

            return encryptedText;
        } catch (GeneralSecurityException e) {
            throw new SecurityError("Unable to encrypt given pair text and key", e).setCode(ENCRYPT_ERROR);
        } catch (UnsupportedEncodingException e) {
            throw new SecurityError("Unable to decrypt given pair text and key. Unsupported Encoding:" + UTF_8.name(), e).setCode(DECRYPT_ERROR);
        }
    }

    public static final CharSequence doDecrypt(String encryptedText, Key key) throws SecurityError {

        assert encryptedText != null && key != null;

        try {
            Cipher cipher = Cipher.getInstance(key.getAlgorithm(), "BC");
            cipher.init(Cipher.DECRYPT_MODE, key);

            byte[] ciphertext = Base64.decode(encryptedText.getBytes(UTF_8.name()));
            byte[] clear = cipher.doFinal(ciphertext);
            String plainText = new String(clear, 0, clear.length, UTF_8.name());

            LOGGER.debug("Text '{}', decrypted successfully to '{}'", encryptedText, plainText);

            return plainText;
        } catch (GeneralSecurityException e) {
            throw new SecurityError("Unable to decrypt given pair text and key", e).setCode(DECRYPT_ERROR);
        } catch (UnsupportedEncodingException e) {
            throw new SecurityError("Unable to decrypt given pair text and key. Unsupported Encoding:" + UTF_8.name(), e).setCode(DECRYPT_ERROR);
        }
    }
}
