package org.openlogics.security;

import com.google.common.collect.ImmutableList;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.*;
import java.util.Enumeration;
import java.util.List;

/**
 * A utility class to handle operations in key stores
 * @author Miguel Vega
 * @version $Id: KeyStores.java 0, 9/26/13 11:44 AM mvega $
 */
public class KeyStores {

    private Logger LOGGER = LoggerFactory.getLogger(KeyStores.class);

    public static List<Certificate> listCertificates(KeyStore keyStore) throws java.security.KeyStoreException {
        ImmutableList.Builder<Certificate> certs = new ImmutableList.Builder<Certificate>();

        Enumeration<String> aliases = keyStore.aliases();
        while(aliases.hasMoreElements()){
            Certificate certificate = keyStore.getCertificate(aliases.nextElement());
            certs.add(certificate);
        }

        return certs.build();
    }

    /**
     * Extract the Private Key from the Keystore.
     *
     * @param keystore
     * @param alias
     * @param password
     * @return
     */
    public static KeyPair getKeyPair(KeyStore keystore, String alias, char[] password) {
        try {
            //get the key from keystore.p12
            Key key = keystore.getKey(alias, password);

            if (key instanceof PrivateKey) {
                java.security.cert.Certificate cert = keystore
                        .getCertificate(alias);
                PublicKey publicKey = cert.getPublicKey();
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (UnrecoverableKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (KeyStoreException e) {
        }
        return null;
    }

    /**
     *
     * @param inputStream
     * @return
     */
    public static Key readFromPEM(InputStream inputStream) throws IOException {
        PEMReader pemReader = new PEMReader(new InputStreamReader(inputStream));

        Object obj;
        //iterate through all certificates
        while ((obj = pemReader.readObject()) != null) {
            if (obj instanceof X509Certificate) {
                System.out.println("X509Certificate found");
            } else if (obj instanceof PrivateKey) {
                PrivateKey key = (PrivateKey) obj;
                System.out.println("Private Key found");
            } else if (obj instanceof KeyPair) {
                KeyPair keyPair = (KeyPair) obj;
                System.out.println("KeyPair found");
                return keyPair.getPrivate();
            } else {
                System.out.println(" SOMETING ELSE FOUND");
            }
        }

        return null;
    }

    /**
     * Converts a string of a Base64 encoded certificate (PEM format) into a byte array.
     * @param str a string representing a Base 64 encoded X509 certificate (PEM format)
     * @return a byte array
     */
    public static byte[] base64PEMStringToByteArray(String str) {
        return str.getBytes();
    }

    /**
     * Converts a byte array to a X509Certificate instance.
     * @param bytes the byte array
     * @return a X509Certificate instance
     * @throws CertificateException if the conversion fails
     */
    public static X509Certificate fromByteArrayToX509Certificate(byte[] bytes) throws CertificateException {
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        InputStream in = new ByteArrayInputStream(bytes);
        X509Certificate cert = (X509Certificate) certFactory.generateCertificate(in);
        return cert;
    }

    /**
     * Converts a X509Certificate instance into a Base64 encoded string (PEM format).
     * @param cert a certificate
     * @return a string (PEM format)
     * @throws CertificateEncodingException if the conversion fails
     */
    public static String x509ToBase64PEMString(X509Certificate cert) throws IOException {
        // Convert certificate to PEM format.
        StringWriter sw = new StringWriter();

        PEMWriter pw = new PEMWriter(sw);
        pw.writeObject(cert);

        return sw.toString();
    }
}
