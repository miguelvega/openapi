package org.openlogics.security;

/**
 * @author Miguel Vega
 * @version $Id: SecurityException.java 0, 5/22/14 11:54 PM, @miguel $
 */
public class SecurityError extends Throwable{

    private String code;

    public SecurityError(String message) {
        super(message);
    }

    public SecurityError(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityError(Throwable cause) {
        super(cause);
    }

    public String getCode() {
        return code;
    }

    public SecurityError setCode(String code) {
        this.code = code;
        return this;
    }
}
