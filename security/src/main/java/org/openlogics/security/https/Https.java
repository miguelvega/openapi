package org.openlogics.security.https;

import org.openlogics.security.HttpsException;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/**
 * This class allows to consume a HTTPS
 *
 * @author Miguel Vega
 * @version $Id: Https.java 0, 2013-11-04 11:49 AM mvega $
 */
public class Https {
    final URL url;

    public Https(URL url) {
        assert url != null;

        this.url = url;
    }

    public InputStream openStream() throws IOException {
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        return con.getInputStream();
    }

    public URL enableSSL(final X509Certificate certificate) throws NoSuchAlgorithmException, KeyManagementException {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{certificate};
            }

            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }
        }};

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        // Create empty HostnameVerifier
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };

        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        return url;
    }

    public void a() throws IOException {
        /**
         *443 is the network port number used by the SSL https:URi scheme.
         */
        int port = 443;

        String hostname = url.getHost();

        SSLSocketFactory factory = HttpsURLConnection.getDefaultSSLSocketFactory();

        System.out.println("Creating a SSL Socket For " + hostname + " on port " + port);

        SSLSocket socket = (SSLSocket) factory.createSocket(hostname, port);

/**
 * Starts an SSL handshake on this connection. Common reasons include a
 * need to use new encryption keys, to change cipher suites, or to
 * initiate a new session. To force complete reauthentication, the
 * current session could be invalidated before starting this handshake.
 * If data has already been sent on the connection, it continues to flow
 * during this handshake. When the handshake completes, this will be
 * signaled with an event. This method is synchronous for the initial
 * handshake on a connection and returns when the negotiated handshake
 * is complete. Some protocols may not support multiple handshakes on an
 * existing socket and may throw an IOException.
 */

        socket.startHandshake();
        System.out.println("Handshaking Complete");

/**
 * Retrieve the server's certificate chain
 *
 * Returns the identity of the peer which was established as part of
 * defining the session. Note: This method can be used only when using
 * certificate-based cipher suites; using it with non-certificate-based
 * cipher suites, such as Kerberos, will throw an
 * SSLPeerUnverifiedException.
 *
 *
 * Returns: an ordered array of peer certificates, with the peer's own
 * certificate first followed by any certificate authorities.
 */
        Certificate[] serverCerts = socket.getSession().getPeerCertificates();
        System.out.println("Retreived Server's Certificate Chain");

        System.out.println(serverCerts.length + "Certifcates Found\n\n\n");
        for (int i = 0; i < serverCerts.length; i++) {
            Certificate myCert = serverCerts[i];
            System.out.println("====Certificate:" + (i + 1) + "====");
            System.out.println("-Public Key-\n" + myCert.getPublicKey());
            System.out.println("-Certificate Type-\n " + myCert.getType());

            System.out.println();
        }

        socket.close();
    }

    public Certificate[] retrieveCertificates() throws HttpsException {


        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {

                    public boolean verify(String hostname, javax.net.ssl.SSLSession
                            sslSession) {
                        if (hostname.equals("")) {
                            try {
                                javax.security.cert.X509Certificate[] serverChain = sslSession.getPeerCertificateChain();
                            } catch (SSLPeerUnverifiedException e) {
                                e.printStackTrace();
                            }
                            return true;
                        }
                        return false;
                    }
                });


        final HttpsURLConnection con;
        try {
            con = (HttpsURLConnection) url.openConnection();
        } catch (IOException e) {
            throw new HttpsException(e);
        }

        if (con != null) {

            try {

                con.connect();

//                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                Certificate[] certs = con.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : "
                            + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : "
                            + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

                return certs;
            } catch (SSLPeerUnverifiedException e) {
                throw new HttpsException(e);
            } catch (IOException e) {
                throw new HttpsException(e);
            }
        }

        return null;
    }

    private void print_content(HttpsURLConnection con) {
        if (con != null) {

            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br =
                        new BufferedReader(
                                new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null) {
                    System.out.println(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
