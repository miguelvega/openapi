package org.openlogics.security;

/**
 * @author Miguel Vega
 * @version $Id: HttpsException.java 0, 2013-11-04 12:22 PM mvega $
 */
public class HttpsException extends Throwable{
    public HttpsException(String message) {
        super(message);
    }

    public HttpsException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpsException(Throwable cause) {
        super(cause);
    }
}
