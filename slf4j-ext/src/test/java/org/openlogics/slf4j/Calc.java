package org.openlogics.slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * @author Miguel Vega
 * @version $Id: Calc.java 0, 8/1/14 10:16 PM, @miguel $
 */
public class Calc {
    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public void sum(BigDecimal a, BigDecimal b){
        debug("sum", a.add(b));
        info("sum", a.add(b));
        trace("sum", a.add(b));
        warn("sum", a.add(b));
        error("sum", a.add(b));
    }

    private void debug(String text, Object o){
        LOGGER.debug("{} - {}", text, o);
    }
    private void info(String text, Object o){
        LOGGER.info("{} - {}", text, o);
    }
    private void warn(String text, Object o){
        LOGGER.warn("{} - {}", text, o);
    }
    private void error(String text, Object o){
        LOGGER.error("{} - {}", text, o);
    }
    private void trace(String text, Object o){
        LOGGER.trace("{} - {}", text, o);
    }

}