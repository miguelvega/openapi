package org.openlogics.slf4j;

import org.apache.log4j.*;
import org.apache.log4j.xml.XMLLayout;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;

import static com.google.common.base.Strings.repeat;

public class LoggersTest {

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Test
    public void testAppenders() throws IOException, SAXException {
        ConsoleAppender appender = new ConsoleAppender(new SimpleLayout());
        Loggers.append(appender);
        LOGGER.info("Hello world, console appender ready");

        String PATTERN = "%d [%p|%c|%C{1}] %m%n";
        ConsoleAppender patternAppender = new ConsoleAppender();
        patternAppender.setLayout(new PatternLayout(PATTERN));
//        appender.setThreshold(Level.FATAL);
        patternAppender.activateOptions();
        Loggers.append(patternAppender);
        LOGGER.info("Console now has pattern");

        FileAppender fa = new FileAppender();
        fa.setName("FileLogger");
        final String file = "/tmp/mylog.log";
        fa.setFile(file);
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        fa.setThreshold(Level.DEBUG);
        fa.setAppend(true);
        fa.activateOptions();
        Loggers.append(fa);

        LOGGER.info("And now in a file: {}", file);

        Loggers.reset();

//        final String rollingFileName = "/tmp/foo.%d{yyyy-MM}.log";
        final String rollingFileName = "/tmp/rolling.log";
        RollingFileAppender rollingFileAppender = new RollingFileAppender();
        rollingFileAppender.setLayout(new XMLLayout());
        rollingFileAppender.setName("Rolling");
        rollingFileAppender.setFile(rollingFileName);
        rollingFileAppender.setMaxBackupIndex(3);
//        rollingFileAppender.setMaximumFileSize(1024);
        rollingFileAppender.setMaxFileSize("3kb");
        rollingFileAppender.setAppend(true);
        rollingFileAppender.activateOptions();
        Loggers.append(rollingFileAppender);

        /*
        Xml xml = new Xml(getClass().getResourceAsStream("/xml-log4j.xml"));
        Loggers.init(xml.getDocument().getDocumentElement());*/

        for (int i = 0; i < 5; i++) {
            LOGGER.info(i + "", new IllegalStateException(":S"));
        }

    }

    @Test
    public void testFetchActiveLoggers() {
        Set<String> lnames = Loggers.fetchActiveLoggers();
        for (String lname : lnames) {
            LOGGER.info("Logger name: {}", lname);
        }
    }

    @Test
    public void testLoadCurrentLoggers() {
        Calc calc = new Calc();

        org.apache.log4j.Logger logger = null;

        Set<Metainf> strings = Loggers.fetchGroupedLoggers("org");
        for (Metainf loggerMeta : strings) {
            LOGGER.info("Logger found: {}", loggerMeta);
            //break;
        }

        logger = org.apache.log4j.Logger.getLogger(strings.iterator().next().getName());

        LOGGER.info(repeat("-", 100));
        calc.sum(BigDecimal.valueOf(1), BigDecimal.valueOf(2));
        debug();
        trace();
        LOGGER.info(repeat("-", 100));

        logger.setLevel(Level.TRACE);

        calc.sum(BigDecimal.valueOf(1), BigDecimal.valueOf(2));
        debug();
        trace();
        LOGGER.info(repeat("-", 100));


    }

    private void debug() {
        LOGGER.debug("Debug triggered");
    }

    private void info() {
        LOGGER.info("Info triggered");
    }

    private void warn() {
        LOGGER.warn("Warn triggered");
    }

    private void error() {
        LOGGER.error("Error triggered");
    }

    private void trace() {
        LOGGER.trace("Trace triggered");
    }
}