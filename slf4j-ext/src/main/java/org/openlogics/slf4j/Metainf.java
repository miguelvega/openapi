package org.openlogics.slf4j;

import java.io.Serializable;

/**
 * @author Miguel Vega
 * @version $Id: LoggerMetadata.java 0, 8/2/14 11:23 AM, @miguel $
 */
public class Metainf implements Serializable, Comparable<Metainf>{
    private final String name;
    private final int children;

    public Metainf(String name, int children) {
        this.children = children;
        this.name = name;
    }

    public int getChildrenCount() {
        return children;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Metainf o) {
        return getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return new StringBuilder(name).append(", affects ").append(children).append(" descendants").toString();
    }
}
