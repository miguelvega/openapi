package org.openlogics.slf4j;

import org.apache.log4j.spi.LoggingEvent;

/**
 * This skeleton will intercept all log messages which matches a specific regular expression within message, thus
 * will send message to a predefined delegate.
 *
 * @author Miguel Vega
 * @version $Id: RegexLoggerAppender.java 0, 8/14/14 11:20 PM, @miguel $
 */
public class RegexAppenderManager extends AppenderSkeletonManager {

    @Override
    protected void append(LoggingEvent event) {

    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return false;
    }
}