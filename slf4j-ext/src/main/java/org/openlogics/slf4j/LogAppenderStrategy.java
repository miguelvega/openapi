package org.openlogics.slf4j;

import org.apache.log4j.pattern.LogEvent;

/**
 * A class which will manage what to do with a message provided by {@link AppenderSkeletonManager}
 * @author Miguel Vega
 * @version $Id: LogAppenderDelegate.java 0, 8/15/14 12:30 PM, @miguel $
 */
public interface LogAppenderStrategy {
    void receive(LogEvent logEvent);
}
