package org.openlogics.slf4j;

import org.apache.log4j.AppenderSkeleton;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Miguel Vega
 * @version $Id: AbstractLoggerSkeleton.java 0, 8/15/14 12:31 PM, @miguel $
 */
public abstract class AppenderSkeletonManager extends AppenderSkeleton{
    //todo, this store must be dynamic maybe even retrieved from a database
    protected static final Set<String> id = new HashSet<String>();

}
