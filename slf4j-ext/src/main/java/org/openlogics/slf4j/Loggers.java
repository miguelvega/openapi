package org.openlogics.slf4j;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.collect.UnmodifiableIterator;
import org.apache.log4j.*;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Element;

import java.io.InputStream;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import static com.google.common.collect.Iterators.forEnumeration;
import static org.apache.log4j.Logger.getRootLogger;

import static org.apache.log4j.Level.ALL;
import static org.apache.log4j.Level.DEBUG;
import static org.apache.log4j.Level.ERROR;
import static org.apache.log4j.Level.FATAL;
import static org.apache.log4j.Level.INFO;
import static org.apache.log4j.Level.TRACE;
import static org.apache.log4j.Level.WARN;
/**
 * @author Miguel Vega
 * @version $Id: LoggerManager.java 0, 8/1/14 8:35 PM, @miguel $
 * @see http://logging.apache.org/log4j/2.x/manual/appenders.html
 *
 *
 */
public class Loggers{

    private static final Logger LOGGER = Logger.getLogger(Loggers.class);

    public static void append(Appender appender){
        Logger.getRootLogger().addAppender(appender);
    }

    public static void init(InputStream inputStream){
        PropertyConfigurator.configure(inputStream);
    }

    public static void init(Element element){
        DOMConfigurator.configure(element);
    }

    public static void reset(){
        Logger.getRootLogger().getLoggerRepository().resetConfiguration();
    }

    public static Set<String> fetchActiveLoggers(){
        UnmodifiableIterator<Logger> unmodifiableIterator =
                forEnumeration(getRootLogger().getLoggerRepository().getCurrentLoggers());

        Set<String> strings = new TreeSet<String>();
        while (unmodifiableIterator.hasNext()) {
            Logger next = unmodifiableIterator.next();
            String name = next.getName();

            StringTokenizer stringTokenizer = new StringTokenizer(name, ".");
            StringBuilder stringBuilder = new StringBuilder(name.length());
            while (stringTokenizer.hasMoreTokens()) {
                String token = stringTokenizer.nextToken();
                stringBuilder.append(token);
                strings.add(stringBuilder.toString());
                if (stringTokenizer.hasMoreTokens()) {
                    stringBuilder.append('.');
                }
            }
        }
        return strings;

        /*UnmodifiableIterator<Logger> unmodifiableIterator = forEnumeration(getRootLogger().getLoggerRepository().getCurrentLoggers());
        Set<String> strings = new TreeSet<String>();

        while (unmodifiableIterator.hasNext()) {

            Logger next = unmodifiableIterator.next();

            String name = next.getName();

            strings.add(name);
        }
        return strings;*/
    }

    public static Set<Metainf> fetchGroupedLoggers(final String loggerName) {
        @SuppressWarnings("unchecked")
        Set<String> toFilter = fetchActiveLoggers();

        final Set<String> allLoggers = fetchActiveLoggers();

        if (!Strings.isNullOrEmpty(loggerName)) {
            toFilter = Sets.filter(toFilter, new Predicate<String>() {

                @Override
                public boolean apply(String input) {
                    return input != null &&
                            input.toUpperCase().contains(loggerName.toUpperCase());
                }
            });
        }

        Set<Metainf> strings = new TreeSet<Metainf>();

        for (String logger : toFilter) {
            int size = 0;
            for (String s : allLoggers) {
                if (s.startsWith(logger)) {
                    //s=a.b.LogMe
                    //logger=a.b.Log
                    if(s.length()>logger.length()) {
                        char c = s.charAt(s.lastIndexOf(logger) + logger.length());
                        if(c=='.')size++;
                        continue;
                    }
                    size++;
                } else {
                    if (size > 0) {
                        break;
                    }
                }
            }

            if (size > 2) {
                strings.add(new Metainf(logger, size - 1));
            } else if (size > 1) {
                strings.add(new Metainf(logger, 1));
            }else
                strings.add(new Metainf(logger, 0));
        }

        LOGGER.debug("loggersModel returning");

        return strings;
    }

}