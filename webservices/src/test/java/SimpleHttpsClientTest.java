import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * @author Miguel Vega
 * @version $Id: SimpleHttpsClientTest.java 0, 12/25/13 10:55 PM miguel $
 */
public class SimpleHttpsClientTest {
    private URL url;

    @Before
    public void init() {
        try {
//            url = new URL("https://192.168.10.102:8443/soap?wsdl");
            url = new URL("https://192.168.10.102:9443/soap?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //for localhost testing only
        //http://stackoverflow.com/questions/10258101/sslhandshakeexception-no-subject-alternative-names-present
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {

                        if (hostname.equals("localhost") || hostname.equals("192.168.10.102")) {
                            return true;
                        }

                        return false;
                    }
                });
    }

    @Test
    public void consumeHttps() throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {
//        System.setProperty("javax.net.ssl.trustStore", "D:/Documents/tmpkeystore.jks");
//        System.setProperty("javax.net.ssl.trustStorePassword", "12345678");
//        System.setProperty("javax.net.ssl.trustStoreType", "JKS");

        /*
        InputStream in = null;
        try {
            in = url.openStream();

            String s = IOStreams.toString(in);

            System.out.println("...\n"+s);

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOStreams.close(in);
        }
        */
    }
}
