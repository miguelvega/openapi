import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.labs.jaxws.SimpleServerImplService;

/**
 * @author Miguel Vega
 * @version $Id: JAXWSClientTest.java 0, 12/26/13 12:06 AM miguel $
 */
public class JAXWSClientTest {
    @Test
    @Ignore("need server running")
    public void testConsume() {

        //for localhost testing only
        //http://stackoverflow.com/questions/10258101/sslhandshakeexception-no-subject-alternative-names-present
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {

                        if (hostname.equals("localhost") || hostname.equals("192.168.10.102")) {
                            return true;
                        }

                        return false;
                    }
                });

        SimpleServerImplService svr = new SimpleServerImplService();
        System.out.println(svr.getSimpleServerImplPort().getDate("yyyy-MM-dd HH:mm:ss zzz"));
    }
}
