package javaproxy;

import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.ServiceFactory;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Miguel Vega
 * @version $Id: WsdlOnTheFlyTest.java 0, February 09, 2015 10:54 PM mvega $
 */
public class WsdlOnTheFlyTest {
    @Test
    public void testSImpleProxy() throws ServiceException, MalformedURLException {
        ServiceFactory serviceFactory = ServiceFactory.newInstance();
        Service helloService = serviceFactory.createService(new URL("http://soaptest.parasoft.com/calculator.wsdl"),
                        new QName("http://www.parasoft.com/wsdl/calculator/", "add"));

    }
}
