package javaproxy;

/**
 * @author Miguel Vega
 * @version $Id: IRobot.java; ene 06, 2015 04:30 PM mvega $
 */
interface IRobot {

    String Name();

    String Name(String title);

    void Talk();

    void Talk(String stuff);

    void Talk(int stuff);

    void Talk(String stuff, int more_stuff);

    void Talk(int stuff, int more_stuff);

    void Talk(int stuff, String more_stuff);
}
