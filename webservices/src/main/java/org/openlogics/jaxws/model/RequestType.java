package org.openlogics.jaxws.model;

/**
 * This class is intented to have basic necessary attributes and methods to make a request.
 * @author Miguel Vega
 * @version $Id: RequestType.java 0, 1/22/14 4:12 PM, @miguel $
 */
public class RequestType {
    private String sessionId;
    private boolean wait;

    /**
     * A valid session id (for most functions you will have to login)
     */
    protected String getSessionId() {
        return sessionId;
    }

    protected void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Some commands require interaction with external systems or external user i.e. To perform a bank operation or to wait for confirmation. By default a SOAP request will wait for all external transactions to complete before returning success. SOAP will wait for external systems like banks and users to contribute to the transaction. If a SOAP response to only indicate initial acceptance of the transaction is required then the wait parameter can be set to false. When this is done, and the response from soap is success (0), this only means that the transaction was successful so far and that an external entity such as a banking platform may still cause the transaction to fail. In order to have SOAP return immediately without waiting for external parties, set the wait parameter to false. By default wait is assumed to be true.
     */
    protected boolean isWait() {
        return wait;
    }

    protected void setWait(boolean wait) {
        this.wait = wait;
    }
}
