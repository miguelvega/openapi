package org.openlogics.gears.oracle;

import static org.openlogics.utils.ImmutableString.of;

/**
 * <p>
 * Since Oracle 12c, this statements are valid. Using a lower version than ORACLE 12c, simply use a subquery as
 * shown below:
 * </p>
 * <p>
 select * from (
 select a.*, ROWNUM rnum from (
 <select statemenet with order by clause>
 ) a where rownum <= MAX_ROW
 ) where rnum >= MIN_ROW
 * </p>
 * @author Miguel Vega
 * @version $Id: SQLClause.java 0, 5/29/14 10:49 PM, @miguel $
 */
public abstract class SQLClause extends org.openlogics.gears.jdbc.builder.SQLClause{

    protected SQLClause() {
        super();
    }

    /**
     * Builds a limit object, this will be ignored when limit is lower than 1
     *
     * @param limit
     * @return
     */
    public static final SQLClause limit(final long limit) {

        if (limit < 0) {
            return null;
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" FETCH NEXT ").add(limit).add(" ROWS ONLY ").build();
            }
        };
    }

    /**
     * Ignores values below 0
     *
     * @param offset
     * @return
     */
    public static SQLClause offset(final long offset) {
        if (offset < 0) {
            return null;
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" OFFSET ").add(offset).add("ROWS ").build();
            }
        };
    }
}
