package org.openlogics.gears.oracle;

import oracle.sql.TIMESTAMP;

import javax.persistence.AttributeConverter;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;

/**
 * A class to provide the convert for Oracle objects
 * @author Miguel Vega
 * @version $Id: AttributeConverters.java 0, 5/29/14 11:45 PM, @miguel $
 */
public class OracleTypes {
    public static final class TimeMapper implements AttributeConverter<Date, Time> {

        @Override
        public Time convertToDatabaseColumn(Date attribute) {
            return new Time(attribute.getTime());
        }

        @Override
        public Date convertToEntityAttribute(Time dbData) {
            return new Date(dbData.getTime());
        }
    }

    public static final class DateMapper implements AttributeConverter<Date, java.sql.Date>{

        @Override
        public java.sql.Date convertToDatabaseColumn(Date attribute) {
            return new java.sql.Date(attribute.getTime());
        }

        @Override
        public Date convertToEntityAttribute(java.sql.Date dbData) {
            return new Date(dbData.getTime());
        }
    }

    public static final class TimestampMapper implements AttributeConverter<Date, TIMESTAMP>{
        @Override
        public TIMESTAMP convertToDatabaseColumn(Date attribute) {
            return new TIMESTAMP(new java.sql.Date(attribute.getTime()));
        }

        @Override
        public Date convertToEntityAttribute(TIMESTAMP dbData) {
            try {
                return new Date(dbData.dateValue().getTime());
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}