package org.openlogics.gears.postgresql;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;

import static java.lang.System.currentTimeMillis;

public class MappersTest {

    Logger LOGGER = LoggerFactory.getLogger(PostgreSQLTypes.class);

    @Test
    public void testTimeMapper(){
        Time time = new Time(currentTimeMillis());
        LOGGER.info(time.toString());
    }
}