package org.openlogics.gears.postgresql;

import org.junit.Test;

import static org.junit.Assert.*;

public class SQLClauseTest {

    @Test
    public void testLegacy(){
        String groupBy = SQLClause.group().by("mike").by(2).by(3).toSQLString();
        System.out.println(groupBy);

        SQLClause limit = SQLClause.limit(50);
        System.out.println(limit.toSQLString());
    }
}