package org.openlogics.gears.postgresql;

import javax.persistence.AttributeConverter;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * A class to provide the convert for postgresql objects
 * @author Miguel Vega
 * @version $Id: TypeMappers.java 0, 5/29/14 11:45 PM, @miguel $
 */
public class PostgreSQLTypes {
    public static final class TimeMapper implements AttributeConverter<Time, Date> {
        @Override
        public Date convertToDatabaseColumn(Time attribute) {
            return new Date(attribute.getTime());
        }

        @Override
        public Time convertToEntityAttribute(Date dbData) {
            return new Time(dbData.getTime());
        }
    }

    public static final class DateMapper implements AttributeConverter<Date, java.sql.Date>{

        @Override
        public java.sql.Date convertToDatabaseColumn(Date attribute) {
            return new java.sql.Date(attribute.getTime());
        }

        @Override
        public Date convertToEntityAttribute(java.sql.Date dbData) {
            return new Date(dbData.getTime());
        }
    }

    public static final class TimestampMapper implements AttributeConverter<Date, Timestamp>{

        @Override
        public Timestamp convertToDatabaseColumn(Date attribute) {
            return new Timestamp(attribute.getTime());
        }

        @Override
        public Date convertToEntityAttribute(Timestamp dbData) {
            return new Date(dbData.getTime());
        }
    }
}