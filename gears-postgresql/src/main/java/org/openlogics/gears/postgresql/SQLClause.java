package org.openlogics.gears.postgresql;

import static org.openlogics.utils.ImmutableString.of;

/**
 * @author Miguel Vega
 * @version $Id: SQLClause.java 0, 5/29/14 10:49 PM, @miguel $
 */
public abstract class SQLClause extends org.openlogics.gears.jdbc.builder.SQLClause{

    protected SQLClause() {
        super();
    }

    /**
     * Builds a limit object, this will be ignored when limit is lower than 1
     *
     * @param limit
     * @return
     */
    public static final SQLClause limit(final long limit) {

        if (limit < 0) {
            return null;
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" LIMIT ").add(limit).build();
            }
        };
    }

    /**
     * Ignores values below 0
     *
     * @param offset
     * @return
     */
    public static SQLClause offset(final long offset) {
        if (offset < 0) {
            return null;
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" OFFSET ").add(offset).build();
            }
        };
    }
}
