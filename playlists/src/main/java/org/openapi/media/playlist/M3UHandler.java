package org.openapi.media.playlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Miguel A. Vega P.
 * @version $Id: M3UHandler.java; Sep 03, 2017 7:27 PM miguel.vega $
 */
public class M3UHandler {

    public M3UHandler() {
        super();
    }

    public void copyToDevice() {
        Path path = Paths.get("/home/miguel/Dropbox/documents/playlists/SilentHill.m3u");
        path = Paths.get("/home/miguel/Dropbox/documents/playlists/NewAge+.m3u");

        System.out.println("File: "+path.toAbsolutePath().toString());

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(Files.newInputStream(path)))){
            
            String line = null;
            boolean doRead = false;
            while((line = reader.readLine())!=null){
                if(!doRead || line.startsWith("#EXTINF")){
                    doRead = true;
                    continue;
                }
                doRead = false;
                //
                System.out.println(line);

                Path source = Paths.get(line);
                copy(source, Paths.get("/home/miguel/Music/tmp/"+source.getFileName().toString()));

            }
        }catch(IOException x){
            throw new IllegalStateException(x);
        }
    }

    protected void copy(Path path, Path to) throws IOException {
        if(Files.exists(to))return;
        Files.copy(path, to);
    }

    public static void main(String[] args) {
        M3UHandler handler = new M3UHandler();
        handler.copyToDevice();
    }
}
