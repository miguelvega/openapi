package org.openlogics.jdbo;

import org.geotools.feature.DefaultFeatureCollection;
import org.opengis.feature.simple.SimpleFeature;

/**
 * @author Miguel Vega
 * @version $Id: ImmutableFeatureCollection.java 0, 2013-07-04 3:55 PM mvega $
 */
public class ImmutableFeatureCollection extends DefaultFeatureCollection {
    ImmutableFeatureCollection() {
    }

    public static ImmutableFeatureCollection of(SimpleFeature... features) {
        ImmutableFeatureCollection collection = new ImmutableFeatureCollection();
        for (SimpleFeature feature : features) {
            collection.add(feature);
        }
        return collection;
    }
}