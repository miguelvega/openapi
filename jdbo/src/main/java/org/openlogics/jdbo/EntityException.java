package org.openlogics.jdbo;

/**
 * @author Miguel Vega
 * @version $Id: EntityException.java 0, 2013-07-04 3:42 PM mvega $
 */
public class EntityException extends Throwable {
    public EntityException() {
        super();
    }

    public EntityException(String message) {
        super(message);
    }

    public EntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityException(Throwable cause) {
        super(cause);
    }
}
