package org.openlogics.jdbo;

import java.io.IOException;

/**
 * @author Miguel Vega
 * @version $Id: Descriptor.java 0, 2013-06-28 5:09 PM mvega $
 */
public interface Descriptor {
    void fetchMetaData() throws IOException;
}
