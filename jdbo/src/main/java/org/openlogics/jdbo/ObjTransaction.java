package org.openlogics.jdbo;

import com.google.common.cache.Cache;
import com.google.inject.Inject;
import org.geotools.data.FeatureSource;
import org.geotools.data.Transaction;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.identity.FeatureId;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: ObjTransaction.java 0, 2013-07-07 14:42 mvega $
 */
public abstract class ObjTransaction {
    @Inject
    private final Cache<String, FeatureSource<SimpleFeatureType, SimpleFeature>> featureSourceCache;

    private Transaction transaction;

    @Inject
    private Map dataStoreSettings;

    ObjTransaction(Cache<String, FeatureSource<SimpleFeatureType, SimpleFeature>> featureSourceCache) {
        this.featureSourceCache = featureSourceCache;
    }

    public <V> Iterator<FeatureId> insert(V... values) {
        throw new UnsupportedOperationException("");
    }

    public void commit() throws IOException {
        throw new UnsupportedOperationException("");
    }
}
