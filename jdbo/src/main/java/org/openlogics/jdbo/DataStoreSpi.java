package org.openlogics.jdbo;

import org.geotools.data.DataStoreFinder;
import org.geotools.jdbc.JDBCDataStore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Map;

/**
 * This class is an implementation of the Geotools (R) JDBCDataStore.
 *
 * @author Miguel Vega
 * @version $Id: DataStoreSpi.java 0, 2013-06-28 1:09 PM mvega $
 */
public class DataStoreSpi {

    /**
     * <code>
     * Usage:
     * Map map = new HashMap();
     * map.put( "dbtype", "postgis");
     * map.put( "jndiReferenceName", "java:comp/env/jdbc/geotools");
     * <p/>
     * </code>
     *
     * @param parameters
     * @return
     * @throws java.io.IOException
     */
    public static JDBCDataStore of(Map<String, Object> parameters) throws IOException {
        return (JDBCDataStore) DataStoreFinder.getDataStore(parameters);
    }

    public static EntityDescriptor describeEntity(JDBCDataStore dataStore, String schema, String name) throws IOException {
        EntityDescriptor e = new EntityDescriptor(dataStore, schema, name);
        e.fetchMetaData();
        return e;
    }

    public static EntityDescriptor describeEntity(@Nonnull JDBCDataStore dataStore, @Nonnull String name) throws IOException {
        return describeEntity(dataStore, null, name);
    }
}