package org.openlogics.jdbo;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Iterables;
import org.geotools.data.DataStore;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FeatureSource;
import org.geotools.data.Transaction;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.factory.Hints;
import org.geotools.jdbc.JDBCFeatureSource;
import org.geotools.jdbc.JDBCFeatureStore;
import org.geotools.jdbc.PrimaryKeyColumn;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.identity.FeatureId;
import org.openlogics.utils.reflect.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.transaction.InvalidTransactionException;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.openlogics.jdbo.Features.decodeFID;
import static org.openlogics.utils.ImmutableString.of;

/**
 * The first generic parameter is the type to be used for insertion and the second is the PK type
 *
 * @author Miguel Vega
 * @version $Id: TransactionManager.java 0, 2013-07-02 23:25 mvega $
 */
public class TransactionManager<U> {
    private boolean updatePKAfterTransaction = false;

    private static final Cache<String, FieldName> cachedFields = CacheBuilder.
            newBuilder().
            expireAfterAccess(1, TimeUnit.HOURS).
            maximumSize(100).
            initialCapacity(10).
            build();

    final Logger logger = LoggerFactory.getLogger(TransactionManager.class);
    private final DataStore dataStore;

    public TransactionManager(DataStore dataStore) {
        this.dataStore = dataStore;
    }

    protected static Transaction getTransaction() {
        return new DefaultTransaction();
    }

    protected static Transaction getAutoCommitTransaction() {
        return Transaction.AUTO_COMMIT;
    }

    /**
     * Quitely closes the transaction
     *
     * @param tx
     */
    protected static void closeTransaction(Transaction tx) {
        try {
            tx.close();
            tx = null;
        } catch (IOException e) {
        }
    }

    /**
     * Simply inserts the record, opens and close the datastore in the same method.
     * This insertion can behave in two ways:
     * 1. The source entity can have an attribute annotated with SERIAL, then the FID will be automatically generated for
     * attribute annotated with ID
     * 2. The PK column can not be SERIAL, then the value for the column annotated with ID must be fetched from the same
     * object to insert
     * todo, need to create another to encapsulate many transactions
     *
     * @param object
     * @return
     * @throws EntityException
     * @throws javax.transaction.InvalidTransactionException
     */
    public FeatureId simpleInsert(@Nonnull U object) throws EntityException, InvalidTransactionException {
        assert object != null;

        Entity entity;

        if ((entity = object.getClass().getAnnotation(Entity.class)) == null) {
            throw new EntityException(of("Object of type '").add(object.getClass().getCanonicalName()).add("' can not be handled as an entity for persist").build());
        }

        //FeatureStore<SimpleFeatureType, SimpleFeature> featureStore, SimpleFeature simpleFeature
        FeatureSource<SimpleFeatureType, SimpleFeature> featureSource;
        try {
            featureSource = dataStore.getFeatureSource(entity.name());
        } catch (Exception e) {
            throw new EntityException(of("Unable to get the entity '").add(entity.name()).add("' from datastore.").build(), e);
        }

        return simpleInsert(object, featureSource);
    }

    /**
     * @param object        the object to insert into database
     * @param featureSource to avoid the metadata fetching on every insert it would be necessary to cache the FeatureStore, and reuse it
     * @return
     * @throws EntityException
     * @throws javax.transaction.InvalidTransactionException
     */
    public FeatureId simpleInsert(@Nonnull final U object, FeatureSource<SimpleFeatureType, SimpleFeature> featureSource) throws EntityException, InvalidTransactionException {
        assert object != null && featureSource != null;
        return simpleInsert(object, featureSource, getAutoCommitTransaction());
    }

    /**
     * @param object
     * @param featureSource
     * @param tx
     * @return
     * @throws EntityException
     * @throws javax.transaction.InvalidTransactionException
     */
    public FeatureId simpleInsert(@Nonnull final U object, FeatureSource<SimpleFeatureType, SimpleFeature> featureSource, Transaction tx) throws EntityException, InvalidTransactionException {
        assert object != null && featureSource != null && tx != null;

        Entity entity;

        if ((entity = object.getClass().getAnnotation(Entity.class)) == null) {
            throw new EntityException(of("Object of type '").add(object.getClass().getCanonicalName()).add("' can not be handled as an entity for persist").build());
        }

        if (featureSource == null) {
            //FeatureStore<SimpleFeatureType, SimpleFeature> featureStore, SimpleFeature simpleFeature
            try {
                featureSource = (JDBCFeatureSource) dataStore.getFeatureSource(entity.name());
            } catch (Exception e) {
                throw new EntityException(of("Unable to get the entity '").add(entity.name()).add("' from data store").build(), e);
            }
        }

        SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;

        try {
            featureStore.setTransaction(tx);

            SimpleFeature simpleFeature = Features.<U>objectToSimpleFeature(featureSource.getSchema(), object);
            List<FeatureId> featureIds = featureStore.addFeatures(ImmutableFeatureCollection.of(simpleFeature));

            tx.commit();

            FeatureId fid = featureIds.iterator().next();

            if (!simpleFeature.getUserData().containsKey(Hints.USE_PROVIDED_FID) && updatePKAfterTransaction) {
                //the column ID has been auto-generated.
                String FID = fid.getID();

                decodeFID(((JDBCFeatureStore) featureStore).getPrimaryKey(), FID, true, new Features.FIDDecoderVisitor() {
                    @Override
                    public void visit(final PrimaryKeyColumn pkColumn, final Object decoded) {

                        String encKey = encodeCacheFieldKey(object, pkColumn.getName());

                        FieldName field = Optional.
                                fromNullable(cachedFields.getIfPresent(encKey)).
                                or(new Supplier<FieldName>() {
                                    @Override
                                    public FieldName get() {
                                        //need to identify the attribute to which this maps
                                        Optional<Field> fieldOptional = Iterables.tryFind(Reflections.getDeclaredFields(object.getClass()),
                                                new Predicate<Field>() {
                                                    @Override
                                                    public boolean apply(@Nullable Field input) {
                                                        Column column = input.getAnnotation(Column.class);
                                                        if (column == null) {
                                                            return false;
                                                        }

                                                        if (!isNullOrEmpty(column.name())) {
                                                            if (column.name().equals(pkColumn.getName())) {
                                                                return true;
                                                            }
                                                        }

                                                        return input.getName().equals(pkColumn.getName());
                                                    }
                                                });

                                        return new FieldName(fieldOptional.orNull());
                                    }
                                });

                        if (field.isUnreferenced()) return;

                        cachedFields.put(encKey, field);

                        //need to execute the setter method to update values in Object
                        Method method = null;
                        try {
                            method = Reflections.getSetterMethod(field.value, object.getClass());
                            method.invoke(object, decoded);
                        } catch (IntrospectionException e) {
                            throw new IllegalStateException(of("Couldn't push back value to object, because of unable to reflect method '").add(method.getName()).add("'").build(), e);
                        } catch (InvocationTargetException e) {
                            throw new IllegalStateException(of("An exception has occured while attempting to set value to method '").add(method.getName()).add("'").build(), e);
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException(of("Unable to access method '").add(method.getName()).add("', due to illegal access.").build(), e);
                        }
                    }
                });
            }

            return fid;
        } catch (IOException x) {
            try {
                tx.rollback();
            } catch (IOException e) {
            } finally {
                throw new InvalidTransactionException("Couldn't execute INSERT transaction. " + x.getMessage());
            }
        } finally {
            closeTransaction(tx);
        }
    }

    private String encodeCacheFieldKey(U object, String name) {
        return of(object.getClass().getCanonicalName()).add(".").add(name).build();
    }

    public boolean isUpdatePKAfterTransaction() {
        return updatePKAfterTransaction;
    }

    public void setUpdatePKAfterTransaction(boolean updatePKAfterTransaction) {
        this.updatePKAfterTransaction = updatePKAfterTransaction;
    }

    /**
     * A very simple class to ease the handling of the Field references in cache
     */
    static class FieldName {
        private static final String UNREFERENCED = "unreferenced";
        String value;

        FieldName(Field field) {
            if (field == null)
                unreferencedFieldName();
            this.value = field.getName();
        }

        protected FieldName unreferencedFieldName() {
            this.value = UNREFERENCED;
            return this;
        }

        protected boolean isUnreferenced() {
            return value.equals(UNREFERENCED);
        }
    }
}
