package org.openlogics.jdbo;

import org.openlogics.utils.ImmutableString;

import java.io.IOException;

/**
 * This class will be used to reference the IMPORTED_KEY in an {@link EntityDescriptor} obj.
 *
 * @author Miguel Vega
 * @version $Id: EntityRef.java 0, 2013-06-28 4:44 PM mvega $
 */
public class EntityRef {
    final String entityName, attributeName, schema;
    final EntityDescriptor child;

    EntityRef(EntityDescriptor child, String schema, String entityName, String attributeName) {
        this.child = child;
        this.schema = schema;
        this.attributeName = attributeName;
        this.entityName = entityName;
    }

    public EntityDescriptor getDescriptor() throws IOException {
        return DataStoreSpi.describeEntity(child.getDataStore(), schema, entityName);
    }

    public String getEntityName() {
        return entityName;
    }

    public String getSchema() {
        return schema;
    }

    public String getAttributeName() {
        return attributeName;
    }

    @Override
    public String toString() {
        return ImmutableString.of(" EntityRef:{").
                add("schema:").add(schema).
                add(", entityName:").add(entityName).
                add(", attributeName:").add(attributeName).
                add("}").build();
    }
}
