package org.openlogics.jdbo.builder;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.geotools.jdbc.JDBCDataStore;
import org.openlogics.jdbo.ColumnDescriptor;
import org.openlogics.jdbo.DataStoreSpi;
import org.openlogics.jdbo.EntityDescriptor;

import javax.persistence.Column;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import static org.openlogics.jdbo.builder.EntityFileBuilder.ConfigurationHint.PACKAGE;

/**
 * @author Miguel Vega
 * @version $Id: EntityFileBuilder.java 0, 3/8/14 12:09 AM mvega $
 */
public class EntityFileBuilder {

    public static enum ConfigurationHint {
        PACKAGE
    }

    final Writer writer;

    final Map<ConfigurationHint, Object> configuration;

    final TypeMapperSpi typeMapperSpi;

    /**
     * @param writer
     * @param configuration, must contain {@link org.openlogics.jdbo.builder.EntityFileBuilder.ConfigurationHint#PACKAGE}
     */
    public EntityFileBuilder(Writer writer, Map<ConfigurationHint, Object> configuration, TypeMapperSpi typeMapperSpi) {
        super();
        this.writer = writer;
        this.configuration = configuration;
        this.typeMapperSpi = typeMapperSpi;
    }

    /**
     * @param dataStore
     * @param table
     * @throws IOException
     */
    public void writeEntity(JDBCDataStore dataStore, String table) throws IOException {
        EntityDescriptor entityDescriptor = DataStoreSpi.describeEntity(dataStore, table);

        VelocityEngine ve = new VelocityEngine();

//        ve.setProperty("file.resource.loader.path", absolutePath + "/src/test/java/org/openlogics/gears/jdbo/generator/velocity");
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

        ve.init();

        Template t = ve.getTemplate("/org/openlogics/jdbo/builder/EntityTemplate.vm", "UTF-8");
        VelocityContext context = new VelocityContext();

        Collection<ColumnDescriptor> columnDescriptors = entityDescriptor.getColumnDescriptors();

        context.put("bindings", fetchImportList(columnDescriptors));
        context.put("columnDescriptors", columnDescriptors);
        context.put("package", configuration.get(PACKAGE));

        String wellformedClassName = getWellformedClassName(entityDescriptor.getName());

        context.put("entityName", Strings.isNullOrEmpty(entityDescriptor.getSchema()) ? entityDescriptor.getName() : entityDescriptor.getSchema().concat(".").concat(entityDescriptor.getName()));
        context.put("className", wellformedClassName);

        context.put("dbtype", dataStore.getClass().getName());

        context.put("dateTool", new org.apache.velocity.tools.generic.DateTool());
        context.put("now", new Date());

        t.merge(context, writer);

        /*
        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        String filecontent = writer.toString();
        File fileEntity = new File("/tmp/"+formatingNameClass(nameTable)+".java");
        if (!fileEntity.exists()) {
            fileEntity.createNewFile();
        }
        FileWriter fw = new FileWriter(fileEntity);
        fw.write(filecontent);
        fw.close();
        */
    }

    /**
     * todo, what happens when thereis an array????
     * Retrieve all types to be imported, all but those that belongs to java.lang package
     *
     * @param columnDescriptors
     * @return
     */
    private List<Class> fetchImportList(Collection<ColumnDescriptor> columnDescriptors) {
        List<Class> importList = Lists.newLinkedList();

        //add the default and required types
        importList.add(Column.class);

        Iterator<ColumnDescriptor> iterator = columnDescriptors.iterator();

        while (iterator.hasNext()) {
            ColumnDescriptor cd = iterator.next();
            Class<?> binding = cd.getAttributeDescriptor().getType().getBinding();
            if (!"java.lang".equals(binding.getPackage().getName())) {
                if (!importList.contains(binding))
                    importList.add(binding);
            }
            //check if the type is Date or Timestamp
            if (binding == java.sql.Date.class || binding == java.sql.Timestamp.class ||
                    binding == java.sql.Time.class) {
                cd.getTypeMappers().add(typeMapperSpi.findTypeMapper(binding));
            }
        }

        return importList;
    }

    /**
     * todo, isther a possibility to provide a dictionary to make the names have a better meaning.
     * todo, e.g. NOW: 'countrylanguage' > 'Countrylanguage'
     * todo, e.g. WITH DICTIONARY: 'countrylanguage' > 'CountryLanguage'
     * Generates the fully qualified class name.
     *
     * @param entityName
     * @return
     */
    private String getWellformedClassName(String entityName) {
        StringBuffer stringBuffer = new StringBuffer("");
        String stringSplit[] = entityName.split("_");
        for (int i = 0; i < stringSplit.length; i++) {
            stringBuffer.append(WordUtils.capitalize(stringSplit[i]));
        }
        return stringBuffer.toString();
    }
}