package geotools;

import org.geotools.data.*;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.jdbc.JDBCDataStore;
import org.geotools.jdbc.JDBCFeatureSource;
import org.junit.Ignore;
import org.junit.Test;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.PropertyDescriptor;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: DataTest.java 0, 2013-06-28 11:31 AM mvega $
 */

public class DataTest {

    private DataStore getDataStore() throws IOException {
        Map params = new HashMap();
        params.put("dbtype", "postgis");        //must be postgis
        params.put("host", "localhost");        //the name or ip address of the machine running PostGIS
        params.put("port", new Integer(5432));  //the port that PostGIS is running on (generally 5432)
        params.put("database", "ptm");      //the name of the database to connect to.
        params.put("user", "postgres");         //the user to connect with
        params.put("passwd", "postgres");               //the password of the user.

        return DataStoreFinder.getDataStore(params);
    }

    @Ignore
    @Test
    public void testQueryPostgis() throws IOException {
        JDBCDataStore pgDatastore = (JDBCDataStore) getDataStore();

        try {

            FeatureSource<SimpleFeatureType, SimpleFeature> featureSource = pgDatastore.getFeatureSource("gpri_cantons");

            JDBCFeatureSource gpri_cantons = pgDatastore.getAbsoluteFeatureSource("gpri_cantons");

            SimpleFeatureType schema = featureSource.getSchema();

            System.out.println("Primery KEY=" + gpri_cantons.getPrimaryKey().getColumns().get(0).getName());
            Collection<PropertyDescriptor> descriptors = schema.getDescriptors();
            for (PropertyDescriptor descriptor : descriptors) {
                System.out.println("Property: " + descriptor.getName() + ", " + descriptor.getType().getBinding());
            }

            System.out.println("domain count: " + featureSource.getCount(Query.ALL));

            FeatureIterator<SimpleFeature> features = featureSource.getFeatures().features();
            while (features.hasNext()) {
                SimpleFeature feat = features.next();
                System.out.println(feat.getAttribute("ddef_desc"));
            }
        } finally {

        }
    }

    @Ignore
    @Test
    public void testInsert() throws IOException {
        DataStore dataStore = getDataStore();

         /*
         * Write the features to the shapefile
         */
        Transaction transaction = new DefaultTransaction("create");

        String typeName = "pri_domains";
        SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);

        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureSource.getSchema());

        if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;

            featureStore.setTransaction(transaction);
            try {

                /*
                 * We create a FeatureCollection into which we will put each Feature created from a record
                 * in the input csv data file
                 */
                DefaultFeatureCollection collection = new DefaultFeatureCollection();
                /* Longitude (= x coord) first ! */
                //Point point = geometryFactory.createPoint(new Coordinate(longitude, latitude));

                featureBuilder.set(new NameImpl("ddef_dom_id"), 1);
                featureBuilder.set(new NameImpl("ddef_desc"), "poipoipoi");
                featureBuilder.set(new NameImpl("ddef_add_date"), new Timestamp(System.currentTimeMillis()));

                SimpleFeature feature = featureBuilder.buildFeature(null);

                collection.add(feature);

                featureStore.addFeatures(collection);
                transaction.commit();
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } finally {
                transaction.close();
            }
            System.exit(0); // success!
        } else {
            System.out.println(typeName + " does not support read/write access");
            System.exit(1);
        }
    }
}
