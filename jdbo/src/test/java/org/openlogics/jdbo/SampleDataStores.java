package org.openlogics.jdbo;

import com.google.common.collect.ImmutableMap;
import org.geotools.data.DataStore;
import org.geotools.data.mysql.MySQLDataStoreFactory;
import org.geotools.data.oracle.OracleNGDataStoreFactory;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.jdbc.JDBCDataStore;

import java.io.IOException;

/**
 * @author Miguel Vega
 * @version $Id: SampleDataStores.java 0, 3/8/14 1:15 AM mvega $
 */
public class SampleDataStores {
    public static JDBCDataStore getOracleDataStore() {
        ImmutableMap params = new ImmutableMap.Builder().
                put(OracleNGDataStoreFactory.HOST, "172.16.8.128").
                put(OracleNGDataStoreFactory.PORT, 1521).
                put(OracleNGDataStoreFactory.DATABASE, "xe").
                put(OracleNGDataStoreFactory.SCHEMA, "xe").
                put(OracleNGDataStoreFactory.USER, "scott").
                put(OracleNGDataStoreFactory.PASSWD, "tiger").
                put(OracleNGDataStoreFactory.DBTYPE.key, "oracle").

                build();

        try {
            DataStore oracle = new OracleNGDataStoreFactory().createDataStore(params);
            return (JDBCDataStore) oracle;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JDBCDataStore getPostgreSQLDataStore() {
        try {
            JDBCDataStore dataStore = DataStoreSpi.of(new ImmutableMap.Builder().

                    put(PostgisNGDataStoreFactory.HOST, "127.0.0.1").
                    put(PostgisNGDataStoreFactory.PORT, 5432).
                    put(PostgisNGDataStoreFactory.DATABASE, "world").
                    put(PostgisNGDataStoreFactory.SCHEMA, "public").
                    put(PostgisNGDataStoreFactory.USER, "postgres").
                    put(PostgisNGDataStoreFactory.PASSWD, "postgres").
                    put(PostgisNGDataStoreFactory.DBTYPE.key, "postgis").

                    build());
            return dataStore;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JDBCDataStore getMySQLDataStore() {
        try {
            JDBCDataStore dataStore = DataStoreSpi.of(new ImmutableMap.Builder().

                    put(MySQLDataStoreFactory.DBTYPE.key, "mysql").
                    put(MySQLDataStoreFactory.HOST.key, "localhost").
                    put(MySQLDataStoreFactory.PORT.key, 3309).
                    put(MySQLDataStoreFactory.DATABASE.key, "database").
                    put(MySQLDataStoreFactory.USER.key, "geotools").
                    put(MySQLDataStoreFactory.PASSWD.key, "geotools").

                    build());
            return dataStore;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
