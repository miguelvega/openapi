/*
 *     JavaTools
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.jdbo.pojo;

import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Miguel Vega
 * @version $Id: Person.java 0, 2012-09-29 12:00 mvega $
 */
@ToString
public class Person {

    @Column(name = "FOO_FNAME")
    private String fname;

    @Column(name = "FOO_LNAME")
    private String lname;

    @Column(name = "FOO_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public long getId() {
        return id;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setId(long id) {
        this.id = id;
    }
}
