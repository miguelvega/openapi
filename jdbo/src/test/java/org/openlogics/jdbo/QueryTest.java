package org.openlogics.jdbo;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.Join;
import org.geotools.data.Query;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ContentFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.identity.FeatureIdImpl;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.jdbc.JDBCDataStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: QueryTest.java 0, 2013-07-18 11:42 PM mvega $
 */
public class QueryTest {

    @Test
    @Ignore("can not run from mvn test")
    public void testSimpleQuery() throws IOException, CQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dbtype", "h2");
        params.put("database", "jdbc:h2:mem:foodb");
        JDBCDataStore dataStore = (JDBCDataStore) DataStoreFinder.getDataStore(params);

        dataStore.setDataSource(basicDataSource);

        Query query = new Query("poipipoi");

        query.setFilter(CQL.toFilter("FOO_FNAME='MIGUEL'"));

        SimpleFeatureIterator features = dataStore.getFeatureSource("FOO").getFeatures(query).features();
        while (features.hasNext()) {
            SimpleFeature feature = features.next();
            System.out.println("Name=" + feature.getAttribute("FOO_FNAME"));
        }
        features.close();
    }


    /**
     * http://docs.codehaus.org/display/GEOTOOLS/Join+Support
     * http://docs.geotools.org/latest/userguide/library/main/builder.html
     *
     * @throws java.io.IOException
     */
    @Test
    @Ignore("can not run from mvn test")
    public void testJoinQuery() throws IOException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dbtype", "h2");
        params.put("database", "jdbc:h2:mem:foodb");
        JDBCDataStore dataStore = (JDBCDataStore) DataStoreFinder.getDataStore(params);

        dataStore.setDataSource(basicDataSource);

        try {
            FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

            Filter joinFilter = CQL.toFilter("PROD_FOO_ID = a.FOO_ID");
            joinFilter = ff.equals(ff.property("b.PROD_FOO_ID"), ff.property("a.FOO_ID"));

            Join join = new Join("PRODUCTS", joinFilter);
            join.setAlias("b");

            //If the FOO record doesn't have any relation with a product,
            // this will ensure the results to return at least the FOO record. If a relation exists, all the
            // data form FOO and PRODUCT will be fetched
            join.setType(Join.Type.OUTER);

            Query query = new Query("FOO");
            query.setAlias("a");

            //to make a query by Primary Key
            Set<FeatureId> fids = new HashSet<FeatureId>();

            //fids.add(new FeatureIdImpl("3"));// record with no products binded,
            fids.add(new FeatureIdImpl("1"));//record with two products bound

            Filter filter = ff.id(fids);
            query.setFilter(filter);

            query.getJoins().add(join);

            ContentFeatureSource foo = dataStore.getFeatureSource("FOO");

            SimpleFeatureIterator features = foo.getFeatures(query).features();

            System.out.println("................................ start");
            while (features.hasNext()) {
                SimpleFeature feature = features.next();

                System.out.println();
                for (AttributeDescriptor ad: feature.getFeatureType().getAttributeDescriptors()) {
                    System.out.print(String.format("%s = %s, ", ad.getName(), String.valueOf(feature.getAttribute(ad.getName()))));
                }
                System.out.println();
            }
            System.out.println("................................end");
            features.close();
        } catch (CQLException e) {
            e.printStackTrace();
        }
    }

    protected IDatabaseTester databaseTester;
    protected BasicDataSource basicDataSource;

    protected Logger logger;

    @Before
    public void setup() {
        this.logger = Logger.getLogger(getClass());

        try {

            basicDataSource = new BasicDataSource();
            basicDataSource.setUrl("jdbc:h2:mem:foodb");
            basicDataSource.setDriverClassName("org.h2.Driver");
            Connection connection = basicDataSource.getConnection();
            URL sql = getResource(getClass(), "foo.sql");
            try {
                connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                connection.close();

                URL resource = getResource(getClass(), "foo.xml");
                FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                databaseTester = new DataSourceDatabaseTester(basicDataSource);
                databaseTester.setDataSet(build);
                databaseTester.onSetup();
            } catch (SQLException x) {

            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    @After
    public void dispose() throws Exception {
        databaseTester.onTearDown();
        basicDataSource.close();
    }
}
