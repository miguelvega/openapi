package org.openlogics.jdbo.builder;

import com.google.common.collect.ImmutableMap;
import org.geotools.jdbc.JDBCDataStore;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.jdbo.SampleDataStores;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: EntityFileBuilderTest.java 0, 3/8/14 1:14 AM mvega $
 */
public class EntityFileBuilderTest {
    @Ignore("No such connection avaiable anywhere")
    @Test
    public void testWriteEntity() throws Exception {
        JDBCDataStore dataStore = SampleDataStores.getPostgreSQLDataStore();

        Map<EntityFileBuilder.ConfigurationHint, Object> config = ImmutableMap.<EntityFileBuilder.ConfigurationHint, Object>of(
                EntityFileBuilder.ConfigurationHint.PACKAGE, "com.demo.doh"
        );

        PrintWriter writer = new PrintWriter(new FileOutputStream("/tmp/a.java"));
        EntityFileBuilder builder = new EntityFileBuilder(writer, config, null);

        builder.writeEntity(dataStore, "cla_roles");

        writer.flush();
        writer.close();
    }
}
