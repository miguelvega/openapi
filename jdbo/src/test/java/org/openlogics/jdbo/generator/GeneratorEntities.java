package org.openlogics.jdbo.generator;

import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.geotools.data.Transaction;
import org.geotools.jdbc.JDBCDataStore;
import org.openlogics.jdbo.ColumnDescriptor;
import org.openlogics.jdbo.DataStoreSpi;
import org.openlogics.jdbo.EntityDescriptor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Erick R. Gutierrez G.
 * @version $Id: GeneratorEntities.java 0, 2013-07-05 23:05 PM erickgugo $
 */
public class GeneratorEntities {
    private final JDBCDataStore dataStore;
    private List<String>  importList;

    public GeneratorEntities(JDBCDataStore jdbcDataStore) {

        this.dataStore = jdbcDataStore;

        importList=new ArrayList<String>();

        //persistence imports
        importList.add("javax.persistence.Column");
    }

    public void startsGeneration() {
        List<Fields> fieldsList;
        try {
            Connection connection = dataStore.getConnection(Transaction.AUTO_COMMIT);
            DatabaseMetaData metaData = connection.getMetaData();
            String[] types = {"TABLE"};
            ResultSet resultSet = metaData.getTables(null, null, "%", types);
            String nameTable;
            while (resultSet.next()) {
                nameTable = resultSet.getString("TABLE_NAME");
                fieldsList = getFields(nameTable);
                createEntityFile(nameTable, fieldsList);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void generateClassFiles(String... tableNames) {
        List<Fields> fieldsList;
        try {
            Connection connection = dataStore.getConnection(Transaction.AUTO_COMMIT);

            for (String tableName : tableNames) {
                System.out.println(">>>>>>>>>>>>>>"+tableName);
                fieldsList = getFields(tableName);
                createEntityFile(tableName, fieldsList);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public List<Fields> getFields(String nameTable) {
        List<Fields> fieldsList = new ArrayList<Fields>();
        try {
            EntityDescriptor ed = DataStoreSpi.describeEntity(dataStore, nameTable);
            Fields fields;
            for (ColumnDescriptor columnDescriptor : ed.getColumnDescriptors()) {
                fields = new Fields();
                fields.setAutogenerated(columnDescriptor.isAutogenerated());
                fields.setColumnName(columnDescriptor.getAttributeDescriptor().getLocalName());
                fields.setForeignKey(columnDescriptor.isForeignKey());
                fields.setNullable(columnDescriptor.getAttributeDescriptor().isNillable());
                fields.setPrimaryKey(columnDescriptor.isPrimaryKey());
                fields.setSimpleType(columnDescriptor.getAttributeDescriptor().getType().getBinding().getSimpleName());
                fields.setTypeName(columnDescriptor.getAttributeDescriptor().getType().getBinding().getName());
                fieldsList.add(fields);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fieldsList;
    }
    public void createEntityFile(String nameTable, List<Fields> fieldsList) {
         try{
             String packages="velocityentidades";
             VelocityEngine ve = new VelocityEngine();
             String absolutePath = new File(Thread.currentThread().getContextClassLoader().getResource("").getFile()).getParentFile().getParentFile().getPath();
             ve.setProperty("file.resource.loader.path", absolutePath + "/src/test/java/org/openlogics/gears/jdbo/generator/velocity");
             ve.init();
             Template t = ve.getTemplate("entity.vm");
             VelocityContext context = new VelocityContext();
             generateParameters(fieldsList);
             context.put("importList", importList);
             context.put("fieldsList", fieldsList);
             context.put("package", packages);
             context.put("class", formatingNameClass(nameTable));

             context.put("dateTool", new org.apache.velocity.tools.generic.DateTool());
             context.put("now", new Date());

             StringWriter writer = new StringWriter();
             t.merge(context, writer);
             String filecontent = writer.toString();
             File fileEntity = new File("/tmp/"+formatingNameClass(nameTable)+".java");
             if (!fileEntity.exists()) {
                 fileEntity.createNewFile();
             }
             FileWriter fw = new FileWriter(fileEntity);
             fw.write(filecontent);
             fw.close();
         }  catch (Exception e){
             e.printStackTrace();
         }
    }

    public String formatingNameClass(String nameClass){
        StringBuffer stringBuffer=new StringBuffer("");
        String stringSplit[]=nameClass.split("_");
        for (String string : stringSplit) {
            stringBuffer.append(WordUtils.capitalize(string));
        }
        return stringBuffer.toString();
    }

    public String formatingNameField(String nameField){
        StringBuffer stringBuffer=new StringBuffer("");
        String stringSplit[]=nameField.split("_");
        stringBuffer.append(stringSplit[0]);
        for (int i = 1; i < stringSplit.length; i++) {
                stringBuffer.append(WordUtils.capitalize(stringSplit[i]));
        }
        return stringBuffer.toString();
    }

    public void generateParameters(List<Fields> listFields){
        for (Fields field : listFields) {
            addImportType(field.getTypeName());
            field.setColumnName(formatingNameField(field.getColumnName()));
        }
    }

    private void addImportType(String typeName){
        if(!typeName.startsWith("java.lang")){
            importList.add(typeName);
        }
    }
}
