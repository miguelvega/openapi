package org.openlogics.jdbo.generator;

import org.junit.Ignore;
import org.junit.Test;

import static org.openlogics.jdbo.SampleDataStores.getPostgreSQLDataStore;

/**
 * @author Miguel Vega
 * @version $Id: GeneratorEntitiesTest.java 0, 2013-11-28 5:02 PM mvega $
 */
public class GeneratorEntitiesTest {
    @Test
    @Ignore(value = "Need of a DataSTore to work")
    public void testCreateEntity() {
        GeneratorEntities ge = new GeneratorEntities(getPostgreSQLDataStore());
        //ge.startsGeneration();
        ge.generateClassFiles("city", "country", "countrylanguage");
    }
}
