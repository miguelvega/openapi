package org.openlogics.jdbo;

import com.google.common.collect.ImmutableMap;
import org.geotools.jdbc.JDBCDataStore;
import org.junit.Test;

import java.io.IOException;

/**
 * @author Miguel Vega
 * @version $Id: EntityDescriptorTest.java 0, 2013-06-28 4:01 PM mvega $
 */
public class EntityDescriptorTest {

    @Test
    public void testFetchSquema() throws IOException {
        JDBCDataStore dataStore = DataStoreSpi.of(new ImmutableMap.Builder().
                //put("schema", "public").
                        put("host", "localhost").
                put("port", 5432).
                put("database", "cliente-ach").
                put("user", "postgres").
                put("passwd", "postgres").
                put("dbtype", "postgis").
                build());

        EntityDescriptor ed = DataStoreSpi.describeEntity(dataStore, "act_mensajes");

        System.out.println("Columns...");
        for (ColumnDescriptor columnDescriptor : ed.getColumnDescriptors()) {
            System.out.println(columnDescriptor);

            if (columnDescriptor.getImportedKeyRef()!=null) {
                System.out.println("\t Imported Key: " + columnDescriptor.getImportedKeyRef());
            }

            if(columnDescriptor.getAttributeDescriptor().getType().getBinding() == Integer.class){
                Object defaultValue = columnDescriptor.getAttributeDescriptor().getDefaultValue();

                String num = (String) defaultValue;
                System.out.println(",,,,,,,,,,,,,,,"+num);
            }
        }

    }
}
