package org.openlogics.jdbo;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.jdbc.JDBCDataStore;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osjava.sj.loader.JndiLoader;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Miguel Vega
 * @version $Id: JNDIDataStoreTest.java 0, 2013-07-05 2:56 PM mvega $
 */
public class JNDIDataStoreTest {
    protected IDatabaseTester databaseTester;
    protected JdbcConnectionPool ds;
    String JNDI_REF_NAME = "java:comp/env/jdbc/geotools";
    private JndiLoader loader;

    @Before
    public void setup() throws ClassNotFoundException {
        /* The default is 'flat', which isn't hierarchial and not what I want. */
        /* Separator is required for non-flat */

        Hashtable contextEnv = new Hashtable();

        /* For GenericContext */
        contextEnv.put(Context.INITIAL_CONTEXT_FACTORY, "org.osjava.sj.memory.MemoryContextFactory");
        contextEnv.put("jndi.syntax.direction", "left_to_right");
        contextEnv.put("jndi.syntax.separator", "/");
        contextEnv.put(JndiLoader.SIMPLE_DELIMITER, "/");

        loader = new JndiLoader(contextEnv);

        try {
            Context ctxt = new InitialContext(contextEnv);

            ctxt.createSubcontext("java:");
            ctxt.createSubcontext("java:/comp");
            ctxt.createSubcontext("java:/comp/env");
            ctxt.createSubcontext("java:/comp/env/jdbc");

            DriverAdapterCPDS dataSource = new DriverAdapterCPDS();
            dataSource.setUrl("jdbc:h2:mem:foodb");
            dataSource.setDriver("org.h2.Driver");

            ds = JdbcConnectionPool.create(dataSource);


            ctxt.bind(JNDI_REF_NAME, ds);


            Connection connection = null;
            try {
                connection = dataSource.getPooledConnection().getConnection();
                URL sql = getResource(getClass(), "foo.sql");
                try {
                    connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                    connection.close();

                    URL resource = getResource(getClass(), "foo.xml");
                    FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);

                    databaseTester = new JdbcDatabaseTester(dataSource.getDriver(), dataSource.getUrl());
                    databaseTester.setDataSet(build);
                    databaseTester.onSetup();
                } catch (Exception x) {
                    x.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                //DbUtils.closeQuietly(connection);
            }


        } catch (NamingException ne) {
            ne.printStackTrace();
        }
    }

    @After
    public void dispose() throws Exception {
        databaseTester.onTearDown();
        ds.dispose();
    }

    @Test
    @Ignore("something is wrong")
    public void testJNDIDataStore() throws IOException {
        Map map = new HashMap();
        map.put("dbtype", "h2");
        map.put("jndiReferenceName", JNDI_REF_NAME);

        DataStore store = DataStoreFinder.getDataStore(map);

        String[] typeNames = store.getTypeNames();
        for (String typeName : typeNames) {
            System.out.println(">>>>>" + typeName);
        }

        assertNotNull(store);

        assertTrue(store instanceof JDBCDataStore);
    }

    @Ignore("Need a JNDI in context")
    @Test
    public void testConnection() throws NamingException, SQLException {
        DataSource ds = (DataSource) new InitialContext().lookup(JNDI_REF_NAME);

        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();

        ResultSet rset = stmt.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES");
        while (rset.next()) {
            System.out.println("<<<\t" + rset.getString("TABLE_NAME"));
        }
    }

    @Test
    @Ignore
    public void testProperties() throws Exception {
        try {
            Properties props = new Properties();
            props.put("foo", "13");
            props.put("bar/foo", "42");
            props.put("bar/mytest/foo", "101");

            Context ctxt = new InitialContext();

            loader.load(props, ctxt);

            assertEquals("13", ctxt.lookup("foo"));
            assertEquals("42", ctxt.lookup("bar/foo"));
            assertEquals("101", ctxt.lookup("bar/mytest/foo"));
        } catch (NamingException ne) {
            ne.printStackTrace();
            fail("NamingException: " + ne.getMessage());
        }

    }
}
