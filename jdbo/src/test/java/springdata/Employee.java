package springdata;

import javax.persistence.*;

@Entity
public class Employee {
    @Id
    private Integer employeeId;
    @Basic(optional = false)
    private String employeeName;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AddressId")
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}