package springdata;

/**
 * @author Miguel Vega
 * @version $Id: EmployeeReppository.java 0, 2015-07-01 10:23 PM mvega $
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    Employee findByEmployeeId(Integer id);
}