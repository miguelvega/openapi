package org.openlogics.swing.ext;

import javax.swing.*;

/**
 * A simple class which encapsulates the most basic features of an UI item.
 * @author Miguel Vega
 * @version $Id: UItem.java 0, 8/20/14 10:24 PM, @miguel $
 */
public class UIItem {
    public String getLabel() {
        return label;
    }

    public Icon getIcon() {
        return icon;
    }

    public String getToolTip() {
        return toolTip;
    }

    public Action getAction() {
        return action;
    }

    final String label;
    final Icon icon;
    final String toolTip;
    final Action action;

    UIItem(String label, Icon icon, String toolTip, Action action) {
        this.label = label;
        this.icon = icon;
        this.toolTip = toolTip;
        this.action = action;
    }

    public static UIItem with(String label, Action action){
        return with(label, null, null, action);
    }

    public static UIItem with(Icon icon, String toolTip, Action action){
        return with(null, icon, toolTip, action);
    }

    public static UIItem with(String label, String toolTip, Action action){
        return with(label, null, toolTip, action);
    }

    public static UIItem with(String label, Icon icon, Action action){
        return with(label, icon, null, action);
    }

    public static UIItem with(String label, Icon icon, String toolTip, Action action){
        return new UIItem(label, icon, toolTip, action);
    }
}
