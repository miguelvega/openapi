package org.openlogics.swing.ext;

import javax.swing.*;
import java.awt.*;

/**
 * @author Miguel Vega
 * @version $Id: org.openlogics.swing.ext.RichUIManager.java 0, 8/17/14 10:25 PM, @miguel $
 */
public class RichUIManager {

    public static final RichGradient GRADIENT_BLUE=new RichGradient(new Color(125, 161, 237),
            new Color(125, 161, 237),new Color(91, 118, 173), new Color(91, 118, 173));

    public static final RichGradient GRADIENT_DARK=new RichGradient(new Color(122, 132, 145),
            new Color(76, 84, 98), new Color(32, 39, 55), new Color(53, 61, 77));

    public static final RichGradient GRADIENT_ORANGE=new RichGradient(new Color(251, 185, 106), new Color(247, 161, 92),
            new Color(232, 135, 40), new Color(222, 125, 0));

    public static final RichGradient GRADIENT_LIME_GREEN=new RichGradient(new Color(182, 219, 136), new Color(158, 211, 102),
            new Color(183, 234, 98, 200), new Color(220, 255, 149, 255));

    public static final RichGradient GRADIENT_DEFAULT =new RichGradient(new Color(230, 232, 222), new Color(228, 231, 213),
            new Color(214, 219, 191), new Color(214, 219, 191));

    public static final RichGradient GRADIENT_SYSTEM_DEFAULT = new RichGradient(Color.white, UIManager.getColor("control"));

    public static final RichGradient GRADIENT_MAC_OS = new RichGradient(Color.decode("#c4c4c4"), Color.decode("#afafaf"),
            Color.decode("#afafaf"), Color.decode("#979797"));

    public enum Property{
        richGradient
    }

    static{
        UIManager.put(Property.richGradient, GRADIENT_SYSTEM_DEFAULT);
    }
}
