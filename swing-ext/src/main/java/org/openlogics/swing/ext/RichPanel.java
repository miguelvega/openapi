package org.openlogics.swing.ext;

import javax.swing.*;
import java.awt.*;

/**
 * @author Miguel Vega
 * @version $Id: GradientPanel.java 0, 8/17/14 9:55 PM, @miguel $
 */
public class RichPanel extends JPanel{

    protected final RichGradientDecorator richGradientDecorator;

    public RichPanel() {
        this(new RichGradientDecorator());
    }
    public RichPanel(RichGradientDecorator richGradientDecorator) {
        addComponentListener(this.richGradientDecorator = richGradientDecorator);
        setLayout(new BorderLayout());
    }

    @Override
    protected void paintComponent(Graphics g) {
        richGradientDecorator.applyStyle(this, g);
    }
}
