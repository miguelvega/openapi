package org.openlogics.swing.ext;

import com.google.common.collect.Iterables;

import javax.inject.Provider;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: RichComboButton.java 0, 8/17/14 11:08 PM, @miguel $
 */
public class RichDropdownButton<T> extends JButton {

    private int selected;

    private Provider<Iterable<Action>> modelProvider;

    private final JPopupMenu popupMenu = new JPopupMenu("dropdownpopup");

    private final ActionListener GENERIC = new DropListener();

    public RichDropdownButton() {
        this("DropwDownButton");
    }

    public RichDropdownButton(String text) {
        super(text);
        setFocusPainted(false);

        //setIcon(new InvertedTriangleIcon());

        setVerticalTextPosition(SwingConstants.CENTER);
        setHorizontalTextPosition(SwingConstants.LEFT);

        super.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Point location = RichDropdownButton.this.getLocationOnScreen();

                popupMenu.setLocation((int) location.getX(), (int) location.getY() + getHeight());

                popupMenu.setVisible(true);
            }
        });
    }

    private void reloadPopup(){

        while(popupMenu.getComponentCount()>0){
            popupMenu.remove(0);
        }

        popupMenu.setInvoker(RichDropdownButton.this);

        Iterator<Action> iterator = modelProvider.get().iterator();

        int cur = 0;
        while (iterator.hasNext()) {

            Action action = iterator.next();

            JMenuItem menuItem = new JMenuItem(action);

            menuItem.addActionListener(GENERIC);

            popupMenu.add(menuItem);
            if(selected == cur++){
                setText((String) action.getValue(Action.NAME));
                setIcon((Icon) action.getValue(Action.SMALL_ICON));
            }
        }
    }

    @Override
    public void addActionListener(ActionListener l) {
        throw new UnsupportedOperationException("Can not add custom action listeners");
    }

    @Override
    public void setAction(Action a) {
        throw new UnsupportedOperationException("Can not add custom action");
    }

    public RichDropdownButton setModelProvider(Provider<Iterable<Action>> modelProvider) {
        this.modelProvider = modelProvider;
        this.selected = 0;
        reloadPopup();
        return this;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //draw the riangle
        new InvertedTriangleIcon().paintIcon(this, g, getWidth()-10, (getHeight()-InvertedTriangleIcon.h) / 2);

    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        if(selected>=Iterables.size(modelProvider.get()) || selected<0)
            return;
        this.selected = selected;

        reloadPopup();
    }

    class InvertedTriangleIcon implements Icon {


        protected static final int h = 8, w = 8;

        public void paintIcon(Component c, Graphics g, int x, int y) {
            int arrowWidth = 4, arrowHeight = 3;
            Graphics2D g2d = (Graphics2D) g;

//            g2d.fillRect(x, y, 30, 30);

            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Polygon poly = new Polygon();
            //int h = getHeight(), w = getWidth();
            int loc = 2;
            poly.addPoint(x + (w - arrowWidth) / loc, y + (h - arrowHeight) / loc);
            poly.addPoint(x + (w + arrowWidth) / loc, y + (h - arrowHeight) / loc);
            poly.addPoint(x + w / loc, y + (h + arrowHeight) / loc);
            poly.addPoint(x + (w - arrowWidth) / loc, y + (h - arrowHeight) / loc);

            g2d.setPaint(Color.BLACK);
            g2d.fill(poly);
            g2d.setPaint(Color.DARK_GRAY);
            g2d.draw(poly);

        }

        public int getIconWidth() {
            return w;
        }

        public int getIconHeight() {
            return h;
        }
    }

    private class DropListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            RichDropdownButton.this.setText(((JMenuItem)e.getSource()).getText());
        }
    }
}
