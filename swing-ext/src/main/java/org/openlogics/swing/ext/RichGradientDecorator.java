package org.openlogics.swing.ext;

import com.google.common.base.Optional;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * @author Miguel Vega
 * @version $Id: RichUI.java 0, 8/17/14 10:06 PM, @miguel $
 */
public class RichGradientDecorator implements ComponentListener {
    private BufferedImage gradientImage;

    public RichGradientDecorator() {
        super();
    }

    protected void applyStyle(Component owner, Graphics graphics) {
        if (owner.isOpaque()) {
            createImageCache(owner);

            if (gradientImage != null) {
                Shape clip = graphics.getClip();
                Rectangle bounds = clip.getBounds();

                Image backgroundImage = gradientImage.getSubimage(bounds.x,
                        bounds.y,
                        bounds.width,
                        bounds.height);
                graphics.drawImage(backgroundImage, bounds.x, bounds.y, null);
            }
        }
    }

    protected void createImageCache(Component owner) {
        int width = owner.getWidth();
        int height = owner.getHeight();

        if (width == 0 || height == 0) {
            return;
        }

        if (gradientImage == null ||
                width != gradientImage.getWidth() ||
                height != gradientImage.getHeight()) {

            gradientImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

            Graphics2D g2 = gradientImage.createGraphics();

            RichGradient richGradient = getRichGradient();

            if(richGradient.isSimpleGradient()){
                GradientPaint painter = richGradient.getUpperGradient(owner);
                g2.setPaint(painter);
                Rectangle2D rect = new Rectangle2D.Double(0, 0, width, height);
                g2.fill(rect);
                return;
            }

            GradientPaint painter = richGradient.getUpperGradient(owner);
            g2.setPaint(painter);
            Rectangle2D rect = new Rectangle2D.Double(0, 0, width, height/2);
            g2.fill(rect);

            painter = richGradient.getLowerGradient(owner);
            g2.setPaint(painter);
            rect = new Rectangle2D.Double(0, height/2, width, height);
            g2.fill(rect);
        }
    }

    private void disposeImageCache() {
        synchronized (gradientImage) {
            gradientImage.flush();
            gradientImage = null;
        }
    }

    public RichGradient getRichGradient() {
        return (RichGradient) Optional.fromNullable(UIManager.get(RichUIManager.Property.richGradient)).or(RichUIManager.GRADIENT_SYSTEM_DEFAULT);
    }


    public void componentResized(ComponentEvent e) {

    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
        disposeImageCache();
    }
}
