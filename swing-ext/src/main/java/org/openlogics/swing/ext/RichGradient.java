package org.openlogics.swing.ext;

import java.awt.*;

/**
 * @author Miguel Vega
 * @version $Id: BiGradient.java 0, 8/17/14 10:08 PM, @miguel $
 */
public class RichGradient<U extends Component> {

    private final Color uu, ul, lu, ll;

    public RichGradient(Color uu, Color ul) {
        this(uu, ul, null, null);
    }

    public RichGradient(Color uu, Color ul, Color lu, Color ll) {
        this.uu = uu;
        this.ul = ul;
        this.lu = lu;
        this.ll = ll;
    }

    public GradientPaint getUpperGradient(U component){
        if(isSimpleGradient())
            return new GradientPaint(0, 0, uu, 0, component.getHeight(), ul);

        return new GradientPaint(0, 0, uu, 0, component.getHeight() / 2, ul);
    }

    public GradientPaint getLowerGradient(U component){
        return new GradientPaint(0, component.getHeight() / 2, lu, 0, component.getHeight(), ll);
    }

    public boolean isSimpleGradient(){
        return lu==null && ll == null;
    }
}
