package org.openlogics.swing.ext;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import javax.inject.Provider;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static org.openlogics.swing.ext.UIItem.with;

public class RichPanelTest {
    @Test
    public void testViewPanel(){
        RichPanel c = new RichPanel();

        final RichDropdownButton btn = new RichDropdownButton("All");

        btn.setModelProvider(new Provider<Iterable<Action>>() {
            @Override
            public Iterable<Action> get() {
                return ImmutableList.<Action>of(
                        new AbstractAction("Todas", null){
                            @Override
                            public void actionPerformed(ActionEvent e) {

                            }
                        }, new AbstractAction("Astronomia", null){
                            @Override
                            public void actionPerformed(ActionEvent e) {

                            }
                        }, new AbstractAction("Biologia", null){
                            @Override
                            public void actionPerformed(ActionEvent e) {

                            }
                        }
                );
            }
        });

        c.add(btn);

        Launcher.launch(c);

        if(GraphicsEnvironment.isHeadless()){
            return;
        }

        while(true){}
    }
}