package org.openlogics.swing.ext;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: Launcher.java 0, 8/17/14 10:42 PM, @miguel $
 */
public class Launcher {
    public static void launch(Component c) {
        JFrame f = new JFrame();

        f.setSize(300, 200);

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(new BorderLayout());
        f.setTitle("launch @ "+new Date());
        f.add(c, BorderLayout.CENTER);
        f.add(c);
//        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
