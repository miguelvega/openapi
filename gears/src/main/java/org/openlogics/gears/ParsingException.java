package org.openlogics.gears;

import java.io.IOException;

/**
 * This class will be useful when an exception is found on a input reading/parsing process
 *
 * @author Miguel Vega
 * @version $Id: ParsingException.java 0, 2013-05-05 04:28 mvega $
 */
public class ParsingException extends IOException {

    private static final long serialVersionUID = -7352942852897185361L;

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParsingException(Throwable cause) {
        super(cause);
    }
}
