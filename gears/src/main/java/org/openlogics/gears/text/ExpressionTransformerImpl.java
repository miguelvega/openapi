/*
 *     JavaTools
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.text;

import com.google.common.primitives.Primitives;
import org.openlogics.gears.jdbc.FunctionParam;
import org.openlogics.utils.reflect.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.Map;

/**
 * This class evaluates a Expression Language dot separated, where each part of the string
 * can be the requested value.
 *
 * @author Miguel Vega
 * @version $Id: ExpressionTransformerImpl.java 0, 2012-09-29 11:22 mvega $
 */
public class ExpressionTransformerImpl implements ExpressionTransformer<String, Object> {
    static final String ATTRIBUTE_SEPARATOR = "\\.";

    final Logger logger = LoggerFactory.getLogger(ExpressionTransformerImpl.class);

    public static String getGetterName(Field field) {
        return getGetterName(field.getName(), field.getType());
    }

    public static String getGetterName(String fname, Class ftype) {
        String getterName = ftype.isAssignableFrom(Boolean.class) || ftype.isAssignableFrom(boolean.class) ? "is" : "get";
        getterName += fname.substring(0, 1).toUpperCase();
        getterName += fname.substring(1, fname.length());
        return getterName;
    }

    public static String getSetterName(String fieldName) {
        String setterName = "set";
        setterName += fieldName.substring(0, 1).toUpperCase();
        setterName += fieldName.substring(1, fieldName.length());
        return setterName;
    }

    /**
     * Need to transform the four possibilities:
     * 1. dictionary.attribute...
     * 2. map.attribute...
     * 3. pojo.param...
     * 4. primitive
     *
     * @param textToEvaluate
     * @param context
     * @return
     */
    @Override
    public <T> T transform(String textToEvaluate, Object context) {
        final String[] tokens = textToEvaluate.split(ATTRIBUTE_SEPARATOR);
        if (tokens.length == 0) {
            //simply return context
            return getFinalValue(context, textToEvaluate);
        } else if (tokens.length == 1) {
            return getFinalValue(context, tokens[0]);
        } else {
            String s = tokens[0];
            int offset = s.length() + 1;
            //make retrieved value the new context
            return (T) transform(textToEvaluate.substring(offset, textToEvaluate.length()), getFinalValue(context, s));
        }
    }

    /**
     * @param context the object from where the value will be taken
     * @param attr    the key used to retrieve the VALUE from the context. e.g. attr='name', context.type=java.org.openlogics.utils.util.Map, then
     *                will try to retrieve the name key from the given map.
     *                If teh context is a primitive, then simply return the value, no need to toSQLString the attr parameter.
     *                If the parameter is a {@link org.openlogics.gears.jdbc.FunctionParam}, then simply return the valur.
     * @param <T>
     * @return the value found in the context using the attr as the key to find it
     */
    protected <T> T getFinalValue(Object context, String attr) {
        if (isWrapperType(context)) {
            return (T) context;
        } else if (Map.class.isAssignableFrom(context.getClass())) {
            return (T) ((Map) context).get(attr);
        } else if (context instanceof Dictionary) {
            return (T) ((Dictionary) context).get(attr);
        } else if (context.getClass().isArray()) {
            //iterate through all the values
            Object[] values = (Object[]) context;
            for (Object value : values) {
                T finalValue = getFinalValue(value, attr);
                if (finalValue == null) continue;
                return finalValue;
            }
        } else if (context instanceof FunctionParam) {
            return (T) context;
        } else {//an unknown object has been provided, A POJO is assumed
            //use the reflection to retrieve the value
            try {
                Method method;
                //it's supposed to be n existing field in the given <b>POJO</b>
                try {
                    Field field = null;
                    Class fieldOwner = context.getClass();
                    while (fieldOwner != null) {
                        try {
                            field = fieldOwner.getDeclaredField(attr);
                            break;
                        } catch (NoSuchFieldException x) {
                            fieldOwner = fieldOwner.getSuperclass();
                        }
                    }
                    if (field == null) {
                        throw new IllegalArgumentException("The dynamic SQL statement defines an unexisting CLASS ATTRIBUTE ('" + attr + "') for object of type: " + context.getClass() + ". Check the '#{}' and '${}' parameters in dynamic SQL.");
                    }

                    //todo, this is throwing an illegal state exception, is this ok???

                    method = Reflections.getGetterMethod(field.getName(), context.getClass());

                    //check if the field has the Annotation for TypeMapper
                    Convert mappedType = field.getAnnotation(Convert.class);
                    if (mappedType == null) {
                        //simply invoke method
                        return (T) method.invoke(context);
                    } else {
                        //if a dialect has been defined, then just send it to handle it
                        Class<? extends AttributeConverter> sqlTypeClass = mappedType.converter();
                        if (sqlTypeClass != null && AttributeConverter.class.isAssignableFrom(sqlTypeClass)) {
                            try {
                                AttributeConverter typeMapper = sqlTypeClass.newInstance();
                                Object invoke = method.invoke(context);

                                try {
                                    return (T) typeMapper.convertToDatabaseColumn(invoke);
                                } catch (ClassCastException e) {
                                    logger.debug("Failed to convert SQL type into a valid Java type", e);
                                }
                            } catch (InstantiationException e) {
                                throw new IllegalStateException("Unable to call getterMethod", e);
                            }
                        }
                        //if after all iteration, the attribute couldn't be handled correctly, throw the last exception caught,
                        throw new SQLException("None of the SQLTypes provided were able to be used for mapping results to object attribute. Use getNextException method to see more details.");
                    }

                    /*
                    getter = getGetterName(fieldOwner.getDeclaredField(attr));
                    method = context.getClass().getMethod(getter);
                    return (T) method.invoke(context);
                    */
                } catch (Exception x) {
                    throw new IllegalArgumentException("Can not reach method.", x);
                }
            } catch (Exception ex) {
                throw new IllegalArgumentException("Unable to reach getter method for variable '" + attr + "' in object of type '" + context.getClass() + "'.", ex);
            }

        }
        return null;
    }

    /**
     * Use Guava to determine if object is primitive
     *
     * @param obj
     * @return
     */
    private boolean isWrapperType(Object obj) {
        return Primitives.isWrapperType(obj.getClass());
    }
}