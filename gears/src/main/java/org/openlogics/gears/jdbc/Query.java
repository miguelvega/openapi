/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc;

import com.google.common.base.Strings;
import org.openlogics.gears.jdbc.builder.Criteria;
import org.openlogics.gears.jdbc.builder.Filter;
import org.openlogics.gears.jdbc.builder.SQLClause;
import org.openlogics.utils.ImmutableString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Query object is delegated to transform the queryString against parameters
 * <p/>
 * todo, this class should exist inside the builder package
 *
 * @author Miguel Vega
 * @version $Id: Query.java 0, 2013-03-01 7:37 PM mvega $
 */
public abstract class Query<U> {
    protected final Logger LOGGER = LoggerFactory.getLogger(Query.class);
    protected Filter filter;
    protected String queryString;
    protected U context;
    protected SQLClause groupBy, orderBy, limitOffset;

    protected long firstResult=-1, maxResults=-1;

    protected Query(String queryString, U context, Filter filter) {
        this.queryString = queryString;
        this.context = context;
        this.queryString = queryString;
        this.filter = filter;
    }

    /**
     * Constructor which supports SQL statement plus the context of where to retrieve data from
     *
     * @param queryString a supported SQL statement. Can contains ?, #{}, or ${} keywords
     * @param ctxt        the object to retrieve data from
     */
    public static <V> Query of(final String queryString, final V... ctxt) {
        return of(queryString, null, ctxt != null && ctxt.length > 0 ? ctxt : null);
    }

    public static <V> Query of(final String queryString, final Filter filter) {
        return of(queryString, filter, (String)null);
    }

    public static <V> Query of(final String queryString, final Filter filter, final V... ctxt) {

        return new Query(queryString, ctxt, filter) {
            /**
             * Evaluate the given query string and replaces the parameters with a symbol for prepared statementt,
             * thus while replacing those characters, the values for each parameter is processed and retrieved for
             * placing them into the prepared statement object.
             *
             * @param dataStore
             * @param data
             * @return
             */
            @Override
            protected String evaluateQueryString(DataStore dataStore, final List data) {
                if (Strings.isNullOrEmpty(queryString)) {
                    throw new IllegalArgumentException("SQL SQLStatement found is NULL");
                }
                String localQueryString = queryString;
                //Before threat all the complexQuery replace the ${ds_schema}
                if (!Strings.isNullOrEmpty(dataStore.getSchema())) {
                    localQueryString = localQueryString.replace("${schema}", dataStore.getSchema());
                }

                if (context != null) {
                    SQLExpressionTransformer el = SQLExpressionTransformer.buildStaticTransformer();
                    //simple access
                    localQueryString = el.transform(localQueryString, context);
                    el = SQLExpressionTransformer.buildDynamicTransformer();
                    localQueryString = el.evaluateFixedParameter(localQueryString, context, "?", new SQLExpressionTransformer.ParameterParsedHandler() {
                        @Override
                        public void handle(Object obj) {
                            data.add(obj);
                        }
                    });
                    //in the case that query is being used as the common DBUtils String and parameters, this will solve such that problem
                    if (data.size() == 0 && context != null) {
                        if (context.getClass().isArray()) {
                            data.addAll(Arrays.asList((V[]) context));
                        } else {
                            data.add(context);
                        }
                    }
                }

                ImmutableString finalQuery = ImmutableString.of(localQueryString);

                if (filter != null) {

                    Filter whereclause = Filter.whereClause(filter);

                    org.openlogics.gears.jdbc.builder.Criteria criteria = whereclause.evaluate();
                    data.addAll(criteria.getQueryValues());

                    finalQuery.add(criteria.getQueryString());
                }

                if (groupBy != null) {
                    finalQuery.add(groupBy.toSQLString());
                }
                if (orderBy != null) {
                    finalQuery.add(orderBy.toSQLString());
                }
                //offset and limit are deprecated, but to keep compatibility, we'll persist in using them
                //if and only if limitOffset clause is missing
                if (limitOffset == null) {
                    /*
                    if (offset != null) {
                        finalQuery.add(offset.toSQLString());
                    }
                    if (limit != null) {
                        finalQuery.add(limit.toSQLString());
                    }
                    */
                } else {
                    finalQuery.add(limitOffset.toSQLString());
                }

                String queryString = finalQuery.build();

                LOGGER.debug("The final query string built is: {}", queryString);

                return queryString;
            }
        };
    }

    /**
     * Specify the group by statements
     *
     * @param groupBy
     * @return
     */
    public Query<U> group(SQLClause groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public Query<U> order(SQLClause orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    /**
     *
     * @param limitOffset
     * @return
     * @deprecated use setMaxResults and setFirstResult methods instead
     */
    @Deprecated
    public Query<U> limitOffset(SQLClause limitOffset) {
        this.limitOffset = limitOffset;
        return this;
    }

    /**
     * todo, not implemented yet
     * @param maxResults
     * @return
     */
    public Query setMaxResults(long maxResults){
        this.maxResults = maxResults;
        return this;
    }

    /**
     * todo, not implemented yet
     * @param firstResult
     * @return
     */
    public Query setFirstResult(long firstResult){
        this.firstResult = firstResult;
        return this;
    }


    public Query<U> withFilter(Filter filter) {
        this.filter = filter;
        return this;
    }

    /**
     * Evaluate the given query string and replaces the parameters with a symbol for prepared statementt,
     * thus while replacing those characters, the values for each parameter is processed and retrieved for
     * placing them into the prepared statement object.
     *
     * @param dataStore
     * @param data
     * @return
     */
    protected abstract String evaluateQueryString(DataStore dataStore, final List data) throws SQLException;

    @Override
    public String toString() {
        return queryString;
    }

    public Filter getFilter() {
        return filter;
    }

    public Query setFilter(Filter filter) {
        this.filter = null;
        this.filter = filter;
        return this;
    }
}
