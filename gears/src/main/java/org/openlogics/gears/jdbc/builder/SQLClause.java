package org.openlogics.gears.jdbc.builder;

import com.google.common.collect.Lists;
import org.openlogics.utils.ImmutableString;

import java.util.List;

import static org.openlogics.utils.ImmutableString.of;
import org.openlogics.utils.Strings;

/**
 * @author Miguel Vega
 * @version $Id: SQLClause.java 0, 2013-11-19 12:27 PM mvega $
 */
public abstract class SQLClause<U> {

    protected List<U> paramList;

    protected SQLClause() {
        this.paramList = Lists.newArrayListWithCapacity(1);
    }

    public SQLClause<U> by(U param) {
        if (param != null) {
            paramList.add(param);
        }
        return this;
    }

    /**
     * todo, this must be protected
     *
     * @return the plain SQL statement String
     */
    public abstract String toSQLString();

    /**
     * Builds a limit object, this will be ignored when limit is lower than 1.
     * This class provides the skeleton of the SQLClause class specific per DBMS.
     *
     * @param limit
     * @return
     * @deprecated use the {@link SQLClause#limitOffset} method instead
     */
    @Deprecated
    public static SQLClause limit(final long limit) {
        throw new UnsupportedOperationException("This method is supported in specific driver version, e.g. gears-postgresql");
    }

    /**
     * Ignores values below 0
     * This class provides the skeleton of the SQLClause class specific per DBMS.
     *
     * @param offset
     * @return
     * @deprecated use the {@link SQLClause#limitOffset} method instead
     */
    @Deprecated
    public static SQLClause offset(final long offset) {
        throw new UnsupportedOperationException("This method is supported in specific driver version, e.g. gears-postgresql");
    }

    public static SQLClause limitOffset(final long offset, final long limit) {
        if (limit < 0 && offset<0) {
            return null;
        }

        if(offset>=0){
            return new SQLClause() {
                @Override
                public String toSQLString() {
                    return of(" LIMIT ").add(limit).add(" OFFSET ").add(offset).build();
                }
            };
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" LIMIT ").add(limit).build();
            }
        };
    }

    public static SQLClause<SortParam> order() {

        return new SQLClause<SortParam>() {
            @Override
            public String toSQLString() {
                if (paramList.isEmpty()) {
                    return "";
                }
                ImmutableString is = ImmutableString.of(" ORDER BY ");

                for (SortParam sortParam : paramList) {
                    is.add(sortParam.getProperty()).add(sortParam.isAscending() ? " ASC" : " DESC").add(", ");
                }

                is = is.removeFromLastMatch(", ");

                return is.build();
            }
        };
    }

    public static SQLClause group() {

        return new SQLClause() {
            @Override
            public String toSQLString() {
                if (paramList.isEmpty()) {
                    return "";
                }

                ImmutableString is = ImmutableString.of(" GROUP BY ");

                for (Object sortParam : paramList) {
                    is.add(Strings.toText(sortParam)).add(", ");
                }

                is = is.removeFromLastMatch(", ");

                return is.build();
            }
        };
    }
}
