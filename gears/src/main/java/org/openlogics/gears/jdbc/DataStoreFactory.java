package org.openlogics.gears.jdbc;

import com.google.common.base.Optional;
import org.openlogics.utils.Provider;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Miguel Vega
 * @version $Id: DataStoreFactory.java 0, 5/27/14 11:37 PM, @miguel $
 */
public class DataStoreFactory {

    /**
     * @param dataSource
     * @return
     * @deprecated use DataSToreFactory@createObjectDataStore method instead
     */
    @Deprecated
    public static final DataStore createDataStore(final DataSource dataSource) {
        return createDataStore(dataSource, false);
    }

    /**
     * Creates a new instance of the {@link org.openlogics.gears.jdbc.DataStore} instance using a {@link javax.sql.DataSource} object,
     * specially used with JNDI connections
     *
     * @param dataSource
     * @param parameterMetaDataKnownBroken
     * @return
     * @deprecated use DataSToreFactory@createObjectDataStore method instead
     */
    @Deprecated
    public static final DataStore createDataStore(final DataSource dataSource, final boolean parameterMetaDataKnownBroken) {
        return new DataStore(parameterMetaDataKnownBroken) {

            @Override
            protected Connection acquireConnection() throws SQLException {
                if (dataSource != null)
                    return dataSource.getConnection();
                throw new SQLException("Unable to use a NULL DataSource connection");
            }
        };
    }

    /**
     * @param url
     * @param connProperties
     * @return
     * @deprecated use DataSToreFactory@createObjectDataStore method instead
     */
    @Deprecated
    public static final DataStore createDataStore(final String url, final Properties connProperties) {
        return createDataStore(url, connProperties, false);
    }

    /**
     * Creates a new instance of the DataSTore instance using a DataSource object, specially used with JNDI connections
     *
     * @param url
     * @param connProperties
     * @param parameterMetaDataKnownBroken
     * @return
     * @deprecated use DataSToreFactory@createObjectDataStore method instead
     */
    @Deprecated
    public static final DataStore createDataStore(final String url, final Properties connProperties, final boolean parameterMetaDataKnownBroken) {
        return new DataStore(parameterMetaDataKnownBroken) {

            @Override
            protected Connection acquireConnection() throws SQLException {
                logger.debug("URL '{}', Properties '{}'", url, connProperties);
                return DriverManager.getConnection(url, Optional.fromNullable(connProperties).or(new Properties()));
            }
        };
    }

    /**
     * @param dataSource
     * @return
     */
    public static final ObjectDataStore createObjectDataStore(final DataSource dataSource) {
        return createObjectDataStore(dataSource, false);
    }

    /**
     * Creates a new instance of the {@link org.openlogics.gears.jdbc.DataStore} instance using a {@link javax.sql.DataSource} object,
     * specially used with JNDI connections
     *
     * @param dataSource
     * @param parameterMetaDataKnownBroken
     * @return
     */
    public static final ObjectDataStore createObjectDataStore(final DataSource dataSource, final boolean parameterMetaDataKnownBroken) {
        return new ObjectDataStore() {
            @Override
            protected Connection acquireConnection() throws SQLException {
                if (dataSource != null)
                    return dataSource.getConnection();
                throw new SQLException("Unable to use a NULL DataSource connection");
            }
        };
    }

    /**
     * @param url
     * @param connProperties
     * @return
     */
    public static final ObjectDataStore createObjectDataStore(final String url, final Properties connProperties) {
        return createObjectDataStore(url, connProperties, false);
    }

    /**
     * Creates a new instance of the DataSTore instance using a DataSource object, specially used with JNDI connections
     *
     * @param url
     * @param connProperties
     * @param parameterMetaDataKnownBroken
     * @return
     */
    public static final ObjectDataStore createObjectDataStore(final String url, final Properties connProperties, final boolean parameterMetaDataKnownBroken) {
        return new ObjectDataStore() {
            @Override
            protected Connection acquireConnection() throws SQLException {
                logger.debug("URL '{}', Properties '{}'", url, connProperties);
                return DriverManager.getConnection(url, connProperties);
            }
        };
    }

    public static final ObjectDataStore createObjectDataStore(final Provider<Connection> connectionProvider, final boolean parameterMetaDataKnownBroken) {
        return new ObjectDataStore() {
            @Override
            protected Connection acquireConnection() throws SQLException {
                return connectionProvider.get();
            }
        };
    }

    public static final ObjectDataStore createObjectDataStore(final Provider<Connection> connectionProvider) {
        return createObjectDataStore(connectionProvider, false);
    }

}
