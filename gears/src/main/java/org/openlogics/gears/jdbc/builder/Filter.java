package org.openlogics.gears.jdbc.builder;

import com.google.common.collect.Lists;
import org.openlogics.utils.ImmutableString;
import org.openlogics.utils.Provider;

import java.math.BigDecimal;
import java.util.List;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.openlogics.utils.ImmutableString.of;

/**
 * @author Miguel Vega
 * @version $Id: Filter.java 0, 2013-10-29 11:24 PM mvega $
 */
public abstract class Filter<T> {

    protected String prepend, append;

    protected boolean excludeNullOrEmptyValue = false;

    private static <T> Filter comparator(final String operator, final String column, final T value) {

        return new Filter() {
            @Override
            public Criteria evaluate() {

                if (excludeNullOrEmptyValue) {
                    Provider<String> valueProvider = new Provider<String>(){
                        @Override
                        public String get() {
                            return value==null?"":String.valueOf(value);
                        }
                    };
                    //should return nothing
                    if (isNullOrEmpty(valueProvider.get()))
                        return new Criteria("");
                }

                if (!isNullOrEmpty(prepend) && !isNullOrEmpty(append)) {
                    //return new Criteria(of(column).add(" ").add(operator).add(" ?").build(), of(prepend).add(value).add(append).build());
                    return new Criteria(of(column).add(" ").add(operator).add(" ").add(prepend).add("?").add(append).build(), value);
                } else if (!isNullOrEmpty(append)) {
                    return new Criteria(of(column).add(" ").add(operator).add(" ?").add(append).build(), value);
                } else if (!isNullOrEmpty(prepend)) {
                    return new Criteria(of(column).add(" ").add(operator).add(" ").add(prepend).add("?").build(), value);
                }
                return new Criteria(of(column).add(" ").add(operator).add(" ?").build(), value);
            }
        };
    }

    public static <T> Filter equal(final String column, final T value) {
        return comparator("=", column, value);
    }

    public static <T> Filter notEqual(final String column, final T value) {
        return comparator("=", column, value);
    }

    public static <T> Filter less(final String column, final T value) {
        return comparator("<", column, value);
    }

    public static <T> Filter lessOrEqual(final String column, final T value) {
        return comparator("<=", column, value);
    }

    public static <T> Filter greater(final String column, final T value) {
        return comparator(">", column, value);
    }

    public static <T> Filter greaterOrEqual(final String column, final T value) {
        return comparator(">=", column, value);
    }

    /**
     * @param column
     * @param value
     * @param <T>
     * @return
     * @deprecated this method is deprecated, because only applies PostgreSQL ILIKE
     */
    @Deprecated
    public static <T> Filter ilike(final String column, final T value) {
        return comparator("ILIKE", column, value);
    }

    /**
     * A simple LIKE filter
     *
     * @param column
     * @param value
     * @param <T>
     * @return
     */
    public static <T> Filter like(final String column, final T value) {
        return comparator("LIKE", column, value);
    }

    /**
     * If value povided is empty or null, the '%' characters won't be concatenated to the value
     *
     * @param column
     * @param value
     * @param <T>
     * @return
     */
    public static <T> Filter likePattern(final String column, final T value) {
        return likePattern(column, value, false);
    }

    /**
     * This method appends automatically the '%' char to the start and to the end of the given value
     *
     * @param column
     * @param value
     * @param <T>
     * @param acceptEmptyValue
     * @return
     */
    public static <T> Filter likePattern(final String column, final T value, boolean acceptEmptyValue) {

        if (value == null) {
            return like(column, value);
        }

        String obj = value.getClass().isAssignableFrom(BigDecimal.class) ? ((BigDecimal) value).toPlainString() : String.valueOf(value);
        if (obj.isEmpty() && !acceptEmptyValue) {
            return like(column, null);
        }

        return like(column, of("%").add(obj).add("%").build());
        //return comparator("LIKE", column, of("%").add(obj).add("%").build());
    }

    public static <T> Filter isNull(final String column) {
        return new Filter() {
            @Override
            public Criteria evaluate() {
                return new Criteria(of(column).add(" IS NULL").build());
            }
        };
    }

    public static <T> Filter not(final Filter filter) {
        return new Filter() {
            @Override
            public Criteria evaluate() {
                Criteria evaluate = filter.evaluate();
                return new Criteria(of("NOT ").add(evaluate.getQueryString()).build(), evaluate.getQueryValues());
            }
        };
    }

    public static <T> Filter between(final String column, final T lower, final T upper) {
        return new Filter() {
            @Override
            public Criteria evaluate() {
                return new Criteria(of(column).add(" BETWEEN ? AND ?").build(), lower, upper);
            }
        };
    }

    /**
     * This shouldn't be visible to any external resource, but for the JDBC handlers defined in this package
     *
     * @param filters
     * @return
     */
    public static Filter whereClause(final Filter... filters) {
        if (filters == null) {
            return new EmptyFilter();
        }

        return new Filter() {
            @Override
            public Criteria evaluate() {
                ImmutableString string = of();
                List values = Lists.newLinkedList();
                for (Filter filter : filters) {
                    if (filter != null) {
                        Criteria evaluated = filter.evaluate();

                        if(evaluated.hasAnyNull() && excludeNullOrEmptyValue)
                            continue;

                        string.add(evaluated.getQueryString().trim()).add(" ");

                        values.addAll(evaluated.getQueryValues());
                    }
                }
                String eval = string.isEmpty() ? "" : of(" WHERE ").add(string.build().trim()).build();
                return new Criteria(eval, values);
            }
        };
    }

    public static Filter and(final Filter... filters) {
        if (filters == null) return new EmptyFilter();

        return new Filter() {
            @Override
            public Criteria evaluate() {
                ImmutableString string = of();
                List values = Lists.newLinkedList();

                for (Filter filter : filters) {
                    if (filter != null) {
                        //
                        Criteria evaluated = filter.evaluate();

                        if (evaluated.hasAnyNull() && excludeNullOrEmptyValue)
                            continue;

                        //if filter is a IS NUL, then need to change this

                        if (!evaluated.getQueryString().endsWith("IS NULL"))
                            string.add(evaluated.getQueryString().trim()).add(evaluated.getQueryValues().isEmpty() ? "" : " AND ");
                        else
                            string.add(evaluated.getQueryString().trim()).add(" AND ");

                        values.addAll(evaluated.getQueryValues());
                    }
                }

                if (string.isEmpty()) {
                    return Criteria.empty();
                }

                String ff = string.removeFromLastMatch("AND").encloseWith("(", ")").build();

                return new Criteria(ff, values);
            }
        };
    }

    public static Filter or(final Filter... filters) {
        if (filters == null) return new EmptyFilter();

        return new Filter() {
            @Override
            public Criteria evaluate() {
                List values = Lists.newLinkedList();
                ImmutableString string = of();

                for (Filter filter : filters) {
                    if (filter != null) {
                        Criteria evaluated = filter.evaluate();

                        if (evaluated.hasAnyNull() && excludeNullOrEmptyValue)
                            continue;

                        if (!evaluated.getQueryString().endsWith("IS NULL"))
                            string.add(evaluated.getQueryString().trim()).add(evaluated.getQueryValues().isEmpty() ? "" : " OR ");
                        else
                            string.add(evaluated.getQueryString().trim()).add(" OR ");

                        values.addAll(evaluated.getQueryValues());
                    }
                }

                if (string.isEmpty()) {
                    return Criteria.empty();
                }

                String exc = string.removeFromLastMatch("OR").encloseWith("(", ")").build();

                return new Criteria(exc, values);
            }
        };
    }

    public abstract Criteria evaluate();

    public Filter prepend(String prepend) {
        this.prepend = prepend;
        return this;
    }

    public Filter append(String append) {
        this.append = append;
        return this;
    }

    public Filter excludeNullOrEmptyValue(boolean excludeNullOrEmptyValue) {
        this.excludeNullOrEmptyValue = excludeNullOrEmptyValue;
        return this;
    }

    /**
     * @return
     */
    private static class EmptyFilter extends Filter {
        @Override
        public Criteria evaluate() {
            return Criteria.empty();
        }
    }
}
