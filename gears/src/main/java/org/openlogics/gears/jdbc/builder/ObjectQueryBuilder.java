package org.openlogics.gears.jdbc.builder;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.openlogics.gears.jdbc.Query;
import org.openlogics.gears.text.ExpressionTransformerImpl;
import org.openlogics.utils.ImmutableString;
import org.openlogics.utils.reflect.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.base.Optional.fromNullable;
import static org.openlogics.utils.reflect.Reflections.getDeclaredFields;
import static org.openlogics.utils.reflect.Reflections.getGetterMethodName;

/**
 * This class allows to build the {@link Query} object based on an
 * annotated Table bean object
 *
 * @author Miguel Vega
 * @version $Id: ObjectQueryBuilder.java 0, 12/6/13 1:43 AM miguel $
 */
public class ObjectQueryBuilder<V> extends ExpressionTransformerImpl {

    private boolean distinct = false;

    private Logger logger = LoggerFactory.getLogger(ObjectQueryBuilder.class);

    /**
     * Look for ID column in entity
     *
     * @param type type to examine
     * @return the {@link Field} object annotated with {@link Id}
     * @throws NoSuchElementException if no field annotated with {@link Id} has been found
     */
    public Field getIDColumn(Class type) throws NoSuchElementException {

        List<Field> declaredFields = getDeclaredFields(type);

        for (Field field : declaredFields) {
            Id col = field.getAnnotation(Id.class);
            if (col == null) continue;

            return field;
        }

        throw new NoSuchElementException("Type " + type.getName() + " has not a field annotated with @javax.persistence.Id");
    }

    /**
     * todo, what happens when none method is annotated with column
     * todo, need to provide filter, groupby, etc
     *
     * @param type
     * @return
     */
    public Query buildSimpleSelect(Class<V> type) {
        return this.buildSimpleSelect(type, null);
    }

    /**
     * @param type
     * @param schema sometimes it's necessary to define a schema to work within specifically
     * @return
     */
    public Query buildSimpleSelect(Class<V> type, final String schema) {

        //first get annotation
        Table table = getTableName(type);

        if (table == null) {
            throw new IllegalArgumentException(ImmutableString.of("Type '").add(type.getName()).add("', must be annotated with javax.persistence.Table.").build());
        }

        List<Field> declaredFields = getDeclaredFields(type);

        ImmutableString is = ImmutableString.of("SELECT ");

        is.add(distinct ? " DISTINCT " : "");

        for (Field field : declaredFields) {

            //Column col = field.getAnnotation(Column.class);

            //if (col == null) continue;

            try {
                is.add(getUsableColumnName(field)).add(", ");
            } catch (NoSuchElementException x) {
                logger.debug("Column for field '{}' not usable to fetch or to save data in database", field.getName());
            }
        }

        is = is.removeFromLastMatch(", ");

        String tablePrefix = fromNullable(schema).transform(new Function<String, String>() {
            public String apply(String input) {
                return Strings.isNullOrEmpty(input) ? "" : schema.concat(".");
            }
        }).or("");

        String name = Strings.isNullOrEmpty(table.name())?type.getSimpleName():table.name();

        is.add(" FROM ").add(tablePrefix).add(name);

        return Query.of(is.build());
    }

    public Query buildPreparedDelete(V v) {
        return buildPreparedDelete(v, null);
    }

    public Query buildPreparedDelete(V v, final String schema) {

        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

        Iterable<Field> fields = getDeclaredFields(type);

        String tablePrefix = fromNullable(schema).transform(new Function<String, String>() {
            public String apply(String input) {
                return Strings.isNullOrEmpty(input) ? "" : schema.concat(".");
            }
        }).or("");

        String name = Strings.isNullOrEmpty(table.name())?type.getSimpleName():table.name();

        ImmutableString is = ImmutableString.of("DELETE FROM ").add(tablePrefix).add(name);

        return Query.of(is.build());
    }

    /**
     * This method builds the UPDATE statement using the '?' in the same way PreparedStatements does.
     *
     * @param v only {@link Table} annotated beans can use this method
     * @return
     */
    public Query buildPreparedUpdate(V v) {
        return buildPreparedUpdate(v, null);
    }

    public Query buildPreparedUpdate(V v, final String schema) {

        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

        Iterable<Field> fields = getDeclaredFields(type);

        String tablePrefix = fromNullable(schema).transform(new Function<String, String>() {
            public String apply(String input) {
                return Strings.isNullOrEmpty(input) ? "" : schema.concat(".");
            }
        }).or("");

        String name = Strings.isNullOrEmpty(table.name())?type.getSimpleName():table.name();

        ImmutableString is = ImmutableString.of("UPDATE ").add(tablePrefix).add(name).add(" SET ");

        final List<Object> values = Lists.newLinkedList();

        for (Field field : fields) {

            Column col = field.getAnnotation(Column.class);
            if (col == null) continue;

            GeneratedValue pk = field.getAnnotation(GeneratedValue.class);

            try {

                values.add(invokeGetterMethod(field, v));

                is.add(getUsableColumnName(field)).add(" = ?, ");

            } catch (IllegalStateException e) {
                //todo
            }
            //}
        }

        is = is.removeFromLastMatch(", ");

        final String qstr = is.build();

        logger.debug("QUERY: " + qstr + ", DATA: " + values);

        return Query.of(qstr, values.toArray());
    }

    /**
     * This method builds the UPDATE statement using the '?' in the same way PreparedStatements does.
     *
     * @param v only {@link Table} annotated beans can use this method
     * @return
     */
    public Query buildPreparedInsert(V v) {
        return buildPreparedInsert(v, null);
    }

    public Query buildPreparedInsert(V v, final String schema) {

        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

//        Iterable<PropertyDescriptor> descriptors = inspectType(type);
        Iterable<Field> fields = getDeclaredFields(type);

        String tablePrefix = fromNullable(schema).transform(new Function<String, String>() {
            public String apply(String input) {
                return Strings.isNullOrEmpty(input) ? "" : schema.concat(".");
            }
        }).or("");

        String name = Strings.isNullOrEmpty(table.name())?type.getSimpleName():table.name();

        ImmutableString is = ImmutableString.of("INSERT INTO ").add(tablePrefix).add(name).add(" (");

        ImmutableString imarks = ImmutableString.of();

        final List<Object> values = Lists.newLinkedList();

        for (Field field : fields) {

            Column col = field.getAnnotation(Column.class);
            if (col == null) continue;

            GeneratedValue pk = field.getAnnotation(GeneratedValue.class);

            if (pk != null) {
                if (pk.strategy() == GenerationType.IDENTITY) {
                    //do not insert

                } else if (pk.strategy() == GenerationType.SEQUENCE) {

                    //When using a postgresql database, column might be SERIAL, which means

                    String generator = pk.generator();

                    if (!Strings.isNullOrEmpty(generator)) {
                        //only if user explicitly specifies the SEQUENCE generator, use it for insert
                        //e.g. generator = "nextval('idsequence')"
                        is.add(getUsableColumnName(field)).add(", ");
                        imarks.add(pk.generator()).add(", ");
                    }
                } else if (pk.strategy() == GenerationType.AUTO) {
                    //todo, what???
                } else if (pk.strategy() == GenerationType.TABLE) {
                    //todo, what???
                }
            } else {
                try {

                    values.add(invokeGetterMethod(field, v));

                    is.add(Strings.isNullOrEmpty(col.name()) ? field.getName() : col.name()).add(", ");
                    imarks.add("?, ");

                } catch (IllegalStateException e) {
                    //todo
                }
            }
        }

        is = is.removeFromLastMatch(", ");
        imarks = imarks.removeFromLastMatch(", ");

        is.add(") VALUES (").add(imarks).add(')');

        final String qstr = is.build();

        logger.debug("QUERY: " + qstr + ", DATA: " + values);

        return Query.of(qstr, values.toArray());
    }

    private boolean isValidInput(V v) {
        return v != null && v.getClass().getAnnotation(Table.class) != null;
    }

    private Table getTableName(Class<?> type) {
        Table ann = type.getAnnotation(Table.class);

        return fromNullable(ann).orNull();
    }

    public Filter buildFilterForId(V v) {
        assert v != null;

        Table e = v.getClass().getAnnotation(Table.class);
        if (e == null)
            throw new IllegalArgumentException(v.getClass().getName() + " is not a valid @Table object");

        List<Field> declaredFields = Reflections.getDeclaredFields(v.getClass());

        for (Field df : declaredFields) {
            Id id = df.getAnnotation(Id.class);
            if (id != null) {
                return Filter.equal(getUsableColumnName(df), invokeGetterMethod(df, v));
            }
        }

        throw new IllegalArgumentException(v.getClass().getName() + " does not contain a attribute annotated with @Id");
    }

    private Object invokeGetterMethod(Field field, Object obj, Object... parameters) {

        Class type = obj.getClass();

        try {
            Method plainValue = Reflections.getGetterMethod(field);

            if (field.getAnnotation(Convert.class) != null) {
                Class<? extends AttributeConverter> attributeConverterType = field.getAnnotation(Convert.class).converter();
                if (attributeConverterType != null && AttributeConverter.class.isAssignableFrom(attributeConverterType)) {
                    try {
                        AttributeConverter typeMapper = attributeConverterType.newInstance();
                        Object invoke = plainValue.invoke(obj, parameters);
                        try {
                            return typeMapper.convertToDatabaseColumn(invoke);
                        } catch (ClassCastException e) {
                            logger.debug("Failed to convert SQL type into a valid Java type", e);
                        }
                    } catch (InstantiationException e) {
                        throw new IllegalStateException("Unable to call getterMethod", e);
                    }
                }
            }

            return plainValue.invoke(obj, parameters);
        } catch (IntrospectionException e) {
            String msg = "Unable to obtain a valid method: " + type.getName() + "#" + getGetterMethodName(field.getName(), field.getType());
            logger.debug(msg);
            throw new IllegalStateException(msg, e);
        } catch (InvocationTargetException e) {
            String msg = "Unable to find a valid GETTER method: " + type.getName() + "." + getGetterMethodName(field.getName(), field.getType());
            logger.debug(msg);
            throw new IllegalStateException(msg, e);
        } catch (IllegalAccessException e) {
            String msg = "Unable to access GETTER method: " + type.getName() + "." + getGetterMethodName(field.getName(), field.getType());
            logger.debug(msg);
            throw new IllegalStateException(msg, e);
        }
    }

    /**
     * Returns a valid column name to use, if field has an empty annotated @Column, then returns the field name
     *
     * @param field
     * @return
     */
    protected String getUsableColumnName(Field field) {
        if(field.getName().equals("stocks")) {
            String sasa = "fii";
        }
        Column column = field.getAnnotation(Column.class);

        if (column == null) {
            //check if the getter method has the Column annotation on it
            try {
                Method getterMethod = Reflections.getGetterMethod(field);
                if (getterMethod != null) {
                    //todo, only accept Fields annotated with @Column, if not avoid this Field
                    column = getterMethod.getAnnotation(Column.class);
                }
            } catch (IntrospectionException e) {
                //todo, only accept Fields annotated with @Column, if not avoid this Field
            }
        }

        if (column == null) {
            throw new NoSuchElementException("Field: " + field.getDeclaringClass().getName() + "#" + field.getName() +
                    " does not have a @Column annotation.");
        }

        return Strings.isNullOrEmpty(column.name()) ? field.getName() : column.name();
    }

    public boolean isDistinct() {
        return distinct;
    }

    public ObjectQueryBuilder setDistinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }
}