package org.openlogics.gears.jdbc.reader;

import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.cache.Cache;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import org.openlogics.gears.ParsingException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Miguel Vega
 * @version $Id: XmlStatementReader.java 0, 2013-05-05 03:11 mvega $
 */
public class XmlStatementReader extends DefaultHandler2 implements StatementReader {
    private static final String ALL = "*";

    /**
     * replaces the cached elements in method
     */
    private boolean detachableCache = false;

    private Cache<String, String> statementCache;

    private List<String> required;
    private Map<String, String> found;

    //Deprecated fields are supposed to be used only by the parser
    @Deprecated
    private String statementIdNeeded, statementBodyRead;
    @Deprecated
    private boolean allowed = false;

    static final String STATEMENT = "statement";

    @Inject
    public XmlStatementReader(Cache<String, String> statementCache) {
        this.statementCache = statementCache;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);

        if (qName.equals(STATEMENT)) {
            String id = Optional.fromNullable(attributes.getValue("id")).or("");

            String statementId = Iterables.tryFind(required, Predicates.equalTo(id)).or("");
            //check if the list is EMPTY, to load it anyway
            if(required.isEmpty()){
                //load it ALL
                statementId = id;
            }

            boolean replaceCachedElement = replaceCachedElement(statementId);
            if (!isNullOrEmpty(statementId) && replaceCachedElement) {
                allowed = true;
                statementIdNeeded = statementId;
            }

            /*
            if (id.equals(statementIdNeeded) && isNullOrEmpty(statementBodyRead)) {
                allowed = true;
            }
            */
        }
    }

    /**
     *
     * @param statementId the statement ID looking for in cache
     * @return true if the cached element must be replaced by this new one
     */
    private boolean replaceCachedElement(String statementId) {
        return statementCache.getIfPresent(statementId)==null || detachableCache;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);

        if (qName.equals(STATEMENT)) {
            if (allowed && !isNullOrEmpty(statementBodyRead)) {
                allowed = false;
                //just to support the oldest version with deprecated versions
                if(found!=null){
                    found.put(statementIdNeeded, statementBodyRead);
                }
                else{
                    endDocument();
                }

                statementBodyRead = null;
                statementIdNeeded = null;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (!allowed) return;

        StringBuilder sb = new StringBuilder(Optional.fromNullable(statementBodyRead).or(""));
        sb.append(new String(ch, start, length));
        this.statementBodyRead = sb.toString();
    }

    @Override
    public XmlStatementReader detachCachedObjects(boolean detach) {
        this.detachableCache = detach;
        return this;
    }

    @Override
    public String getStatementById(String statementId) {
        return statementCache.getIfPresent(statementId);
    }

    /**
     * {@inheritDoc}
     **/
    @Override
    public StatementReader loadStatements(InputStream inputStream) throws ParsingException {
        return loadStatements(inputStream, ALL);
    }

    /**
     * {@inheritDoc}
     **/
    @Override
    public StatementReader loadStatements(InputStream inputStream, String... statementId) throws ParsingException {
        //first check if cache already contains the required values
        if(statementCache.getAllPresent(Arrays.asList(statementId)).size() == statementId.length){
            return this;
        }

        if(statementId.length==1 && statementId[0].equals(ALL)){
            //we'll send an empty list, which means that request is LOAD ALL
            this.required = Collections.emptyList();
        }else{
            this.required = Arrays.asList(statementId);
        }

        try {
            this.found = Maps.newConcurrentMap();

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(inputStream, this);

            if (!found.isEmpty()) {
                //iterate through each one of the values found
                Iterator<String> iterator = found.keySet().iterator();
                while(iterator.hasNext()){
                    String stId = iterator.next();
                    statementCache.put(stId, found.remove(stId));
                }
            }

        } catch (ParserConfigurationException e) {
            throw new ParsingException(e);
        } catch (SAXException e) {
            throw new ParsingException(e);
        } catch (IOException e) {
            throw new ParsingException(e);
        }finally {
            required = null;
            found.clear();
            found = null;
        }
        return this;
    }
}
