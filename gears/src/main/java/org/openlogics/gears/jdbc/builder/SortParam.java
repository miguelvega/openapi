package org.openlogics.gears.jdbc.builder;

/**
 * A copy of the SortParam of wicket.
 * @author Miguel Vega
 * @version $Id: SortParam.java 0, 2013-11-08 4:47 PM mvega $
 */
public class SortParam<T> {
    private static final long serialVersionUID = 1L;
    private final T property;
    private final boolean ascending;

    /**
     * @param property  sort property
     * @param ascending <code>true<code> if sort order is ascending, <code>false</code> if sort order is
     *                  descending
     */
    public SortParam(final T property, final boolean ascending) {
        assert property!=null;

        this.property = property;
        this.ascending = ascending;
    }

    public SortParam(final T property) {
        this(property, true);
    }

    /**
     * @return sort property
     */
    public T getProperty() {
        return property;
    }

    /**
     * check if sort order is ascending
     *
     * @return <code>true<code> if sort order is ascending, <code>false</code> if sort order is
     *         descending
     */
    public boolean isAscending() {
        return ascending;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SortParam)) {
            return false;
        }

        @SuppressWarnings("unchecked")
        SortParam<T> sortParam = (SortParam<T>) o;

        return (ascending == sortParam.ascending) && property.equals(sortParam.property);
    }

    @Override
    public int hashCode() {
        int result = property.hashCode();
        result = 31 * result + (ascending ? 1 : 0);
        return result;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new StringBuilder().append("[SortParam property=")
                .append(getProperty())
                .append(" ascending=")
                .append(ascending)
                .append("]")
                .toString();
    }
}
