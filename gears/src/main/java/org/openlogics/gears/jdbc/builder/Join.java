package org.openlogics.gears.jdbc.builder;

import com.google.common.collect.Lists;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.Query;
import org.openlogics.utils.ImmutableString;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: Join.java 0, 1/2/14 3:20 PM miguel $
 */
public class Join extends Query {

    private final List<Table> tables;
    //this string contains all the columns to be selected
    private String selectColumnsString;

    protected Join(String queryString, Object context, Filter filter) {
        super(queryString, context, filter);

        this.tables = Lists.newArrayList();
    }

    public static Join of(Table table) {
        assert table != null;
        Join join = new Join(null, null, null);

        join.tables.add(table);

        return join;
    }

    public Join on(Table table) {

        assert table != null;

        tables.add(table);

        return this;
    }

    public Join select(Column... columns) {
        ImmutableString is = ImmutableString.of();
        for (Column column : columns) {
            is.add(column.name).add(", ");
        }
        this.selectColumnsString = is.removeFromLastMatch(", ").build();
        return this;
    }

    @Override
    protected String evaluateQueryString(DataStore dataStore, List data) throws SQLException {

        ImmutableString queryString = ImmutableString.of();

        queryString.add("SELECT ").add(selectColumnsString).add(" FROM ");

        for (Table table : tables) {
            queryString.add(tables.get(0).getQualifiedName());
        }


        return null;
    }

    public static final class Table {
        private final String alias, name;
        private String relationColumn;

        public Table(String name) {
            this(name, name);
        }

        public Table(String name, String alias) {

            assert name != null;

            this.name = name;
            this.alias = alias != null ? alias : name;
        }

        public String getRelationColumn() {
            return relationColumn;
        }

        public Table setRelationColumn(String relationColumn) {
            this.relationColumn = relationColumn;
            return this;
        }

        public Column getColumn(String cname, String calias) {
            ImmutableString is = ImmutableString.of();
            is.add(alias.concat(".")).add(calias != null ? cname.concat(" AS ").concat(calias) : cname);

            return new Column(is.build());
        }

        private String getQualifiedName() {
            return alias.equals(name) ? name : name.concat(" AS ").concat(alias);
        }
    }

    /**
     * Only can be created via {@link org.openlogics.gears.jdbc.builder.Join.Table} object
     */
    static final class Column {
        protected final String name;

        public Column(String name) {
            this.name = name;
        }
    }
}
