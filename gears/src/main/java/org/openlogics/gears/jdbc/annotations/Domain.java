package org.openlogics.gears.jdbc.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation allows to a validator determine if a given value is allowed or not.
 * When a database has constraints, for example: <code>constraint CKC_PARAM_TYPE_PRI_PARA check (PARAM_TYPE is null or (PARAM_TYPE in ('string','int','long','decimal','boolean','image')))</code>.
 * This will prevent given value to use one of these.
 * TODO, this might be implemented in the POJO generator.
 * @author Miguel Vega
 * @version $Id: Domain.java 0, 2013-12-03 1:51 PM mvega $
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface Domain {
    String[] values();
}
