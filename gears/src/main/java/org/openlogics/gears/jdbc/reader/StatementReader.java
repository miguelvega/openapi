package org.openlogics.gears.jdbc.reader;

import org.openlogics.gears.ParsingException;

import java.io.InputStream;

/**
 * This class allows SQL statement read
 *
 * @author Miguel Vega
 * @version $Id: StatementReader.java 0, 2013-05-05 03:02 mvega $
 */
public interface StatementReader {

    <T extends StatementReader> T detachCachedObjects(boolean detach);

    /**
     * Retrieve the required statement by its ID
     * @param statementId
     * @return
     */
    String getStatementById(String statementId);

    /**
     * This method is created to load all the statements from given inputStream, trying to find the statement IDS provided
     * @param inputStream
     * @param statementId, comma separated element IDs for each statement. Can use a * symbol to load all the statements in file
     * @return
     */
    StatementReader loadStatements(InputStream inputStream, String... statementId) throws ParsingException;

    /**
     * Method to load all statements in cache
     * @param inputStream
     * @return
     * @throws ParsingException
     */
    StatementReader loadStatements(InputStream inputStream) throws ParsingException;
}