/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc;

import java.sql.CallableStatement;
import java.sql.SQLException;

/**
 * This class helps to struct a call to a function.
 *
 * @author Miguel Vega
 * @version $Id: FunctionParam.java 0, 2013-03-01 5:48 PM mvega $
 */
public abstract class FunctionParam<T> {

    protected T model;

    /**
     * Creates a new instance of a IN parameter for a function
     *
     * @param in the value to add into the call
     * @return
     */
    public static <V> FunctionParam<V> IN(final V in) {
        return new FunctionParam<V>() {
            {
                this.model = in;
            }

            @Override
            public void onStatementPopulation(CallableStatement callableStatement, int place) throws SQLException {
                callableStatement.setObject(place, in);
            }

            @Override
            public void onStatementExecuted(CallableStatement callableStatement, int place) {
                //dummy
            }
        };
    }

    /**
     * Creates a new instance of a OUT parameter for a function
     *
     * @param out
     * @return
     */
    public static <V> FunctionParam<Out<V>> OUT(final Out<V> out) {
        return new FunctionParam<Out<V>>() {
            {
                this.model = out;
            }

            @Override
            public void onStatementPopulation(CallableStatement callableStatement, int place) throws SQLException {
                callableStatement.registerOutParameter(place, out.sqlType);
            }

            @Override
            public void onStatementExecuted(CallableStatement callableStatement, int place) throws SQLException {
                Object obj = callableStatement.getObject(place);
                getModel().value = (V) obj;
            }
        };
    }

    /**
     * Creates a new instance of a INOUT parameter for a function
     *
     * @param inOut this parameter will hold the output type and the input value
     * @param <V>
     * @return
     */
    public static <V> FunctionParam<InOut<V>> INOUT(final InOut<V> inOut) {
        return new FunctionParam<InOut<V>>() {
            {
                this.model = inOut;
            }

            @Override
            public void onStatementPopulation(CallableStatement callableStatement, int place) throws SQLException {
                callableStatement.setObject(place, inOut.value);
                callableStatement.registerOutParameter(place, inOut.sqlType);
            }

            @Override
            public void onStatementExecuted(CallableStatement callableStatement, int place) throws SQLException {
                getModel().value = (V) callableStatement.getObject(place);
            }
        };
    }

    /**
     * Handles the given data to place it into callableStatement in the correct manner.
     *
     * @param callableStatement
     * @param place
     * @throws SQLException
     */
    public abstract void onStatementPopulation(CallableStatement callableStatement, int place) throws SQLException;

    /**
     * This method will be called after the call to the procedure has been done.
     *
     * @param callableStatement
     * @param place
     */
    public abstract void onStatementExecuted(CallableStatement callableStatement, int place) throws SQLException;

    public T getModel() {
        return model;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getName());
        sb.append("{Object=");
        sb.append(getModel());
        sb.append("}");
        return sb.toString();
    }

    /**
     * @param <V>
     */
    public static class Out<V> {
        protected int sqlType;
        protected V value;

        public Out(int sqlType) {
            this.sqlType = sqlType;
        }

        public V getValue() {
            return value;
        }

        @Override
        public String toString() {
            StringBuilder s = new StringBuilder(getClass().getName());
            s.append("{");
            s.append("SQLTYPE=");
            s.append(sqlType);
            s.append(", ");
            s.append("VALUE=");
            s.append(value);
            s.append("}");
            return s.toString();
        }

    }

    /**
     * An atomic class useful to onStatementPopulation the IN/OUT parameters
     *
     * @param <U>
     */
    public static class InOut<U> extends Out<U> {

        /**
         * @param sqlType this variable must be one of the provided by: {@link java.sql.Types}.
         * @param value   the output
         */
        public InOut(int sqlType, U value) {
            super(sqlType);
            this.value = value;
        }
    }
}