/*
 *     gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.openlogics.gears.jdbc.handler.JPAEntityHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: DataStore.java 0, 2012-10-05 01:16 mvega $
 */
public abstract class DataStore {
    protected Logger logger;
    private Connection connection;
    private boolean autoClose = true;
    private boolean autoCommit = true;
    //when true, avoids to query parameter metadata from functions or stored procedures
    protected boolean parameterMetaDataKnownBroken = false;
    /*
     * transactionIsolation, allows to define the isolation level for the transaction,
     * useful when need to lock rows from database.
     */
    private int transactionIsolation = -1;
    private String schema;

    DataStore() {
        this(false);
    }

    /**
     * @param parameterMetaDataKnownBroken, if {@link java.sql.CallableStatement#getParameterMetaData()} or {@link java.sql.PreparedStatement#getParameterMetaData()}
     *                                      generates any problem, need to define this property as true. At least PostgreSQL driver 9 v4. produces errors an error
     *                                      attempting to use this method.
     */
    DataStore(boolean parameterMetaDataKnownBroken) {
        this.parameterMetaDataKnownBroken = parameterMetaDataKnownBroken;
        logger = LoggerFactory.getLogger(getClass());
    }

    /**
     * @param query
     * @return
     * @throws SQLException
     */
    public int[] update(BatchQuery query) throws SQLException {
        try {
            return query.getPreparedStatement().executeBatch();
        } finally {
            closeDataStoreConnection();
            query.clearCache();
        }
    }

    /**
     * Executes the given statement
     *
     * @param query
     */
    public int update(Query query) throws SQLException {
        return update(query, getQueryRunner());
    }

    /**
     * This method has been created to allow customization in results handling
     * @param query the query to execute
     * @param queryRunner query runner to execute query
     * @return
     * @throws SQLException
     */
    public int update(Query query, QueryRunner queryRunner) throws SQLException {
        List p = Lists.newLinkedList();
        String queryString = query.evaluateQueryString(this, p);
        try {
            return update(queryString, p, queryRunner);
        } finally {
            p.clear();
        }
    }

    /**
     * @param query
     * @param handler
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> T select(Query query, ResultSetHandler<? extends T> handler) throws SQLException {
        return select(query, handler, getQueryRunner());
    }

    /**
     * Allows customization in results handling
     * @param query
     * @param handler
     * @param queryRunner
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> T select(Query query, ResultSetHandler<? extends T> handler, QueryRunner queryRunner) throws SQLException {
        //a simple list to hold data about select
        List params = Lists.newLinkedList();
        try {
            String queryString = query.evaluateQueryString(this, params);
            return select(queryString, handler, params, queryRunner);
        } finally {
            params.clear();
        }
    }

    /**
     * Performs a query and handler will receive parsed objects in its {@link ObjectResultVisitor#visit} method
     *
     * @param query
     * @param resultType
     * @param handler
     */
    public <T> void select(Query query, Class<?> resultType, ObjectResultVisitor<? extends T> handler) throws SQLException {
        select(query, resultType, handler, getQueryRunner());
    }

    /**
     * Allows customizatio in results handling
     * @param query
     * @param resultType
     * @param handler
     * @param queryRunner
     * @param <T>
     * @throws SQLException
     */
    public <T> void select(Query query, Class<?> resultType, ObjectResultVisitor<? extends T> handler, QueryRunner queryRunner) throws SQLException {
        List params = Lists.newLinkedList();

        String queryString = query.evaluateQueryString(this, params);

        try {
            JPAEntityHandler toJPAEntityHandler = new JPAEntityHandler(handler, resultType);
            select(queryString, toJPAEntityHandler, params, queryRunner);
        } finally {
            params.clear();
        }
    }

    /**
     * Performs a query and the results as instances of type parameter will be returned into a {@link List}
     *
     * @param query
     * @param type
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> List<T> select(Query query, Class<T> type) throws SQLException {
        return select(query, type, getQueryRunner());
    }

    public <T> List<T> select(Query query, Class<T> type, QueryRunner queryRunner) throws SQLException {
        List params = Lists.newLinkedList();
        final ImmutableList.Builder<T> builder = new ImmutableList.Builder<T>();
        ObjectResultVisitor<T> handler = new ObjectResultVisitor<T>() {
            @Override
            public void visit(T result) throws SQLException {
                builder.add(result);
            }
        };
        JPAEntityHandler<T> toJPAEntityHandler = new JPAEntityHandler<T>(handler, type);
        String queryString = query.evaluateQueryString(this, params);
        try {
            select(queryString, toJPAEntityHandler, params, queryRunner);
            return builder.build();
        } finally {
            params.clear();
        }
    }

    /**
     * Executes the given statement using the {@link org.apache.commons.dbutils.QueryRunner} class
     *
     * @param query
     * @param handler
     * @param data
     * @param <E>
     * @return
     * @throws SQLException
     */
    private <E> E select(String query, ResultSetHandler<E> handler, List data, QueryRunner queryRunner) throws SQLException {

        logger.debug("Attempting to make a query: SQL=" + query + ", PARAMS=" + data);

        try {
            //ExtQueryRunner qr = new ExtQueryRunner(parameterMetaDataKnownBroken).setStatementFactoryStrategy(statementFactoryStrategy);
            //QueryRunner qr = getQueryRunner();

            return queryRunner.query(getConnection(), query, handler, data.toArray());
        } finally {
            closeDataStoreConnection();
        }
    }

    /**
     * Executes the given statement using the {@link org.apache.commons.dbutils.QueryRunner} class
     *
     * @param query
     * @param data
     * @return
     * @throws SQLException
     */
    private int update(String query, List data, QueryRunner queryRunner) throws SQLException {

        logger.debug("Attempting to perform an execution: SQL=" + query + ", PARAMS=" + data);

        try {
            //ExtQueryRunner qr = new ExtQueryRunner(parameterMetaDataKnownBroken).setStatementFactoryStrategy(statementFactoryStrategy);
            return queryRunner.update(getConnection(), query, data.toArray());
        } finally {
            closeDataStoreConnection();
        }
    }

    protected QueryRunner getQueryRunner() {
        return new QueryRunner(parameterMetaDataKnownBroken);
    }

    /**
     * This method will executes any call, with IN/OUT parameters
     *
     * @param query, where each parameter must be an implementation of {@link FunctionParam} object
     */
    public boolean call(Query<FunctionParam[]> query) throws SQLException {
        try {
            List p = Lists.newLinkedList();
            String queryString = query.evaluateQueryString(this, p);

            //toSQLString the callable
            CallableQueryRunner callableQueryRunner = new CallableQueryRunner(parameterMetaDataKnownBroken);
            return callableQueryRunner.call(getConnection(), false, queryString, query.context);
        } finally {
            closeDataStoreConnection();
        }
    }

    /**
     * The very basic connection to access the whole
     *
     * @return database connection
     */
    protected abstract Connection acquireConnection() throws SQLException;

    /**
     * This method will performs the commit if and only if the AUTOCOMMIT is true, and will close
     * the connection only if AUTOCLOSE is true. However either of two options will be executed if possible.
     *
     * @throws SQLException
     */
    public void tryCommitAndClose() throws SQLException {
        if (autoCommit && autoClose) {
            DbUtils.commitAndClose(connection);
        } else if (autoCommit) {
            commit();
        } else if (autoClose) {
            DbUtils.close(connection);
        }
    }

    /**
     * @throws SQLException
     */
    public void commit() throws SQLException {
        if (connection != null) {
            connection.commit();
        }
    }

    /**
     * rollback all changes
     *
     * @throws SQLException
     */
    public void rollBack() throws SQLException {
        if (connection != null) {
            connection.rollback();
        }
    }

    /**
     * @throws SQLException
     */
    public void rollBackAndClose() throws SQLException {
        DbUtils.rollbackAndClose(connection);
    }

    /**
     * This method was created for executing many transactions using a common connection, avoiding unnecessary connection openings.
     * DO NOT forget to close the connection when all processes ended.
     *
     * @return database connection
     */
    public Connection getConnection() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            return connection;
        } else {

            forceCloseConnection();
            this.connection = acquireConnection();

            if (transactionIsolation != -1) {
                connection.setTransactionIsolation(transactionIsolation);
                DatabaseMetaData dbmd = connection.getMetaData();
                logger.debug("Attempting to use a TRANSACTION ISOLATION, '" + transactionIsolation + "', connection " +
                        (dbmd.supportsTransactionIsolationLevel(transactionIsolation) ? "does NOT " : " does ") + "support Isolation Level.");
            }
        }

        connection.setAutoCommit(autoCommit);

        return connection;
    }

    /**
     * This method is called only whne a connection has been auto-close disabled or any major error has been found.
     *
     * @throws SQLException
     */
    public void forceCloseConnection() throws SQLException {
        DbUtils.close(connection);
        this.connection = null;
    }

    /**
     * This method is always called after any transaction is executed.
     *
     * @throws SQLException
     */
    private void closeDataStoreConnection() throws SQLException {
        if (autoClose) {
            forceCloseConnection();
        }
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public boolean isAutoClose() {
        return autoClose;
    }

    public synchronized void setAutoClose(boolean autoClose) {
        logger.debug("Attempting to modify the connection AUTO CLOSE type to: " + autoClose);
        this.autoClose = autoClose;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    /**
     * This automatically disables the auto-commit and auto-closeable (if FALSE) features of the connection.
     * <br/><code>Warning!, if disable, be sure to close the connection when finishing.</code>
     *
     * @param autoCommit TRUE if commit per transaction is allowed, else disable the autocommit
     */
    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
        //if autocommit is false, is necessary that connection auto close becomes false
        this.autoClose = autoCommit == false ? false : autoClose;

        logger.debug("Attempting to modify the connection AUTO COMMIT type to: " + autoCommit + ". This causes that auto close is "+(autoClose?"enabled":"disabled"));
    }

    public int getTransactionIsolation() {
        return transactionIsolation;
    }

    public void setTransactionIsolation(int transactionIsolation) {
        this.transactionIsolation = transactionIsolation;
    }

    public void setTransactionIsolationNone() {
        this.transactionIsolation = -1;
    }
}