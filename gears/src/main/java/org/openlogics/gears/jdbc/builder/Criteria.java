package org.openlogics.gears.jdbc.builder;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Immutable class which returns the SQL String decoded and their respective values in the relative order.
 *
 * @author Miguel Vega
 * @version $Id: Criteria.java 0, 2013-10-30 12:32 AM mvega $
 */
public class Criteria {
    private final String sql;
    private final List values;

    protected Criteria(String sql, List values) {
        this.values = values;
        this.sql = sql;
    }

    protected Criteria(String sql, Object... values) {
        this.values = Arrays.asList(values);
        this.sql = sql;
    }

    public static Criteria empty() {
        return new Criteria("");
    }

    /**
     * @return the SQL string
     */
    public String getQueryString() {
        return sql;
    }

    /**
     * @return a list of values for each parameter provided
     */
    public List getQueryValues() {
        return values;
    }

    @Override
    public String toString() {
        return getQueryString();
    }

    public boolean hasAnyNull() {
        try {
            Object o = Iterables.find(values, new Predicate() {
                @Override
                public boolean apply(@Nullable Object input) {
                    return input == null;
                }
            });
            return true;
        } catch (NoSuchElementException x) {
            return false;
        }
    }
}