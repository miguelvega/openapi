package org.openlogics.gears.jdbc;

import org.apache.commons.dbutils.ResultSetHandler;
import org.openlogics.gears.jdbc.apache.ExtQueryRunner;
import org.openlogics.gears.jdbc.builder.Filter;
import org.openlogics.gears.jdbc.builder.ObjectQueryBuilder;
import org.openlogics.gears.jdbc.builder.SQLClause;
import org.openlogics.gears.jdbc.handler.JPAEntityHandler;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.collect.Lists.newLinkedList;
import static org.openlogics.utils.reflect.Reflections.getGetterMethodName;
import static org.openlogics.utils.reflect.Reflections.getSetterMethod;

/**
 * Created with IntelliJ IDEA.
 * User: miguel
 * Date: 12/6/13
 * Time: 1:12 AM
 */
public abstract class ObjectDataStore extends DataStore {

    /**
     * A simple method for easier insert
     *
     * @param t
     * @param <T>
     * @return the provided object with it's primary key assigned if and only if Driver supports and the table has an
     * autogenerated PK column
     * @throws SQLException
     */
    public <T> T add(final T t) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildPreparedInsert(t, getSchema());

        int update = update(query,
                new ExtQueryRunner(parameterMetaDataKnownBroken, new ObjectResultVisitor() {
                    @Override
                    public void visit(Object obj) throws SQLException {
                        //the PK will be received here
                        try {
                            Field idColumn = qb.getIDColumn(t.getClass());
                            try {
                                Method setterMethod = getSetterMethod(idColumn);

                                //todo, enhance this implementation of convert automatically BigDecimal to the required value

                                if(idColumn.getAnnotation(Convert.class)!=null){
                                    Class<? extends AttributeConverter> attrConvType = idColumn.getAnnotation(Convert.class).converter();
                                    if (attrConvType != null && AttributeConverter.class.isAssignableFrom(attrConvType)) {
                                        try {
                                            AttributeConverter typeMapper = attrConvType.newInstance();

                                            setterMethod.invoke(t, typeMapper.convertToEntityAttribute(obj));
                                        } catch (InstantiationException e) {
                                            throw new IllegalStateException("Unable to call getterMethod", e);
                                        }
                                    }
                                }else{
                                    if(obj.getClass().isAssignableFrom(BigDecimal.class)){
                                        if(setterMethod.getParameterTypes()[0]==Integer.class){
                                            setterMethod.invoke(t, ((BigDecimal)obj).intValue());
                                            return;
                                        }else if(setterMethod.getParameterTypes()[0]==Long.class){
                                            setterMethod.invoke(t, ((BigDecimal)obj).longValue());
                                            return;
                                        }else if(setterMethod.getParameterTypes()[0]==Double.class){
                                            setterMethod.invoke(t, ((BigDecimal)obj).doubleValue());
                                            return;
                                        }else if(setterMethod.getParameterTypes()[0]==Float.class){
                                            setterMethod.invoke(t, ((BigDecimal)obj).floatValue());
                                            return;
                                        }
                                    }

                                    setterMethod.invoke(t, obj);

                                }
                            } catch (IntrospectionException e) {
                                logger.debug("Unable to obtain a valid SETTER method: " + t.getClass().getName() + "." + getGetterMethodName(idColumn.getName(), idColumn.getType()));
                            } catch (InvocationTargetException e) {
                                logger.debug("Unable to find a valid SETTER method: " + t.getClass().getName() + "." + getGetterMethodName(idColumn.getName(), idColumn.getType()));
                            } catch (IllegalAccessException e) {
                                logger.debug("Unable to access SETTER method: " + t.getClass().getName() + "." + getGetterMethodName(idColumn.getName(), idColumn.getType()));
                            }
                        } catch (NoSuchElementException x) {
                            //nothing to update
                            logger.debug(x.getMessage());
                        }
                    }
                }).setStatementFactoryStrategy(ExtQueryRunner.StatementFactoryStrategy.RETURN_GENERATED_KEYS));

        if (update != 1) {
            logger.debug("The insertion returned " + update + " (rows affected) where 1 was expecting.");
        }

        return t;
    }

    /**
     * Updates given record, using the columns annotated with {@link javax.persistence.Id} to create a default Filter.
     * @param t
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> T modify(final T t) throws SQLException {
        assert t != null;

        ObjectQueryBuilder<T> oqb = new ObjectQueryBuilder<T>();
        Filter filter = oqb.buildFilterForId(t);

        return modify(t, filter);
    }

    /**
     *
     * @param t
     * @param filter
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> T modify(final T t, Filter filter) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildPreparedUpdate(t, getSchema());

        int update = update(query.setFilter(filter));

        if (update != 1) {
            logger.debug("The insertion returned " + update + " (rows affected) where 1 was expecting.");
        }

        return t;
    }

    /**
     * removes given object using the {@link javax.persistence.Id} annotation
     * @param t
     * @param <T>
     * @return
     * @throws SQLException
     */
    public <T> T remove(final T t) throws SQLException {
        assert t != null;

        ObjectQueryBuilder<T> oqb = new ObjectQueryBuilder<T>();

        return remove(t, oqb.buildFilterForId(t));
    }

    public <T> T remove(final T t, Filter filter) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildPreparedDelete(t, getSchema());

        int update = update(query.setFilter(filter));

        if (update != 1) {
            logger.debug("The deletion returned " + update + " (rows affected) where 1 was expecting.");
        }

        return t;
    }

    public <T> List<T> getResultList(final Class<T> t) throws SQLException {
        return getResultList(t, null);
    }

    public <T> List<T> getResultList(final Class<T> t, Filter filter) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildSimpleSelect(t, getSchema());

        final List<T> results = newLinkedList();

        select(query.setFilter(filter), new JPAEntityHandler<T>(
                new ObjectResultVisitor<T>() {
                    @Override
                    public void visit(T obj) throws SQLException {
                        results.add(obj);
                    }
                }, t));

        return results;
    }

    public <T> List<T> getResultList(final Class<T> t, Filter filter, SQLClause limitOffset) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildSimpleSelect(t, getSchema()).limitOffset(limitOffset);

        final List<T> results = newLinkedList();

        select(query.setFilter(filter), new JPAEntityHandler<T>(
                new ObjectResultVisitor<T>() {
                    @Override
                    public void visit(T obj) throws SQLException {
                        results.add(obj);
                    }
                }, t));

        return results;
    }

    public <T> T getSingleResult(final Class<T> t, Filter filter) throws SQLException {
        //first than all, need to define prepared statement allowed to return last PK inserted

        final ObjectQueryBuilder<T> qb = new ObjectQueryBuilder<T>();

        Query query = qb.buildSimpleSelect(t, getSchema());

        return select(query.setFilter(filter), new ResultSetHandler<T>() {
            @Override
            public T handle(ResultSet rs) throws SQLException {
                JPAEntityHandler<T> brh = JPAEntityHandler.getSimpleBeanMapper(t);
                if(rs.next()){
                    return brh.mapResultSet(rs);
                }
                return null;
            }
        });
    }
}