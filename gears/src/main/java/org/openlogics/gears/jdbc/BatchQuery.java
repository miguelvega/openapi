package org.openlogics.gears.jdbc;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.DbUtils;
import org.openlogics.gears.jdbc.builder.Filter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Used for executing batch statements.
 * If already using Guice for injection, take a look to the @{GuicedBatchQuery} to take advantage of injection.
 *
 * @author Miguel Vega
 * @version $Id: BatchQuery.java 0, 2012-11-21 20:36 mvega $
 */
public class BatchQuery<E> extends Query<E> {
    private PreparedStatement preparedStatement;

    public BatchQuery(String queryString) {
        this(queryString, null);
    }

    public BatchQuery(String queryString, Filter filter) {
        super(queryString, null, filter);
    }

    public <E> BatchQuery addBatch(DataStore dataStore, E context) throws SQLException {
        assert context != null;
        List list = Lists.newLinkedList();

        String plainQuery = Query.of(queryString, context).evaluateQueryString(dataStore, list);
        if (preparedStatement == null) {
            preparedStatement = newPreparedStatement(plainQuery, list, dataStore);
        } else {

            preparedStatement = populatePreparedStatement(preparedStatement, list);
        }
        preparedStatement.addBatch();
        return this;
    }

    /**
     * Creates a new {@link PreparedStatement} object
     *
     * @param preparedSql
     * @param params
     * @return
     * @throws SQLException
     */
    protected PreparedStatement newPreparedStatement(String preparedSql, List params, DataStore dataStore) throws SQLException {
        Connection conn = dataStore.getConnection();
        PreparedStatement preparedSt = conn.prepareStatement(preparedSql);
        return populatePreparedStatement(preparedSt, params);
    }


    protected PreparedStatement populatePreparedStatement(PreparedStatement preparedStatement, List params) throws SQLException {

        //preparedStatement.clearParameters();
        for (int i = 0; i < params.size(); i++) {
            Object object = params.get(i);
            preparedStatement.setObject(i + 1, object);
        }
        return preparedStatement;
    }

    protected PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    /**
     * Executes the batch, and clears cache to prevent from memory leaks
     */
    public void clearCache() throws SQLException {
        DbUtils.close(preparedStatement);
        this.preparedStatement = null;
    }

    @Override
    protected String evaluateQueryString(DataStore dataStore, List data) {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}