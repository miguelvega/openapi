package org.openlogics.gears.jdbc;

import org.junit.Test;
import org.openlogics.gears.jdbc.builder.Filter;
import pojo.Foo;

import javax.persistence.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.openlogics.gears.jdbc.builder.Filter.equal;

/**
 * @author Miguel Vega
 * @version $Id: ObjectDataStoreTest.java 0, 12/11/13 12:29 AM miguel $
 */
public class ObjectDataStoreTest extends TestStub{

    @Test
    public void testDoInsert() throws Exception {
        ObjectDataStore ds = DataStoreFactory.createObjectDataStore(basicDataSource);

        Foo foo = new Foo();
        foo.setFname("Miguel");
        foo.setLname("Vega");
        foo.setRate(70.5f);
        foo.setAddDate(new Timestamp(System.currentTimeMillis()));

        foo = ds.add(foo);

        logger.info("INSERTED>"+foo);
    }

    @Test
    public void testDoUpdate() throws Exception {
        ObjectDataStore ds = DataStoreFactory.createObjectDataStore(basicDataSource);

        Filter filter = equal("FOO_ID", "1");

        Foo foo = ds.getSingleResult(Foo.class, filter);
        logger.info(" > > > > > "+foo);

        foo = new Foo();
        foo.setAddDate(new Date());
        foo.setFname("Miguel");
        foo.setLname("Vega Updated");

        foo = ds.modify(foo, filter);

        logger.info("MODIFIED>"+foo);
    }

    @Test
    public void testSelection(){
        ObjectDataStore ds = DataStoreFactory.createObjectDataStore(basicDataSource);

        try {
            List<Foo> list = ds.getResultList(Foo.class);

            for (Foo foo : list) {
                System.out.println("............................................................"+foo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
