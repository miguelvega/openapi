package org.openlogics.gears.jdbc.oracle;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.gears.jdbc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Map;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.openlogics.gears.jdbc.FunctionParam.IN;
import static org.openlogics.gears.jdbc.FunctionParam.OUT;
import static org.openlogics.gears.jdbc.Query.of;

/**
 * @author Miguel Vega
 * @version $Id: OracleTest.java 0, 2013-04-17 7:22 PM mvega $
 */
public class OracleTest {
    Logger LOGGER = LoggerFactory.getLogger(OracleTest.class);

    public void showAll(String a[]) {
        for (String s : a) {
            System.out.println(s);
        }
    }

    public void showAllNew(String... a) {
        for (String s : a) {
            System.out.println(s);
        }
    }

    @Test
    public void print() {
        showAll(new String[]{"a", "b", "c"});
        System.out.println("----------------");
        showAllNew("A", "B", "C");
        System.out.println("----------------");
        showAllNew();
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testDateRetriever() {
        try {
            Properties properties = new Properties();
            properties.put("driver", "oracle.jdbc.OracleDriver");
            properties.put("user", "tigo");
            properties.put("password", "tigo");
            String url = "jdbc:oracle:thin:@192.168.4.9:1521:DESA";
            properties.put("url", url);

            DataStore dataStore = DataStoreFactory.createDataStore(url, properties);

            // ADD:
            Map<String, Object> map = dataStore.select(
                    of("SELECT * FROM BC_USSD_USUARIOS WHERE USR_LOGIN=? AND USR_FECHA_BAJA IS NULL", "admin"),
                    new MapHandler());

            System.out.println("RESULT: " + map + ", " + map.get("USR_FECHA_ALTA").getClass());

            Foo foo = dataStore.select(
                    of("SELECT * FROM BC_USSD_USUARIOS WHERE USR_LOGIN=? AND USR_FECHA_BAJA IS NULL", "admin"),
                    Foo.class).get(0);

            System.out.println("FOO=" + foo.getLogin() + ", " + foo.getAddDate());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testInsertDate() {
        try {
            Properties properties = new Properties();
            properties.put("driver", "oracle.jdbc.OracleDriver");
            properties.put("user", "tigo");
            properties.put("password", "tigo");
            String url = "jdbc:oracle:thin:@192.168.4.9:1521:DESA";
            properties.put("url", url);

            DataStore dataStore = DataStoreFactory.createDataStore(url, properties);

            Foo foo = new Foo();
            foo.setAddDate(new java.util.Date());
            foo.setId(8584);
            foo.setLogin("Miguelon!");

            int update = dataStore.update(
                    of("INSERT INTO BC_USSD_USUARIOS (USR_ID, USR_LOGIN, USR_FECHA_ALTA)" +
                            "VALUES" +
                            "(#{id}, #{login}, #{addDate})", foo));
            assertEquals(1, update);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testPlainFunction() {
        try {
            Properties properties = new Properties();
            properties.put("driver", "oracle.jdbc.OracleDriver");
            properties.put("user", "oracle");
            properties.put("password", "oracle");
            String url = "jdbc:oracle:thin:@192.168.127.128:1521:xe";
            properties.put("url", url);

            DataStore dataStore = DataStoreFactory.createDataStore(url, properties);

            // ADD:
            java.sql.CallableStatement cstmt = dataStore.getConnection().prepareCall("{?=call sumthemall (?, ?, ?)}");

            cstmt.registerOutParameter(1, Types.DECIMAL);
            cstmt.setLong(2, 2L);
            cstmt.setLong(3, 1L);
            cstmt.setLong(4, 33);
            cstmt.execute();
            Long res = cstmt.getLong(1);
            System.out.println("Sum: " + res);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testPlainProcedure() {
        try {
            Properties properties = new Properties();
            properties.put("driver", "oracle.jdbc.OracleDriver");
            properties.put("user", "oracle");
            properties.put("password", "oracle");
            String url = "jdbc:oracle:thin:@192.168.127.128:1521:xe";
            properties.put("url", url);

            DataStore dataStore = DataStoreFactory.createDataStore(url, properties);

            // ADD:
            java.sql.CallableStatement cstmt = dataStore.getConnection().
                    prepareCall("{call procOneINOneOUTParameter(?, ?)}");

            cstmt.setString(1, "Miguel Vega");
            cstmt.registerOutParameter(2, Types.VARCHAR);
            cstmt.execute();
            String res = cstmt.getString(2);
            System.out.println("Greetings: " + res);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private BasicDataSource getBasicDataSource() {
        BasicDataSource bds = new BasicDataSource();
        bds.setUrl("jdbc:oracle:thin:@192.168.127.128:1521:xe");
        bds.setDriverClassName("oracle.jdbc.OracleDriver");
        bds.setUsername("oracle");
        bds.setPassword("oracle");
        bds.setMaxIdle(10);
        return bds;
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testSumThemAllAsQuery() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource());

        BigDecimal sum = ds.select(of("select sumthemall (?, ?, ?) from dual", 1, 2, 3), new ScalarHandler<BigDecimal>(1));

        assertEquals(BigDecimal.valueOf(6), sum);
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testSumThemAll() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource());

        FunctionParam<FunctionParam.Out<BigDecimal>> out = OUT(new FunctionParam.Out<BigDecimal>(Types.DECIMAL));

        boolean res = ds.call(Query.<FunctionParam>of("{?=call sumthemall (?, ?, ?)}", out, IN(50f), IN(60f), IN(70f)));

        assertFalse(res);
        assertEquals(BigDecimal.valueOf(180), out.getModel().getValue());
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testPlainManyOutParameters() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource());

        Connection dbConnection = ds.getConnection();

        //getDBUSERByUserId is a stored procedure
        String getDBUSERByUserIdSql = "{call GETFOOBYID(?,?,?,?)}";
        CallableStatement callableStatement = dbConnection.prepareCall(getDBUSERByUserIdSql);
        callableStatement.setInt(1, 6);
        callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
        callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
        callableStatement.registerOutParameter(4, java.sql.Types.DATE);

        // execute store procedure
        callableStatement.executeUpdate();

        String fname = callableStatement.getString(2);
        String lname = callableStatement.getString(3);
        Date createdDate = callableStatement.getDate(4);

        assertEquals("miguel", fname);
        assertEquals("vega", lname);
        LOGGER.info("Date retrieved for given call = " + createdDate);
    }

    @Test
    @Ignore("Execute only when you ensure that ORACLE is properly configured with script provided")
    public void testManyOutParameters() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource());

        FunctionParam<FunctionParam.Out<String>> fname = OUT(new FunctionParam.Out<String>(Types.VARCHAR));
        FunctionParam<FunctionParam.Out<String>> lname = OUT(new FunctionParam.Out<String>(Types.VARCHAR));
        FunctionParam<FunctionParam.Out<Timestamp>> date = OUT(new FunctionParam.Out<Timestamp>(Types.TIMESTAMP));

        boolean res = ds.call(Query.<FunctionParam>of("{call GETFOOBYID(?,?,?,?)}", IN(6), fname, lname, date));

        assertFalse(res);
        assertEquals("miguel", fname.getModel().getValue());
        assertEquals("vega", lname.getModel().getValue());
        LOGGER.info("Date retrieved for given call = " + date.getModel().getValue());
    }

    @Test
    @Ignore
    public void testPlainAdd() {
        try {
            Properties prop = new Properties();
            prop.put("driver", "oracle.jdbc.OracleDriver");
            prop.put("user", "app");
            prop.put("password", "app2013");
            String url = "jdbc:oracle:thin:@192.168.50.6:1521:orcl";
            prop.put("url", url);
            DataStore ds = DataStoreFactory.createDataStore(url, prop);

            // ADD:
            java.sql.CallableStatement cstmt = ds.getConnection().
                    prepareCall("{? = call expendedorAdd(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");

            cstmt.registerOutParameter(1, Types.BIGINT);

            cstmt.setObject(2, "E");
            cstmt.setObject(3, "E");
            cstmt.setObject(4, "192.168.50.99");
            cstmt.setObject(5, "Desc");
            cstmt.setObject(6, "Dir");
            cstmt.setObject(7, 201L);
            cstmt.setObject(8, 1201L);
            cstmt.setObject(9, 1L);
            cstmt.setObject(10, "2013/07/16 00:00:00");
            cstmt.setObject(11, BigDecimal.ONE);
            cstmt.setObject(12, BigDecimal.TEN);
            cstmt.setObject(13, "S1");
            cstmt.setObject(14, "S2");
            cstmt.setObject(15, "DVIOREL");
            cstmt.setObject(16, "DVIOREL");
            cstmt.setObject(17, BigDecimal.TEN);

            cstmt.execute();
            Long res = cstmt.getLong(1);
            System.out.println("ID: " + res);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore("Danny's problem")
    public void testAddFunct() throws SQLException {
        Properties prop = new Properties();
        prop.put("driver", "oracle.jdbc.OracleDriver");
        prop.put("user", "app");
        prop.put("password", "app2013");
        String url = "jdbc:oracle:thin:@192.168.50.6:1521:orcl";
        prop.put("url", url);
        DataStore ds = DataStoreFactory.createDataStore(url, prop);

        FunctionParam<FunctionParam.Out<Long>> out =
                FunctionParam.OUT(new FunctionParam.Out<Long>(Types.BIGINT));

        ds.call(Query.<FunctionParam>of("{? = call expendedorAdd(?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}",
                out,
                IN("E"),
                IN("E"),
                IN("192.168.50.100"),
                IN("Desc"),
                IN("Dir"),
                IN(201L),
                IN(1201L),
                IN(1L),
                IN("2013/07/16 00:00:00"),
                IN(BigDecimal.ONE),
                IN(BigDecimal.TEN),
                IN("S1"),
                IN("S2"),
                IN("DVIOREL"),
                IN("DVIOREL"),
                IN(BigDecimal.TEN)
        ));
        System.out.println(">>>>>>" + out.getModel().getValue());
    }
}
