/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc.sqlserver;

import org.apache.commons.dbutils.handlers.MapListHandler;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Miguel Vega
 * @version $Id: SQLServerTest.java 0, 2013-02-15 2:38 PM mvega $
 */
public class SQLServerTest {
    @Test
    @Ignore("Execute only when you ensure that SQLSERVER is properly configured with script provided")
    public void testConnection() throws SQLException {
        Properties p = new Properties();
        p.setProperty("driver", "net.sourceforge.jtds.jdbc.Driver");
        p.setProperty("user", "net.sourceforge.jtds.jdbc.Driver");
        p.setProperty("password", "net.sourceforge.jtds.jdbc.Driver");
        //p.setProperty("url", "jdbc:jtds:sqlserver://172.16.231.129;instance=SQLEXPRESS;DatabaseName=goxpend");
        String url = "jdbc:jtds:sqlserver://172.16.231.129:1433/goxpend";
        p.setProperty("url", url);

        DataStore ds = DataStoreFactory.createDataStore(url, p);

        int i = 1;
        List<Map<String, Object>> results = ds.select(Query.of("select * from ciudad"), new MapListHandler());
        for (Map<String, Object> result : results) {
            System.out.println(i++ + " > " + result);
        }
    }


    @Test
    public void testPaginator() {
        int numpages = 37624;
        float coef = (float) numpages / (float) 30;
        System.out.println("A=" + coef);
        System.out.println("B=" + Math.ceil(coef));
        int pages = (int) Math.ceil(coef);
        System.out.println("NUm Pages=" + pages);

    }
}
