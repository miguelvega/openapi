package org.openlogics.gears.jdbc.builder;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;

/**
 * @author Miguel Vega
 * @version $Id: GroupTest.java 0, 2013-11-19 12:22 PM mvega $
 */
public class GroupTest {

    Logger LOGGER = LoggerFactory.getLogger(GroupTest.class);

    @Test
    public void testSimpleGroupBy() {
        SQLClause ob = SQLClause.group().
                by("name").
                by(1);

        assertEquals(" GROUP BY name, 1", ob.toSQLString());

        LOGGER.info("testSimpleGroupBy, OK");
    }

    @Test
    public void testEmptyGroupBy() {
        SQLClause ob = SQLClause.group();
        assertEquals("", ob.toSQLString());

        LOGGER.info("testEmptyGroupBy, OK");
    }

}
