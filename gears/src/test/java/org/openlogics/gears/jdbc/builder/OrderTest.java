package org.openlogics.gears.jdbc.builder;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.openlogics.gears.jdbc.builder.SQLClause.order;

/**
 * @author Miguel Vega
 * @version $Id: OrderTest.java 0, 2013-11-08 5:18 PM mvega $
 */
public class OrderTest {

    @Test
    public void testSimpleOrderBy() {
        SQLClause ob = order().
                by(new SortParam("name", true)).
                by(new SortParam("details", false));

        assertEquals(" ORDER BY name ASC, details DESC", ob.toSQLString());
    }

    @Test
    public void testEmptyOrderBy() {
        SQLClause ob = order();
        assertEquals("", ob.toSQLString());
    }
}
