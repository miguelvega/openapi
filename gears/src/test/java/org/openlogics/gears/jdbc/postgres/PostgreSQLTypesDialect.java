package org.openlogics.gears.jdbc.postgres;

import javax.persistence.AttributeConverter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: PostgreSQLTypesDialect.java 0, 1/29/14 12:24 PM, @miguel $
 */
public class PostgreSQLTypesDialect {
    public static class TimestampTypeMapper implements AttributeConverter<Timestamp, Date> {

        @Override
        public Date convertToDatabaseColumn(Timestamp attribute) {
            return new Date(attribute.getTime());
        }

        @Override
        public Timestamp convertToEntityAttribute(Date dbData) {
            return new Timestamp(dbData.getTime());
        }
    }
}