package org.openlogics.gears.jdbc;

import javax.persistence.AttributeConverter;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author Miguel Vega
 * @version $Id: DefaultTypeMapper.java; dic 02, 2014 05:03 PM mvega $
 */
public class DefaultTypeMapper {

    public class DateMapper implements AttributeConverter<java.util.Date, Date>{
        @Override
        public Date convertToDatabaseColumn(java.util.Date attribute) {
            return new Date(attribute.getTime());
        }

        @Override
        public java.util.Date convertToEntityAttribute(Date dbData) {
            return new java.util.Date(dbData.getTime());
        }
    }

    public class TimestampMapper implements AttributeConverter<java.util.Date, Timestamp> {

        public TimestampMapper() {
            super();
        }

        @Override
        public Timestamp convertToDatabaseColumn(java.util.Date attribute) {
            return new Timestamp(attribute.getTime());
        }

        @Override
        public java.util.Date convertToEntityAttribute(Timestamp dbData) {
            return new java.util.Date(dbData.getTime());
        }
    }
}
