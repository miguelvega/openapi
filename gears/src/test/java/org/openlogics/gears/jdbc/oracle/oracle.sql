CREATE TABLE foo
(
  foo_id          NUMBER(10) NOT NULL,
  foo_lname       VARCHAR2(30),
  foo_fname       VARCHAR2(30),
  foo_rating      NUMERIC(4, 1),
  foo_add_date    TIMESTAMP,
  "CaseSensitive" VARCHAR2(20),
  CONSTRAINT foo_pkey PRIMARY KEY (foo_id)
);
/
CREATE SEQUENCE foo_seq;
/
CREATE OR REPLACE TRIGGER foo_insertion
BEFORE INSERT ON foo
FOR EACH ROW

  BEGIN
    SELECT
      foo_seq.NEXTVAL
    INTO :new.foo_id
    FROM dual;
  END;
/
INSERT INTO foo (foo_fname, foo_lname, foo_rating, foo_add_date) VALUES ('miguel', 'vega', 50, sysdate);
INSERT INTO foo (foo_fname, foo_lname, foo_rating, foo_add_date) VALUES ('marcela', 'gomez', 60, sysdate);
INSERT INTO foo (foo_fname, foo_lname, foo_rating, foo_add_date) VALUES ('juan', 'perez', 70, sysdate);
INSERT INTO foo (foo_fname, foo_lname, foo_rating, foo_add_date) VALUES ('alejandro', 'petrus', 40, sysdate);
/

/*
Functions for testing purposes start here
*/
CREATE OR REPLACE PROCEDURE getFooById(
  p_id    IN  FOO.foo_id%TYPE,
  o_fname OUT foo.foo_fname%TYPE,
  o_lname OUT foo.foo_lname%TYPE,
  o_date  OUT foo.foo_add_date%TYPE)
IS
  BEGIN
    SELECT
      foo_fname,
      foo_lname,
      foo_add_date
    INTO o_fname, o_lname, o_date
    FROM foo
    WHERE foo_id = p_id;
  END;
/

--A very simple function which returns one result
CREATE OR REPLACE
FUNCTION sumthemall
  (
    value0 IN NUMBER,
    value1 IN NUMBER,
    value2 IN NUMBER
  )
  RETURN NUMBER
IS
  result NUMBER;
  BEGIN

    SELECT
      (value0 + value1 + value2)
    INTO result
    FROM dual;

    RETURN result;
  END;
/


/*
--Danny's function, used in testAddFunct mytest
create or replace FUNCTION ExpendedorAdd
  (
    codigo IN CHAR,
    nombre IN VARCHAR2,
    ip IN VARCHAR2,
    descripcion IN VARCHAR2,
    direccion IN VARCHAR2,
    idCiudad IN NUMBER,
    idCiudadSim IN NUMBER,
    usuariocreacion IN NUMBER,
    fechacreacion IN VARCHAR2,
    codAlmacen IN NUMBER,
    codArticulo IN NUMBER,
    planActivacionesNuevasSIM IN VARCHAR2,
    planReposicionSIM IN VARCHAR2,
    cajero IN VARCHAR2,
    usuarioAlmacen IN VARCHAR2,
    codigoPersona IN NUMBER
  )
  RETURN NUMBER
IS
  idnumber NUMBER;
  BEGIN

    INSERT INTO EXPENDEDOR(Codigo, Nombre, Ip, Descripcion, Direccion, IdCiudad, IdCiudadSim,
                           UsuarioCreacion, UsuarioEliminacion, FechaCreacion, FechaEliminacion, CodAlmacen, CodArticulo,
                           PlanActivacionesNuevasSIM, PlanReposicionSIM, Cajero, UsuarioAlmacen, CodigoPersona, Activo)
      VALUES (codigo, nombre, ip, descripcion, direccion, idCiudad,  idCiudadSim,
              usuarioCreacion, NULL, to_date(fechaCreacion, 'yyyy/MM/dd HH24:MI:SS'),
              NULL, codAlmacen, codArticulo, planActivacionesNuevasSIM, planReposicionSIM,
              cajero, usuarioAlmacen, codigoPersona, 1);

    SELECT SEQ_EXPENDEDOR_IdExpendedor.currval
    INTO idnumber
    FROM dual;

    RETURN idnumber;
  END;
  /
*/