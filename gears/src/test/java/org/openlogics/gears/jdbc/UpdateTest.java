/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.log4j.Logger;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import pojo.Foo;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;
import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ARNOLD
 * Date: 23-11-12
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 */

public class UpdateTest {
    protected IDatabaseTester databaseTester;
    protected BasicDataSource basicDataSource;

    protected Logger logger;

    @Before
    public void setup() {
        this.logger = Logger.getLogger(getClass());

        try {

            basicDataSource = new BasicDataSource();
            basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
            basicDataSource.setDriverClassName("org.h2.Driver");
            Connection connection = basicDataSource.getConnection();
            URL sql = getResource(TestStub.class, "foo.sql");
            try {
                connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                connection.close();

                URL resource = getResource(TestStub.class, "foo.xml");
                FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                databaseTester = new DataSourceDatabaseTester(basicDataSource);
                databaseTester.setDataSet(build);
                databaseTester.onSetup();
            } catch (SQLException x) {

            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    protected void viewAll(DataStore ds) throws SQLException {
        logger.info(System.getProperty("line.separator"));
        Query query = Query.of("select FOO_ID, " +
                "FOO_FNAME, " +
                "FOO_LNAME, " +
                "FOO_RATE as rate, " +
                "FOO_ADD_DATE from FOO");
        List<Foo> stds = ds.select(query, Foo.class);
        //List<Map<String, Object>> stds = ds.select(query, new MapListHandler());
        logger.info("************************************ VIEW ALL, BEGIN *****************************************************");
        for (Foo std : stds) {
            //for (Map<String, Object> std : stds) {
            logger.info("Result > " + std);
        }
        logger.info("************************************* VIEW ALL, END****************************************************");
        logger.info(System.getProperty("line.separator"));
    }

    @Test
    public void testSimpleUpdate() {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);
        try {
            int count = ds.update(Query.of("UPDATE FOO SET FOO_FNAME = 'ARNOLD' WHERE FOO_ID = #{parameter}", 5));

            assertEquals(1, count);

            Map<String, Object> result = ds.select(Query.of("SELECT * FROM FOO WHERE FOO_ID = ?", 5), new MapHandler());

            viewAll(ds);

            assertEquals(1, count);
            assertEquals("ARNOLD", result.get("FOO_FNAME"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimplePojoUpdate() {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);
        try {
            Foo foo = new Foo();
            foo.setId(5);
            foo.setFname("MAURICIO");
            foo.setLname("RAMIREZ");

            int count = ds.update(Query.of("UPDATE FOO SET FOO_FNAME = #{fname}, FOO_LNAME = #{lname} WHERE FOO_ID = #{id}", foo));
            assertEquals(1, count);

            Map<String, Object> res = ds.select(Query.of("SELECT * FROM FOO WHERE FOO_ID = #{id}", foo), new MapHandler());

            viewAll(ds);

            logger.info("count " + count);
            logger.info("listMap.size()->" + res.size());
            logger.info("get(\"FOO_FNAME\")->" + res.get("FOO_FNAME"));
            logger.info("get(\"FOO_LNAME\")->" + res.get("FOO_LNAME"));

            assertEquals("MAURICIO", res.get("FOO_FNAME"));
            assertEquals("RAMIREZ", res.get("FOO_LNAME"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleMapUpdate() {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", 5);
            map.put("fname", "MAURICIO");
            map.put("lname", "RAMIREZ");

            int count = ds.update(Query.of("UPDATE FOO SET FOO_FNAME = #{fname}, FOO_LNAME = #{lname} WHERE FOO_ID = #{id}", map));
            Map<String, Object> res = ds.select(Query.of("SELECT * FROM FOO WHERE FOO_ID = #{id}", map), new MapHandler());

            viewAll(ds);

            logger.info("count->" + count);
            logger.info("listMap.size()->" + res.size());
            logger.info("listMap.get(0).get(\"FOO_FNAME\")->" + res.get("FOO_FNAME"));
            logger.info("listMap.get(0).get(\"FOO_LNAME\")->" + res.get("FOO_LNAME"));

            assertEquals(1, count);
            assertEquals("MAURICIO", res.get("FOO_FNAME"));
            assertEquals("RAMIREZ", res.get("FOO_LNAME"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
