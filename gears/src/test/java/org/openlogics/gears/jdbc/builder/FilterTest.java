package org.openlogics.gears.jdbc.builder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openlogics.gears.jdbc.builder.Filter.*;

/**
 * @author Miguel Vega
 * @version $Id: FilterTest.java 0, 2013-10-31 12:18 PM mvega $
 */
public class FilterTest {

    @Test
    public void testCompossedFilter(){
        Filter filter = Filter.like("upper(name)", "%juan%").prepend("upper(").append(")");
        System.out.println(filter.evaluate().getQueryString());
    }

    @Test
    public void testNullable(){
        Filter notNulls = Filter.and(Filter.equal("user", "any"), equal("password", "any"), equal("salt", null));
        Criteria crit = notNulls.excludeNullOrEmptyValue(true).evaluate();
        String queryString = crit.getQueryString();
        assertEquals("(user = ? AND password = ?)", queryString);
        System.out.println(queryString+"::"+crit.getQueryValues());

        Filter nullable = Filter.and(Filter.equal("user", "any"), equal("password", "any"), equal("salt", null)).excludeNullOrEmptyValue(false);
        crit = nullable.evaluate();
        queryString = crit.getQueryString();
        assertEquals("(user = ? AND password = ? AND salt = ?)", queryString);
        System.out.println(queryString+"::"+crit.getQueryValues());
    }

    @Test
    public void testPrependAppendFilter(){
        Filter f = Filter.like("name", "josue").prepend("%");
        System.out.println(f.evaluate().getQueryString());
    }

    @Test
    public void testWhere() {

        Filter where = whereClause(
                or(
                        equal("name", "miguel"),
                        null,
                        and(
                                equal("type", 1),
                                equal("age", "29")),
                        Filter.between("rate", 10, 50)
                )
        );

        assertEquals(" WHERE (name = ? OR (type = ? AND age = ?) OR rate BETWEEN ? AND ?)", where.evaluate().getQueryString());

        where = whereClause(null);

        assertTrue(where.evaluate().getQueryString().isEmpty());


        where = whereClause(and(null, or(null, null)));
        System.out.println(">"+where.evaluate().getQueryString());
        assertTrue(where.evaluate().getQueryString().isEmpty());

        Filter fIlter = Filter.between("fecha", "12-10-14", "12-11-14");
        System.out.println(fIlter.evaluate().getQueryString());
    }

    @Test
    public void testOr() {

        Filter or = or(
                equal("name", "miguel"),
                null,
                equal("age", "29")
        );

        assertEquals("(name = ? OR age = ?)", or.evaluate().getQueryString());
    }

    @Test
    public void testAnd() {

        Filter and = and(
                null,
                null,
                equal("age", "29")
        );

        assertEquals("(age = ?)", and.evaluate().getQueryString());

        and = and(
                equal("name", "mike"),
                null,
                equal("age", "29")
        );

        assertEquals("(name = ? AND age = ?)", and.evaluate().getQueryString());

        assertEquals("NOT (name = ? AND age = ?)", Filter.not(and).evaluate().getQueryString());

        System.out.println(">>>>>"+and(isNull("promo_rm_date"), equal("promo_pk", 8584)).evaluate().getQueryString());
    }
}