package org.openlogics.gears.jdbc.builder;

import org.junit.Test;
import org.openlogics.gears.jdbc.Query;
import pojo.Customer;
import pojo.Person;

/**
 * @author Miguel Vega
 * @version $Id: ObjectQueryBuilderTest.java 0, 12/6/13 1:56 AM miguel $
 */
public class ObjectQueryBuilderTest {
    @Test
    public void testBuildSimpleSelect() throws Exception {
        ObjectQueryBuilder qb = new ObjectQueryBuilder();

        Query query = qb.buildSimpleSelect(Person.class);

        System.out.println("::"+query.toString());
    }

    @Test
    public void testBuildSimpleInsert() throws Exception {

        Person p = new Person();
        p.setId(8584);
        p.setLname("vega");
        p.setFname("miguel");

        ObjectQueryBuilder qb = new ObjectQueryBuilder();

        Query query = qb.buildPreparedInsert(p);
    }

    @Test
    public void testBuildSimpleInsertUnnamed() throws Exception {

        ObjectQueryBuilder qb = new ObjectQueryBuilder();

        Query query = qb.buildPreparedInsert(new Customer());
        System.out.println(query.toString());
    }

    @Test
    public void testBuildSimpleUpdate() throws Exception {

        Person p = new Person();
        p.setId(8584);
        p.setLname("vega");
        p.setFname("miguel");

        ObjectQueryBuilder qb = new ObjectQueryBuilder();

        Query query = qb.buildPreparedUpdate(p);
    }
}
