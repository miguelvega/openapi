/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.gears.jdbc;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.gears.jdbc.builder.Filter;
import org.openlogics.gears.jdbc.builder.SortParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.Foo;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.openlogics.gears.jdbc.FunctionParam.IN;
import static org.openlogics.gears.jdbc.Query.of;
import static org.openlogics.gears.jdbc.builder.Filter.equal;
import static org.openlogics.gears.jdbc.builder.SQLClause.group;
import static org.openlogics.gears.jdbc.builder.SQLClause.order;

/**
 * @author Miguel Vega
 * @version $Id: CallableQueryRunnerTest.java 0, 2013-03-13 12:05 AM mvega $
 */
public class CallableQueryRunnerTest {

    private Logger LOGGER = LoggerFactory.getLogger(CallableQueryRunnerTest.class);

    private BasicDataSource getBasicDataSource(String database) {
        BasicDataSource bds = new BasicDataSource();
        bds.setUrl("jdbc:postgresql://localhost:5432/" + database);
        bds.setDriverClassName("org.postgresql.Driver");
        bds.setUsername("postgres");
        bds.setPassword("postgres");
        bds.setMaxIdle(10);
        return bds;
    }

    @Test
    @Ignore("need to be online with database")
    public void simpleQueryWithFilter() throws SQLException {

        DataStore dataStore = DataStoreFactory.createDataStore(getBasicDataSource("foo"));

        Foo foo = new Foo();
        foo.setAddDate(new Timestamp(System.currentTimeMillis()));

        dataStore.update(of("update foo set foo_add_date = #{addDate}", equal("foo_id", 37), foo));

        List<Map<String, Object>> select = dataStore.select(of("select * from foo", Filter.greater("foo_id", 36)), new MapListHandler());

        for (Map<String, Object> map : select) {
            System.out.println(" > " + map);
        }
    }

    @Test
    @Ignore("need to be online with database")
    public void simpleQueryWithFilterGroupOrder() throws SQLException {

        DataStore dataStore = DataStoreFactory.createDataStore(getBasicDataSource("riskc"));

        //to avoid building a large plain query like the next
        Query query = of("select * from detalle_variables \n" +
                "where estado = 'A' " +
                "GROUP BY variable_id, 1 order by variable_id asc, detalle_variable_id \n" +
                "desc offset 1 limit 10;");

        //a new feature has been developed to make the query building clearer
        query = of("select * from detalle_variables", equal("estado", "A")).
                group(group().by("variable_id").by(1)).
                setMaxResults(10).setFirstResult(1).
                order(order().by(new SortParam("variable_id")).by(new SortParam("detalle_variable_id", false)));

        List<Map<String, Object>> select = dataStore.select(
                query,
                new MapListHandler());

        Assert.assertEquals(10, select.size());

        for (Map<String, Object> map : select) {
            LOGGER.info(" > " + map);
        }
    }

    @Test
    @Ignore("need to be online")
    public void testSimpleProcedure() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"));

        //warn, the following value might vary up to the number of SEQUENCE number for the desired row
        int fooId = 14;

        ds.call(Query.<FunctionParam>of("{call proc_addfoo(?, ?::real, ?::real, ?::real)}", IN(fooId), IN(50f), IN(60f), IN(70f)));

        BigDecimal rating = ds.select(
                of("select foo_rating from foo where foo_id = ?", fooId),
                new ScalarHandler<BigDecimal>());
        assertEquals(BigDecimal.valueOf(60f), rating);
    }
}