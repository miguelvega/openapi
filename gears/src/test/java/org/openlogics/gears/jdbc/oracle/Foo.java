package org.openlogics.gears.jdbc.oracle;

import org.openlogics.gears.jdbc.postgres.PostgreSQLTypesDialect;

import javax.persistence.Column;
import javax.persistence.Convert;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: Foo.java 0, 2013-09-17 5:51 PM mvega $
 */
public class Foo {
    @Column(name = "USR_ID")
    private int id;

    @Column(name = "USR_LOGIN")
    private String login;

    @Column(name = "USR_FECHA_ALTA")
    @Convert(converter = OracleTypesDialect.TimestampTypeMapper.class)
    private Date addDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    /*
    public TIMESTAMP getAddDate() {
        return addDate;
    }

    public void setAddDate(TIMESTAMP addDate) {
        this.addDate = addDate;
    }
    */
}