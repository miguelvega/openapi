-- Function increments the input value by 1
CREATE OR REPLACE FUNCTION increment(i INT) RETURNS INT AS $$
BEGIN
  RETURN i + 1;
END;
$$ LANGUAGE plpgsql;

-- An example how to use the function (Returns: 11)
--SELECT increment(10);


CREATE OR REPLACE FUNCTION refcursorfunc() RETURNS refcursor AS '
DECLARE
   mycurs refcursor;
BEGIN
   OPEN mycurs FOR SELECT 1 UNION SELECT 2;
   RETURN mycurs;
END;'
language plpgsql;