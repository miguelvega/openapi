package org.openlogics.gears.jdbc;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import pojo.Foo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;

import static java.lang.System.currentTimeMillis;
import static junit.framework.Assert.assertEquals;

/**
 * @author Miguel Vega
 * @version $Id: InsertTest.java 0, 2012-11-21 19:49 mvega $
 */
public class InsertTest extends TestStub {
    @Test
    public void testSimplePojoInsert() throws SQLException {
        DataStore ds = DataStoreFactory.createObjectDataStore(basicDataSource);

        long icount = countAll(ds);

        Foo std = new Foo();
        std.setFname("Mr.");
        std.setLname("Bean");
        std.setRate(100);
        std.setAddDate(new Timestamp(currentTimeMillis()));

        int count = ds.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                "values " +
                "(#{fname}, #{lname}, #{rate}, #{addDate})", std));

        assertEquals(1, count);
        assertEquals(1 + icount, countAll(ds));

        viewAll(ds);
    }

    @Test
    public void testSimpleMapInsert() {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);

        try {

            long icount = countAll(ds);

            Map<String, Object> map = ImmutableMap.<String, Object>of(
                    "fname", "Mr.",
                    "lname", "Bean",
                    "rate", 100f,
                    "addDate", new Timestamp(currentTimeMillis())
            );

            int count = ds.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                    "values " +
                    "(#{fname}, #{lname}, #{rate}, #{addDate})", map));

            assertEquals(1, count);
            assertEquals(1 + icount, countAll(ds));
            logger.info("Showing MAP Insertion results...");
            viewAll(ds);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void plainInsertTest() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);

        long icount = countAll(ds);

        int count = ds.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                "values " +
                "(?, ?, ?, ?)", "Miguel", "Vega", 12.2f, new Timestamp(System.currentTimeMillis())));

        assertEquals(1, count);
        assertEquals(1 + icount, countAll(ds));
        logger.info("Showing MAP Insertion results...");
        viewAll(ds);
    }

    @Test
    public void batchInsert() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(basicDataSource);
        ds.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);

        Foo std = new Foo();
        std.setFname("Mr.");
        std.setLname("Bean");
        std.setRate(100);
        std.setAddDate(new Timestamp(currentTimeMillis()));

        BatchQuery<Map<String, Object>> query = new BatchQuery<Map<String, Object>>(
                "insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE)" +
                        " values " +
                        "(#{fname}, #{lname}, #{rate}, #{addDate})");

        //in the following insertion, we combine two type of inputs: Map and Pojo
        query.
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 1", "lname", "Last 1", "rate", 61f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 2", "lname", "Last 2", "rate", 62f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 3", "lname", "Last 3", "rate", 63f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 4", "lname", "Last 4", "rate", 64f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 5", "lname", "Last 5", "rate", 65f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, ImmutableMap.<String, Object>of("fname", "User 6", "lname", "Last 6", "rate", 66f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ds, std);

        int[] update = ds.update(query);

        //as the inserts were OK, there must be 7 new records in the table
        assertEquals(7, update.length);

        logger.info("Showing update batch results");
        viewAll(ds);

    }
}
