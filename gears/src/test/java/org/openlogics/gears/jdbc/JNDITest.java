package org.openlogics.gears.jdbc;

import com.google.common.io.Resources;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbutils.ResultSetHandler;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openlogics.utils.naming.ContainerNamingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;
import static junit.framework.Assert.assertEquals;

/**
 * @author Miguel Vega
 * @version $Id: TestJNDI.java 0, 5/26/14 9:34 AM, @miguel $
 */
public class JNDITest {

    static Logger LOGGER = LoggerFactory.getLogger(JNDITest.class);
    static String JNDI_REF_NAME = "java:comp/env/jdbc/geotools";
    static protected IDatabaseTester databaseTester;

    @BeforeClass
    public static void setUpClass() throws Exception {
        // rcarver - setup the jndi context and the datasource
        try {

            // Construct DataSource
            OracleConnectionPoolDataSource oracleDS = new OracleConnectionPoolDataSource();
            oracleDS.setURL("jdbc:oracle:thin:@host:port:db");
            oracleDS.setUser("MY_USER_NAME");
            oracleDS.setPassword("MY_USER_PASSWORD");

            //ic.bind("java:/comp/env/jdbc/nameofmyjdbcresource", oracleDS);


            //H2 memory database
            DriverAdapterCPDS dataSource = new DriverAdapterCPDS();
            dataSource.setUrl("jdbc:h2:mem:foodb");
            dataSource.setDriver("org.h2.Driver");

            JdbcConnectionPool ds = JdbcConnectionPool.create(dataSource);

            ContainerNamingContext.installJDBCDataSource("nameofmyjdbcresource", ds);

            Connection connection;
            try {
                connection = dataSource.getPooledConnection().getConnection();
                URL sql = getResource(JNDITest.class, "foo.sql");
                try {
                    connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                    connection.close();

                    URL resource = getResource(JNDITest.class, "foo.xml");
                    FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);

                    databaseTester = new JdbcDatabaseTester(dataSource.getDriver(), dataSource.getUrl());
                    databaseTester.setDataSet(build);
                    databaseTester.onSetup();
                } catch (Exception x) {
                    x.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                //DbUtils.closeQuietly(connection);
            }
        } catch (NamingException ex) {
            LOGGER.error(null, ex);
        }
    }

    @Test
    public void testSimpleSelect() throws NamingException, SQLException {
        Context initContext = new InitialContext();
        Context webContext = (Context) initContext.lookup("java:/comp/env");

        DataSource ds = (DataSource) webContext.lookup("jdbc/nameofmyjdbcresource");

        DataStore dataStore = DataStoreFactory.createDataStore(ds);

        Query query = Query.of("select * from FOO");
        String result = query.toString();
        assertEquals("select * from FOO", result);

        String response = dataStore.select(query, new ResultSetHandler<String>() {
            @Override
            public String handle(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    LOGGER.debug("Record found..." + rs.getInt(1));
                }
                return "success";
            }
        });

        assertEquals("success", response);

    }
}
