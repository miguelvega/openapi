package org.openlogics.gears.jdbc;

import org.apache.commons.dbutils.ResultSetHandler;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.Assert.*;

public class DataStoreFactoryTest {
    /**
     * @see {@link http://jdbc.postgresql.org/documentation/80/connect.html}
     */
    @Test
    @Ignore
    public void testPostgreSQLConnection() throws SQLException {
        String url = "jdbc:postgresql://localhost/world";
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "postgres");
        props.setProperty("charSet", "ISO-8859-1");
//        props.setProperty("ssl","true");

        DataStore dataStore = DataStoreFactory.createDataStore(url, props);

        dataStore.select(Query.of("select * from city"), new ResultSetHandler<Void>() {
            @Override
            public Void handle(ResultSet rs) throws SQLException {
                while (rs.next()){
                    System.out.println(">>>>>" + rs.getString("name"));
                }
                return null;
            }
        });
    }

    /**
     * @see {@link http://dev.mysql.com/doc/connector-j/en/connector-j-reference-configuration-properties.html}
     * @throws SQLException
     */
    @Test
    @Ignore
    public void testMySQLConnection() throws SQLException {
        String url = "jdbc:mysql://localhost/mysql";
        Properties props = new Properties();
        props.setProperty("user", "root");
        props.setProperty("password", "mysql");
        props.setProperty("charSet", "ISO-8859-1");

        DataStore dataStore = DataStoreFactory.createDataStore(url, props);

        dataStore.select(Query.of("show databases"), new ResultSetHandler<Void>() {
            @Override
            public Void handle(ResultSet rs) throws SQLException {
                while (rs.next()){
                    System.out.println(">>>>>" + rs.getString(1));
                }
                return null;
            }
        });
    }

    /**
     * @see {@link http://docs.oracle.com/cd/E11882_01/appdev.112/e13995/oracle/jdbc/OracleDriver.html}
     * @throws SQLException
     */
    @Test
    @Ignore
    public void testOracleConnection() throws SQLException {
        String url = "jdbc:oracle:thin:tigo/tigo@192.168.4.9:1521:desa";

        Properties props = null;

        /*props = new Properties();
        props.setProperty("user", "tigo");
        props.setProperty("password", "tigo");
        props.setProperty("defaultRowPrefetch", "15");
        props.setProperty("database", "192.168.4.9:1521:desa");
        url = "jdbc:oracle:thin:@";*/

        DataStore dataStore = DataStoreFactory.createDataStore(url, props);

        dataStore.select(Query.of("select * from bc_ussd_parametros"), new ResultSetHandler<Void>() {
            @Override
            public Void handle(ResultSet rs) throws SQLException {
                while (rs.next()){
                    System.out.println(">>>>>" + rs.getBigDecimal(1)+"-"+rs.getString("PRMT_CLAVE"));
                }
                return null;
            }
        });
    }
}