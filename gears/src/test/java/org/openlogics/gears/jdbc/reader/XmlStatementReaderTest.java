package org.openlogics.gears.jdbc.reader;

import com.google.common.cache.CacheBuilder;
import com.google.common.io.Resources;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: XmlStatementReaderTest.java 0, 2013-05-05 04:04 mvega $
 */
public class XmlStatementReaderTest {
    @Test
    public void testParsing() throws IOException {

        StatementReader statementReader = new XmlStatementReader(CacheBuilder.newBuilder().<String, String>build());

        InputStream inputStream = Resources.getResource("sql-statements.xml").openStream();

        //first attempting to load only one of all statements defined
        statementReader.loadStatements(inputStream, "getStatus");

        String sql = statementReader.getStatementById("getStatus");

        assertTrue(sql.contains("status_name as \"entityName\""));
        assertNull(statementReader.getStatementById("another"));

        //now let's load all the statements
        inputStream = Resources.getResource("sql-statements.xml").openStream();
        statementReader.detachCachedObjects(true).loadStatements(inputStream);

        sql = statementReader.getStatementById("getStatus");
        assertTrue(sql.contains("status_name as \"entityName\""));

        sql = statementReader.getStatementById("another");
        assertTrue(sql.contains("poipoipoi"));
    }
}
