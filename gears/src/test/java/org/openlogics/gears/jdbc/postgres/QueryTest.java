package org.openlogics.gears.jdbc.postgres;

import org.junit.Before;
import org.junit.Test;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.openlogics.gears.jdbc.postgres.entities.CityEntity;
import org.openlogics.gears.jdbc.postgres.entities.CountryEntity;
import org.openlogics.utils.naming.ContainerNamingContext;
import org.openlogics.utils.naming.ContainerNamingContextFactory;
import org.openlogics.utils.naming.EnvironmentConfigurationException;
import org.osjava.sj.loader.JndiLoader;
import org.postgresql.jdbc2.optional.SimpleDataSource;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: QueryTest.java 0, December 30, 2014 11:29 PM mvega $
 */
public class QueryTest {

    @Before
    public void init() throws EnvironmentConfigurationException {

        ContainerNamingContextFactory.configureDefaultCatalinaContainer();

        try {
            SimpleDataSource dataSource = new SimpleDataSource();
            dataSource.setUser("postgres");
            dataSource.setPassword("postgres");
            dataSource.setDatabaseName("world");
            dataSource.setServerName("localhost");
            dataSource.setPortNumber(5432);

            ContainerNamingContext.installJDBCDataSource("world-ds", dataSource);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleQuery() throws NamingException, SQLException {
        ObjectDataStore ods = DataStoreFactory.createObjectDataStore((DataSource) ContainerNamingContext.lookupJDBCContext().lookup("world-ds"));
        List<CountryEntity> resultList = ods.getResultList(CountryEntity.class);
        for (CountryEntity countryEntity: resultList) {
            System.out.println("..."+countryEntity.getName());
        }
    }
}
