package org.openlogics.gears.jdbc.postgres;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.FunctionParam;
import org.openlogics.gears.jdbc.Query;

import java.sql.*;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;
import static org.openlogics.gears.jdbc.FunctionParam.IN;
import static org.openlogics.gears.jdbc.FunctionParam.OUT;

/**
 * @author Miguel Vega
 * @version $Id: PostgreSQLTest.java 0, 2013-04-19 23:21 mvega $
 */
public class PostgreSQLTest {

    private BasicDataSource getBasicDataSource(String database) {
        BasicDataSource bds = new BasicDataSource();
        bds.setUrl("jdbc:postgresql://localhost:5432/" + database);
        bds.setDriverClassName("org.postgresql.Driver");
        bds.setUsername("postgres");
        bds.setPassword("postgres");
        bds.setMaxIdle(10);
        return bds;
    }

    private Properties getDataSourceProperties(String database) {
        Properties properties = new Properties();
        properties.put("url", "jdbc:postgresql://localhost:5432/" + database);
        properties.put("host", "localhost");
        properties.put("port", "5432");
        properties.put("database", database);
        properties.put("driver", "org.postgresql.Driver");
        properties.put("user", "postgres");
        properties.put("password", "postgres");
        return properties;
    }

    @Test
    @Ignore("Execute only when you ensure that POSTGRESQL is properly configured with script provided")
    public void testFunctionAsQuery() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"));

        int NUM = 10;

        int inc = ds.select(Query.of("select increment(?)", NUM), new ScalarHandler<Integer>(1));

        assertEquals(NUM + 1, inc);
    }

    @Test
    @Ignore("Execute only when you ensure that POSTGRESQL is properly configured with script provided")
    public void testFunctionAsCall() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"), true);

        int NUM = 10;

        FunctionParam<FunctionParam.Out<Integer>> out =
                OUT(new FunctionParam.Out<Integer>(Types.INTEGER));

        boolean inc = ds.call(Query.<FunctionParam>of("{? = call increment(? :: int)}", out, IN(NUM)));

        assertEquals(NUM + 1, out.getModel().getValue().intValue());
    }

    /**
     * A tip to identify if JDBC driver is 'parameterMetaDataKnownBroken', simply execute the {@link java.sql.CallableStatement#getParameterMetaData()} method
     */
    @Test(expected = IllegalStateException.class)
    @Ignore("Execute only when you ensure that POSTGRESQL is properly configured with script provided")
    public void testDetectParameterMetaDataKnownBroken() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"), false);

        Connection conn = ds.getConnection();

        Statement stmt = ds.getConnection().createStatement();

        // We must be inside a transaction for cursors to work.
        conn.setAutoCommit(false);

        // Procedure call.
        CallableStatement upperProc = conn.prepareCall("{ ? = call upper( ? ) }");

        try {
            upperProc.getParameterMetaData();
        } catch (SQLException x) {
            throw new IllegalStateException("Unable to execute getParameterMetadata using the given JDBC Driver.", x);
        } finally {
            ds.forceCloseConnection();
        }
    }

    @Test(expected = IllegalStateException.class)
    @Ignore("Execute only when you ensure that POSTGRESQL is properly configured with script provided")
    public void testParameterMetaDataKnownBroken() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"));

        FunctionParam<FunctionParam.Out<String>> out = OUT(new FunctionParam.Out<String>(Types.VARCHAR));

        String variable = "variable";

        try {
            ds.call(Query.<FunctionParam>of("{ ? = call upper( ? ) }", out, IN(variable)));
        } catch (SQLException x) {
            throw new IllegalStateException("Unable to execute getParameterMetadata using the given JDBC Driver.", x);
        } finally {
            ds.forceCloseConnection();
        }
    }

    @Test
    @Ignore("Execute only when you ensure that POSTGRESQL is properly configured with script provided")
    public void plainJdbcCallTest() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("foo"));

        Connection conn = ds.getConnection();

        Statement stmt = ds.getConnection().createStatement();

        // We must be inside a transaction for cursors to work.
        conn.setAutoCommit(false);

        // Procedure call.
        CallableStatement upperProc = conn.prepareCall("{ ? = call upper( ? ) }");

        upperProc.registerOutParameter(1, Types.VARCHAR);
        upperProc.setString(2, "lowercase to uppercase");
        upperProc.execute();
        String upperCased = upperProc.getString(1);
        upperProc.close();

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>" + upperCased);

        CallableStatement proc = conn.prepareCall("{ ? = call refcursorfunc() }");
        proc.registerOutParameter(1, Types.OTHER);
        proc.execute();
        ResultSet results = (ResultSet) proc.getObject(1);
        while (results.next()) {
            // do something with the results...
            System.out.println(">" + results.getObject(1));
        }
        results.close();
        proc.close();
    }

    @Test
    @Ignore
    public void testQueryManyTimes() throws SQLException {
        DataStore ds = DataStoreFactory.createDataStore(getBasicDataSource("geobase"));
        for (int i = 0; i < 100; i++) {
            Object gid = ds.select(Query.of("select * from geo_barrios"), new ResultSetHandler<Object>() {
                @Override
                public Object handle(ResultSet rs) throws SQLException {
                    rs.next();
                    return rs.getString("gid");
                }
            });
            System.out.println("-" + new java.util.Date() + " FOR " + gid);
        }
    }
}