package org.openlogics.gears.jdbc.oracle;

import oracle.sql.TIMESTAMP;

import javax.persistence.AttributeConverter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: OracleTypesDialect.java 0, 2013-09-18 3:38 PM mvega $
 */
public class OracleTypesDialect {
    public static class TimestampTypeMapper implements AttributeConverter<TIMESTAMP, Date> {

        @Override
        public Date convertToDatabaseColumn(TIMESTAMP attribute) {
            try {
                return new Date(attribute.dateValue().getTime());
            } catch (SQLException e) {
                throw new IllegalStateException("Unable to retrieve TIMESTAMP value from database", e);
            }
        }

        @Override
        public TIMESTAMP convertToEntityAttribute(Date dbData) {
            return new TIMESTAMP(new Timestamp(dbData.getTime()));
        }
    }
}
