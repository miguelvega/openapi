package org.openlogics.gears.security;

import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * @author Miguel Vega
 * @version $Id: PasswordsTest.java 0, 2013-01-09 7:58 PM mvega $
 */
public class PasswordsTest {
    Logger logger;

    @Before
    public void init() {
        logger = Logger.getLogger(getClass());
    }

    @Test
    public void testGenerateRandomPassword() throws Exception {
        Passwords pwds = new Passwords();

        String pwd = "hola";
        byte[] salt = pwds.genSalt();
        String hash = pwds.hash(pwd.toCharArray(), salt);
        assertNotNull(hash);
        Assert.assertEquals(97, hash.length());
        logger.info("Hash: " + hash);

        //try generating hashes for Hello World!!!
        hash = pwds.hash(pwd);
        logger.info("(" + hash.length() + ")Next 1: " + hash);
        hash = pwds.hash(pwd);
        logger.info("(" + hash.length() + ")Next 2: " + hash);
        hash = pwds.hash(pwd);
        logger.info("(" + hash.length() + ")Next 3: " + hash);
        hash = pwds.hash(pwd);
        logger.info("(" + hash.length() + ")Next 4: " + hash);
    }

    @Test
    public void testVerifyPassword() throws InvalidKeySpecException, NoSuchAlgorithmException {
        Passwords pwds = new Passwords();

        byte[] salt = pwds.genSalt();
        logger.info("Salt: [" + new String(salt) + "], with " + salt.length + " bytes length");
        String phrase = "Hello";
        String hash = pwds.hash(phrase.toCharArray(), salt);
        logger.info("Hash 0: " + hash);

        //restore
        String hash1 = pwds.hash(phrase.toCharArray(), salt);
        logger.info("Hash 1: " + hash1);
        assertEquals(hash1, hash);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void testNullPassword() throws InvalidKeySpecException, NoSuchAlgorithmException {
        new Passwords().hash((String) null);
    }

    @Test
    public void testRandomPassword() {
        Passwords pwds = new Passwords();
        logger.info("Ranndom Password: " + pwds.genPassword(25));
    }
}
