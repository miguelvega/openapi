package generic.h2;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.DatabaseTestCase;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.net.URL;
import java.sql.*;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: SimpleConnectionTest.java; dic 02, 2014 09:55 AM mvega $
 */
public class SimpleConnectionTest {

    final Logger log = LoggerFactory.getLogger(getClass());

    private final BasicDataSource dataSource;

    public SimpleConnectionTest() {

        dataSource = new BasicDataSource();
        dataSource.setUrl("jdbc:h2:mem:test");
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setMaxActive(10);

        //initialize the database based on XML

        Connection conn = null;
        Statement st = null;
        try {
            conn = dataSource.getConnection();
            st = conn.createStatement();
            st.execute(Resources.toString(getResource(getClass(), "cube.sql"), US_ASCII));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(conn);
        }

        try {
            URL resource = getResource(getClass(), "cube.xml");
            FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
            DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(dataSource, "public");
            databaseTester.setDataSet(build);
            databaseTester.onSetup();
        } catch (DataSetException e) {
            throw new IllegalStateException(e);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void testJNDI() throws NamingException {

        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,
                    "org.apache.naming");
            InitialContext ic = new InitialContext();

            ic.createSubcontext("java:");
            ic.createSubcontext("java:/comp");
            ic.createSubcontext("java:/comp/env");
            ic.createSubcontext("java:/comp/env/jdbc");

            // Construct DataSource

            ic.bind("java:/comp/env/jdbc/nameofmyjdbcresource", dataSource);
        } catch (NamingException ex) {
            log.error(null, ex);
        }

        //when the JNDI has been setted up
        Context initContext = new InitialContext();
        Context webContext = (Context)initContext.lookup("java:/comp/env");

        DataSource ds = (DataSource) webContext.lookup("jdbc/nameofmyjdbcresource");

        log.info("----"+ds);
    }

    @Test
    public void testSimpleQuery() throws SQLException {
        Connection conn = dataSource.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("Select * from products");
        log.info("Number of columns: " + rs.getMetaData().getColumnCount());
        while(rs.next()){
            log.info("Record..."+rs.getObject(2));
        }
        st.close();
        conn.close();
    }

    @Test
    public void testSimpleConnection() throws SQLException {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:h2:mem:test", "sa", "");
            assertTrue(conn.getCatalog().indexOf("TEST") > 0);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }
}
