package generic;

import com.google.common.collect.Iterables;
import org.junit.*;

import java.io.*;

/**
 * @author Miguel Vega
 * @version $Id: RenameFiles.java 0, 2013-07-02 9:40 AM mvega $
 */
public class RenameFiles {
    @Test
    public void testRename() {
        File[] files = new File("/media/64AC1AD5AC1AA21A/usr/java/openlogics/opendroid/protostore/src/main/java/com/mc4/protostore/data").listFiles(
                new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().endsWith(".jpg");
                    }
                }
        );
        for (int i = 0; i < files.length; i++) {
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(new File(files[i].getParent(), "laptop_" + i + ".jpg"));
                com.google.common.io.ByteStreams.copy(new FileInputStream(files[i]), fos);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Test
    public void testPopulateColumns() {
        int colcount = 13;
        int count = 0;

        for (int i = 0; i < colcount; i++) {
            count = ((i % colcount) % 2);
            System.out.println(">>>" + count);


            System.out.println(">>>>>>>" + i % 2);
        }
    }
}
