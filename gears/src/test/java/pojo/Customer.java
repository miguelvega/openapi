package pojo;

import org.openlogics.gears.jdbc.DefaultTypeMapper;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Miguel Vega
 * @version $Id: Customer.java 0, 6/15/14 10:09 PM, @miguel $
 */
@Entity(name = "v_clientes")
@Table(name = "v_clientes")
public class Customer implements Serializable {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    private Long user_id;
    @Column
    private Long id_sucursal;
    @Column
    private Long id_perfil;
    @Column
    private String tipo_cliente;
    @Column
    private String nombres;
    @Column
    private String apellido_paterno;
    @Column
    private String apellido_materno;
    @Column
    private String nacionalidad;
    @Column
    private String doc_personal;
    @Column
    private String nro_carnet;
    @Column
    private String lugar_carnet;
    @Column
    private String nit_cliente;
    @Convert(converter = DefaultTypeMapper.DateMapper.class)
    @Column
    private String fecha_nacimiento;
    @Column
    private String email;
    @Column
    private String sexo;
    @Column
    private String telefono;
    @Column
    private String celular;
    @Column
    private String avenida_calle;
    @Column
    private String zona;
    @Column
    private String num_domiclio;
    @Column
    private String edificio;
    @Column
    private String piso;
    @Column
    private String departamento;
    @Column
    private String estado_civil;
    @Column
    private String nombre_empleo;
    @Column
    private String direccion_empleo;
    @Column
    private String telefono_empleo;
    @Column
    private String web_empleo;
    @Column
    private String nombre_completo_ref;
    @Column
    private String telefono_ref;
    @Column
    private String celular_ref;
    @Column
    private String direccion_ref;
    @Column
    @Convert(converter = DefaultTypeMapper.DateMapper.class)
    private String created_at;
    @Column
    @Convert(converter = DefaultTypeMapper.DateMapper.class)
    private String updated_at;

    private String formGroupFeedback;

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public Long getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(long id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public Long getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(long id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getDoc_personal() {
        return doc_personal;
    }

    public void setDoc_personal(String doc_personal) {
        this.doc_personal = doc_personal;
    }

    public String getNro_carnet() {
        return nro_carnet;
    }

    public void setNro_carnet(String nro_carnet) {
        this.nro_carnet = nro_carnet;
    }

    public String getLugar_carnet() {
        return lugar_carnet;
    }

    public void setLugar_carnet(String lugar_carnet) {
        this.lugar_carnet = lugar_carnet;
    }

    public String getNit_cliente() {
        return nit_cliente;
    }

    public void setNit_cliente(String nit_cliente) {
        this.nit_cliente = nit_cliente;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getNum_domiclio() {
        return num_domiclio;
    }

    public void setNum_domiclio(String num_domiclio) {
        this.num_domiclio = num_domiclio;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }

    public String getNombre_empleo() {
        return nombre_empleo;
    }

    public void setNombre_empleo(String nombre_empleo) {
        this.nombre_empleo = nombre_empleo;
    }

    public String getDireccion_empleo() {
        return direccion_empleo;
    }

    public void setDireccion_empleo(String direccion_empleo) {
        this.direccion_empleo = direccion_empleo;
    }

    public String getTelefono_empleo() {
        return telefono_empleo;
    }

    public void setTelefono_empleo(String telefono_empleo) {
        this.telefono_empleo = telefono_empleo;
    }

    public String getWeb_empleo() {
        return web_empleo;
    }

    public void setWeb_empleo(String web_empleo) {
        this.web_empleo = web_empleo;
    }

    public String getNombre_completo_ref() {
        return nombre_completo_ref;
    }

    public void setNombre_completo_ref(String nombre_completo_ref) {
        this.nombre_completo_ref = nombre_completo_ref;
    }

    public String getTelefono_ref() {
        return telefono_ref;
    }

    public void setTelefono_ref(String telefono_ref) {
        this.telefono_ref = telefono_ref;
    }

    public String getCelular_ref() {
        return celular_ref;
    }

    public void setCelular_ref(String celular_ref) {
        this.celular_ref = celular_ref;
    }

    public String getDireccion_ref() {
        return direccion_ref;
    }

    public void setDireccion_ref(String direccion_ref) {
        this.direccion_ref = direccion_ref;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getFormGroupFeedback() {
        return formGroupFeedback;
    }

    public void setFormGroupFeedback(String formGroupFeedback) {
        this.formGroupFeedback = formGroupFeedback;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getAvenida_calle() {
        return avenida_calle;
    }

    public void setAvenida_calle(String avenida_calle) {
        this.avenida_calle = avenida_calle;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }
}
