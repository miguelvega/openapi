package pojo;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: Service.java 0, 2015-11-28 12:43 AM mvega $
 */
@Entity
@Table(name = "services")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    @Enumerated(EnumType.STRING)
    private ServiceType type;

//    @Transient
    @Column
    @Temporal(TemporalType.DATE)
    private Date onDate;

    public enum ServiceType{
        REST, SOAP
    }

    public Date getOnDate() {
        return onDate;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public ServiceType getType() {
        return type;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
