package my.java.net.mytest;

import com.google.common.io.ByteStreams;
import org.junit.Ignore;
import org.junit.Test;
import org.openlogics.utils.io.StringOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Miguel Vega
 * @version $Id: ConnectivityTest.java 0, 2013-08-23 12:21 PM mvega $
 */
public class ConnectivityTest {

    @Test()
    @Ignore
    public void testConnection() throws IOException {
        URL url = new URL("http://www.java.net");

        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        HttpURLConnection.setFollowRedirects(false);

        huc.setConnectTimeout(5);

        huc.setRequestMethod("GET");
        huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
        huc.connect();

        InputStream input = huc.getInputStream();

        StringOutputStream stream = new StringOutputStream();
        ByteStreams.copy(input, stream);

        System.out.println(stream.getString());
    }
}
