package poi;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Miguel Vega
 * @version $Id: ExcelWriterTest.java 0, 8/6/14 1:32 PM, @miguel $
 */
public class ExcelWriterTest {
    @Test
    public void testWrite() throws IOException {
        ExcelWriter ew = new ExcelWriter("Reportes");

        ew.addCell("Column 0");
        ew.addCell("Column 1");
        ew.addCell("Column 2");
        ew.addCell("Column 3");

        ew.newRow();

        ew.addCell("Column 0\njust a simple line feed\nqwerty\nasdfghj\nzxcvbn");
        ew.addCell(15487546);
        ew.addCell("lorem ipsum");
        ew.addCell("I am a line feed "+System.getProperty("line.separator")+" poipoipoipoipoipoipoipoi");

        ew.write(new FileOutputStream("/tmp/zzzz.xlsx"));
    }
}
