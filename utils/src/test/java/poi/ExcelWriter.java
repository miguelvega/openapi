package poi;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Miguel Vega
 * @version $Id: ExcelWriter.java 0, 8/6/14 1:17 PM, @miguel $
 */
public class ExcelWriter {
    final XSSFWorkbook workbook;
    final XSSFSheet sheet;

    int curRow = 0;
    int curCol = 0;

    public ExcelWriter(String sheetName) {
        workbook = new XSSFWorkbook();
        this.sheet = workbook.createSheet(sheetName);
        sheet.createRow(curRow);
    }

    public void addCell(String data) {
        XSSFRow row = sheet.getRow(curRow);
        row.createCell(curCol);
        row.getCell(curCol++).setCellValue(data);
    }

    public void addCell(double data) {
        XSSFRow row = sheet.getRow(curRow);
        row.createCell(curCol);
        row.getCell(curCol++).setCellValue(data);
    }

    public void newRow() {
        this.newRow(0);
    }

    public void newRow(int startAtColumn) {
        this.curRow++;
        this.curCol = startAtColumn;

        sheet.createRow(curRow);
    }

    public void write(OutputStream outputStream) throws IOException {
        workbook.write(outputStream);
    }
}
