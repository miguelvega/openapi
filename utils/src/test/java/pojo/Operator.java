package pojo;

/**
 * @author Miguel Vega
 * @version $Id: Operator.java 0, 3/7/14 3:56 PM, @miguel $
 */
public class Operator<T extends Number> {

    private String symbol;
    private T defaultValue;

    public Operator withSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    /*public void setSymbol(String symbol) {
        this.symbol = symbol;
    }*/

    public String getSymbol() {
        return symbol;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(T defaultValue) {
        this.defaultValue = defaultValue;
    }
}
