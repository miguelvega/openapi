package org.openlogics.utils;

import org.junit.*;
/**
 * @author Miguel Vega
 * @version $Id: ImmutableStringTest.java 0, 2013-10-29 11:49 PM mvega $
 */
public class ImmutableStringTest {

    @Test
    public void testExludeLast(){
        ImmutableString s = ImmutableString.of(" name = ? and age = ? and ");
        Assert.assertEquals(" name = ? and age = ? ", s.removeFromLastMatch("and").build());
    }

    @Test
    public void testExludeFirst(){
        ImmutableString s = ImmutableString.of("and name = ? and age = ?");
        Assert.assertEquals(" name = ? and age = ?", s.removeToFirstMatch("and").build());
    }
}
