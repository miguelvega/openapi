package org.openlogics.utils.collect;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import static org.openlogics.utils.collect.FnCollections.*;
import static org.openlogics.utils.collect.Pair.of;

public class FnCollectionsTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableList() {
        List<String> strings = immutableList("A", "B", "...", "X", "Y", "Z");

        System.out.println("0." + strings);

        String A = strings.get(0);
        A = "a";

        System.out.println("1." + strings);

        strings.add("Invalid");
    }

    @Test
    public void testImmutableMap() {
        Map<String, Integer> map = immutableMapOfSet(new String[]{"a", "b"}, new Integer[]{1, 2});
        System.out.println(map);
    }

    @Test
    public void testImmutableMapNullKey() {
        Map<String, Integer> map = immutableMapOfSet(new String[]{"a", "b", null, null}, new Integer[]{1, 2, 3, 4});
        System.out.println(map);
    }

    @Test(expected = NullPointerException.class)
    public void testImmutableMapFromNullArrays() {
        Map<String, Integer> map = immutableMapOfSet(null, new Integer[]{1, 2, 3, 4});
        System.out.println(map);
    }

    @Test
    public void testImmutableMapFromList() {
        Map<String, Integer> map = immutableMapOfSet(immutableList("a", "b", "c", "d"), immutableList(1, 2, 3));
        System.out.println(map);
    }

    @Test
    public void testImmutableMapFromPairs() {
        Map<String, String> map = immutableMapOf(of("1", "one"), of("2", "two"), of("n", "null"));
        System.out.println(map);
    }

    @Test
    public void testMessageDigest() throws NoSuchAlgorithmException {
        String s = "a";
        byte[] digest = DigestUtils.md5(s);
        System.out.println(new Hex().encodeHex(digest));
    }
}