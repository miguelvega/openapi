package org.openlogics.utils.io;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 * @author Miguel Vega
 * @version $Id: FilesTest.java 0, 2013-10-01 2:37 PM mvega $
 */
public class FilesTest {
    @Test
    public void testJARFileLocation(){
        File jar = Files.getAccessibleFileSystemPath(Before.class);
        System.out.println("..."+jar.getAbsolutePath());
    }
}
