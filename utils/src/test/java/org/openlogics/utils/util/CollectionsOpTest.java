package org.openlogics.utils.util;

import org.junit.Assert;
import org.junit.Test;
import org.openlogics.utils.collect.CollectionsOp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: CollectionsOpTest.java 0, 2013-06-10 3:58 PM mvega $
 */
public class CollectionsOpTest {

    @Test
    public void testIntersection() {
        List<String> list1 = new ArrayList<String>(Arrays.asList("A", "B", "C"));
        List<String> list2 = new ArrayList<String>(Arrays.asList("B", "C", "D", "E", "F"));

        Collection<String> res = CollectionsOp.intersection(list1, list2);

        System.out.println(res);

        Assert.assertTrue(res.size() == 2);
    }

    @Test
    public void testUnion() {
        List<String> list1 = new ArrayList<String>(Arrays.asList("A", "B", "C", "Z"));
        List<String> list2 = new ArrayList<String>(Arrays.asList("B", "C", "D", "E", "F"));

        Collection<String> res = CollectionsOp.union(list1, list2);

        System.out.println(res);
    }


    @Test
    public void testDiff() {
        List<String> list1 = new ArrayList<String>(Arrays.asList("A", "B", "C", "M", "N"));
        List<String> list2 = new ArrayList<String>(Arrays.asList("B", "C", "D", "E", "F"));

        Collection<String> res = CollectionsOp.difference(list2, list1);
        System.out.println("B diff A: " + res);

        res = CollectionsOp.difference(list1, list2);
        System.out.println("A diff B: " + res);
    }
}