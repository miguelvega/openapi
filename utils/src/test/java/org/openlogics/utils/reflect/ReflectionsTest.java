package org.openlogics.utils.reflect;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import pojo.Operator;
import pojo.Teacher;

import javax.annotation.Nullable;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import static org.openlogics.utils.reflect.Reflections.getDeclaredFields;
import static org.openlogics.utils.reflect.Reflections.inspectType;

/**
 * @author Miguel Vega
 * @version $Id: ReflectionsTest.java 0, 2013-07-04 2:09 PM mvega $
 */
public class ReflectionsTest {
    @Test
    public void testGetAttributes() {
        List<Field> fields = getDeclaredFields(Teacher.class);
        for (Field field : fields) {
            System.out.println("Field..." + field);
        }
    }

    @Test
    public void testListSetters() throws NoSuchFieldException, IntrospectionException, InvocationTargetException, IllegalAccessException {
        Operator<Integer> operator =  new Operator();

        List<Field> declaredFields = Reflections.getDeclaredFields(operator.getClass());

        Field field = Iterables.find(declaredFields, new Predicate<Field>() {
            @Override
            public boolean apply(@Nullable Field input) {
                return "symbol".equals(input.getName());
            }
        });

        Method setter = Reflections.getSetterMethod(field);

        setter.invoke(operator, "+");

        System.out.println(operator.getSymbol());
    }

    @Test
    public void testDescribeType() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Map describe = BeanUtils.describe(new Teacher());
        for (Object o : describe.keySet()) {
            System.out.println(o + "<--->" + describe.get(o));
        }
    }

    @Test
    public void testInspectType() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IntrospectionException {
        Iterable<PropertyDescriptor> desc = inspectType(Teacher.class);
        for (PropertyDescriptor pd : desc) {
            System.out.println("Descriptor: " + pd.getName() + ", " + pd.getReadMethod().getName());
        }
    }

    @Test
    public void testInspectFields() {
        Iterable<Field> desc = getDeclaredFields(Teacher.class);
        for (Field pd : desc) {
            System.out.println("Field: " + pd);
        }
    }
}
