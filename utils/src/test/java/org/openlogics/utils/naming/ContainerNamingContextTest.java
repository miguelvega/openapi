package org.openlogics.utils.naming;

import org.apache.log4j.Level;
import org.junit.Assert;
import org.junit.Test;
import org.osjava.sj.loader.JndiLoader;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Hashtable;
import java.util.logging.Logger;

public class ContainerNamingContextTest {

    final DataSource ds = new DataSource() {
        public Connection getConnection() throws SQLException {
            return null;
        }

        public Connection getConnection(String username, String password) throws SQLException {
            return null;
        }

        public PrintWriter getLogWriter() throws SQLException {
            return null;
        }

        public void setLogWriter(PrintWriter out) throws SQLException {}

        public void setLoginTimeout(int seconds) throws SQLException {}

        public int getLoginTimeout() throws SQLException {
            return 0;
        }

        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            return null;
        }

        public <T> T unwrap(Class<T> iface) throws SQLException {
            return null;
        }

        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return false;
        }
    };

    {
        org.apache.log4j.Logger.getRootLogger().setLevel(Level.DEBUG);
        try {
            ContainerNamingContextFactory.configureDefaultCatalinaContainer();
        } catch (EnvironmentConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void testWrapDataSourceTomcat() throws NamingException {
        String jndiRef = "java:/comp/env/jdbc/tcds";

        ContainerNamingContext.bindFullyQualifiedInstance(jndiRef, ds);

        Context initContext = new InitialContext();
        Context webContext = (Context)initContext.lookup("java:/comp/env");

        DataSource ds = (DataSource) webContext.lookup("jdbc/tcds");

        System.out.println("..............."+ds);
    }

    @Test
    public void testWrapDataSourceWebLogic() throws NamingException {
        //String jndiRef = "java:/comp/env/jdbc/tcds";

        ContainerNamingContext.installJDBCDataSource("tcds", ds);

        Context webContext = ContainerNamingContext.lookupEnvironmentContext();

        DataSource ds = (DataSource) webContext.lookup("jdbc/tcds");

        System.out.println("..............."+ds);
    }

    @Test
    public void testCustomContainer() throws NamingException {
        //         The default is 'flat', which isn't hierarchial and not what I want.
//         Separator is required for non-flat

        Hashtable contextEnv = new Hashtable();

//         For GenericContext
        contextEnv.put(Context.INITIAL_CONTEXT_FACTORY, "org.osjava.sj.memory.MemoryContextFactory");
        contextEnv.put("jndi.syntax.direction", "left_to_right");
        contextEnv.put("jndi.syntax.separator", "/");
        contextEnv.put(JndiLoader.SIMPLE_DELIMITER, "/");


        try {
            ContainerNamingContextFactory.configureContainer(contextEnv);
        } catch (EnvironmentConfigurationException e) {
            throw new IllegalStateException(e);
        }

        ContainerNamingContext.installJDBCDataSource("poipoipoi", ds);

        Context webContext = ContainerNamingContext.lookupEnvironmentContext();

        DataSource ds = (DataSource) webContext.lookup("jdbc/poipoipoi");

        Assert.assertNotNull(ds);

    }
}