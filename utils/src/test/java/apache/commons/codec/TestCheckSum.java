package apache.commons.codec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.util.IOUtils;
import org.apache.xmlbeans.impl.common.IOUtil;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;

/**
 * @author Miguel Vega
 * @version $Id: TestCheckSum.java; nov 11, 2014 12:48 PM mvega $
 */
public class TestCheckSum {

    @Ignore
    @Test
    public void testChangeData() throws IOException {
        File file = createSampleFile();

        int tri = 0;

        do{
            InputStream in = new FileInputStream(file);
            byte[] digest = DigestUtils.md5(in);
            String checksum = new String(new Hex().encodeHex(digest));

            IOUtils.closeQuietly(in);

            System.out.println(tri+"."+checksum);

            File tmpfile = update(file, checksum);

            if(tri++>10){
                break;
            }
        }while(true);
    }

    private File update(File file, String checksum) throws IOException {
        File tmp = File.createTempFile("0000", ".txt");

        InputStream in = new FileInputStream(file);
        OutputStream out = new FileOutputStream(tmp);

        IOUtil.copyCompletely(in, out);

        IOUtils.closeQuietly(in);
        IOUtils.closeQuietly(out);

        PrintWriter w = new PrintWriter(tmp);
        w.println(checksum);

        IOUtils.closeQuietly(w);
        return tmp;
    }

    private File createSampleFile() throws IOException {
        File file = File.createTempFile("0000", ".txt");

        System.out.println("..."+file.getCanonicalPath());

        FileOutputStream out= new FileOutputStream(file);

        PrintWriter writer = new PrintWriter(out);
        writer.println("This is a simple test");
        IOUtils.closeQuietly(writer);

        return file;
    }
}
