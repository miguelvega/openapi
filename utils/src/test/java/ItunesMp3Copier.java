import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by miguel on 4/19/2015.
 */
public class ItunesMp3Copier {
    public static void main(String[] args) throws IOException {
        M3UCopy(new File("/home/miguel/hindi.m3u"), new File("/home/miguel/Music/copy"));
    }

    static void M3UCopy(File file, File output){
        try(FileInputStream source = new FileInputStream(file)){
            Scanner scanner = new Scanner(source);
            scanner.useDelimiter("\n");
            AtomicBoolean found = new AtomicBoolean(false);
            while(scanner.hasNext()){
                String line = scanner.next();
//                System.out.println(">>>"+line);
                if(found.get()){
                    Path path = Paths.get(line);
                    Path copy = Files.copy(path, Paths.get(output.getAbsolutePath() + "/" + path.getFileName()));
                    System.out.println(copy.toAbsolutePath().toString()+" copied");
                    found.set(false);
                    continue;
                }

                int indexOf = line.indexOf("#EXTINF:");
                found.set(indexOf ==0);
            }
        }catch(IOException x){
            x.printStackTrace();
        }
    }

    static void plainTextCopier(File f) throws IOException {
        //File f = new File("C:\\media\\OST.txt");

        CSVReader reader = new CSVReader(new FileReader(f), '\t', '\"', '*');

        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {

            String x = nextLine[nextLine.length - 1];

            System.out.println(x);

            Path source = Paths.get(x);

            if (Files.isDirectory(source) || !Files.exists(source)) continue;

            Path copy = Files.copy(source, Paths.get("c:/media/" + source.getFileName()));
            System.out.println("copied to..." + copy.getFileName());
        }
    }
}
