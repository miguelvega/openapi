package org.openlogics.utils.el;

/**
 * @author Miguel Vega
 * @version $Id: ELExpr.java 0, 5/8/14 5:11 PM, @miguel $
 */
public interface ELExpr<T, U> {
    T evaluate(U u);
}
