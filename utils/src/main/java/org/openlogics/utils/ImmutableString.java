package org.openlogics.utils;

import javax.annotation.Nullable;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Miguel Vega
 * @version $Id: ImmutableString.java 0, 2013-05-10 2:06 PM mvega $
 */
public class ImmutableString {
    private final StringBuilder sb;

    ImmutableString(@Nullable Object s) {
        sb = new StringBuilder();
        add(s);
    }

    public static ImmutableString of(Object s) {
        return new ImmutableString(s);
    }

    public static ImmutableString of() {
        return new ImmutableString("");
    }

    /**
     * Will remove all the text after the given 'regex' included the 'regex' string
     *
     * @param regex
     * @return a new instance
     */
    public ImmutableString removeFromLastMatch(String regex) {
        assert regex != null;

        final String s = build();
        try {
            return new ImmutableString(s.substring(0, s.lastIndexOf(regex)));
        } catch (StringIndexOutOfBoundsException x) {
            return new ImmutableString(s);
        }
    }

    /**
     * Removes all tex strings that area before 'regex' including such that 'regex'
     * @param regex
     * @return
     */
    public ImmutableString removeToFirstMatch(String regex) {
        assert regex != null;

        final String s = build();

        try {
            return new ImmutableString(s.substring(s.indexOf(regex) + regex.length()));
        } catch (StringIndexOutOfBoundsException x) {
            return new ImmutableString(s);
        }
    }

    private ImmutableString encloseWith(String source, String prepend, String append) {
        return ImmutableString.of(prepend).add(source).add(append);
    }

    public ImmutableString encloseWith(String string) {
        return encloseWith(build().trim(), string, string);
    }

    public ImmutableString encloseWith(String prepend, String append) {
        return encloseWith(build().trim(), prepend, append);
    }

    public boolean isEmpty() {
        return sb.toString().trim().length()==0;
    }

    /**
     * Line feed
     *
     * @return                   sb
     */
    public ImmutableString ln() {
        sb.append(System.getProperty("line.separator"));
        return this;
    }

    public ImmutableString add(ImmutableString immStr) {
        add(immStr.build());
        return this;
    }

    public ImmutableString add(Object obj) {
        if (obj == null) return this;

        String s = String.class.isAssignableFrom(obj.getClass()) ? (String) obj : String.valueOf(obj);
        if (!isNullOrEmpty(s)) {
            sb.append(s);
        }
        return this;
    }

    public final String build() {
        final String tmp = sb.toString();
        //sb.delete(0, sb.length());
        return tmp;
    }

    @Override
    public String toString() {
        return build();
    }
}
