package org.openlogics.utils;

import com.google.common.base.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Miguel Vega
 * @version $Id: Numbers.java 0, 2013-05-09 7:14 PM mvega $
 */
public class Numbers {
    public static boolean isInteger(String num) {
        Optional.fromNullable("").or("");
        if (isNullOrEmpty(num))
            return false;

        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    public static boolean isLong(String num) {
        if (isNullOrEmpty(num))
            return false;

        try {
            Long.parseLong(num);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    public static long asLong(String value) {
        return asLong(value, -1l);
    }

    public static long asLong(String value, long optional) {
        try {
            return Long.parseLong(value.trim());
        } catch (Exception x) {
            return optional;
        }
    }

    public static int asInt(String value) {
        return asInt(value, -1);
    }

    public static int asInt(String value, int optional) {
        try {
            return Integer.parseInt(value.trim());
        } catch (Exception x) {
            return optional;
        }
    }
}
