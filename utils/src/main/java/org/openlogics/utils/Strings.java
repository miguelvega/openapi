package org.openlogics.utils;

import com.google.common.base.Optional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Miguel Vega
 * @version $Id: Strings.java 0, 2013-11-17 5:57 PM mvega $
 */
public class Strings {

    /**
     * Produces a new String instance from all objects provided as an array
     * @param values
     * @return
     */
    public static final String asText(Object... values){
        final StringBuffer stringBuffer = new StringBuffer();
        for (Object value : values) {
            Object or = Optional.fromNullable(value).or("");
            stringBuffer.append(or.toString());
        }
        return stringBuffer.toString();
    }

    /**
     * Search pattern is source text, where text is a REGEX. This only works for the first match.
     * @param source
     * @param text
     * @return true if pattern is found in source text, false if not
     */
    public static Optional<String> stringBefore(String source, String text){
        Pattern pattern = Pattern.compile(text);
        Matcher matcher = pattern.matcher(source);
        //only the first match will be reported
        if(matcher.find()){
            return Optional.of(source.substring(0, matcher.start()));
        }
        return Optional.absent();
    }

    /**
     * Search pattern is source text, where text is a REGEX. This only works for the first match.
     * @param source
     * @param text
     * @return true if pattern is found in source text, false if not
     */
    public static Optional<String> stringAfter(String source, String text){
        Pattern pattern = Pattern.compile(text);
        Matcher matcher = pattern.matcher(source);
        //only the first match will be reported
        if(matcher.find()){
            return Optional.of(source.substring(matcher.end()));
        }
        return Optional.absent();
    }

    /**
     * Retrieves the String value of the given object, if object is NULL, then return s an empty String.
     * @param obj
     * @return
     */
    public static final String toText(Object obj){
        if(obj!=null)
            return String.valueOf(obj);
        return "";
    }

    /**
     * Retrieves the value of given object, if object is
     * @param obj
     * @return
     */
    public static final String fromNullable(Object obj){
        if(obj!=null)
            return String.valueOf(obj);
        return null;
    }
}
