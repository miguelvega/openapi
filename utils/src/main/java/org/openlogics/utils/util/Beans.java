package org.openlogics.utils.util;

import org.openlogics.utils.Exceptions;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;

/**
 * @author Miguel Vega
 * @version $Id: Beans.java 0, 2013-07-23 6:53 PM mvega $
 */
public class Beans {
    public static String toString(Object o) {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append(o.getClass().getName());
        result.append(" Object {");
        result.append(newLine);

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = o.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getName());
                result.append(": ");
                //requires access to private field:
                //result.append( field.get(o) );
                result.append(new PropertyDescriptor(field.getName(), o.getClass()).getReadMethod().invoke(o));
            } catch (Exception ex) {
                result.append(Exceptions.toString(ex));
                //System.out.println(ex);
            }
            result.append(newLine);
        }
        result.append("}");

        return result.toString();
    }
}