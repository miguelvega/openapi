package org.openlogics.utils.collect;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author Miguel Vega
 * @version $Id: FnCollections.java; nov 11, 2014 10:32 AM mvega $
 */
public class FnCollections {

    private final static Logger LOGGER = LoggerFactory.getLogger(FnCollections.class);

    public static final <T> List<T> immutableList(T... elements){
        return Collections.unmodifiableList(Arrays.asList(elements));
    }

    public static final <U, V> Map<U, V> immutableMapOfSet(List<U> us, List<V> vs){
        return (Map<U, V>) immutableMapOfSet(us.toArray(), vs.toArray());
    }

    public static final <U, V> Map<U, V> immutableMapOfSet(U[] us, V[] vs){
        Preconditions.checkNotNull(us, "List of map KEYS can't be null");
        Preconditions.checkNotNull(us, "List of map VALUES can't be null");

        int len = Math.min(us.length, vs.length);
        final Map<U, V> map = new HashMap<U, V>(len);

        if(us.length!=vs.length)
            LOGGER.warn("Given sets of pairs do not match in length");

        for (int i = 0; i < len; i++) {
            U u = us[i];
            tryPut(map, u, vs[i], i);
        }

        return Collections.unmodifiableMap(map);
    }

    public static final <U, V> Map<U, V> immutableMap(U u0, V v0, U u1, V v1, U u2, V v2, U u3, V v3, U u4, V v4){
        final Map<U, V> map = new HashMap<U, V>(5);
        map.put(u0, v0);
        map.put(u1, v1);
        map.put(u2, v2);
        map.put(u3, v3);
        map.put(u4, v4);
        return Collections.unmodifiableMap(map);
    }

    public static final <U, V> Map<U, V> immutableMap(U u0, V v0, U u1, V v1, U u2, V v2, U u3, V v3){
        final Map<U, V> map = new HashMap<U, V>(4);
        map.put(u0, v0);
        map.put(u1, v1);
        map.put(u2, v2);
        map.put(u3, v3);
        return Collections.unmodifiableMap(map);
    }

    public static final <U, V> Map<U, V> immutableMap(U u0, V v0, U u1, V v1, U u2, V v2){
        final Map<U, V> map = new HashMap<U, V>(3);
        map.put(u0, v0);
        map.put(u1, v1);
        map.put(u2, v2);
        return Collections.unmodifiableMap(map);
    }

    public static final <U, V> Map<U, V> immutableMap(U u0, V v0, U u1, V v1){
        final Map<U, V> map = new HashMap<U, V>(2);
        map.put(u0, v1);
        map.put(u1, v0);
        return Collections.unmodifiableMap(map);
    }

    public static final <U, V> Map<U, V> immutableMap(U u, V v){
        final Map<U, V> map = new HashMap<U, V>(1);
        map.put(u, v);
        return Collections.unmodifiableMap(map);
    }

    public final static <A, B> Map<A, B> immutableMapOf(Pair<A, B>... pairs){
        final Map<A, B> map = new HashMap<A, B>(pairs.length);
        for (int i = 0; i < pairs.length; i++) {
            tryPut(map, pairs[i].id, pairs[i].object, i);
        }

        return Collections.unmodifiableMap(map);
    }

    private static final <U, V> Map<U, V> tryPut(Map<U, V> map, U u, V v, int index){
        if(u == null) {
            LOGGER.warn("Excluding the {}-th element of the sets, because KEY can not be NULL", index);
            return map;
        }
        map.put(u, v);
        return map;
    }
}