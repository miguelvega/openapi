package org.openlogics.utils.collect;

import com.google.common.collect.Lists;

import java.util.*;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author Miguel Vega
 * @version $Id: CollectionsOp.java 0, 2013-06-10 3:43 PM mvega $
 */
public class CollectionsOp {

    /**
     * @param listOne
     * @param listTwo
     * @param <T>
     * @return
     */
    public static <T> Collection<T> difference(Collection<T> listOne, Collection<T> listTwo) {
        List<T> res = Lists.newArrayList();

        for (T el : listOne) {
            if (!listTwo.contains(el)) {
                res.add(el);
            }
        }

        return res;
    }

    /**
     * @param listOne
     * @param listTwo
     * @param <T>
     * @return
     */
    public static <T> Collection<T> intersection(Collection<T> listOne, Collection<T> listTwo) {
        List<T> result = Lists.newArrayList();
        result.addAll(listOne);

        result.retainAll(listTwo);

        return result;
    }

//    public static <T> List<T> union(List<T> listOne, List<T> listTwo){
//        List<T> result = newArrayList();
//
//        result.addAll(listOne);
//        result.addAll(listTwo);
//
//        return result;
//    }

    /**
     * Retrieves the union of both lists
     *
     * @param listOne
     * @param listTwo
     * @param <T>
     * @return
     */
    public static <T> Collection<T> union(Collection<T> listOne, Collection<T> listTwo) {
        Set<T> set = new LinkedHashSet<T>();

        set.addAll(listOne);
        set.addAll(listTwo);

        return Collections.unmodifiableList(new ArrayList<T>(set));
    }

    /**
     * Permforms union of two lists
     * @param listOne
     * @param listTwo
     * @param <T>
     * @return
     */
//    public <T> List<T> intersection(List<T> listOne, List<T> listTwo) {
//        List<T> list = new ArrayList<T>();
//        for (T t : listOne) {
//            if(listTwo.contains(t)) {
//                list.add(t);
//            }
//        }
//        return list;
//    }
}
