package org.openlogics.utils.collect;

import com.google.common.base.Preconditions;

/**
 * @author Miguel Vega
 * @version $Id: Pair.java; nov 11, 2014 11:19 AM mvega $
 */
public class Pair<S, T> {
    final S id;
    final T object;

    Pair(S u, T v) {
        Preconditions.checkNotNull(u, "A NULL value is not a valid identifier");
        this.id = u;
        this.object = v;
    }

    public static final <S, T> Pair of(S id, T object){
        return new Pair(id, object);
    }

    public boolean equals(Pair pair){
        if(pair==null)return false;
        return pair.id.equals(id) && pair.object.equals(object);
    }

    public S getId(){
        return id;
    }

    public T getObject(){
        return object;
    }
}
