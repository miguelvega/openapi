package org.openlogics.utils.io;

import java.io.File;
import java.io.FileFilter;
import java.net.URL;

/**
 * @author Miguel Vega
 * @version $Id: Files.java 0, 2013-10-01 12:54 PM mvega $
 */
public class Files {
    /**
     * Retrieves the last accessible file which might contains the given class.
     * The class can be within the JAR file, which is returned.
     * Or the class might be inside another JAR within the JAR file instance to be returned.
     * However, this won't return the sub JAR path, but the parent JAR file reference.
     *
     * @param clazz this class must belong to the JAR file
     * @return a file
     */
    public static final File getAccessibleFileSystemPath(Class clazz) {
        URL location = clazz.getProtectionDomain().getCodeSource().getLocation();
        String path = location.toString();

        path = path.substring(path.indexOf("/"), path.length());
        //the following works well on a JAR file
        try {
            path = path.substring(0, path.lastIndexOf("!"));
            //to get the parent
            //path = path.substring(0, path.lastIndexOf("/"));//something like: /home/miguel/app
            return new File(path);
        } catch (StringIndexOutOfBoundsException x) {
            //the class couldn't be found inside a JAR, it's available from directories
            return new File(path);
        }
    }

    /**
     * Looks up the required directory starting in the provided directory
     * @param directory
     * @return if the directory is not found, then returns null.
     */
    public static final File findDirectoryBackward(File directory, final String required){
        assert directory!=null && required != null;

        File cur = new File(directory.getAbsolutePath());

        while(cur!=null){
            File[] files = cur.listFiles(new FileFilter() {
                @Override
                public boolean accept(File child) {
                    return child.isDirectory() && child.getName().equals(required);
                }
            });

            if(files!=null && files.length==1){
                return files[0];
            }

            if(cur.getName().equals(required))return cur;

            cur = cur.getParentFile();
        }

        return null;
    }
}
