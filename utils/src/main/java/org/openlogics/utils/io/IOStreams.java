package org.openlogics.utils.io;

import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;

import java.io.*;
import java.nio.CharBuffer;

/**
 * @author Miguel Vega
 * @version $Id: IOStreams.java 0, 2013-06-01 01:48 mvega $
 */
public class IOStreams {
    /**
     * Counts the number of lines in a file.
     *
     * @param inputStream text plain input stream
     * @return
     * @throws java.io.IOException
     */
    public static int countLines(InputStream inputStream) throws IOException {
        LineNumberReader reader = null;
        try {
            reader = new LineNumberReader(new InputStreamReader(inputStream));
            while ((reader.readLine()) != null) ;
            return reader.getLineNumber() + 1;
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    public static String toString(final InputStream inputStream) throws IOException {
        return new String(ByteStreams.toByteArray(inputStream));
    }

    /**
     * silently closes an {@link java.io.InputStream} object
     *
     * @param inputStream
     */
    public static void close(InputStream inputStream) {
        if (inputStream == null) return;
        try {
            inputStream.close();
        } catch (IOException e) {
        }
    }

    /**
     * Silently closes the reader
     * @param reader
     */
    public static void close(Reader reader) {
        if (reader == null) return;
        try {
            reader.close();
        } catch (IOException e) {
        }
    }

    /**
     * silently closes an {@link java.io.OutputStream} object
     *
     * @param outputStream
     */
    public static void close(OutputStream outputStream) {
        if (outputStream == null) return;
        try {
            outputStream.close();
        } catch (IOException e) {
        }
    }

    public static void close(Writer writer) {
        if (writer == null) return;
        try {
            writer.close();
        } catch (IOException e) {
        }
    }
}
