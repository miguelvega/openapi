package org.openlogics.utils.io;

import java.io.ByteArrayOutputStream;

/**
 * @author Miguel Vega
 * @version $Id: StringOutputStream.java 0, 2013-06-24 6:31 PM mvega $
 */
public class StringOutputStream extends ByteArrayOutputStream {

    public StringOutputStream() {

    }

    public StringOutputStream(int capacity) {
        super(capacity);
    }

    public String getString() {
        return new String(toByteArray());
    }
}