package org.openlogics.utils;

import java.sql.SQLException;

/**
 * @author Miguel Vega
 * @version $Id: Exceptions.java 0, 2013-07-23 6:00 PM mvega $
 */
public class Exceptions {
    public static final String toString(Throwable throwable) {
        if (throwable == null) return "";

        ImmutableString str = ImmutableString.of(throwable.getMessage());

        if (throwable instanceof SQLException) {
            SQLException ex = (SQLException) throwable;

            while ((ex = ex.getNextException()) != null) {
                str.add(", caused by: ").add(ex.getMessage());
            }

        } else {

            while ((throwable = throwable.getCause()) != null) {
                str.add(", caused by: ").add(throwable.getMessage());
            }
        }

        return str.build();
    }
}
