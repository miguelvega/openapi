package org.openlogics.utils;

/**
 * @author Miguel Vega
 * @version $Id: Attempt.java 0, 8/22/14 9:29 PM, @miguel $
 */
public abstract class Attempt<V>{
    final String attemptDescription;

    public Attempt(String attemptDescription) {
        this.attemptDescription = attemptDescription;
    }

    public String getAttemptDescription() {
        return attemptDescription;
    }

    public abstract V doTry() throws Throwable;
}