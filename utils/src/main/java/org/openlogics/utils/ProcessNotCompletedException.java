package org.openlogics.utils;

/**
 * A MESSAGE has left being correctly structured when this exception occurs
 * @author Miguel Vega
 * @version $Id: IntegrityDataException.java 0, 8/22/14 2:54 PM, @miguel $
 */
public class ProcessNotCompletedException extends Throwable {
    public ProcessNotCompletedException(String s, Throwable e) {
        super(s, e);
    }
}