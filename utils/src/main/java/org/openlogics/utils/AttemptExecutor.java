package org.openlogics.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class allows to perform executions avoiding some expected error. WIll try to perform a routine by a certain
 * number of times, if this process doesn't complete successfully, an exception of type {@link ProcessNotCompletedException}
 * is thrown.
 *
 * @author Miguel Vega
 * @version $Id: ContingencyAttempt.java 0, 8/22/14 9:24 PM, @miguel $
 */
public class AttemptExecutor<T, U extends Throwable> {

    final Logger LOGGER;

    /**
     * Number of tries to  perform before throwing an {@link ProcessNotCompletedException}
     */
    final int maxAttemptTries;

    /**
     * If an attempt fails before reaches the limit of tries (maxAttemptTries), operation will sleep for a while
     * defined in this variable
     */
    final long timeUntilNextAttempt;

    /**
     * There are many times where an exception can be produced while executing a process, if we can handle such that
     * exceptions, those will be declared here.
     */
    final Class<U> possibleExcepctedError;

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int maxAttemptTries){
        return new AttemptExecutor<X, Y>(maxAttemptTries, 10000l, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(long timeUntilNextAttempt){
        return new AttemptExecutor<X, Y>(10, timeUntilNextAttempt, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int maxAttemptTries, long timeUntilNextAttempt){
        return new AttemptExecutor<X, Y>(maxAttemptTries, timeUntilNextAttempt, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(){
        return createExecutor(null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(Class<Y> possibleExcepctedError){
        return new AttemptExecutor<X, Y>(10, 10000l, possibleExcepctedError);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            long timeUntilNextAttempt, Class<Y> type){
        return new AttemptExecutor<X, Y>(10, timeUntilNextAttempt, type);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int maxAttemptTries,
            Class<Y> possibleExcepctedError){
        return new AttemptExecutor<X, Y>(maxAttemptTries, 10000l, possibleExcepctedError);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int maxAttemptTries,
            long timeUntilNextAttempt,
            Class<Y> type){
        return new AttemptExecutor<X, Y>(maxAttemptTries, timeUntilNextAttempt, type);
    }

    AttemptExecutor(int maxAttemptTries, long timeUntilNextAttempt, Class<U> possibleExcepctedError) {
        this.maxAttemptTries = maxAttemptTries;
        this.LOGGER = LoggerFactory.getLogger(getClass());
        this.timeUntilNextAttempt = timeUntilNextAttempt;
        this.possibleExcepctedError = possibleExcepctedError;
    }

    public T execute(final Attempt<T> attempt) throws ProcessNotCompletedException, U {
        return this.execute(attempt, "Unable to perform {}. A unrecoverable state has been reached, contact immediately with administrator.");
    }

    public T execute(final Attempt<T> attempt, final String messageOnFailure) throws ProcessNotCompletedException, U {

        final AtomicInteger count = new AtomicInteger(0);

        Throwable caught = null;

        do {
            try {

                count.set(count.incrementAndGet());

                T call = attempt.doTry();

                return call;

            }catch (Throwable throwable){

                if(possibleExcepctedError !=null && throwable.getClass().isAssignableFrom(possibleExcepctedError)){
                    throw (U)throwable;
                }

                LOGGER.warn(" ***************** Contingency attempt has failed {} time(s) while executing {} process, {} tries remain", new Object[]{count.get(), attempt.getAttemptDescription(), maxAttemptTries-count.get()});

                if (count.get() >= maxAttemptTries) {
                    break;
                }

                caught = throwable;

                try {
                    Thread.sleep(timeUntilNextAttempt);
                } catch (InterruptedException e) {
                }
            }
        } while (true);

        throw new ProcessNotCompletedException(messageOnFailure, caught);
    }

    public T executeUnchecked(final Attempt<T> attempt) throws ProcessNotCompletedException {
        return executeUnchecked(attempt, "Unable to perform {}. A unrecoverable state has been reached, contact immediately with administrator.");
    }

    public T executeUnchecked(final Attempt<T> attempt, final String messageOnFailure) throws ProcessNotCompletedException {

        final AtomicInteger count = new AtomicInteger(0);

        Throwable caught = null;

        do {
            try {

                count.set(count.incrementAndGet());

                T call = attempt.doTry();

                return call;

            }catch (Throwable throwable){

                LOGGER.warn(" X X X X X X X X Contingency attempt has failed while executing: "+attempt.getAttemptDescription()+", will try up to "+maxAttemptTries+" current try is "+count.get(), throwable);

                if (count.get() == maxAttemptTries) {
                    break;
                }

                caught = throwable;

                try {
                    Thread.sleep(timeUntilNextAttempt);
                } catch (InterruptedException e) {
                }
            }
        } while (true);

        throw new ProcessNotCompletedException(messageOnFailure, caught);
    }
}
