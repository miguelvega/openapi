package org.openlogics.utils;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * A generic replacement for
 * @author Miguel Vega
 * @version $Id: Immutable.java 0, 8/20/14 3:50 PM, @miguel $
 */
public abstract class Builder<T extends Serializable> {

    public abstract <U extends Builder<T>> U append(T data);

    public abstract <U extends Builder<T>> U append(Object data);

    static final Map<Class, Class<? extends Builder>> factory = new ConcurrentHashMap<Class, Class<? extends Builder>>();

    static {
        registerBuilder(String.class, new Builder<String>() {
            private final StringBuilder sb;

            {
                sb = new StringBuilder();
            }

            @Override
            public Builder<String> append(String data) {
                sb.append(data);
                return this;
            }

            @Override
            public Builder<String> append(Object data) {

                assert data != null;

                String s = String.class.isAssignableFrom(data.getClass()) ? (String) data: String.valueOf(data);
                if (!isNullOrEmpty(s)) {
                    sb.append(s);
                }

                return this;
            }
        });
    }

    public static <U extends Serializable> Builder<U> of(U data){

        assert data != null;

        Builder<U> builder;
        try {
            builder = factory.get(data.getClass()).newInstance();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }

        return builder.append(data);
    }

    public static <U extends Serializable> void registerBuilder(Class<U> type, Builder<U> builder){
        factory.put(type, builder.getClass());
    }
}