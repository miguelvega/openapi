package org.openlogics.utils.naming;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Hashtable;

import static org.openlogics.utils.naming.ContainerNamingContext.*;

/**
 * @author Miguel Vega
 * @version $Id: JNDIContainerContext.java; dic 02, 2014 02:49 PM mvega $
 */
public abstract class ContainerNamingContextFactory {

    public static final void configureContainer(Hashtable contextEnv) throws EnvironmentConfigurationException {
        try {
            Context ctxt = new InitialContext(contextEnv);
            ctxt.createSubcontext(JAVA);
            ctxt.createSubcontext(JAVA_COMP);
            ctxt.createSubcontext(JAVA_COMP_ENV);
            ctxt.createSubcontext(JAVA_COMP_ENV_JDBC);
        } catch (Throwable x) {
            throw new EnvironmentConfigurationException("Unable to deploy a environment configuration.", x);
        }
    }

    public static final void configureDefaultCatalinaContainer() throws EnvironmentConfigurationException {
        try {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

            Context ic = new InitialContext();

            ic.createSubcontext(JAVA);
            ic.createSubcontext(JAVA_COMP);
            ic.createSubcontext(JAVA_COMP_ENV);
            ic.createSubcontext(JAVA_COMP_ENV_JDBC);
        } catch (Throwable x) {
            throw new EnvironmentConfigurationException("Unable to deploy a environment configuration.", x);
        }
    }
}