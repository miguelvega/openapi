package org.openlogics.utils.naming;

/**
 * @author Miguel Vega
 * @version $Id: EnvironmentConfigurationException.java; dic 02, 2014 04:13 PM mvega $
 */
public class EnvironmentConfigurationException extends Throwable{
    public EnvironmentConfigurationException(String message) {
        super(message);
    }

    public EnvironmentConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
