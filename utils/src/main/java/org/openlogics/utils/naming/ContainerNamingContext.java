package org.openlogics.utils.naming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import static org.openlogics.utils.ImmutableString.of;
import static org.openlogics.utils.Strings.stringAfter;
import static org.openlogics.utils.Strings.stringBefore;

/**
 * This allows set up a COntext environment naming to make it accessible via {@link javax.naming.Context}
 *
 * To build a mocked environment, call any of the static methods for {@link org.openlogics.utils.naming.ContainerNamingContextFactory}.
 *
 * {@link javax.naming.NoInitialContextException} is thrown when environment has not been configured.
 *
 * @author Miguel Vega
 * @version $Id: JNDIWrapper.java; dic 02, 2014 01:24 PM mvega $
 */
public class ContainerNamingContext {

    private static final Logger log = LoggerFactory.getLogger(ContainerNamingContext.class);

    public static final String JAVA_COMP_ENV_JDBC = "java:/comp/env/jdbc";
    public static final String JAVA_COMP_ENV = "java:/comp/env";
    public static final String JAVA_COMP = "java:/comp";
    public static final String JAVA = "java:";

    /*static {
        try {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,
                    "org.apache.naming");

            Context ic = new InitialContext();

            ic.createSubcontext(JAVA);
            ic.createSubcontext(JAVA_COMP);
            ic.createSubcontext(JAVA_COMP_ENV);
            ic.createSubcontext(JAVA_COMP_ENV_JDBC);
        } catch (NamingException e) {
            throw new IllegalStateException("An unexpected error has occurred", e);
        }
    }*/

    public static Context lookupJDBCContext() throws NamingException {
        Context ic = new InitialContext();
        return (Context) ic.lookup(JAVA_COMP_ENV_JDBC);
    }

    public static synchronized Context lookupEnvironmentContext() throws NamingException {
        return (Context) new InitialContext().lookup(JAVA_COMP_ENV);
    }

    public static synchronized Context lookupContainerContext() throws NamingException {
        return (Context) new InitialContext().lookup(JAVA_COMP);
    }

    /**
     * Installs a new context reference within: java:/comp/env/jdbc/
     *
     * @param jndiRef
     * @param dataSource
     * @throws NamingException
     */
    public static synchronized void installJDBCDataSource(String jndiRef, DataSource dataSource) throws NamingException {
        Context context = new InitialContext();
        String name = of(JAVA_COMP_ENV_JDBC).add("/").add(jndiRef).build();
        context.bind(name, dataSource);
    }

    /**
     * This method
     *
     * @param fullyQualifiedName looks like 'java:/comp/env/jdbc/nameofjdbcresource'
     * @param dataSource
     */
    public static synchronized void bindFullyQualifiedInstance(String fullyQualifiedName, DataSource dataSource) throws NamingException {

        final Context ic = new InitialContext();

        if (fullyQualifiedName.contains(":")) {
            //retrieve the first part of the String to start parsing
            String prefix = stringBefore(fullyQualifiedName, "/").get();
//            ic.createSubcontext(prefix);
            createSubcontext(ic, prefix);

            log.info("Binding to context: {}", prefix);

            String body = stringAfter(fullyQualifiedName, "/").get();
            String part;
            while (!(part = stringBefore(body, "/").or("")).isEmpty()) {
                body = stringAfter(body, "/").get();
                prefix = of(prefix).add("/").add(part).build();
//                ic.createSubcontext(prefix);
                createSubcontext(ic, prefix);

                log.info("Binding to context: {}", prefix);
            }

            String finalJNDIRef = of(prefix).add("/").add(body).build();
            ic.bind(finalJNDIRef, dataSource);

            log.info("Object '{}' has been successfully bound in context", finalJNDIRef);
        }
    }

    private static synchronized void createSubcontext(Context context, String subcontext) throws NamingException {
        if(context.lookup(subcontext)==null)
            context.createSubcontext(subcontext);
    }
}
