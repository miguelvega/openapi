package org.openapi.datamigrator.extract;

import org.openapi.datamigrator.FieldMetaData;

/**
 * A function interface
 * @author Miguel A. Vega P.
 * @version $Id: Extractor.java; Jun 04, 2016. 11:31 PM mvega $
 * @source $URL$
 */
public interface Extractor<X>{
    /**
     * This transformer an contain many reslts and with thsi method will move to the next result of the cursor
     * @return true if ther are more results to visit
     */
    boolean next();

    /**
     * Input fields metadata
     * @return
     */
    FieldMetaData getFieldMetaData();

    /**
     * Returns the value for the current for to the given field name
     * @param fieldName
     */
    <T> T getValueOf(String fieldName);

    /**
     * Returns the value for the current for to the given field name
     * @param index
     * @param <T>
     * @return
     */
    <T> T getValueOf(int index);
}