package org.openapi.datamigrator.load;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Loader.java; Jun 05, 2016. 12:21 AM mvega $
 * @source $URL$
 */
public interface Loader<Y>{
    /**
     * When starting reading a new row
     */
    void prepare();

    /**
     * After a new row has been advised to be read, this is where a cell value comes in
     * @param target
     * @param value1
     */
    void setValue(Object target, String value1);

    /**
     * the entire row has been read
     */
    void detach();
}
