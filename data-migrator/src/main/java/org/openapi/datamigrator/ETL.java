package org.openapi.datamigrator;

import org.openapi.datamigrator.csv.TransformerImpl;
import org.openapi.datamigrator.extract.Extractor;
import org.openapi.datamigrator.load.Loader;
import org.openapi.datamigrator.transform.FieldMapping;
import org.openapi.datamigrator.transform.Transformer;

/**
 * @author Miguel Vega
 * @version $Id: Loader.java 0, 2013-05-21 12:00 PM mvega $
 */
public class ETL <X, Y>{

    void doETL(Extractor<X> extractor, Loader<Y> loader, FieldMapping<X, Y> fieldMapping){

        Transformer<X, Y> transformer = new TransformerImpl<>(extractor, loader);

        while(extractor.next()){
            Y y = transformer.transformTuple(extractor, loader, fieldMapping);
        }
    }
}
