package org.openapi.datamigrator;

/**
 * @author Miguel Vega
 * @version $Id: ETLException.java 0, 2013-05-21 12:01 PM mvega $
 */
public class ETLException extends Exception {
    private Type type;

    public enum Type{
        LOAD, TRANSFORM, EXTRACT;
    }

    public ETLException(String message) {
        super(message);
    }

    public ETLException(String message, Throwable cause) {
        super(message, cause);
    }

    public ETLException(Throwable cause) {
        super(cause);
    }

    /*protected ETLException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }*/

    public ETLException setType(Type type){
        this.type = type;
        return this;
    }
}
