package org.openapi.datamigrator;

import com.google.common.base.Function;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains the source field names, types and so on.
 * @author Miguel A. Vega P.
 * @version $Id: FieldMetaData.java; Jun 05, 2016. 12:50 AM mvega $
 * @source $URL$
 */
public class FieldMetaData {

    final BiMap<String, Integer> fieldNames;

    final Map<String, Function<?, ?>> transformations = new HashMap(0);

    public FieldMetaData(List<String> fields) {

        this.fieldNames = HashBiMap.create(fields.size());

        for (int i = 0; i < fields.size(); i++) {
            fieldNames.put(fields.get(i), i);
        }
    }

    /*public <X, Y> Iterator<Pair<String, Function<X, Y>>> getFieldIterator(){

        final Iterator<String> names = fieldNames.iterator();

        return new Iterator<Pair<String, Function<X, Y>>>() {

            @Override
            public boolean hasNext() {
                return names.hasNext();
            }

            @Override
            public Pair<String, Function<X, Y>> next() {
                String field = names.next();
                return new Pair(field, (Function<X, Y>) Optional.ofNullable(transformations.get(field)).orElse(new Function() {
                    @Nullable
                    @Override
                    public Object apply(@Nullable Object input) {
                        return input;
                    }
                }));
            }
        };
    }*/

    public BiMap<String, Integer> getFieldNames() {
        return fieldNames;
    }

    public Map<String, Function<?, ?>> getTransformations() {
        return transformations;
    }
}