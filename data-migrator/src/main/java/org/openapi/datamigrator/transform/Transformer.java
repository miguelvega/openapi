package org.openapi.datamigrator.transform;

import org.openapi.datamigrator.extract.Extractor;
import org.openapi.datamigrator.load.Loader;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Transformer.java; Jun 05, 2016. 12:17 AM mvega $
 * @source $URL$
 */
public interface Transformer<X, Y> {

    /**
     *
     * @param extractor the input from extractor
     * @param loader
     * @param fieldMapping the transformations to apply
     * @return the value for loader
     */
    Y transformTuple(Extractor<X> extractor, Loader<Y> loader, FieldMapping<X, Y> fieldMapping);
}