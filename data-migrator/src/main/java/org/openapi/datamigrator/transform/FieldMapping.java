package org.openapi.datamigrator.transform;

import com.google.common.base.Function;
import org.javatuples.Pair;

import java.util.List;
import java.util.Map;

/**
 * @author Miguel A. Vega P.
 * @version $Id: FieldMapping.java; Jun 05, 2016. 1:46 AM mvega $
 * @source $URL$
 */
public class FieldMapping<X, Y> {

    final List<Pair<String, String>> mapping;

    final Map<Integer, Function<X, Y>> transforms;

    public FieldMapping(List<Pair<String, String>> mapping, Map<Integer, Function<X, Y>> transforms) {
        super();
        this.mapping = mapping;
        this.transforms = transforms;
    }

    public List<Pair<String, String>> getMapping() {
        return mapping;
    }

    public Map<Integer, Function<X, Y>> getTransforms() {
        return transforms;
    }
}