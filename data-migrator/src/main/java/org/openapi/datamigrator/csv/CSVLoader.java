package org.openapi.datamigrator.csv;

import com.google.common.collect.Lists;
import org.openapi.datamigrator.load.Loader;

import java.util.List;

/**
 * @author Miguel A. Vega P.
 * @version $Id: CSVLoader.java; Jun 05, 2016. 12:24 AM mvega $
 * @source $URL$
 */
public class CSVLoader implements Loader<String[]>{

    final List data = Lists.newLinkedList();

    @Override
    public void prepare() {

    }

    @Override
    public void setValue(Object target, String value1) {

    }

    @Override
    public void detach() {

    }
}
