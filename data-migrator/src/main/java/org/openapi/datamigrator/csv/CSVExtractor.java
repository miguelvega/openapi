package org.openapi.datamigrator.csv;

import com.google.common.collect.Lists;
import com.opencsv.CSVReader;
import org.openapi.datamigrator.FieldMetaData;
import org.openapi.datamigrator.extract.Extractor;

import java.io.IOException;
import java.io.Reader;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Miguel A. Vega P.
 * @version $Id: CSVExtractor.java; Jun 04, 2016. 11:34 PM mvega $
 * @source $URL$
 */
public class CSVExtractor implements Extractor<String[]> {

    final CSVReader csvReader;

    final FieldMetaData inputMetaData;

    final AtomicReference<String[]> ref = new AtomicReference<>(null);

    public CSVExtractor(Reader reader) {
        this(new CSVReader(reader));
    }

    public CSVExtractor(Reader reader, char separator) {
        this(new CSVReader(reader, separator));
    }

    public CSVExtractor(CSVReader csvReader) {
        try {
            this.csvReader = csvReader;
            String[] header = csvReader.readNext();
            this.inputMetaData = new FieldMetaData(Lists.newArrayList(header));
        } catch (IOException e) {
            throw new IllegalStateException("An unexpected error occurred", e);
        }
    }


    public FieldMetaData getFieldMetaData() {
        return inputMetaData;
    }

    @Override
    public <T> T getValueOf(String fieldName) {
        inputMetaData.getFieldNames();
        return (T) ref.get()[inputMetaData.getFieldNames().get(fieldName)];
    }

    @Override
    public <T> T getValueOf(int index) {
        return (T) ref.get()[index];
    }

    @Override
    public boolean next() {
        try {
            ref.set(csvReader.readNext());
            return ref.get() != null;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to fetch next record", e);
        }
    }

}