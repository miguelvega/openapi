package org.openapi.datamigrator.csv;

import com.google.common.base.Function;
import org.javatuples.Pair;
import org.openapi.datamigrator.FieldMetaData;
import org.openapi.datamigrator.extract.Extractor;
import org.openapi.datamigrator.load.Loader;
import org.openapi.datamigrator.transform.FieldMapping;
import org.openapi.datamigrator.transform.Transformer;

import java.util.List;

/**
 * @author Miguel A. Vega P.
 * @version $Id: TransformerImpl.java; Jun 05, 2016. 12:20 AM mvega $
 * @source $URL$
 */
public class TransformerImpl<X, Y> implements Transformer<X, Y>{

    final FieldMetaData input;

    public TransformerImpl(Extractor<X> extractor, Loader<Y> loader){
        this.input = extractor.getFieldMetaData();
    }

    @Override
    public Y transformTuple(Extractor<X> extractor, Loader<Y> loader, FieldMapping<X, Y> fieldMapping) {

        List<Pair<String, String>> mapping = fieldMapping.getMapping();

        FieldMetaData in = extractor.getFieldMetaData();

        while(extractor.next()){

            loader.prepare();

            for (Pair<String, String> map : mapping) {
                String inField = map.getValue0();

                Function function = in.getTransformations().get(inField);
                Object source = extractor.getValueOf(inField);

                Object target = function.apply(source);

                loader.setValue(target, map.getValue1());
            }

            loader.detach();
        }

        return null;
    }
}
