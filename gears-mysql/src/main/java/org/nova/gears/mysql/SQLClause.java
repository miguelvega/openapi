package org.nova.gears.mysql;

import static org.openlogics.utils.ImmutableString.of;

/**
 * @author Miguel Vega
 * @version $Id: SQLClause.java 0, 5/29/14 10:49 PM, @miguel $
 */
public abstract class SQLClause extends org.openlogics.gears.jdbc.builder.SQLClause{

    protected SQLClause() {
        super();
    }

    public static final SQLClause limitOffset(final long offset, final long limit) {

        if (limit < 0 && offset<0) {
            return null;
        }

        if(offset>=0){
            return new SQLClause() {
                @Override
                public String toSQLString() {
                    return of(" LIMIT ").add(offset).add(", ").add(limit).build();
                }
            };
        }

        return new SQLClause() {
            @Override
            public String toSQLString() {
                return of(" LIMIT ").add(limit).build();
            }
        };
    }

    /**
     * Builds a limit object, this will be ignored when limit is lower than 1
     *
     * @param limit
     * @return
     */
    public static final SQLClause limit(final long limit) {
        throw new UnsupportedOperationException("This method is not supported, use the limitOffset method instead");
    }

    /**
     * Ignores values below 0
     *
     * @param offset
     * @return
     */
    public static SQLClause offset(final long offset) {
        throw new UnsupportedOperationException("This method is not supported, use the limitOffset method instead");
    }
}
