/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import javax.faces.bean.*;
import java.lang.annotation.Annotation;

/**
 * @author Miguel Vega
 *
 */
public class JSFScopeBuilder {
	
	/**
	 * build a corresponding Guice scope implementation that
	 * handles the custom scoping for a particular bean via
	 * its name
	 * 
	 * @param beanName
	 * @param scopeAnnotationClass
	 * @return
	 */
	public JSFScope build(String beanName,
			Class<? extends Annotation> scopeAnnotationClass) {
		//the resolver instance
		JSFScope resolver = null;
		
		//if no scope annotation is found, use request scoped by default
		if(scopeAnnotationClass == null){
			scopeAnnotationClass = RequestScoped.class;
		}
		
		if(scopeAnnotationClass.equals(RequestScoped.class)){
			resolver = new JSFRequestScope(beanName);
		}else if(scopeAnnotationClass.equals(SessionScoped.class)){
			resolver = new JSFSessionScope(beanName);
		}else if(scopeAnnotationClass.equals(ApplicationScoped.class)){
			resolver = new JSFApplicationScope(beanName);
		}else if(scopeAnnotationClass.equals(ViewScoped.class)){
			resolver = new JSFViewScope(beanName);
		}else if(scopeAnnotationClass.equals(NoneScoped.class)){
			resolver = new JSFNoneScope(beanName);
		}else{
			ScopeHandler scopeResolver = 
				scopeAnnotationClass.getAnnotation(ScopeHandler.class);
			
			if(scopeResolver != null){
				//get the managed bean resolver class
				Class<? extends JSFScope> customScopeHandler
					= scopeResolver.scope();
				
				//create the managed bean resolver class
				try {
					resolver = (JSFScope) 
						customScopeHandler.getConstructor(String.class).newInstance(beanName);
				} catch(Exception ex){

					throw new RuntimeException("could not create custom scope object.");
				}
			}else{
				throw new RuntimeException("no support for annotation '"+scopeAnnotationClass.getCanonicalName()+"', scope annotation on managed bean should be either standard JSF scope annotations or customized annotation with @ScopeResolver associated with it.");
			}
		}
		return resolver;
	}
}