/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import com.google.inject.Key;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * @author Miguel Vega
 *
 */
public abstract class JSFScope implements com.google.inject.Scope {
	/**
	 * get the logger
	 */
	private static Logger logger = LoggerFactory.getLogger(JSFScope.class);
	
	/**
	 * the key and bean name mappings for managed beans
	 */
	private final String beanName;

	/**
	 * set the name of the bean
	 * 
	 * @param beanName
	 */
	public JSFScope(String beanName) {
		this.beanName = beanName;
	}
	
	/**
	 * get the name of the bean
	 * 
	 * @return
	 */

	protected final String getBeanName() {
		return beanName;
	}

	/**
	 * get the faces context
	 * @return
	 */
	protected final FacesContext getFacesContext(){
		return FacesContext.getCurrentInstance();
	}
	
	/*
	 * @see com.google.inject.Scope#scope(com.google.inject.Key,
	 * com.google.inject.Provider)
	 */
	@Override
	public final <T> Provider<T> scope(final Key<T> beanKey,
			final Provider<T> unscoped) {
		
		//create a provider 
		return new Provider<T>() {
			@SuppressWarnings("unchecked")
			@Override
			public T get() {
				if (logger.isDebugEnabled()) {
					logger.debug("trying to provide bean instance with name '"+getBeanName()+"'.");
				}
				
				T beanInstance = null;
				if (getObjectMap() != null) {
					if (getObjectMap().containsKey(getBeanName()) == true) {
						if (logger.isDebugEnabled()) {
							logger
								.debug("bean '"+getBeanName()+"' exists in the scope, get the existing instance.");
						}
					} else {
						if (logger.isDebugEnabled()) {
							logger
								.debug("no instance exists in the scope, provide a new instance.");
						}
						getObjectMap().put(getBeanName(), unscoped.get());
					}
					beanInstance = (T) getObjectMap().get(beanName);
				}
				return beanInstance;
			}
		};
	}

	/**
	 * get the object map that stores the object in a particular scope
	 * 
	 * @return
	 */
	protected abstract Map<String, Object> getObjectMap();
}