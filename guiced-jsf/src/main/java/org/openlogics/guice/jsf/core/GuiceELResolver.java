/**
 * 
 */
package org.openlogics.guice.jsf.core;

import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.faces.context.FacesContext;
import java.beans.FeatureDescriptor;
import java.util.Iterator;

/**
 * The GuiceELResolver to inject members into the JSF managed beans
 *
 * @author Miguel Vega
 *
 */
public class GuiceELResolver extends ELResolver {
	/**
	 * the logger instance
	 */
	private static Logger logger = LoggerFactory.getLogger(GuiceELResolver.class);

	/*
	 * @see javax.el.ELResolver#getCommonPropertyType(javax.el.ELContext,
	 * java.lang.Object)
	 */
	@Override
	public Class<?> getCommonPropertyType(ELContext context, Object base) {
		return base != null ? null : Object.class;
	}

	/*
	 * @see javax.el.ELResolver#getFeatureDescriptors(javax.el.ELContext,
	 * java.lang.Object)
	 */
	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context,
			Object base) {
		return null;
	}

	/*
	 * @see javax.el.ELResolver#getType(javax.el.ELContext, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public Class<?> getType(ELContext context, Object base, Object property) {
		return base != null || property == null ? null : property.getClass();
	}

	/*
	 * @see javax.el.ELResolver#getValue(javax.el.ELContext, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public Object getValue(ELContext context, Object base, Object property) {
		if (base != null) {
			return null;
		} else {
			FacesContext facesContext = (FacesContext) context
					.getContext(FacesContext.class);
			
			//get the managed bean config
			ManagedBeanConfig config = (ManagedBeanConfig) 
				facesContext.getExternalContext().getApplicationMap().get(ManagedBeanConfig.KEY);
			
			if(logger.isDebugEnabled()){
				logger.debug("GuiceELResolver->resolve property '"
						+property+"' by Guice injector.");
			}
			
			//resolve the object by the guice injector from its
			//corresponding scope implementation
			Injector injector = InjectorFinder.getInjector();
			Class<?> managedBeanClass = config.getManagedBeanClass((String)property);
			
			//resolve the object
			Object resolved = injector.getInstance(managedBeanClass);
			
			if(resolved != null){
				context.setPropertyResolved(true);
			}
			
			// return the resolved object
			return resolved;
		}
	}

	/*
	 * @see javax.el.ELResolver#isReadOnly(javax.el.ELContext, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean isReadOnly(ELContext context, Object base, Object property) {
		return true;
	}

	/*
	 * @see javax.el.ELResolver#setValue(javax.el.ELContext, java.lang.Object,
	 * java.lang.Object, java.lang.Object)
	 */
	@Override
	public void setValue(ELContext context, Object base, Object property,
			Object value) {
		// ignore
	}
}