package org.openlogics.guice.jsf.scope;

import java.util.Map;


/**
 * Guice Scope implementation that understand the JSF ViewScoped
 * annotation semantics
 * 
 * @author Miguel Vega
 *
 */
class JSFViewScope extends JSFScope {
	/**
	 * view scope resolver
	 * @param beanName
	 */
	public JSFViewScope(String beanName) {
		super(beanName);
	}
	
	/*
	 * @see com.starchu.guice.jsf.scope.JSFScope#getObjectMap()
	 */
	@Override
	protected Map<String, Object> getObjectMap() {
		if(getFacesContext()!=null && getFacesContext().getViewRoot()!=null){
			return getFacesContext().getViewRoot().getViewMap();
		}
		return null;
	}	
}
