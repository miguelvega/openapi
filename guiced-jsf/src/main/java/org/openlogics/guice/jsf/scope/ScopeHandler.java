/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Meta annotation used to identify the guice scope
 * implementations for custom scope under JSF
 * 
 * @author Miguel Vega
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE})
public abstract @interface ScopeHandler{
	/**
	 * provide the managedBean resolver class associated with this scope
	 * annotation
	 * 
	 * @return
	 */
	Class<? extends JSFScope> scope();
}