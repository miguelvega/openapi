/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import java.util.Map;


/**
 * @author Miguel Vega
 *
 */
class JSFRequestScope extends JSFScope {

	/**
	 * constructor
	 * @param beanName
	 */
	public JSFRequestScope(String beanName) {
		super(beanName);
	}

	/*
	 * @see com.starchu.guice.jsf.scope.JSFScope#getObjectMap()
	 */
	@Override
	protected Map<String, Object> getObjectMap() {
		if(getFacesContext()!=null && getFacesContext().getExternalContext()!=null){
			return getFacesContext().getExternalContext().getRequestMap();
		}
		return null;
	}
}
