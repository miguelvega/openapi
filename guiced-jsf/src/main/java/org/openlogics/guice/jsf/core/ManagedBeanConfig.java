/**
 * 
 */
package org.openlogics.guice.jsf.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The ManagedBean config stores all the manage bean class metadata information
 * to link the name of the bean by its scope
 * 
 * @author starchu
 * @author Miguel Vega
 *
 */
public class ManagedBeanConfig {
	/*
	 * the key to store in the web application scope
	 */
	public static final String KEY = "com.starchu.guice.jsf.core.managedBeanConfig";

	/**
	 * the logger instance
	 */
	private static Logger logger = LoggerFactory.getLogger(ManagedBeanConfig.class);

	/**
	 * the list of managed bean metadata
	 */
	private Map<String, ManagedBeanMetadata> managedBeans = new ConcurrentHashMap<String, ManagedBeanMetadata>();

	/**
	 * get the class name of the managed bean
	 * @param beanName
	 * @return
	 */
	public Class<?> getManagedBeanClass(String beanName){
		Class<?> clazz = null;
		if(managedBeans.containsKey(beanName) == true){
			clazz = managedBeans.get(beanName).getBeanClass();
		}
		return clazz;
	}
	
	/**
	 * get the scope of the managed bean giving the name of the bean
	 * 
	 * @param beanName
	 * @return
	 */
	public Class<? extends Annotation> getManagedBeanScope(String beanName) {
		Class<? extends Annotation> scope = null;
		if (managedBeans.containsKey(beanName) == true) {
			scope = managedBeans.get(beanName).getScope();
		}

		if (scope != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("found managed bean '" + beanName + "' annotated with scope '"
						+ scope.getCanonicalName() + "'");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("could not find managed bean '" + beanName
						+ "' in any of the predefined scopes.");
			}
		}
		return scope;
	}

	/**
	 * add the managed bean into runtime config
	 * @param beanName
	 * @param beanClass
	 * @param scope
	 * @param eager
	 */
	public void addManagedBean(String beanName, Class<?> beanClass,
			Class<? extends Annotation> scope, boolean eager) {
		ManagedBeanMetadata metadata = new ManagedBeanMetadata(beanName,
				beanClass, scope, eager);
		
		if (managedBeans.containsKey(beanName) == false) {
			managedBeans.put(beanName, metadata);
			
			if(logger.isDebugEnabled()){
				logger.debug("add type'"+beanClass.getCanonicalName()+" identified as '"+beanName+"' into runtime config under scope '"+scope.getCanonicalName()+"'");
			}
		}		
	}
	
	/**
	 * 
	 * @param scopeAnnotationClass
	 * @return
	 */
	List<ManagedBeanMetadata> getManagedBeanMetadata(Class<? extends Annotation> scopeAnnotationClass){
		List<ManagedBeanMetadata> metadata = new ArrayList<ManagedBeanMetadata>();
		
		for(ManagedBeanMetadata each : managedBeans.values()){
			if(scopeAnnotationClass == null || each.getScope().equals(scopeAnnotationClass)){
				metadata.add(each);
			}
		}
		return metadata;
	}

	/**
	 * get all the managed bean metadata
	 * @return
	 */
	List<ManagedBeanMetadata> getManagedBeanMetadata() {
		return getManagedBeanMetadata(null);
	}
}

/**
 * 
 * @author starchu
 * 
 */
class ManagedBeanMetadata implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8282275935604524945L;

	private final Class<? extends Annotation> scope;

	private final String beanName;

	private final Class<?> beanClass;

	private final boolean eager;
	
	public ManagedBeanMetadata(String beanName, Class<?> beanClass,
			Class<? extends Annotation> scope, boolean eager) {
		this.beanName = beanName;
		this.beanClass = beanClass;
		this.scope = scope;
		this.eager = eager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((beanName == null) ? 0 : beanName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManagedBeanMetadata other = (ManagedBeanMetadata) obj;
		if (beanName == null) {
			if (other.beanName != null)
				return false;
		} else if (!beanName.equals(other.beanName))
			return false;
		return true;
	}

	public String getBeanName() {
		return beanName;
	}

	public Class<?> getBeanClass() {
		return beanClass;
	}

	public Class<? extends Annotation> getScope() {
		return scope;
	}
	
	public boolean isEager(){
		return eager;
	}
}