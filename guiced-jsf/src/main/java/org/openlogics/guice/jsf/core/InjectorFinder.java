package org.openlogics.guice.jsf.core;

import com.google.inject.Injector;

import javax.faces.context.FacesContext;

/**
 * the injector finder is responsible for getting the injector 
 * from the current faces context
 * 
 * @author Miguel Vega
 *
 */
public final class InjectorFinder {
	/**
	 * the injector name
	 */
	static final String INJECTOR_NAME = Injector.class.getName();
	
	/**
	 * get the injector instance stored in the current context
	 * @return
	 */
	public static final Injector getInjector(){
		Injector injector = null;
		try{
			injector = (Injector) 
				FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get(INJECTOR_NAME);
		}catch(Exception ex){
			
		}
		return injector;
	}
}
