/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import java.util.Map;


/**
 * @author Miguel Vega
 *
 */
class JSFSessionScope extends JSFScope {

	/**
	 * constructor
	 * @param beanName
	 */
	public JSFSessionScope(String beanName) {
		super(beanName);
	}

	/*
	 * @see com.starchu.guice.jsf.scope.JSFScope#getObjectMap()
	 */
	@Override
	protected Map<String, Object> getObjectMap() {
		if(getFacesContext()!=null && getFacesContext().getExternalContext()!=null){
			return getFacesContext().getExternalContext().getSessionMap();
		}
		return null;
	}
}
