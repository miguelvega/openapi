/**
 * 
 */
package org.openlogics.guice.jsf.scope;

import java.util.Map;


/**
 * Guice Scope implementation which understands the JSF NoneScoped
 * annotation semantics
 * 
 * @author Miguel Vega
 *
 */
class JSFNoneScope extends JSFScope {
	/**
	 * constructor
	 * @param beanName
	 */
	public JSFNoneScope(String beanName) {
		super(beanName);
	}

	/*
	 * @see com.starchu.guice.jsf.scope.JSFScope#getObjectMap()
	 */
	@Override
	protected Map<String, Object> getObjectMap() {
		//simply return null, indicating there is no store for 
		//any objects, every time, a new instance will be created
		return null;
	}
}
