package guice.aop0;

import com.google.inject.AbstractModule;

import static com.google.inject.matcher.Matchers.any;
import static com.google.inject.matcher.Matchers.subclassesOf;

/**
 * @author Miguel Vega
 * @version $Id: ExampleModule.java 0, 2013-08-14 6:06 PM mvega $
 */

public class ExampleModule extends AbstractModule {

    public void configure() {

        bindInterceptor(
                subclassesOf(VideoRental.class),
                any(),
                new TracingInterceptor());
    }
}