package org.openlogics.aop.jdbc;

import com.google.common.base.Strings;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.matcher.Matchers;
import guice.aop0.VideoRental;
import org.junit.Before;
import org.junit.Test;
import org.openlogics.aop.jdbc.service.CommonService;
import org.openlogics.aop.jdbc.pojo.Foo;
import org.openlogics.aop.jdbc.pojo.Product;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.sql.Timestamp;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;

/**
 * @author Miguel Vega
 * @version $Id: DataStoreTransactionInterceptorTest.java 0, 2013-08-14 6:19 PM mvega $
 * @deprecated using an older and not developed version of ObservableModule
 */
@Deprecated
public class DataStoreTransactionInterceptorTest {
    @Inject
    private VideoRental videoRental;

    @Inject
    CommonService commonService;

    private Logger logger = org.slf4j.LoggerFactory.getLogger(DataStoreTransactionInterceptorTest.class);

    @Before
    public void setup() {

        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {

                DataStoreTransactionInterceptor dataStoreTransactionInterceptor = new DataStoreTransactionInterceptor();

                requestInjection(dataStoreTransactionInterceptor);

                bindInterceptor(
                        Matchers.any(),
                        Matchers.annotatedWith(Transactional.class),
                        dataStoreTransactionInterceptor);
            }
        }, new DataStoreModule());

        injector.injectMembers(this);
        injector.injectMembers(commonService);
    }

    @Test
    public void testRentMovie() throws Exception {
        try {
            videoRental.rentMovie(1);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    /**
     * This test mjst generate an SQLException, due to the insertion with NULL values
     * @throws SQLException
     */
    @Test(expected = SQLException.class)
    public void testInsertTransaction() throws SQLException {

        logger.info(Strings.repeat(System.getProperty("line.separator"), 10));

        logger.info("Inicio Insercion ... " + Strings.repeat("*", 50));
        commonService.showFoos();
        logger.info(Strings.repeat("*", 50));
        try {
            Foo foo = new Foo();
            //this is supposed to throw an exception, because it can not accept NULL for NAME
            commonService.multipleTransactionDoer(foo);
        } catch (SQLException e) {
            throw e;
        } finally {
            logger.info("Fin Insercion ... " + Strings.repeat("*", 50));
            commonService.showFoos();
            logger.info(Strings.repeat("*", 50));
        }

        commonService.showFoos();
    }

    @Test
    public void testComplexTransaction() {
        try {
            //initially the number of products and foos is as follows
            assertEquals(5, commonService.getFooCount());
            assertEquals(4, commonService.getProductCount());

            //let's try to insert a record to each table
            Foo foo = new Foo();
            foo.setFname("Foo FName");
            foo.setLname("Foo LName");
            foo.setAddDate(new Timestamp(currentTimeMillis()));
            foo.setRate(100f);
            commonService.insertFooAndProduct(
                    foo,
                    new Product().setProdName("New Product 1").setProdPrice(50.1f));

            //initially the number of products and foos is as follows
            assertEquals(6, commonService.getFooCount());
            assertEquals(5, commonService.getProductCount());

            //let's try to insert a record to each table
            commonService.insertFooAndProduct(
                    new Foo(),//because of the CONSTRAINTS an exception will be thrown
                    new Product().setProdName("New Product 2").setProdPrice(55.1f));

        } catch(SQLException x){
            //do anything
        }
        finally {
            //as the last transaction has a failure, the number of records shouldn't change after the first insertion
            try {
                assertEquals(6, commonService.getFooCount());
                assertEquals(5, commonService.getProductCount());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testObjectDataStore() throws SQLException {
        commonService.deleteAll();

        commonService.showFoos();
    }

}
