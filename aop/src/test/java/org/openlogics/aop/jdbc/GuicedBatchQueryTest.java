package org.openlogics.aop.jdbc;

import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;
import org.openlogics.aop.jdbc.pojo.Foo;
import org.openlogics.gears.jdbc.DataStore;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import static java.lang.System.currentTimeMillis;
import static junit.framework.Assert.assertEquals;

/**
 * @author Miguel Vega
 * @version $Id: GuicedBatchQueryTest.java 0, 2013-11-01 11:07 AM mvega $
 */
public class GuicedBatchQueryTest extends TestStub{
    @Test
    public void batchInsert() throws SQLException {

        Injector injector = Guice.createInjector(new DataStoreModule(), new AbstractModule() {
            @Override
            protected void configure() {
                bind(GuicedBatchQuery.class);
            }
        });

        DataStore ds = injector.getInstance(DataStore.class);

        ds.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);

        Foo std = new Foo();
        std.setFname("Mr.");
        std.setLname("Bean");
        std.setRate(100);
        std.setAddDate(new Timestamp(currentTimeMillis()));

        GuicedBatchQuery query = injector.getInstance(GuicedBatchQuery.class);

        query.applyQueryString(
                "insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE)" +
                        " values " +
                        "(#{fname}, #{lname}, #{rate}, #{addDate})");

        //in the following insertion, we combine two type of inputs: Map and Pojo
        query.
                addBatch(ImmutableMap.<String, Object>of("fname", "User 1", "lname", "Last 1", "rate", 61f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ImmutableMap.<String, Object>of("fname", "User 2", "lname", "Last 2", "rate", 62f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ImmutableMap.<String, Object>of("fname", "User 3", "lname", "Last 3", "rate", 63f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ImmutableMap.<String, Object>of("fname", "User 4", "lname", "Last 4", "rate", 64f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ImmutableMap.<String, Object>of("fname", "User 5", "lname", "Last 5", "rate", 65f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(ImmutableMap.<String, Object>of("fname", "User 6", "lname", "Last 6", "rate", 66f, "addDate", new Timestamp(currentTimeMillis()))).
                addBatch(std);

        int[] update = ds.update(query);

        //as the inserts were OK, there must be 7 new records in the table
        assertEquals(7, update.length);

        logger.info("Showing update batch results");
        viewAll(ds);

    }
}
