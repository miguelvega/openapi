/*
 *     JavaTools
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.aop.jdbc.pojo;

import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import java.sql.Timestamp;

/**
 * TODO, setter which returns the same object instance is not supported
 * @author Miguel Vega
 * @version $Id: Student.java 0, 2012-09-29 12:00 mvega $
 */
@ToString(callSuper = true)
@Entity(name = "FOO")
public class Foo extends Person {

    @JoinColumn()
    @Column(name = "FOO_RATE")
    private float rate;

    @Column(name = "FOO_ADD_DATE")
    private Timestamp addDate;

    public Timestamp getAddDate() {
        return addDate;
    }

    public void setAddDate(Timestamp addDate) {
        this.addDate = addDate;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "rate=" + rate +
                ", addDate=" + addDate +
                '}'+super.toString();
    }
}
