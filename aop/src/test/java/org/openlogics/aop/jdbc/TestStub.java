package org.openlogics.aop.jdbc;

import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.openlogics.aop.jdbc.pojo.Foo;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: TestStub.java 0, 2013-11-01 11:08 AM mvega $
 */
public abstract class TestStub {
    protected Logger logger;
    {
        logger = LoggerFactory.getLogger(getClass());
    }
    protected void viewAll(DataStore ds) throws SQLException {
        logger.info(System.getProperty("line.separator"));
        Query query = Query.of("select FOO_ID, " +
                "FOO_FNAME, " +
                "FOO_LNAME, " +
                "FOO_RATE as rate, " +
                "FOO_ADD_DATE from FOO");
        List<Foo> stds = ds.select(query, Foo.class);
        //List<Map<String, Object>> stds = ds.select(query, new MapListHandler());
        logger.info("************************************ VIEW ALL, BEGIN *****************************************************");
        for (Foo std : stds) {
            //for (Map<String, Object> std : stds) {
            logger.info("Result > " + std);
        }
        logger.info("************************************* VIEW ALL, END****************************************************");
        logger.info(System.getProperty("line.separator"));
    }

    protected long countAll(DataStore ds) throws SQLException {
        Query query = Query.of("select COUNT(FOO_ID) from FOO");
        return ds.select(query, new ScalarHandler<Long>(1));
    }
}