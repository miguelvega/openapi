package org.openlogics.aop.jdbc.pojo;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Miguel Vega
 * @version $Id: Recipe.java; mar 30, 2015 02:35 PM mvega $
 */
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @Column(name = "cat_id")
    private Integer id;
    @Column(name = "cat_name")
    private String name;
    @Column(name = "cat_desc")
    private String desc;

    private Set<Stock> stocks = Sets.newHashSet();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cat_id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "cat_name", nullable = false, length = 10)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "[DESC]", nullable = false)
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "categories")
    public Set<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }
}
