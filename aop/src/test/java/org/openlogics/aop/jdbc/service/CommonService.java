/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.aop.jdbc.service;

import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.openlogics.aop.jdbc.DS01;
import org.openlogics.aop.jdbc.SalesDS;
import org.openlogics.aop.jdbc.Transactional;
import org.openlogics.aop.jdbc.pojo.Foo;
import org.openlogics.aop.jdbc.pojo.Product;
import org.openlogics.aop.jdbc.pojo.Stock;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.openlogics.gears.jdbc.ObjectResultVisitor;
import org.openlogics.gears.jdbc.Query;
import org.openlogics.gears.jdbc.builder.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: CommonService.java 0, 2012-11-30 7:16 PM mvega $
 */
public class CommonService {

    @Inject
    @DS01
    private ObjectDataStore dataStore;

    @Inject
    @SalesDS
    private ObjectDataStore salesDataStore;

    private Logger logger = LoggerFactory.getLogger(CommonService.class);

    @Transactional
    public boolean rentMovie(long movieId) {

        System.out.println(
                String.format("Movie %s rented.", movieId));

        return true;
    }

    /**
     * This method will perform all teh operations within, if an error occurs, a rollback will be
     * performed for annotated DataStores with {@link org.openlogics.aop.jdbc.DS01} and {@link org.openlogics.aop.jdbc.SalesDS}
     *
     * @param foo
     * @throws SQLException
     */
    @Transactional(survey = {DS01.class, SalesDS.class})
    public void multipleTransactionDoer(Foo foo) throws SQLException {

        //retrieve correctly the Stock
        Stock stock = salesDataStore.getSingleResult(Stock.class, Filter.equal("stk_id", 1l));
        logger.info("Stock requested successfully. {}", stock);

        stock.setName("naaame!!!!");
        salesDataStore.modify(stock);

        stock = salesDataStore.getSingleResult(Stock.class, Filter.equal("stk_id", 1l));
        logger.info("... Stock updated successfully. {}", stock);

        dataStore.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                "values " +
                "(#{fname}, #{lname}, #{rate}, #{addDate})", foo));

    }

    @Transactional(survey = {DS01.class})
    public void singleTransactionDoer(Foo foo) throws SQLException {

        //retrieve correctly the Stock
        Stock stock = salesDataStore.getSingleResult(Stock.class, Filter.equal("stk_id", 1l));
        logger.info("Stock requested successfully. {}", stock);

        stock.setName("naaame!!!!");
        salesDataStore.modify(stock);

        stock = salesDataStore.getSingleResult(Stock.class, Filter.equal("stk_id", 1l));
        logger.info("Stock updated successfully. {}", stock);

        dataStore.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                "values " +
                "(#{fname}, #{lname}, #{rate}, #{addDate})", foo));

    }

    @Transactional
    public void update(Foo foo) throws SQLException {
        dataStore.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) " +
                "values " +
                "(#{fname}, #{lname}, #{rate}, #{addDate})", foo));
    }

    public void showFoos() throws SQLException {
        Query query = Query.of("select FOO_ID, " +
                "FOO_FNAME, " +
                "FOO_LNAME, " +
                "FOO_RATE as rate, " +
                "FOO_ADD_DATE from FOO");

        dataStore.select(query, Foo.class, new ObjectResultVisitor<Foo>() {
            @Override
            public void visit(Foo result) throws SQLException {
                logger.info("POJO > " + result.toString());
            }
        });
    }

    public long getFooCount() throws SQLException {
        Query query = Query.of("select count(FOO_ID) from FOO");

        return dataStore.select(query, new ScalarHandler<Long>());
    }

    public long getProductCount() throws SQLException {
        Query query = Query.of("select count(PROD_FOO_ID) from PRODUCTS");

        return dataStore.select(query, new ScalarHandler<Long>());
    }

    /**
     * Just to demonstrate than many statements can be executed within a method, and all those changes will roll back
     * if an error occurs
     *
     * @param foo
     * @param product
     * @throws SQLException
     */
    @Transactional
    public void insertFooAndProduct(Foo foo, Product product) throws SQLException {
        logger.info("A...autpcommit={}, autocclose={}", dataStore.isAutoCommit(), dataStore.isAutoClose());
        dataStore.update(Query.of("insert into PRODUCTS (PROD_FOO_ID, PROD_NAME, PROD_PRICE) values (#{prodFooId}, #{prodName}, #{prodPrice})", product));
        logger.info("B...autpcommit={}, autocclose={}", dataStore.isAutoCommit(), dataStore.isAutoClose());
        dataStore.update(Query.of("insert into FOO (FOO_FNAME, FOO_LNAME, FOO_RATE, FOO_ADD_DATE) values (#{fname}, #{lname}, #{rate}, #{addDate})", foo));
        logger.info("C...autpcommit={}, autocclose={}", dataStore.isAutoCommit(), dataStore.isAutoClose());
    }

    @Transactional
    public void deleteAll() throws SQLException {

        ObjectDataStore ods = (ObjectDataStore) dataStore;

        List<Foo> resultList = ods.getResultList(Foo.class);

        for (Foo foo : resultList) {
            ods.remove(foo);
        }

        logger.info("Everything has been removed!");
    }
}
