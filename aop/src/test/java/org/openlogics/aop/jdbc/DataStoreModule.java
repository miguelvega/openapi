package org.openlogics.aop.jdbc;

import com.google.common.io.Resources;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.DataStoreFactory;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: DataStoreModule.java 0, 2013-08-14 7:10 PM mvega $
 * @deprecated all this features are configured in new @{link DataStoreModule2} module
 */
@Deprecated
public class DataStoreModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(DataStore.class).toProvider(new Provider<DataStore>() {
            @Override
            public DataStore get() {
                final BasicDataSource basicDataSource = new BasicDataSource();
                try {
                    basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
                    basicDataSource.setDriverClassName("org.h2.Driver");
                    Connection connection = basicDataSource.getConnection();
                    URL sql = getResource(getClass(), "foo.sql");
                    try {
                        connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                        connection.close();

                        URL resource = getResource(getClass(), "foo.xml");
                        FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                        DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(basicDataSource);
                        databaseTester.setDataSet(build);
                        databaseTester.onSetup();

                    } catch (SQLException x) {

                    }
                } catch (Exception x) {
                    x.printStackTrace();
                }

                //return new JdbcDataStore(basicDataSource);
                return DataStoreFactory.createDataStore(basicDataSource);
            }
        }).in(Scopes.SINGLETON);
    }
}
