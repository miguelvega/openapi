package org.openlogics.aop.jdbc;

import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.openlogics.aop.jdbc.pojo.Category;
import org.openlogics.aop.jdbc.pojo.Stock;
import org.openlogics.aop.jdbc.service.CommonService;
import org.openlogics.aop.jdbc.pojo.Foo;
import org.openlogics.aop.jdbc.service.SalesService;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.openlogics.gears.jdbc.builder.Filter;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransactionObserverTest {
    @Inject
    CommonService commonService;

    @Inject
    SalesService salesService;

    final Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

    @Before
    public void setup() {

        Injector injector = Guice.createInjector(new DataStoreModule2());

        injector.injectMembers(this);

        injector.injectMembers(commonService);
        injector.injectMembers(salesService);
    }

    @Test
    public void testSimpleUpdate() throws SQLException {
        salesService.showStock();

        //update data in sales service

        Stock stock = salesService.findById(1l);

        salesService.showStock(stock);

        ObjectDataStore dataStore = salesService.getDataStore();

        stock.setName("qwerty nam");
        dataStore.modify(stock);

        stock = dataStore.getSingleResult(Stock.class, Filter.equal("stk_id", stock.getId()));

        salesService.showStock(stock);
    }

    @Test
    public void testMultipleDataStoreTransaction() throws SQLException {
        logger.info(Strings.repeat(System.getProperty("line.separator"), 10));

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Inicio Insercion ... " + Strings.repeat("*", 50));
        commonService.showFoos();
        logger.info(Strings.repeat("*", 50));
        salesService.showStock();
        logger.info(Strings.repeat("+", 50));

        try {
            Foo foo = new Foo();
            //this is supposed to throw an exception, because it can not accept NULL for NAME
            commonService.multipleTransactionDoer(foo);

            Assert.assertTrue(false);
        } catch (SQLException e) {
            logger.error("Errors expected have been found", e);
            Assert.assertTrue(true);
        } finally {
            logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Fin Insercion ... " + Strings.repeat("*", 50));
            commonService.showFoos();
            logger.info(Strings.repeat("*", 50));
            salesService.showStock();
            logger.info(Strings.repeat("+", 50));
        }

        commonService.showFoos();

        List<Category> cats = salesService.getAllCategories();
        for (Category cat : cats) {
            logger.info(">>>" + cat.getName());
        }

        //Transaction has been annotated to survey DS01 and SalesDS data stores, so transactions which belng to
        //these annotated data stores will rolled back on failure
        assertEquals("Beer", salesService.findById(1).getName());
    }

    @Test
    public void testSingleDataStoreTransaction() throws SQLException {
        logger.info(Strings.repeat(System.getProperty("line.separator"), 10));

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Inicio Insercion ... " + Strings.repeat("*", 50));
        commonService.showFoos();
        logger.info(Strings.repeat("*", 50));
        salesService.showStock();
        logger.info(Strings.repeat("+", 50));

        try {
            Foo foo = new Foo();
            //this is supposed to throw an exception, because it can not accept NULL for NAME
            commonService.singleTransactionDoer(foo);

            Assert.assertTrue(false);
        } catch (SQLException e) {
            logger.error("Errors expected have been found", e);
            Assert.assertTrue(true);
        } finally {
            logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Fin Insercion ... " + Strings.repeat("*", 50));
            commonService.showFoos();
            logger.info(Strings.repeat("*", 50));
            salesService.showStock();
            logger.info(Strings.repeat("+", 50));
        }

        commonService.showFoos();

        List<Category> cats = salesService.getAllCategories();
        for (Category cat : cats) {
            logger.info(">>>" + cat.getName());
        }
        //Transaction has been annotated to survey DS01 data store only, so transactions data store annotated with SalesDS
        // committed while transactions for DS01 rolled back
        assertEquals("naaame!!!!", salesService.findById(1).getName());
    }
}