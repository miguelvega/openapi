package org.openlogics.aop.jdbc.pojo;

import javax.persistence.Column;

/**
 * @author Miguel Vega
 * @version $Id: Product.java 0, 2013-08-15 10:41 AM mvega $
 */
public class Product {
    @Column(name = "PROD_ID")
    private int prodId;
    @Column(name = "PROD_FOO_ID")
    private int prodFooId;
    @Column(name = "PROD_NAME")
    private String prodName;
    @Column(name = "PROD_PRICE")
    private float prodPrice;
    @Column(name = "PROD_AVAILABLE")
    private boolean prodAvail;

    public int getProdId() {
        return prodId;
    }

    public Product setProdId(int prodId) {
        this.prodId = prodId;
        return this;
    }

    public int getProdFooId() {
        return prodFooId;
    }

    public Product setProdFooId(int prodFooId) {
        this.prodFooId = prodFooId;
        return this;
    }

    public String getProdName() {
        return prodName;
    }

    public Product setProdName(String prodName) {
        this.prodName = prodName;
        return this;
    }

    public float getProdPrice() {
        return prodPrice;
    }

    public Product setProdPrice(float prodPrice) {
        this.prodPrice = prodPrice;
        return this;
    }

    public boolean isProdAvail() {
        return prodAvail;
    }

    public Product setProdAvail(boolean prodAvail) {
        this.prodAvail = prodAvail;
        return this;
    }
}
