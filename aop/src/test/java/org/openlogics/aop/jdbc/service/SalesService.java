package org.openlogics.aop.jdbc.service;

import com.google.inject.Inject;
import org.openlogics.aop.jdbc.SalesDS;
import org.openlogics.aop.jdbc.pojo.Category;
import org.openlogics.aop.jdbc.pojo.Stock;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.openlogics.gears.jdbc.builder.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: SalesService.java; mar 30, 2015 03:48 PM mvega $
 */
public class SalesService {

    private final ObjectDataStore dataStore;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    public SalesService(@SalesDS ObjectDataStore dataStore) {
        this.dataStore = dataStore;
//        this.dataStore.setSchema("sales");
    }

    public List<Category> getAllCategories() throws SQLException {

        return dataStore.getResultList(Category.class);
    }

    public List<Stock> getAllStock() throws SQLException {
        return dataStore.getResultList(Stock.class);
    }

    public void showStock(Stock stock) {
        logger.info("-------.*.*.*.*.*.*.*.*.*.*.*.*.*.ID={}, NAME={}, CODE={}", stock.getId(), stock.getName(), stock.getCode());
    }

    public void showStock() throws SQLException {
        List<Stock> resultList = dataStore.getResultList(Stock.class);
        for (Stock stock : resultList) {
            logger.info(".*.*.*.*.*.*.*.*.*.*.*.*.*.ID={}, NAME={}, CODE={}", stock.getId(), stock.getName(), stock.getCode());
        }
    }

    public Stock findById(long i) throws SQLException {
        return dataStore.getSingleResult(Stock.class, Filter.equal("stk_id", i));
    }

    public ObjectDataStore getDataStore() {
        return dataStore;
    }
}
