package org.openlogics.aop.jdbc;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Miguel Vega
 * @version $Id: H2DBDS.java; mar 26, 2015 05:15 PM mvega $
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@BindingAnnotation
public @interface DS01 {
}
