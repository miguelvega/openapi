package org.openlogics.aop.jdbc.pojo;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Miguel Vega
 * @version $Id: CookBook.java; mar 30, 2015 02:35 PM mvega $
 */
@Entity
@Table(name = "stock")
public class Stock {
    @Id
    @Column(name = "stk_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "stk_name", nullable = false, length = 20)
    private String name;

    private String code;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "rel_stock_cat", catalog = "sales", joinColumns = {
            @JoinColumn(name = "stock_id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "cat_id", nullable = false, updatable = false)}
    )
    private Set<Category> categories = Sets.newHashSet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "stk_code", unique = true, nullable = false, length = 10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", categories=" + categories +
                '}';
    }
}
