package org.openlogics.aop.jdbc;

import com.google.common.io.Resources;
import com.google.inject.Provider;
import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.ObjectDataStore;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: DataStoreModule2.java; mar 26, 2015 03:52 PM mvega $
 */
public class DataStoreModule2 extends ObservableDataStoreModule {

    @Override
    protected void configureDataStores() {
        Provider<ObjectDataStore> provider = new Provider<ObjectDataStore>() {
            @Override
            public ObjectDataStore get() {
                final BasicDataSource basicDataSource = new BasicDataSource();
                try {
                    basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
                    basicDataSource.setDriverClassName("org.h2.Driver");
                    Connection connection = basicDataSource.getConnection();
                    URL sql = getResource(getClass(), "foo.sql");
                    try {
                        connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                        connection.close();

                        URL resource = getResource(getClass(), "foo.xml");
                        FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                        DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(basicDataSource);
                        databaseTester.setDataSet(build);
                        databaseTester.onSetup();

                    } catch (SQLException x) {

                    }
                } catch (Exception x) {
                    x.printStackTrace();
                }

                return DataStoreFactory.createObjectDataStore(basicDataSource);
            }
        };

        Provider<ObjectDataStore> sales = new Provider<ObjectDataStore>() {
            @Override
            public ObjectDataStore get() {

                final BasicDataSource basicDataSource = new BasicDataSource();
                try {
                    basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
                    basicDataSource.setDriverClassName("org.h2.Driver");
                    Connection connection = basicDataSource.getConnection();
                    URL sql = getResource(getClass(), "sales.sql");
                    try {
                        connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                        connection.close();

                        URL resource = getResource(getClass(), "sales.xml");

                        /*
                        IDatabaseConnection dbUnitConn = new DatabaseConnection(connection, "public");
                        DatabaseConfig dbCfg = dbUnitConn.getConfig();
                        dbCfg.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, Boolean.TRUE);

                        IDataSet dataSet = new FlatXmlDataSetBuilder().build(ClassLoader.getSystemResourceAsStream("mydbunitdata.xml"));
                        DatabaseOperation.REFRESH.execute(dbUnitConn, dataSet);
                        */

                        FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                        DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(basicDataSource, null);

                        databaseTester.getConnection().getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, Boolean.TRUE);

                        databaseTester.setDataSet(build);
                        databaseTester.onSetup();

                    } catch (SQLException x) {

                    }
                } catch (Exception x) {
                    x.printStackTrace();
                }

                return DataStoreFactory.createObjectDataStore(basicDataSource);
            }
        };

        eagerSingletonDataStore(ObjectDataStore.class, DS01.class, provider);
        eagerSingletonDataStore(ObjectDataStore.class, SalesDS.class, sales);
    }
}
