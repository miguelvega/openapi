package org.openlogics.aop.jdbc;

import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: TransactionInterceptorTest.java 0, 2013-08-14 5:49 PM mvega $
 */
public class TransactionInterceptorTest {
    /*
    private static Logger logger = LoggerFactory.getLogger(TransactionInterceptorTest.class);
    @Inject
    SimpleTransactor transactor = null;
    CommonService commonService = new CommonService();


    @Before
    public void setup() {
        final BasicDataSource basicDataSource = new BasicDataSource();
        try {
            basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
            basicDataSource.setDriverClassName("org.h2.Driver");
            Connection connection = basicDataSource.getConnection();
            URL sql = getResource(getClass(), "foo.sql");
            try {
                connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                connection.close();

                URL resource = getResource(getClass(), "foo.xml");
                FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                DataSourceDatabaseTester databaseTester = new DataSourceDatabaseTester(basicDataSource);
                databaseTester.setDataSet(build);
                databaseTester.onSetup();
            } catch (SQLException x) {

            }
        } catch (Exception x) {
            x.printStackTrace();
        }

        final TransactionInterceptor tob = new TransactionInterceptor();

        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                //requestInjection(tob);

                bindInterceptor(Matchers.any(), Matchers.annotatedWith(Transactional.class), tob);

                //inject datastore
                bind(DataStore.class).toInstance(new JdbcDataStore(basicDataSource));
            }
        });

        injector.injectMembers(tob);
        injector.injectMembers(commonService);
    }

    @Test(expected = SQLException.class)
    public void testInsertTransaction() throws SQLException {
        logger.info(Strings.repeat("*", 100));
        commonService.showFoos();
        logger.info(Strings.repeat("*", 100));
        try {
            commonService.insert(new Foo());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            logger.info(Strings.repeat("*", 150));
            commonService.showFoos();
            logger.info(Strings.repeat("*", 150));
        }
    }

    @Test
    public void testInsertionSuccess() throws SQLException {
        SimpleTransactor t = new SimpleTransactor();
        t.insertFine();
    }

    @Test(expected = IllegalStateException.class)
    public void testInsertionFail() {
        SimpleTransactor t = new SimpleTransactor();
        try {
            t.insertFail();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public static class SimpleTransactor {
        public SimpleTransactor() {

        }

        @Transactional
        public void insertFine() throws SQLException {
            logger.info("Simulating a perfect insertion");
        }

        @Transactional
        public void insertFail() throws SQLException {
            throw new SQLException("Unable to perform transaction");
        }
    }
    */
}
