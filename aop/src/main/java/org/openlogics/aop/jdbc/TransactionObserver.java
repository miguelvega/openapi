package org.openlogics.aop.jdbc;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: TransactionalObserver.java; mar 26, 2015 04:17 PM mvega $
 */
public class TransactionObserver implements MethodInterceptor {

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Inject
    private Injector injector;

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Transactional target = invocation.getMethod().getAnnotation(Transactional.class);

        Class<? extends Annotation>[] surveyablesArray = target.survey();

        final StringBuilder relatedAnnotations = new StringBuilder();

        Iterable<DataStore> surveyables = null;

        if(surveyablesArray.length>0) {
            //it's possible to annotate a method with Transactional and declare the survey attribute, this means
            //that the observer must look up an instance of {@link DataStore} annotated with given parameters
            surveyables = Iterables.transform(Arrays.asList(surveyablesArray), new Function<Class<? extends Annotation>, DataStore>() {
                @Nullable
                @Override
                public DataStore apply(Class<? extends Annotation> input) {
                    try {
                        ObjectDataStore instance = injector.getInstance(Key.get(ObjectDataStore.class, input));
                        relatedAnnotations.append(input).append(",");
                        return instance;
                    } catch (Exception x) {
                        LOGGER.debug("ObjectDataStore class annotated with " + input + ", was not found in injector. Attempt to retrieve DataStore class", x);
                        try {
                            DataStore instance = injector.getInstance(Key.get(DataStore.class, input));
                            relatedAnnotations.append(input).append(",");
                            return instance;
                        } catch (Exception y) {
                            LOGGER.debug("Neither DataStore class annotated with " + input + ", was bound in injector. Return null", x);
                            return null;
                        }
                    }
                }
            });
        }else{
            LOGGER.debug("A Transactional operation has been found, but SURVEY classes have not been declared. Attempting to look up non annotated instance of either DataStore or ObjectDataStore");
            //it's possible to annotate a method with Transactional, but not declare the survey attribute, this means
            //that the observer must look up an non annotated instance of {@link DataStore}
            DataStore ds = injector.getInstance(Key.get(ObjectDataStore.class));
            if(ds==null){
                surveyables = ImmutableList.of(ds);
            }else{
                ds = injector.getInstance(Key.get(DataStore.class));
                if(ds==null)
                    surveyables = ImmutableList.of(ds);
            }
            if(surveyables == null){
                LOGGER.error("A transactional operation has been found for '{}.{}', but DataStore instance was not found for SURVEY attribute null. Specify SURVEY attribute for annotated DataStore objects or make sure that a DataStore instance is injected",
                        invocation.getMethod().getDeclaringClass().getName(), invocation.getMethod().getName());
                return invocation.proceed();
            }
        }

        List<Boolean> autoCommits = Lists.newLinkedList();
        List<Boolean> autoCloseables = Lists.newLinkedList();

        try {
            //retrieve data stores initial states
            for (DataStore surveyable : surveyables) {
                if (surveyable == null) continue;
                autoCommits.add(surveyable.isAutoCommit());
                autoCloseables.add(surveyable.isAutoClose());
                surveyable.setAutoClose(false);
                surveyable.setAutoCommit(false);
            }

            Object proceed = invocation.proceed();

            //commit must go here, but if an exception is not thrown, the commit can be executed in teh finally
            //statement.
            //TODO, verify if this is efficient

            LOGGER.debug("Transactional operation has been performed successfully");

            return proceed;
        } catch (Throwable x) {
            //if an exception is thrown, then rollback, else commit
            LOGGER.error("Transactional operation couldn't be executed due to an exception, all operations of {"+
                    relatedAnnotations
                    +"} will rollback", x);

            //retrieve data stores initial states
            for (DataStore surveyable : surveyables) {
                if (surveyable == null) continue;
                surveyable.rollBack();
            }

            throw x;
        } finally {
            //restore states
            //retrieve data stores initial states
            int at = 0;
            for (DataStore surveyable : surveyables) {
                if (surveyable == null) continue;

                surveyable.setAutoClose(autoCommits.get(at));
                surveyable.setAutoCommit(autoCloseables.get(at));
                at++;

                surveyable.tryCommitAndClose();
            }
        }
    }

    private String getKey(Class<? extends Annotation> annotation, Class<? extends DataStore> dataStoreType) {
        return annotation.getClass().getName().concat(":").concat(dataStoreType.getClass().getName());
    }
}
