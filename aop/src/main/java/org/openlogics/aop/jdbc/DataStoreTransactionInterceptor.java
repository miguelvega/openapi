package org.openlogics.aop.jdbc;

import com.google.inject.Inject;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.openlogics.gears.jdbc.DataStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A requirement to use this interceptor is that DataStore need to be injected by Guice.
 *
 * TODO: a serious problem occurred when attempting to implement a more complex app, using annotations required for more than one DataStore based app
 * @author Miguel Vega
 * @version $Id: DataStoreTransactionInterceptor.java 0, 2013-08-14 6:10 PM mvega $
 * @deprecated need to upgrade this implementation to be more fexible
 */
@Deprecated
public class DataStoreTransactionInterceptor implements MethodInterceptor {

    Logger LOGGER = LoggerFactory.getLogger(DataStoreTransactionInterceptor.class);

    @Inject(optional = false)
    private DataStore dataStore;

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        //check if method is annotated with the {@link TransactionInterceptor} class
        Transactional target = methodInvocation.getMethod().getAnnotation(Transactional.class);

        if (target == null) {
            return methodInvocation.proceed();
        }

        //need to set the data store to autocommit mode FALSE
        boolean autoCommit = dataStore.isAutoCommit();
        boolean autoClose = dataStore.isAutoClose();

        try {
            dataStore.setAutoCommit(false);

            Object proceed = methodInvocation.proceed();

            //commit must go here, but if an exception is not thrown, the commit can be executed in teh finally
            //statement.
            //TODO, verify if this is efficient

            LOGGER.debug("Transactional operation has been performed successfully");

            return proceed;
        } catch (Throwable x) {
            //if an exception is thrown, then rollback, else commit
            LOGGER.error("Transactional operation couldn't be executed due to an exception all operations will rollback", x);
            dataStore.rollBack();
            throw x;
        } finally {
            dataStore.setAutoCommit(autoCommit);
            dataStore.setAutoClose(autoClose);
            dataStore.tryCommitAndClose();
        }
    }
}
