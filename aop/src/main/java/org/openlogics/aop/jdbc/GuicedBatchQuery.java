package org.openlogics.aop.jdbc;

import com.google.inject.Inject;
import org.openlogics.gears.jdbc.BatchQuery;
import org.openlogics.gears.jdbc.DataStore;

import java.sql.SQLException;

/**
 * This version of {@link BatchQuery}, takes advantage of an injected {@link DataStore} object.
 * Thus avoiding the parameter sending.
 *
 * Remember to provide the query string via {@link GuicedBatchQuery#applyQueryString(String)}, because query string is
 * null initially.
 *
 * @author Miguel Vega
 * @version $Id: GuicedBatchQuery.java 0, 2013-11-01 11:01 AM mvega $
 */
public class GuicedBatchQuery extends BatchQuery{

    private DataStore dataStore;

    @Inject
    public GuicedBatchQuery(DataStore dataStore) {
        super(null);

        this.dataStore = dataStore;
    }

    public GuicedBatchQuery applyQueryString(String queryString){
        this.queryString = queryString;
        return this;
    }

    public GuicedBatchQuery addBatch(Object context) throws SQLException {
        super.addBatch(dataStore, context);
        return this;
    }

}
