package org.openlogics.aop.jdbc;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import com.google.inject.matcher.Matchers;
import org.openlogics.gears.jdbc.DataStore;

import java.lang.annotation.Annotation;

/**
 * @author Miguel Vega
 * @version $Id: DataStoreModule.java; mar 26, 2015 03:48 PM mvega $
 */
public abstract class ObservableDataStoreModule extends AbstractModule{

    public final void configure(){
        TransactionObserver transactionObserver = new TransactionObserver();

        requestInjection(transactionObserver);

        bindInterceptor(
                Matchers.any(),
                Matchers.annotatedWith(Transactional.class),
                transactionObserver);

        configureDataStores();
    }

    protected abstract void configureDataStores();

    /**
     * If application will use a simple DataSTore along all the application, this will be applied by default on every
     * injection
     * @param dsType
     * @param provider
     */
    public void bindEagerSingletonDataStore(Class<? extends DataStore> dsType, Provider provider){
        bind(dsType).toProvider(provider).asEagerSingleton();
    }

    public <T extends DataStore> void bindEagerSingletonDataStore(Provider<T> provider){
        final DataStore instance = provider.get();
        bind(instance.getClass()).toProvider(new Provider() {
            @Override
            public Object get() {
                return instance;
            }
        }).asEagerSingleton();
    }

    /**
     * Most of the applications need to manage more than one DataStore, the more comfortable way of doing it is
     * using annotations to differentiate them.
     * @param dsType the target Type to match
     * @param annotationType
     * @param provider
     */

    public <T extends DataStore> void eagerSingletonDataStore(Class<? extends DataStore> dsType, Class<? extends Annotation> annotationType, Provider provider){
        bind(dsType).annotatedWith(annotationType).toProvider(provider).asEagerSingleton();
    }

    public void bindNoScopeDataStore(Class<? extends DataStore> dsType, Annotation annotation, Provider provider){
        bind(dsType).annotatedWith(annotation).toProvider(provider).in(Scopes.NO_SCOPE);
    }
}
