/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openlogics.aop.jdbc;

import java.lang.annotation.*;

/**
 * This annotation will ensure that the transactions inside this block get committed or rolled back,
 * if a {@link java.lang.Throwable} is thrown, the process inside wont be persisted.
 *
 * @author Miguel Vega
 * @version $Id: Transactional.java 0, 2012-11-30 5:31 PM mvega $
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Transactional {
    /**
     * Many applications can use more than just one {@link org.openlogics.gears.jdbc.DataStore}, so also a
     * transaction can involve many datastores to perform such that process. Given this class array
     * the {@link org.openlogics.aop.jdbc.TransactionObserver} will catch those DataStores annotated
     * and will commit or rollback after method is executed.
     * @return
     */
    Class<? extends Annotation>[] survey() default {};
}
