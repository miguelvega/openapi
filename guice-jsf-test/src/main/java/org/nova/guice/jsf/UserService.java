package org.nova.guice.jsf;

/**
 * @author Miguel Vega
 * @version $Id: UserService.java 0, 5/29/14 11:29 AM, @miguel $
 */
public interface UserService {
    String sayHello();
}
