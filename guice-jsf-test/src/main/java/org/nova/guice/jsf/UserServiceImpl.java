package org.nova.guice.jsf;

import com.google.inject.Inject;

/**
 * @author Miguel Vega
 * @version $Id: UserServiceImpl.java 0, 5/29/14 11:29 AM, @miguel $
 */
public class UserServiceImpl implements UserService {

    @Inject
    @Config
    private String greetings;

    @Override
    public String sayHello() {
        return greetings;
    }
}
