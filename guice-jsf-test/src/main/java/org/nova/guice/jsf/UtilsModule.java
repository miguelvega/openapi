package org.nova.guice.jsf;

import com.google.inject.AbstractModule;

/**
 * @author Miguel Vega
 * @version $Id: UtilsModule.java 0, 5/29/14 11:24 AM, @miguel $
 */
public class UtilsModule extends AbstractModule{
    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Config.class).toInstance("Hello");
    }
}
