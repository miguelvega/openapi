package org.nova.guice.jsf;

/**
 * @author Miguel Vega
 * @version $Id: HelloBean.java 0, 5/28/14 4:29 PM, @miguel $
 */

import com.google.inject.Inject;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class HelloBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    @Inject
    private UserService userService;

    public String getName() {
        return userService.sayHello()+name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String gotoGoogle(){
        try {
            ((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse()).sendRedirect("http://www.google.com");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}