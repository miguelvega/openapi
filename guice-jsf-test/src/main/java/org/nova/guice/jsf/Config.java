package org.nova.guice.jsf;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation will allow to explicitly define the TYPE dialect handler of data used by the JDBC driver to handle specific
 * column values.
 * <br/>
 * e.g.: oracle.jdbc.OracleDriver, uses timestamp values as oracle.sql.TIMESTAMP, then a dialect handler can be explicitly declared here.
 *
 * @author Miguel Vega
 * @version $Id: MappedType.java 0, 2013-09-17 6:43 PM mvega $
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@BindingAnnotation
public @interface Config{
}