package org.nova.guice.jsf;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

/**
 * @author Miguel Vega
 * @version $Id: ServiceModule.java 0, 5/29/14 11:24 AM, @miguel $
 */
public class ServiceModule extends AbstractModule{
    @Override
    protected void configure() {
        bind(UserService.class).to(UserServiceImpl.class);
    }
}
