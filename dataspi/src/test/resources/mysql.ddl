

-- DROP TABLE public.customer;

CREATE TABLE currencies
(
  curr_id    SMALLINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  curr_abbrev VARCHAR(4) NOT NULL,
  curr_desc   VARCHAR(10) NOT NULL,
  curr_xchange DECIMAL(10, 2) NOT NULL
);

CREATE TABLE account_types
(
  acctp_id    SMALLINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  acctp_desc VARCHAR(50) NOT NULL
);

CREATE TABLE customers
(
  cust_id BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY ,
  cust_phone VARCHAR(10) NOT NULL,
  cust_saved DATETIME NOT NULL DEFAULT now(),
  cust_full_name VARCHAR(30) NOT NULL,
  cust_nat_id VARCHAR(10) NOT NULL
);

-- DROP TABLE public.accounts;
CREATE TABLE accounts
(
  acc_nbr BIGINT NOT NULL,
  acc_cust_id BIGINT NOT NULL REFERENCES customers(cust_id),
  acc_type SMALLINT NOT NULL REFERENCES account_types (acctp_id),
  acc_currency SMALLINT NOT NULL REFERENCES currencies(curr_id),
  acc_holder VARCHAR(30),
  acc_due_date date,
  PRIMARY KEY (acc_nbr)
);

CREATE TABLE account_transactions
(
  tx_id BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY ,
  tx_acc DECIMAL(10,0) NOT NULL REFERENCES accounts(acc_nbr),
  tx_date DATETIME NOT NULL,
  tx_amount DECIMAL(18, 2) NOT NULL,
  tx_branch MEDIUMINT,
  tx_cashier MEDIUMINT,
  tx_seq BIGINT
);

/******************************* INSERT *************************************/
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('BOB', 'Bolivianos', 1);
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('USD', 'Dollar', 6.87);
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('EUR', 'Euro', 7.68);

INSERT INTO account_types (acctp_desc) VALUES ('Cuenta corriente');
INSERT INTO account_types (acctp_desc) VALUES ('Caja de ahorro');
INSERT INTO account_types (acctp_desc) VALUES ('Linea de credito');

INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('79630533', 'Miguel Angel Vega Pabon', '6105078');
INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('77390925', 'Carlos Cuellar Urgel', '4589657');
INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('75856895', 'Lizeth Marcela Gomez Ocampo', '6245899');

INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2968194014, 1, 1, 1, 'Miguel Angel Vega Pabon', '2012-04-28');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2968195010, 1, 2, 2, 'Miguel Angel Vega Pabon', '2014-09-10');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (165484014, 1, 1, 1, 'Carlos Cuellar Urgel', '2009-02-01');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (165485014, 1, 2, 2, 'Rossy Gumiel Romero', '2012-06-12');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2889995010, 1, 3, 2, 'Julio Leon Suarez', '2013-01-15');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (4950504014, 1, 1, 3, 'Lizeth Marcela Gomez Ocampo', '2015-11-18');