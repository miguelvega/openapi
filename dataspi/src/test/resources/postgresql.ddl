

-- DROP TABLE public.customer;

CREATE TABLE public.currencies
(
  curr_id    SERIAL,
  curr_abbrev CHARACTER VARYING(4) NOT NULL,
  curr_desc   CHARACTER VARYING(10) NOT NULL,
  curr_xchange NUMERIC (10, 2) NOT NULL,
  CONSTRAINT currency_pkey PRIMARY KEY (curr_id)
);

CREATE TABLE public.account_types
(
  acctp_id    SERIAL,
  acctp_desc CHARACTER VARYING(50) NOT NULL,
  CONSTRAINT acc_type_pkey PRIMARY KEY (acctp_id)
);

CREATE TABLE public.customers
(
  cust_id SERIAL,
  cust_phone character varying(10) NOT NULL,
  cust_saved timestamp without time zone NOT NULL DEFAULT now(),
  cust_full_name character varying(30) NOT NULL,
  cust_nat_id character varying(10) NOT NULL,
  CONSTRAINT customer_pkey PRIMARY KEY (cust_id)
)
WITH (
OIDS=FALSE
);

-- DROP TABLE public.accounts;
CREATE TABLE public.accounts
(
  acc_nbr numeric(10,0) NOT NULL,
  acc_cust_id integer,
  acc_type integer,
  acc_currency integer,
  acc_holder character varying(30),
  acc_due_date date,
  CONSTRAINT account_pkey PRIMARY KEY (acc_nbr),
  CONSTRAINT accounts_acc_currency_fkey FOREIGN KEY (acc_currency)
  REFERENCES public.currencies (curr_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT accounts_acc_type_fkey FOREIGN KEY (acc_type)
  REFERENCES public.account_types (acctp_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT accounts_acct_cust_id_fkey FOREIGN KEY (acc_cust_id)
  REFERENCES public.customers (cust_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

CREATE TABLE public.account_transactions
(
  tx_id SERIAL,
  tx_acc numeric(10,0) NOT NULL,
  tx_date TIMESTAMP NOT NULL,
  tx_amount NUMERIC(18, 2) NOT NULL,
  tx_branch INTEGER,
  tx_cashier INTEGER,
  tx_seq INTEGER,
  CONSTRAINT transaction_pkey PRIMARY KEY (tx_id),
  CONSTRAINT acctx_acc_id_fkey FOREIGN KEY (tx_acc)
  REFERENCES public.accounts (acc_nbr) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

/******************************* INSERT *************************************/
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('BOB', 'Bolivianos', 1);
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('USD', 'Dollar', 6.87);
INSERT INTO currencies (curr_abbrev, curr_desc, curr_xchange) VALUES ('EUR', 'Euro', 7.68);

INSERT INTO account_types (acctp_desc) VALUES ('Cuenta corriente');
INSERT INTO account_types (acctp_desc) VALUES ('Caja de ahorro');
INSERT INTO account_types (acctp_desc) VALUES ('Linea de credito');

INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('79630533', 'Miguel Angel Vega Pabon', '6105078');
INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('77390925', 'Carlos Cuellar Urgel', '4589657');
INSERT INTO customers (cust_phone, cust_full_name, cust_nat_id) VALUES ('75856895', 'Lizeth Marcela Gomez Ocampo', '6245899');

INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2968194014, 1, 1, 1, 'Miguel Angel Vega Pabon', '2012-04-28');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2968195010, 1, 2, 2, 'Miguel Angel Vega Pabon', '2014-09-10');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (165484014, 1, 1, 1, 'Carlos Cuellar Urgel', '2009-02-01');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (165485014, 1, 2, 2, 'Rossy Gumiel Romero', '2012-06-12');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (2889995010, 1, 3, 2, 'Julio Leon Suarez', '2013-01-15');
INSERT INTO accounts (acc_nbr, acc_cust_id, acc_type, acc_currency, acc_holder, acc_due_date) VALUES (4950504014, 1, 1, 3, 'Lizeth Marcela Gomez Ocampo', '2015-11-18');