/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openapi.dataspi;

import com.google.common.io.Resources;
import org.apache.commons.dbcp2.BasicDataSource;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: DataSourceProvider.java 1, 2012-10-05 10:10 AM mvega $
 */
public abstract class DataSourceProvider {
    protected IDatabaseTester databaseTester;
    protected BasicDataSource memDataSource;

    protected Logger logger;

    @Before
    public void setup() {
        this.logger = LoggerFactory.getLogger(getClass());

        try {

            memDataSource = new BasicDataSource();
            memDataSource.setUrl("jdbc:h2:mem:parametrostest");
            memDataSource.setDriverClassName("org.h2.Driver");
            Connection connection = memDataSource.getConnection();
            URL sql = getResource(DataSourceProvider.class, "foo.sql");
            try {
                String DDL = Resources.toString(sql, US_ASCII);
                connection.createStatement().execute(DDL);
                connection.close();

                URL resource = getResource(DataSourceProvider.class, "foo.xml");
                FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                databaseTester = new DataSourceDatabaseTester(memDataSource);
                databaseTester.setDataSet(build);
                databaseTester.onSetup();
            } catch (SQLException x) {
                x.printStackTrace();
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    @After
    public void dispose() throws Exception {
        if(databaseTester!=null)databaseTester.onTearDown();
        if(memDataSource!=null)memDataSource.close();
    }

    /*
    protected void viewAll(DataStore ds) throws SQLException {
        logger.info(System.getProperty("line.separator"));
        Query query = Query.of("select FOO_ID, " +
                "FOO_FNAME, " +
                "FOO_LNAME, " +
                "FOO_RATE as rate, " +
                "FOO_ADD_DATE from FOO");
        List<Foo> stds = ds.select(query, Foo.class);
        //List<Map<String, Object>> stds = ds.select(query, new MapListHandler());
        logger.info("************************************ VIEW ALL, BEGIN *****************************************************");
        for (Foo std : stds) {
            //for (Map<String, Object> std : stds) {
            logger.info("Result > " + std);
        }
        logger.info("************************************* VIEW ALL, END****************************************************");
        logger.info(System.getProperty("line.separator"));
    }

    protected long countAll(DataStore ds) throws SQLException {
        Query query = Query.of("select COUNT(FOO_ID) from FOO");
        return ds.select(query, new ScalarHandler<Long>(1));
    }
    */
}
