package org.openapi.dataspi;

import org.junit.Before;
import org.junit.Test;
import org.openapi.dataspi.entity.Currency;
import org.openapi.dataspi.entity.Invoice;
import org.openapi.dataspi.entity.InvoiceType;
import org.openapi.dataspi.entity.Product;
import org.openapi.dataspi.handler.EntityHandler;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Miguel A. Vega P.
 * @version $Id: JPAQueryTest.java; Apr 27, 2016. 11:26 PM mvega $
 * @source $URL$
 */
public class JPAQueryTest extends DataSourceProvider{

    private DataStore dataStore;

    @Before
    public void init(){
        dataStore = DetachableDataStore.getDataStore(memDataSource);
    }


    @Test
    public void testGenerateProductBigData(){
        String xml = "<PRODUCTS PROD_NAME=\"$1\" PROD_PRICE=\"1.25\" PROD_CURR_ID=\"2\"/>";

        for (int i = 0; i < 1000; i++) {
            System.out.println(xml.replace("$1", "Product "+i));
        }
    }

    @Test
    public void testComparingReaders(){

        int times = 100;

        AtomicLong t1 = new AtomicLong(0);
        AtomicLong t2 = new AtomicLong(0);

        for (int i = 0; i < times; i++) {
            t2.addAndGet(readAll(Product.class, false));
            t1.addAndGet(readAll2(Product.class, false));
        }

        logger.info("The OLD flavor type took average of {} milliseconds", ((double)t1.get()/(double)times));
        logger.info("The NEW flavor type took average of {} milliseconds", ((double)t2.get()/(double)times));
    }

    @Test
    public void testQuery(){
        JPAQuery<Product> query = dataStore.prepareJpaQuery(Product.class);
        Product single = query.getSingleResult();

        logger.info("\n");
        logger.info("Product {}", single);

        logger.info("\n");

        readAll(Product.class, true);

        //test query of invoices
        readAll(Invoice.class, true);
    }

    private long readAll(Class<?> clazz, boolean display){
        AtomicLong delay = new AtomicLong(System.currentTimeMillis());

        logger.info("********************** {} ********************", clazz.getName());
        JPAQuery<?> iQuery = dataStore.prepareJpaQuery(clazz);
        List<?> resultList = iQuery.getResultList();

        if(display)
        for (Object o : resultList) {
            logger.info("{}", o);
        }
        long time = System.currentTimeMillis() - delay.get();
        logger.info("New flavor: List with {} elements took {} milliseconds\n", resultList.size(), time);
        return time;
    }

    private long readAll2(Class<?> clazz, boolean display){
        AtomicLong delay = new AtomicLong(System.currentTimeMillis());

        logger.info("********************** {} ********************", clazz.getName());
        JPAQuery<?> iQuery = dataStore.prepareJpaQuery(clazz);
        List<?> resultList = iQuery.getResultListOldFlavor();

        if(display)
        for (Object o : resultList) {
            logger.info("{}", o);
        }
        long time = System.currentTimeMillis() - delay.get();
        logger.info("Old flavor: List with {} elements took {} milliseconds\n", resultList.size(), time);

        return time;
    }

    @Test
    public void testEntityReader(){
        SQLQuery sqlQuery = dataStore.prepareSqlQuery("select * from products");
        Product prod = sqlQuery.getSingleResult(new EntityHandler<>(Product.class));
        logger.info("Product is: {}", prod);
    }

    @Test
    public void testInsertProduct(){
        JPAQuery<Product> query = dataStore.prepareJpaQuery(Product.class);

        Product p = new Product();
        p.setAvailable(false);
        p.setName("A dummy");
        p.setPrice(new BigDecimal("100.50"));
        p.setCurrency(Currency.Euro);

        Product insert = query.insert(p);

        logger.info("I fetched my ID as...{}", insert.getId());

        readAll(Product.class, true);
    }

    @Test
    public void testInsertInvoice(){
        JPAQuery<Invoice> query = dataStore.prepareJpaQuery(Invoice.class);

        Invoice invoice = new Invoice().setInvoiceType(InvoiceType.E1).setCustNbr("2968194014").
                setDueDate(new GregorianCalendar()).setEmissionDate(new Date()).setName("My dmmy invoice").setTotal(BigDecimal.ONE);

        invoice = query.insert(invoice);

        logger.info("I fetched my ID as...{}", invoice.getId());

        readAll(Invoice.class, true);
    }

    @Test
    public void testUpdateInvoice(){
        JPAQuery<Invoice> query = dataStore.prepareJpaQuery(Invoice.class);

        Invoice invoice = query.getSingleResult();

        logger.info("0. "+invoice);

        query.update(invoice);

        readAll(Invoice.class, true);
    }
}