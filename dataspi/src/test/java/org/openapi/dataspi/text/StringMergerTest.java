package org.openapi.dataspi.text;

import org.junit.Test;
import org.openapi.lang.StringMerger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.openapi.lang.StringMerger.of;
import static org.openapi.lang.StringMerger.withSeparator;

/**
 * @author Miguel A. Vega P.
 * @version $Id: StringMergerTest.java; Apr 23, 2016. 4:52 PM mvega $
 * @source $URL$
 */
public class StringMergerTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testCapabilities(){
        String merge = of("1", "9", "8", "4").withStyle(StringMerger.Style.CAPITALIZED).merge();
        assertEquals("1984", merge);

        merge = withSeparator(" ").app("miguel").app("angel").app("vega").app("pabon").merge();
        assertEquals("miguel angel vega pabon", merge);

        logger.info(of("FROM HERE").app("'").prep("'").merge());
    }
}