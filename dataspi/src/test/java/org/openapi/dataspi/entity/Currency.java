package org.openapi.dataspi.entity;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Currency.java; May 07, 2016. 10:13 AM mvega $
 * @source $URL$
 */
public enum Currency {
    Boliviano, Dolar, Euro
}
