package org.openapi.dataspi.entity;

/**
 * @author Miguel A. Vega P.
 * @version $Id: InvoiceType.java; Sep 15, 2017 1:21 AM miguel.vega $
 */
public enum InvoiceType {
    E0, E1
}
