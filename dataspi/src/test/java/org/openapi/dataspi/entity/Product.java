package org.openapi.dataspi.entity;

import org.openapi.lang.StringMerger;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Product.java; May 07, 2016. 10:10 AM mvega $
 * @source $URL$
 */
@Table(name = "products")
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROD_ID")
    private Long id;

    @Column(name = "PROD_NAME")
    private String name;

    @Column(name = "PROD_PRICE")
    private BigDecimal price;

    @Column(name = "PROD_CURR_ID")
    @Enumerated(value = EnumType.ORDINAL)
    private Currency currency;

    @Column(name = "PROD_AVAILABLE")
    private boolean available;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return StringMerger.of("Product:{ID=", id, ", name=", name, ", price=", price, ", currency=", currency, ", available=", available, "}").merge();
    }
}