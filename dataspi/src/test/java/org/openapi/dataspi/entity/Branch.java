package org.openapi.dataspi.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Branch.java; Sep 15, 2017 8:37 PM miguel.vega $
 */
@Table(schema= "BANKING_CORE")
public class Branch {
    @EmbeddedId
    private BranchId branchId;

    @Column
    private String desc;

    @Column
    private boolean active;

    public BranchId getBranchId() {
        return branchId;
    }

    public Branch setBranchId(BranchId branchId) {
        this.branchId = branchId;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public Branch setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public Branch setActive(boolean active) {
        this.active = active;
        return this;
    }
}
