package org.openapi.dataspi.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Miguel A. Vega P.
 * @version $Id: BranchId.java; Sep 15, 2017 8:39 PM miguel.vega $
 */
@Embeddable
public class BranchId {
    @Column()
    int cityId;
    @Column(name = "branch_id")
    int branchId;

    public int getCityId() {
        return cityId;
    }

    public BranchId setCityId(int cityId) {
        this.cityId = cityId;
        return this;
    }

    public int getBranchId() {
        return branchId;
    }

    public BranchId setBranchId(int branchId) {
        this.branchId = branchId;
        return this;
    }
}
