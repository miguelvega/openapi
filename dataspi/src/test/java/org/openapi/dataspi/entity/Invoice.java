package org.openapi.dataspi.entity;

import org.openapi.lang.StringMerger;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Invoice.java; Sep 15, 2017 1:17 AM miguel.vega $
 */
@Table(name = "invoices")
@Entity
public class Invoice {
    @Id
    @GeneratedValue
    @Column(name = "INV_ID")
    private long id;
    @Column(name = "INV_NAME")
    private String name;
    @Column(name = "INV_CUST_NBR")
    private String custNbr;
    @Column(name = "INV_TOTAL")
    private BigDecimal total;
    @Column(name = "INV_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date emissionDate;
    @Column(name = "INV_DUE_DATE")
    @Temporal(TemporalType.DATE)
    private Calendar dueDate;
    @Column(name = "INV_TYPE")
    @Enumerated(EnumType.STRING)
    private InvoiceType invoiceType;

    public long getId() {
        return id;
    }

    public Invoice setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Invoice setName(String name) {
        this.name = name;
        return this;
    }

    public String getCustNbr() {
        return custNbr;
    }

    public Invoice setCustNbr(String custNbr) {
        this.custNbr = custNbr;
        return this;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public Invoice setTotal(BigDecimal total) {
        this.total = total;
        return this;
    }

    public Date getEmissionDate() {
        return emissionDate;
    }

    public Invoice setEmissionDate(Date emissionDate) {
        this.emissionDate = emissionDate;
        return this;
    }

    public Calendar getDueDate() {
        return dueDate;
    }

    public Invoice setDueDate(Calendar dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public Invoice setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
        return this;
    }

    @Override
    public String toString() {
        return StringMerger.of("Invoice:{ID=", id, ", name=", name, ", total=", total, ", type=", invoiceType, ", date=", emissionDate,
                ", due date=", dueDate, ", customer=", custNbr, "}").merge();
    }
}
