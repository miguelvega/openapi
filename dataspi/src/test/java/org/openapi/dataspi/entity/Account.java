package org.openapi.dataspi.entity;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Account.java; May 16, 2016. 5:13 PM mvega $
 * @source $URL$
 */
@Entity
@Table(name = "accounts")
public class Account {

    @Column(name = "acc_nbr")
    @Id
    @GeneratedValue
    private BigInteger id;

    @Column(name = "acc_cust_id")
    private Long customerId;

    @Column(name = "acc_type")
    private int accType;

    @Column(name = "acc_currency")
    private int currency;

    @Column(name = "acc_holder")
    private String bound;

    @Column(name = "acc_due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;


    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public int getAccType() {
        return accType;
    }

    public void setAccType(int accType) {
        this.accType = accType;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public String getBound() {
        return bound;
    }

    public void setBound(String bound) {
        this.bound = bound;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
}
