package org.openapi.dataspi.handler;

import org.javatuples.Pair;
import org.junit.Test;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.entity.Currency;
import org.openapi.dataspi.entity.Invoice;
import org.openapi.dataspi.entity.InvoiceType;
import org.openapi.dataspi.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Miguel A. Vega P.
 * @version $Id: SQLStatementBuilderTest.java; Aug 03, 2017 12:11 AM miguel.vega $
 */
public class SQLStatementBuilderTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testBuildInsert() {
        SQLStatementBuilder<Product> builder = new SQLStatementBuilder<>(new DefaultSQLDialect());

        Product p = getDummyProduct();

        Pair<String, List> pair = builder.buildPreparedInsert(p);

        logger.info("Product. "+builder.prettyPrintStatemet(pair));

        SQLStatementBuilder<Invoice> ibuilder = new SQLStatementBuilder<>(new DefaultSQLDialect());
        Invoice i = getDummyInvoice();

        pair = ibuilder.buildPreparedInsert(i);

        logger.info("Invoice. "+ibuilder.prettyPrintStatemet(pair));
    }

    public static Product getDummyProduct(){
        Product p = new Product();
        p.setName("Cookies");
        p.setAvailable(true);
        p.setCurrency(Currency.Boliviano);
        p.setId(1001l);
        p.setPrice(BigDecimal.TEN);
        return p;
    }

    public static Invoice getDummyInvoice(){
        return new Invoice().setInvoiceType(InvoiceType.E0).setCustNbr("2968194014").
                setDueDate(Calendar.getInstance()).setEmissionDate(new Date()).setTotal(new BigDecimal("123.45")).
                setId(10).setName("The invoice name");
    }


}