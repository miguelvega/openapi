package org.openapi.dataspi.jpa.lite;

import org.junit.Test;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.entity.*;
import org.openapi.dataspi.handler.meta.ColumnFieldProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: StatementBuilderTest.java 0, 2017-sep-15 06:39 PM miguelvega $
 */
public class StatementBuilderTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testBuildSelect() {
        AbstractStatementBuilder<Product> builder = new SelectStatementBuilder(Product.class).setShowFullSQLStatement(true);
        builder.buildStatement(null, new DefaultSQLDialect());

        try {
            ColumnFieldProcessor processor = builder.getEntityMetaData().getIdentifierField();

            logger.info("Found the following as primary key: {}", processor);

        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void testBuildInsert() {
        StatementBuilder<Invoice> builder = new InsertStatementBuilder(Invoice.class).setShowFullSQLStatement(true);

        Invoice invoice = new Invoice().setInvoiceType(InvoiceType.E0).setCustNbr("2968194014").
                setDueDate(Calendar.getInstance()).setEmissionDate(new Date()).setTotal(new BigDecimal("123.45")).
                setId(10).setName("The invoice name");

        builder.buildStatement(invoice, new DefaultSQLDialect());
    }

    @Test
    public void testBuildUpdate() {
        StatementBuilder<Invoice> builder = new UpdateStatementBuilder<>(Invoice.class).setShowFullSQLStatement(true);

        Invoice invoice = new Invoice().setInvoiceType(InvoiceType.E0).setCustNbr("2968194014").
                setDueDate(Calendar.getInstance()).setEmissionDate(new Date()).setTotal(new BigDecimal("123.45")).
                setId(10).setName("The invoice name");

        builder.buildStatement(invoice, new DefaultSQLDialect());
    }

    @Test
    public void testBuildUpdateEmbeddedPK() {
        StatementBuilder<Branch> builder = new UpdateStatementBuilder<>(Branch.class).setShowFullSQLStatement(true);

        Branch branch= new Branch().setDesc("La Paz").setBranchId(new BranchId().setBranchId(1).setCityId(10));

        builder.buildStatement(branch, new DefaultSQLDialect());
    }

    @Test
    public void testBuildDelete() {
        StatementBuilder<Invoice> builder = new DeleteStatementBuilder(Invoice.class).setShowFullSQLStatement(true);

        Invoice invoice = new Invoice().setInvoiceType(InvoiceType.E0).setCustNbr("2968194014").
                setDueDate(Calendar.getInstance()).setEmissionDate(new Date()).setTotal(new BigDecimal("123.45")).
                setId(10).setName("The invoice name");

        builder.buildStatement(invoice, new DefaultSQLDialect());
    }

}