package org.openapi.dataspi.jpa.lite;

import org.junit.Test;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.entity.Branch;

/**
 * @author Miguel A. Vega P.
 * @version $Id: OtherStatementBuilderTest.java; Sep 15, 2017 8:44 PM miguel.vega $
 */
public class OtherStatementBuilderTest {

    @Test
    public void testEmbeddedId(){
        StatementBuilder<Branch> builder = new SelectStatementBuilder<>(Branch.class).setShowFullSQLStatement(true);

        builder.buildStatement(null, new DefaultSQLDialect());
    }

}
