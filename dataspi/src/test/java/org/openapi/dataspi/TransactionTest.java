package org.openapi.dataspi;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

/**
 * @author Miguel A. Vega P.
 * @version $Id: TransactionTest.java; Apr 23, 2016. 10:09 PM mvega $
 * @source $URL$
 */
public class TransactionTest extends DataSourceProvider{

    TransactionalDataStore h2DataStore;
    TransactionalDataStore pgsqlDataStore;

    @Before
    public void init() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUsername("postgres");
        ds.setPassword("postgres");
        ds.setUrl("jdbc:postgresql://127.0.0.1:5432/dataspi");

        this.pgsqlDataStore = TransactionalDataStore.getDataStore(ds);
        this.h2DataStore = TransactionalDataStore.getDataStore(memDataSource);
    }

    @Test
    public void testTransactionWithoutCmmit(){

        Query query = pgsqlDataStore.prepareSqlQuery("INSERT INTO account_transactions (tx_acc, tx_date, tx_amount) VALUES (?, ?, ?)",
                new BigDecimal("2968194014"), new Timestamp(System.currentTimeMillis()), new BigDecimal(10.50d));

        int i = query.executeUpdate();

        logger.info("Transaction didn't commit after execution with {} rows affected", i);

        pgsqlDataStore.commitAndCloseQuetly();
    }

    @Test
    public void testTransaction() throws SQLException {

        Transaction tx = new Transaction(pgsqlDataStore, h2DataStore);

        Integer rows = tx.doTransact(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                for (int i = 0; i < 10; i++) {
                    Query query = pgsqlDataStore.prepareSqlQuery("INSERT INTO account_transactions (tx_acc, tx_date, tx_amount) VALUES (?, ?, ?)",
                            new BigDecimal("2968194014"), new Timestamp(System.currentTimeMillis()), new BigDecimal(10.50d));
                    query.executeUpdate();
                }

                throw new NullPointerException("A contolled exception");

            }
        });

        logger.info("Transaction didn't commit after execution with {} rows affected", rows);
    }
}