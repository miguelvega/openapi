package org.openapi.dataspi;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author Miguel A. Vega P.
 * @version $Id: SQLQueryTest.java; Apr 23, 2016. 6:11 PM mvega $
 * @source $URL$
 */
public class SQLQueryTest extends DataSourceProvider{

    private DataStore dataStore;

    @Before
    public void init(){
        dataStore = DetachableDataStore.getDataStore(memDataSource);
    }

    @Test
    public void testSelectResultsAll(){

        Query query = dataStore.prepareSqlQuery("select * from products");

        List resultList = query.getResultList();

        for (Object row : resultList) {
            logger.info("ROW:{}", row);
        }
    }

    @Test
    public void testSelectSingleResult(){
        Query query = dataStore.prepareSqlQuery("select * from products where Prod_ID = ?", 1);

        Map<String, Object> row = ((SQLQuery)query).getSingleResult();

        logger.info("ROW:{}", row);

    }

    @Test
    public void testExecuteUpdate(){
        Query query = dataStore.prepareSqlQuery("insert into products (PROD_NAME, PROD_PRICE) values (?, ?)",
                "Nuevo Producto", new BigDecimal("10.50"));

        int i = query.executeUpdate();

        logger.info("{} rows have been affected", i);

        testSelectResultsAll();
    }

    @Test
    public void testInsertBatch(){
        BatchQuery batchQuery = dataStore.prepareBatchQuery("insert into products (PROD_NAME, PROD_PRICE) values (?, ?)");

        batchQuery.addBatch("Producto 10", new BigDecimal(10));
        batchQuery.addBatch("Producto 11", new BigDecimal(11));
        batchQuery.addBatch("Producto 12", new BigDecimal(12));
        batchQuery.addBatch("Producto 13", new BigDecimal(13));

        int[] results = batchQuery.executeBatch();

        for (int result : results) {
            logger.info("{} rows affected by batch exec", result);
        }

        testSelectResultsAll();
    }
}