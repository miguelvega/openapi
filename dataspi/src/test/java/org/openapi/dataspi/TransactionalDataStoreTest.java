package org.openapi.dataspi;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.openapi.dataspi.entity.Account;
import org.openapi.dataspi.handler.EntityHandler;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Miguel A. Vega P.
 * @version $Id: TransactionalDataStoreTest.java; May 07, 2016. 2:09 AM mvega $
 * @source $URL$
 */
public class TransactionalDataStoreTest extends DataSourceProvider{

    @Before
    public void init(){

    }

    private DataSource getPostgreSQL(){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUsername("postgres");
        ds.setPassword("postgres");
        ds.setUrl("jdbc:postgresql://127.0.0.1:5432/dataspi");
        return ds;
    }

    private DataSource getMySQL(){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername("root");
        ds.setPassword("Mavp8584*");
        ds.setUrl("jdbc:mysql://127.0.0.1:3306/dataspi");
        return ds;
    }

    @Test
    public void testQuery() throws SQLException {
        TransactionalDataStore mysql = TransactionalDataStore.getDataStore(getMySQL());
        TransactionalDataStore pgsql = TransactionalDataStore.getDataStore(getPostgreSQL());

        SQLQuery sqlQuery = mysql.prepareSqlQuery("Select * from accounts");
        Account account = sqlQuery.getSingleResult(new EntityHandler<Account>(Account.class));

        System.out.println(">>>>>> "+account.getBound());

    }
}