package org.openapi.dataspi;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Hints.java; Apr 23, 2016. 2:08 AM mvega $
 * @source $URL$
 */
public class Hints {

    private boolean closeImmediatelty = true;

    private boolean commitImmediately = true;

    //when true, avoids to query parameter metadata from functions or stored procedures
    private boolean parameterMetaDataKnownBroken = false;

    /*
     * transactionIsolation, allows to define the isolation level for the transaction,
     * useful when need to lock rows from database.
     */
    private int transactionIsolation = -1;

    public boolean isCloseImmediatelty() {
        return closeImmediatelty;
    }

    public void setCloseImmediatelty(boolean closeImmediatelty) {
        this.closeImmediatelty = closeImmediatelty;
    }

    public boolean isCommitImmediately() {
        return commitImmediately;
    }

    public void setCommitImmediately(boolean commitImmediately) {
        this.commitImmediately = commitImmediately;
    }

    public int getTransactionIsolation() {
        return transactionIsolation;
    }

    public void setTransactionIsolation(int transactionIsolation) {
        this.transactionIsolation = transactionIsolation;
    }

    public boolean isParameterMetaDataKnownBroken() {
        return parameterMetaDataKnownBroken;
    }

    public void setParameterMetaDataKnownBroken(boolean parameterMetaDataKnownBroken) {
        this.parameterMetaDataKnownBroken = parameterMetaDataKnownBroken;
    }
}
