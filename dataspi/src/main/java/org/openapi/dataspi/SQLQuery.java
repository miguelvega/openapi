package org.openapi.dataspi;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import javax.persistence.*;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * An Apache Commons DBUtils implementation
 * This class works with a {@link java.util.Map} to make the transactions
 *
 * @author Miguel A. Vega P.
 * @version $Id: SQLQuery.java; Apr 23, 2016. 10:16 AM mvega $
 * @source $URL$
 */
public class SQLQuery extends DBUtilsQuery implements Query {

    SQLQuery(DataStore dataStore, String sqlStatement, Object... parameters) {
        super(dataStore, sqlStatement, parameters);
    }

    /**
     *
     * @return
     * @throws PersistenceException on any SQL error, this embeds the original {@link SQLException}
     */
    @Override
    public List<Map<String, Object>> getResultList() {
        return query(new MapListHandler());
    }

    /**
     *
     * @return
     * @throws PersistenceException on any SQL error, this embeds the original {@link SQLException}
     */
    @Override
    public Map<String, Object> getSingleResult() {
        return query(new MapHandler());
    }

    /**
     *
     * @param scalarHandler
     * @param <Y>
     * @return
     */
    public <Y> Y getSingleResult(ResultSetHandler<Y> scalarHandler) {
        return query(scalarHandler);
    }

    @Override
    public Query setMaxResults(int maxResult) {
        this.maxResult = maxResult;
        return this;
    }

    @Override
    public Query setFirstResult(int startPosition) {
        this.startPosition = startPosition;
        return this;
    }

    @Override
    public Query setHint(String hintName, Object value) {
        hints.put(hintName, value);
        return this;
    }

    /**
     * This method is not implemented, because this uses only the '?' characters to specify parameters which have been declared
     * in the constructor.
     * @param param
     * @param value
     * @param <T>
     * @return
     */
    @Override
    public <T> Query setParameter(Parameter<T> param, T value) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(Parameter<Calendar> param, Calendar value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(Parameter<Date> param, Date value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(String name, Object value) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(String name, Calendar value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(String name, Date value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(int position, Object value) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(int position, Calendar value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setParameter(int position, Date value, TemporalType temporalType) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    @Override
    public Query setFlushMode(FlushModeType flushMode) {
        this.flushMode = flushMode;
        return null;
    }

    @Override
    public Query setLockMode(LockModeType lockMode) {
        this.lockMode = lockMode;
        return this;
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        throw new UnsupportedOperationException("Method not supported");
    }
}
