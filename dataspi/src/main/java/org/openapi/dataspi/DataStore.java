package org.openapi.dataspi;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Miguel A. Vega P.
 * @version $Id: DataStore.java; Apr 23, 2016. 1:36 AM mvega $
 * @source $URL$
 */
public interface DataStore {

    SQLDialect getDialect();

    DataStore setDialect(SQLDialect sqlDialect);

    /**
     * @param sqlStatement
     * @param ctx
     * @param <CTX>
     * @return
     */
//    <CTX> javax.persistence.Query prepareSqlQuery(String sqlStatement, CTX ctx);

    SQLQuery prepareSqlQuery(String sqlStatement, Object... params);

    <X> JPAQuery<X> prepareJpaQuery(Class<X> type);

    BatchQuery prepareBatchQuery(String sqlStatement);

    /**
     * Provides a JDBC connection
     *
     * @return
     */
    Connection getConnection() throws SQLException;

    /**
     * This method is always called after any operation against DataBase is executed.
     * It's up to the final implementor what to do when this happens.
     */
    void poll();
}