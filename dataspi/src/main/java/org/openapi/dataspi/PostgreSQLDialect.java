package org.openapi.dataspi;

import org.openapi.lang.StringMerger;

/**
 * @author Miguel A. Vega P.
 * @version $Id: PostgreSQLDialect.java; Apr 23, 2016. 10:12 PM mvega $
 * @source $URL$
 */
public class PostgreSQLDialect extends DefaultSQLDialect implements SQLDialect {
    @Override
    public String forMaxResults(String sqlStatement, int maxResults) {
        return StringMerger.withSeparator(" ", sqlStatement).app("LIMIT").app(maxResults).merge();
    }

    @Override
    public String forFirstResult(String sqlStatement, int firstResult) {
        return StringMerger.withSeparator(" ", sqlStatement).app("OFFSET").app(firstResult).merge();
    }
}
