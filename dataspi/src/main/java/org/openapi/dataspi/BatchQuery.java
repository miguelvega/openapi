package org.openapi.dataspi;

import com.google.common.collect.Lists;

import javax.persistence.PersistenceException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Iterables.getLast;

/**
 * @author Miguel A. Vega P.
 * @version $Id: BatchQuery.java; Apr 24, 2016. 1:17 AM mvega $
 * @source $URL$
 */
public class BatchQuery extends SQLQuery {

    List<List> batches;

    BatchQuery(DataStore dataStore, String sqlStatement) {
        super(dataStore, sqlStatement);
        this.batches = Lists.newLinkedList();
    }

    public void addBatch(Object... params){
        batches.add(Arrays.asList(params));
    }

    public int getBatchCount(){
        return batches.size();
    }

    protected List getParameterList() {
        return Arrays.asList(getLast(batches), Collections.emptyList());
    }

    public int[] executeBatch() {
        try {

            Object[][] data = new Object[batches.size()][getParameterList().size()];
            for (int i = 0; i < batches.size(); i++) {
                data[i] = batches.get(i).toArray();
            }

            return getQueryRunner().batch(dataStore.getConnection(), sqlStatement, data);
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }finally {
            dataStore.poll();
        }
    }
}
