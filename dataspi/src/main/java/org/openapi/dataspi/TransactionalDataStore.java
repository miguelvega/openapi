package org.openapi.dataspi;

import org.apache.commons.dbutils.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * This implementation of {@link DataStore} will release a connection only when a group of transactions is executed successfully.
 * The acquired in an instance of this class will be used only once to prevent issues with unclosed connections.
 * @author Miguel A. Vega P.
 * @version $Id: TransactionalDataStore.java; Apr 23, 2016. 6:42 PM mvega $
 * @source $URL$
 */
public abstract class TransactionalDataStore extends DataStoreAdapter implements DataStore {

    /**
     * This is the connection that will be opened since the first time that the
     */
    final Connection connection;

    TransactionalDataStore(Connection connection) throws SQLException {
        assert connection!=null;
        this.connection = connection;
        this.connection.setAutoCommit(false);
    }

    /**
     *
     * @param dataSource
     * @return
     * @throws SQLException if connection ouldn't be acquired
     */
    public static TransactionalDataStore getDataStore(DataSource dataSource) throws SQLException {
        return new TransactionalDataStore(dataSource.getConnection()){};
    }

    public Connection getConnection() {
        return connection;
    }

    public void poll() {
        //dummy
    }

    /**
     * This might be called mnually or even via AOP
     */
    public void releaseConnection(){
        DbUtils.closeQuietly(connection);
    }

    public void commitAndClose() throws SQLException {
        DbUtils.commitAndClose(connection);
    }

    public void commitAndCloseQuetly() {
        DbUtils.commitAndCloseQuietly(connection);
    }

    public void rollbackAndClose() throws SQLException {
        DbUtils.rollbackAndClose(connection);
    }

    public void rollbackAndCloseQuetly() {
        DbUtils.rollbackAndCloseQuietly(connection);
    }
}
