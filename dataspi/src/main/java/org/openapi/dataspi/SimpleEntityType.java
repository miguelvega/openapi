package org.openapi.dataspi;

import javax.persistence.Entity;
import javax.persistence.metamodel.*;
import java.util.Set;

/**
 * @author Miguel A. Vega P.
 * @version $Id: SimpleEntityType.java; May 16, 2016. 10:13 PM mvega $
 * @source $URL$
 */
public class SimpleEntityType<X> implements EntityType<X>{

    final Class<X> type;

    public SimpleEntityType(Class<X> type) {
        this.type = type;
    }


    @Override
    public String getName() {
        Entity a = type.getAnnotation(Entity.class);
        return a.name().isEmpty() ? type.getSimpleName() : a.name();
    }

    /**
     * TODO
     * @return
     */
    @Override
    public BindableType getBindableType() {
        return BindableType.ENTITY_TYPE;
    }

    @Override
    public Class<X> getBindableJavaType() {
        return type;
    }

    @Override
    public <Y> SingularAttribute<? super X, Y> getId(Class<Y> type) {
        return  null;
    }

    @Override
    public <Y> SingularAttribute<X, Y> getDeclaredId(Class<Y> type) {
        return null;
    }

    @Override
    public <Y> SingularAttribute<? super X, Y> getVersion(Class<Y> type) {
        return null;
    }

    @Override
    public <Y> SingularAttribute<X, Y> getDeclaredVersion(Class<Y> type) {
        return null;
    }

    @Override
    public IdentifiableType<? super X> getSupertype() {
        return null;
    }

    @Override
    public boolean hasSingleIdAttribute() {
        return false;
    }

    @Override
    public boolean hasVersionAttribute() {
        return false;
    }

    @Override
    public Set<SingularAttribute<? super X, ?>> getIdClassAttributes() {
        return null;
    }

    @Override
    public Type<?> getIdType() {
        return null;
    }

    @Override
    public Set<Attribute<? super X, ?>> getAttributes() {
        return null;
    }

    @Override
    public Set<Attribute<X, ?>> getDeclaredAttributes() {
        return null;
    }

    @Override
    public <Y> SingularAttribute<? super X, Y> getSingularAttribute(String name, Class<Y> type) {
        return null;
    }

    @Override
    public <Y> SingularAttribute<X, Y> getDeclaredSingularAttribute(String name, Class<Y> type) {
        return null;
    }

    @Override
    public Set<SingularAttribute<? super X, ?>> getSingularAttributes() {
        return null;
    }

    @Override
    public Set<SingularAttribute<X, ?>> getDeclaredSingularAttributes() {
        return null;
    }

    @Override
    public <E> CollectionAttribute<? super X, E> getCollection(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <E> CollectionAttribute<X, E> getDeclaredCollection(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <E> SetAttribute<? super X, E> getSet(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <E> SetAttribute<X, E> getDeclaredSet(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <E> ListAttribute<? super X, E> getList(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <E> ListAttribute<X, E> getDeclaredList(String name, Class<E> elementType) {
        return null;
    }

    @Override
    public <K, V> MapAttribute<? super X, K, V> getMap(String name, Class<K> keyType, Class<V> valueType) {
        return null;
    }

    @Override
    public <K, V> MapAttribute<X, K, V> getDeclaredMap(String name, Class<K> keyType, Class<V> valueType) {
        return null;
    }

    @Override
    public Set<PluralAttribute<? super X, ?, ?>> getPluralAttributes() {
        return null;
    }

    @Override
    public Set<PluralAttribute<X, ?, ?>> getDeclaredPluralAttributes() {
        return null;
    }

    @Override
    public Attribute<? super X, ?> getAttribute(String name) {
        return null;
    }

    @Override
    public Attribute<X, ?> getDeclaredAttribute(String name) {
        return null;
    }

    @Override
    public SingularAttribute<? super X, ?> getSingularAttribute(String name) {
        return null;
    }

    @Override
    public SingularAttribute<X, ?> getDeclaredSingularAttribute(String name) {
        return null;
    }

    @Override
    public CollectionAttribute<? super X, ?> getCollection(String name) {
        return null;
    }

    @Override
    public CollectionAttribute<X, ?> getDeclaredCollection(String name) {
        return null;
    }

    @Override
    public SetAttribute<? super X, ?> getSet(String name) {
        return null;
    }

    @Override
    public SetAttribute<X, ?> getDeclaredSet(String name) {
        return null;
    }

    @Override
    public ListAttribute<? super X, ?> getList(String name) {
        return null;
    }

    @Override
    public ListAttribute<X, ?> getDeclaredList(String name) {
        return null;
    }

    @Override
    public MapAttribute<? super X, ?, ?> getMap(String name) {
        return null;
    }

    @Override
    public MapAttribute<X, ?, ?> getDeclaredMap(String name) {
        return null;
    }

    @Override
    public PersistenceType getPersistenceType() {
        return null;
    }

    @Override
    public Class<X> getJavaType() {
        return null;
    }
}
