package org.openapi.dataspi;

import javax.persistence.PersistenceException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * This is a manual way of doing this operation, but a AOP implementation would be the right one.
 * Make sure to call the destroy method alway if the object won't be used any longer.
 * @author Miguel A. Vega P.
 * @version $Id: Transaction.java; Apr 23, 2016. 8:39 PM mvega $
 * @source $URL$
 */
public class Transaction {

    final List<TransactionalDataStore> txDataStores;

    public Transaction(TransactionalDataStore... dataStores) {
        txDataStores = new LinkedList<TransactionalDataStore>();
        txDataStores.addAll(Arrays.asList(dataStores));
    }

    /**
     * Executes a block of transactional related operations. This releases the connection of each {@link TransactionalDataStore}
     *
     * @param callable
     * @param <V>
     * @return
     * @throws SQLException
     */
    public <V> V doTransact(Callable<V> callable) throws SQLException {
        try{

            V call = callable.call();

            try {
                commitAll();

                return call;

            }catch(SQLException x){
                throw new IllegalStateException("Unable to apply changes in databases, due to an internal error.", x);
            }
        } catch (Throwable t){
            //rollback on each data store
            try {
                rollBackAll();
            }catch(SQLException x){
                throw new IllegalStateException("Unable to apply changes in databases, due to an internal error.", x);
            }

            throw new PersistenceException("An error occurred while executing the given transactional block", t);

        }finally {
            for (TransactionalDataStore txDataStore : txDataStores) {
                txDataStore.releaseConnection();
            }
        }
    }

    /**
     * Destroys all stored objects and make sure to free their conenctions.
     */
    public void destoy(){
        while(txDataStores.size()>0){
            TransactionalDataStore txDataStore = txDataStores.remove(0);
            txDataStore.releaseConnection();
        }
    }

    private void rollBackAll() throws SQLException {
        for (TransactionalDataStore txDataStore : txDataStores) {
            txDataStore.getConnection().rollback();
        }
    }

    private void commitAll() throws SQLException {
        for (TransactionalDataStore txDataStore : txDataStores) {
            txDataStore.getConnection().commit();
        }
    }
}
