package org.openapi.dataspi;

import org.javatuples.Pair;
import org.openapi.dataspi.handler.EntityCollectionHandler;
import org.openapi.dataspi.handler.EntityHandler;
import org.openapi.dataspi.handler.EntityListHandler;
import org.openapi.dataspi.handler.SQLStatementBuilder;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Miguel A. Vega P.
 * @version $Id: JPAQuery.java; Apr 23, 2016. 10:28 AM mvega $
 * @source $URL$
 */
class JPAQuery<X> extends DBUtilsQuery implements TypedQuery<X>{

    final Class<X> entityType;

    JPAQuery(DataStore dataStore, Class<X> entityType) {
        super(dataStore, null);
        this.entityType = entityType;
    }

    public X insert(X x){

        Pair<String, List> pair = new SQLStatementBuilder(dataStore.getDialect()).buildPreparedInsert(x);

        super.update(x, pair.getValue0(), pair.getValue1());

        //throw new UnsupportedOperationException("Method not supported");
        return x;
    }

    public X update(X x){
        Pair<String, List> pair = new SQLStatementBuilder(dataStore.getDialect()).buildPreparedUpdate(x);

        logger.info("UPDATE={}", SQLStatementBuilder.prettyPrintStatemet(pair));

        return x;
    }

    public X delete(X x){
        throw new UnsupportedOperationException("Method not supported");
//        return x;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    public List<X> getResultList() {
        this.sqlStatement = new SQLStatementBuilder<X>(dataStore.getDialect()).buildSimpleSelect(entityType);
        return (List<X>) query(new EntityCollectionHandler<>(entityType));
    }

    /**
     * A new and better way to fetch data from datastore, using the metadata firstm and then the assignations
     * @return
     */
    public List<X> getResultListOldFlavor() {
        this.sqlStatement = new SQLStatementBuilder<X>(dataStore.getDialect()).buildSimpleSelect(entityType);
        return (List<X>) query(new EntityListHandler<>(entityType));
    }

    /**
     * {@inheritDoc}
     */
    public X getSingleResult() {

        this.sqlStatement = new SQLStatementBuilder<X>(dataStore.getDialect()).buildSimpleSelect(entityType);

        return query(new EntityHandler<X>(entityType));
    }

    public TypedQuery<X> setMaxResults(int maxResult) {
        this.maxResult = maxResult;
        return this;
    }

    public TypedQuery<X> setFirstResult(int startPosition) {
        return null;
    }

    public TypedQuery<X> setHint(String hintName, Object value) {
        return null;
    }

    public <T> TypedQuery<X> setParameter(Parameter<T> param, T value) {
        return null;
    }

    public TypedQuery<X> setParameter(Parameter<Calendar> param, Calendar value, TemporalType temporalType) {
        return null;
    }

    public TypedQuery<X> setParameter(Parameter<Date> param, Date value, TemporalType temporalType) {
        return null;
    }

    public TypedQuery<X> setParameter(String name, Object value) {
        return null;
    }

    public TypedQuery<X> setParameter(String name, Calendar value, TemporalType temporalType) {
        return null;
    }

    public TypedQuery<X> setParameter(String name, Date value, TemporalType temporalType) {
        return null;
    }

    public TypedQuery<X> setParameter(int position, Object value) {
        return null;
    }

    public TypedQuery<X> setParameter(int position, Calendar value, TemporalType temporalType) {
        return null;
    }

    public TypedQuery<X> setParameter(int position, Date value, TemporalType temporalType) {
        return null;
    }


    public TypedQuery<X> setFlushMode(FlushModeType flushMode) {
        this.flushMode = flushMode;return this;
    }

    public TypedQuery<X> setLockMode(LockModeType lockMode) {
        this.lockMode = lockMode;
        return this;
    }

}
