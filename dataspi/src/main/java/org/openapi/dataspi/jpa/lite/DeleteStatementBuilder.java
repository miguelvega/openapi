package org.openapi.dataspi.jpa.lite;

import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.jpa.lite.builder.CriteriaBuilder;
import org.openapi.lang.StringMerger;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: InsertStatementBuilder.java 0, 2017-sep-15 06:13 PM miguelvega $
 */
public class DeleteStatementBuilder<X> extends AbstractStatementBuilder<X> {

    protected DeleteStatementBuilder(Class<X> type) {
        super(type);
    }

    @Override
    public Pair<String, Collection> buildStatement(String schema, X x, SQLDialect sqlDialect) throws IllegalArgumentException {

        checkEntityValidity(x);

        String qualified = entityMetaData.getQualifiedTableName(schema);

        final StringMerger is = StringMerger.of("DELETE FROM ").app(qualified);

        final List<Object> values = new LinkedList<>();

        this.filter = new CriteriaBuilder().buildPrimaryKeyFilter(entityMetaData, x);

        return completeStatement(new Pair(is.removeLastToken(PARAM_SEPARATOR).merge(), values));
    }
}
