package org.openapi.dataspi.jpa.lite;

import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.handler.meta.TransientProcessor;
import org.openapi.lang.StringMerger;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: SelectStatementBuilder.java 0, 2017-sep-15 05:34 PM miguelvega $
 */
public class SelectStatementBuilder<X> extends AbstractStatementBuilder<X> {

    protected SelectStatementBuilder(Class<X> type) {
        super(type);
    }

    /**
     *
     * @param schema
     * @param x useless parameter
     * @param sqlDialect
     * @return
     */
    @Override
    public Pair<String, Collection> buildStatement(String schema, X x, SQLDialect sqlDialect) {
        final StringMerger is = StringMerger.of("SELECT ");

        entityMetaData.list().stream().forEach(assignator -> {
            if (TransientProcessor.class.isAssignableFrom(assignator.getClass()))
                return;
            is.app(assignator.getColumnName()).app(PARAM_SEPARATOR);
        });

        StringMerger result = is.removeLastToken(PARAM_SEPARATOR);

        String qualified = entityMetaData.getQualifiedTableName(schema);

        result.app(" FROM ").app(qualified);

        return completeStatement(new Pair<>(result.merge(), Collections.emptyList()));
    }
}
