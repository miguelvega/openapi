package org.openapi.dataspi.jpa.lite.filter;

import org.javatuples.Pair;
import org.openapi.lang.StringMerger;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Miguel A. Vega P.
 * @version $Id: BinaryComparisonOperator.java; Sep 17, 2017 10:01 PM miguel.vega $
 */
public abstract class BinaryComparisonOperator<T> implements Filter {

    final String column;
    final T value;

    final BinaryOperator binaryOperator;

    BinaryComparisonOperator(BinaryOperator binaryOperator, String column, T value) {
        this.binaryOperator = binaryOperator;
        this.column = column;
        this.value = value;
    }

    @Override
    public Pair<String, Collection> evaluate() {
        return new Pair<String, Collection>(StringMerger.of(column, binaryOperator.asText(), "?").merge(), Collections.singleton(value));
    }

    @Override
    public Filter purge() {
        return this;
    }

    protected enum BinaryOperator{
        EQ {
            @Override
            String asText() {
                return "=";
            }
        }, NEQ {
            @Override
            String asText() {
                return "<>";
            }
        }, GT {
            @Override
            String asText() {
                return ">";
            }
        }, GTE {
            @Override
            String asText() {
                return ">=";
            }
        }, LT {
            @Override
            String asText() {
                return "<";
            }
        }, LTE {
            @Override
            String asText() {
                return "<=";
            }
        }, LIKE {
            @Override
            String asText() {
                return "LIKE";
            }
        };

        abstract String asText();
    }
}
