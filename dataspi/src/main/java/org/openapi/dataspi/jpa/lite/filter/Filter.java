package org.openapi.dataspi.jpa.lite.filter;

import org.javatuples.Pair;

import java.util.Collection;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Filter.java; Sep 17, 2017 9:58 PM miguel.vega $
 */
public interface Filter {

    /**
     * Evaluates filter and returns teh string and values for filter to be applied
     * @return
     */
    Pair<String, Collection> evaluate();

    /**
     * Detachs all values to avoid memory leaks
     * @return
     */
    Filter purge();
}
