package org.openapi.dataspi.jpa.lite.builder;

import org.javatuples.Pair;
import org.openapi.dataspi.handler.meta.ColumnFieldProcessor;
import org.openapi.dataspi.handler.meta.EmbeddedProcessor;
import org.openapi.dataspi.handler.meta.EntityMetaData;
import org.openapi.dataspi.jpa.lite.filter.Filter;
import org.openapi.dataspi.jpa.lite.filter.FilterFactory;
import org.openapi.lang.StringMerger;

import java.lang.reflect.Field;
import java.util.Collection;

import static org.openapi.dataspi.jpa.lite.filter.FilterFactory.and;
import static org.openapi.lang.Reflections.fetch;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: CriteriaBuilder.java 0, 2017-sep-15 07:03 PM miguelvega $
 */
public class CriteriaBuilder<X> {
    /**
     * Creates a String with the WHERE clause using the PRIMARY KEY fields in given object
     * @param x
     * @return
     */
    public String withWhereClauseOnPrimaryKeys(EntityMetaData<X> metaData, X x)throws NoSuchFieldException{
        ColumnFieldProcessor identifier = metaData.getIdentifierField();

        Field field = identifier.getField();

        final StringMerger builder = StringMerger.of("WHERE ");

        if(EmbeddedProcessor.class.isAssignableFrom(identifier.getClass())){
            //need to fetch all fields used as primary key
            Collection<Pair<String, Field>> embeddedFields = ((EmbeddedProcessor)identifier).getEmbeddedFields();
            embeddedFields.stream().forEach(pair -> {
                builder.app(identifier.getColumnName()).app("=").app(fetch(x, field));
            });
        }else{
            builder.app(identifier.getColumnName()).app("=").app(fetch(x, field));
        }

        return null;
    }

    /**
     *
     * @param metaData
     * @param x
     * @return
     * @throws IllegalStateException if entity ha not PK fields declared
     */
    public Filter buildPrimaryKeyFilter(EntityMetaData<X> metaData, X x) throws IllegalStateException{
        try {
            ColumnFieldProcessor identifierProcessor = metaData.getIdentifierField();

            if(EmbeddedProcessor.class.isAssignableFrom(identifierProcessor.getClass())){
                Collection<Pair<String, Field>> embeddedFields = ((EmbeddedProcessor) identifierProcessor).getEmbeddedFields();

                Filter[] filters = new Filter[embeddedFields.size()];

                int i = 0;

                for (Pair<String, Field> embeddedField : embeddedFields) {
                    filters[i++] = FilterFactory.equals(embeddedField.getValue0(), fetch(fetch(x, identifierProcessor.getField()), embeddedField.getValue1()));
                }

                return and(filters);
            }

            //apply filter for IDs
            return FilterFactory.equals(identifierProcessor.getColumnName(), fetch(x, identifierProcessor.getField()));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Type "+metaData.getEntityType().getName()+
                    " has not a valid PK column declared, delete will remove all data from table "+metaData.getQualifiedTableName(null));
        }
    }
}
