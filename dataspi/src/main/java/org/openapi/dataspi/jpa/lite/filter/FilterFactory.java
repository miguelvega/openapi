package org.openapi.dataspi.jpa.lite.filter;

/**
 * @author Miguel A. Vega P.
 * @version $Id: FilterFactory.java; Sep 17, 2017 11:03 PM miguel.vega $
 */
public class FilterFactory {


    public static <T> Filter equals(String column, T value){
        return new BinaryComparisonOperator<T>(BinaryComparisonOperator.BinaryOperator.EQ, column, value) {};
    }

    public static <T> Filter and(Filter... filters){
        return new BinaryLogicOperator(BinaryLogicOperator.BinaryOperator.AND, filters) {};
    }

    public static <T> Filter or(Filter... filters){
        return new BinaryLogicOperator(BinaryLogicOperator.BinaryOperator.OR, filters) {};
    }

}
