package org.openapi.dataspi.jpa.lite.filter;

import org.javatuples.Pair;
import org.openapi.lang.StringMerger;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author Miguel A. Vega P.
 * @version $Id: BinaryLogicOperator.java; Sep 17, 2017 11:21 PM miguel.vega $
 */
public class BinaryLogicOperator implements Filter{

    final Filter[] children;

    final BinaryOperator binaryOperator;

    public BinaryLogicOperator(BinaryOperator binaryOperator, Filter... filters) {
        this.binaryOperator = binaryOperator;
        this.children = filters;
    }

    @Override
    public Pair<String, Collection> evaluate() {

        StringMerger merger = StringMerger.of("(");

        final Collection collection = new LinkedList();

        for (Filter filter : children) {
            Pair<String, Collection> evaluated = filter.evaluate();

            merger.app(" ").app(evaluated.getValue0()).app(" ").app(binaryOperator.asText());

            collection.addAll(evaluated.getValue1());
        }

        return new Pair(merger.removeLastToken(binaryOperator.asText()).app(")").merge(), collection);
    }

    @Override
    public Filter purge() {
        for (Filter filter : children) {
            filter.purge();
        }

        return this;
    }

    protected enum BinaryOperator{
        OR {
            @Override
            String asText() {
                return "OR";
            }
        }, AND {
            @Override
            String asText() {
                return "AND";
            }
        };

        abstract String asText();
    }
}
