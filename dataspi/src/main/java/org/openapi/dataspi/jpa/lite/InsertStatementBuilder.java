package org.openapi.dataspi.jpa.lite;

import com.google.common.collect.Lists;
import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.handler.meta.TransientProcessor;
import org.openapi.lang.StringMerger;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: InsertStatementBuilder.java 0, 2017-sep-15 06:13 PM miguelvega $
 */
public class InsertStatementBuilder<X> extends AbstractStatementBuilder<X> {

    protected InsertStatementBuilder(Class<X> type) {
        super(type);
    }

    @Override
    public Pair<String, Collection> buildStatement(String schema, X x, SQLDialect sqlDialect) throws IllegalArgumentException{

        checkEntityValidity(x);

        String qualified = entityMetaData.getQualifiedTableName(schema);

        final StringMerger insertSQLStatement = StringMerger.of("INSERT INTO ").app(qualified).app(" (");

        final StringMerger wildcardMarks = StringMerger.of();

        final List<Object> values = Lists.newLinkedList();

        entityMetaData.list().stream().forEach(assignator -> {
            if (TransientProcessor.class.isAssignableFrom(assignator.getClass()))
                return;

            GeneratedValue pk = assignator.getField().getAnnotation(GeneratedValue.class);

            if (pk != null) {
                if (pk.strategy() == GenerationType.SEQUENCE) {
                    //When using a postgresql database, column might be SERIAL, which means
                    String generator = pk.generator();
                    if (!isNullOrEmpty(generator)) {
                        //only if user explicitly specifies the SEQUENCE generator, use it for insert
                        //e.g. generator = "nextval('idsequence')"
                        insertSQLStatement.app(assignator.getColumnName()).app(PARAM_SEPARATOR);
                        wildcardMarks.app(pk.generator()).app(PARAM_SEPARATOR);
                    }
                } else if (pk.strategy() == GenerationType.TABLE) {
                    throw new UnsupportedOperationException("This feture is not implemented yet");
                } else {
                    //AUTO, IDENTITY or NULL
                    //simply do nothing, skip such this column
                }
            } else {
                try {

                    values.add(javaType2Database(assignator.getField(), x, sqlDialect));

                    insertSQLStatement.app(assignator.getColumnName()).app(PARAM_SEPARATOR);
                    wildcardMarks.app("?").app(PARAM_SEPARATOR);

                } catch (IllegalStateException e) {
                    logger.warn("Can not concrete the SQL building for field: {}#{}", assignator.getField().getDeclaringClass(),
                            assignator.getField().getName(), e);
                }
            }
        });

        StringMerger result = insertSQLStatement.removeLastToken(PARAM_SEPARATOR);

        result.app(") VALUES (").app(wildcardMarks.removeLastToken(PARAM_SEPARATOR).merge()).app(')');

        final String qstr = result.merge();

        return completeStatement(new Pair(qstr, values));
    }
}
