package org.openapi.dataspi.jpa.lite;

import com.google.common.collect.Lists;
import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.jpa.lite.builder.CriteriaBuilder;
import org.openapi.lang.StringMerger;

import java.util.Collection;
import java.util.List;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: InsertStatementBuilder.java 0, 2017-sep-15 06:13 PM miguelvega $
 */
public class UpdateStatementBuilder<X> extends AbstractStatementBuilder<X> {

    protected UpdateStatementBuilder(Class<X> type) {
        super(type);
    }

    @Override
    public Pair<String, Collection> buildStatement(String schema, X x, SQLDialect sqlDialect) throws IllegalArgumentException {

        checkEntityValidity(x);

        String qualified = entityMetaData.getQualifiedTableName(schema);

        final StringMerger is = StringMerger.of("UPDATE ").app(qualified).app(" SET ");

        final List<Object> values = Lists.newLinkedList();

        entityMetaData.list().stream().forEach(assignator -> {

            try {
                if(assignator.equals(entityMetaData.getIdentifierField())){
                    return;
                }
            } catch (NoSuchFieldException e) {
            }

            values.add(javaType2Database(assignator.getField(), x, sqlDialect));

            is.app(assignator.getColumnName()).app(" = ?").app(PARAM_SEPARATOR);
        });

        this.filter = new CriteriaBuilder().buildPrimaryKeyFilter(entityMetaData, x);

        return completeStatement(new Pair(is.removeLastToken(PARAM_SEPARATOR).merge(), values));
    }
}
