package org.openapi.dataspi.jpa;

import com.google.common.collect.Lists;
import org.javatuples.Pair;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import java.util.List;
import java.util.Set;

/**
 * @author Miguel A. Vega P.
 * @version $Id: DefaultCriteriaQuery.java; May 16, 2016. 5:26 PM mvega $
 * @source $URL$
 */
public class DefaultCriteriaQuery<T> implements CriteriaQuery<T>{

    final List<Pair<Class, String>> keyAliases = Lists.newLinkedList();

    @Override
    public CriteriaQuery<T> select(Selection<? extends T> selection) {
        return null;
    }

    @Override
    public CriteriaQuery<T> multiselect(Selection<?>... selections) {
        return null;
    }

    @Override
    public CriteriaQuery<T> multiselect(List<Selection<?>> selectionList) {
        return null;
    }

    @Override
    public <X> Root<X> from(Class<X> entityClass) {
        return new DefaultRoot<>(entityClass);
    }

    @Override
    public <X> Root<X> from(EntityType<X> entity) {
        return null;
    }

    @Override
    public CriteriaQuery<T> where(Expression<Boolean> restriction) {
        return null;
    }

    @Override
    public CriteriaQuery<T> where(Predicate... restrictions) {
        return null;
    }

    @Override
    public CriteriaQuery<T> groupBy(Expression<?>... grouping) {
        return null;
    }

    @Override
    public CriteriaQuery<T> groupBy(List<Expression<?>> grouping) {
        return null;
    }

    @Override
    public CriteriaQuery<T> having(Expression<Boolean> restriction) {
        return null;
    }

    @Override
    public CriteriaQuery<T> having(Predicate... restrictions) {
        return null;
    }

    @Override
    public CriteriaQuery<T> orderBy(Order... o) {
        return null;
    }

    @Override
    public CriteriaQuery<T> orderBy(List<Order> o) {
        return null;
    }

    @Override
    public CriteriaQuery<T> distinct(boolean distinct) {
        return null;
    }

    @Override
    public Set<Root<?>> getRoots() {
        return null;
    }

    @Override
    public Selection<T> getSelection() {
        return null;
    }

    @Override
    public List<Expression<?>> getGroupList() {
        return null;
    }

    @Override
    public Predicate getGroupRestriction() {
        return null;
    }

    @Override
    public boolean isDistinct() {
        return false;
    }

    @Override
    public Class<T> getResultType() {
        return null;
    }

    @Override
    public List<Order> getOrderList() {
        return null;
    }

    @Override
    public Set<ParameterExpression<?>> getParameters() {
        return null;
    }

    @Override
    public <U> Subquery<U> subquery(Class<U> type) {
        return null;
    }

    @Override
    public Predicate getRestriction() {
        return null;
    }
}
