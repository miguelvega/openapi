package org.openapi.dataspi.jpa.lite;

import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.handler.meta.EntityMetaData;
import org.openapi.dataspi.jpa.lite.filter.Filter;
import org.openapi.lang.Reflections;
import org.openapi.lang.StringMerger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: AbstractStatementBuilder.java 0, 2017-sep-15 05:35 PM miguelvega $
 */
public abstract class AbstractStatementBuilder<X> implements StatementBuilder<X>{

    public static final String PARAM_SEPARATOR = ", ";

    final Logger logger = LoggerFactory.getLogger(getClass());

    final protected Class<X> type;
    final EntityMetaData<X> entityMetaData;

    private boolean showFullSQLStatement = false;

    protected Filter filter;

    protected AbstractStatementBuilder(Class<X> type) {
        this.type = type;
        this.entityMetaData = new EntityMetaData<>(type);
    }

    /**
     * This method will print if can, also will take variables to null, to avoid leaks
     * @param pair
     * @return
     */
    protected Pair<String, Collection> completeStatement(Pair<String, Collection> pair){

        //filters
        if(filter!=null) {
            Pair<String, Collection> evaluated = filter.evaluate();

            pair.getValue1().addAll(evaluated.getValue1());

            StringMerger merger = StringMerger.of(pair.getValue0());

            merger.app(" WHERE ").app(evaluated.getValue0());

            pair = pair.setAt0(merger.merge());

            this.filter.purge();

            this.filter = null;
        }
        return printStatement(pair);
    }

    /**
     * Allows to pretty print the statement created
     * @return
     */
    public Pair<String, Collection> printStatement(Pair<String, Collection> pair){
        if(showFullSQLStatement) {

            StringBuilder buffer = new StringBuilder("SQL Statement [");

            buffer.append(pair.getValue0()).append("]");

            buffer.append("\nwith parameters [");

            int i = 0;
            for (Object o : pair.getValue1()) {
                if (o == null) {
                    buffer.append(o);
                    continue;
                }
                buffer.append(o).append(":").append(o.getClass().getName()).append(++i < pair.getValue1().size() ? ", " : "");
            }

            buffer.append("]");

            logger.info(buffer.toString());
        }

        return pair;

    }

    /**
     * Checks whether an isntance is assignable from the type defined here
     * @param x
     * @return
     */
    protected boolean isManageable(X x) {
        return x != null && x.getClass().getAnnotation(Table.class) != null;
    }

    /**
     * Converts the java object into a valid database type, use the dialect to achieve this conversion
     * @param field
     * @param declaringObject
     * @param sqlDialect
     * @return if Temporal a {@link java.util.Date} or a {@link java.util.Calendar} object if not null
     */
    protected Object javaType2Database(Field field, Object declaringObject, SQLDialect sqlDialect) {

        final boolean accessible = field.isAccessible();

        try {

            field.setAccessible(true);

            Method readMethod = Reflections.getGetterMethod(field.getName(), declaringObject.getClass());

            Object plain = field.get(declaringObject);

            if(plain == null)
                return plain;

            if(field.getAnnotation(Temporal.class)!=null || readMethod.getAnnotation(Temporal.class)!=null){
                Temporal temporal = Optional.ofNullable(field.getAnnotation(Temporal.class)).orElse(readMethod.getAnnotation(Temporal.class));
                switch(temporal.value()){
                    case DATE:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLDate((Date)plain);
                        else
                            return sqlDialect.getSQLDate((Calendar) plain);
                    case TIME:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLTime((Date) plain);
                        else
                            return sqlDialect.getSQLTime((Calendar) plain);
                    case TIMESTAMP:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLTimeStamp((Date) plain);
                        else
                            return sqlDialect.getSQLTimeStamp((Calendar) plain);
                };
            }

            if(field.getAnnotation(Enumerated.class)!=null || readMethod.getAnnotation(Enumerated.class)!=null) {
                Enumerated enu = Optional.
                        ofNullable(field.getAnnotation(Enumerated.class)).
                        orElse(readMethod.getAnnotation(Enumerated.class));

                Class<Enum> etype = (Class<Enum>) plain.getClass();

                //plain is a Enum
                if(enu.value() == EnumType.STRING){
                    return Enum.valueOf(etype, plain.toString()).name();
                }else{
                    //by default also
                    return Enum.valueOf(etype, plain.toString()).ordinal();
                }
            }

            return plain;

        } catch (IntrospectionException e) {
            throw new IllegalStateException("Method not found ["+field.getName()+"] in object of type ["+declaringObject.getClass()+"]", e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Unexcpected error", e);
        }finally {
            field.setAccessible(accessible);
        }
    }

    protected void checkEntityValidity(X x){
        if (x == null)
            throw new IllegalArgumentException("Value provided for insert can not be null");
        if(type.getAnnotation(Table.class)==null){
            throw new IllegalArgumentException("Object given is not properly annotated with @Table");
        }

    }

    @Override
    public Class<X> getType() {
        return type;
    }

    @Override
    public Pair<String, Collection> buildStatement(X x, SQLDialect sqlDialect) {
        return buildStatement(null, x, sqlDialect);
    }

    public AbstractStatementBuilder<X> setShowFullSQLStatement(boolean showFullSQLStatement) {
        this.showFullSQLStatement = showFullSQLStatement;
        return this;
    }

    public EntityMetaData<X> getEntityMetaData() {
        return entityMetaData;
    }
}
