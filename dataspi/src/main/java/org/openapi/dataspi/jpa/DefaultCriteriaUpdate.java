package org.openapi.dataspi.jpa;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: DefaultCriteriaUpdate.java 0, 2017-sep-15 05:25 PM miguelvega $
 */
public class DefaultCriteriaUpdate<T> implements CriteriaUpdate<T>{
    @Override
    public Root<T> from(Class<T> entityClass) {
        return null;
    }

    @Override
    public Root<T> from(EntityType<T> entity) {
        return null;
    }

    @Override
    public Root<T> getRoot() {
        return null;
    }

    @Override
    public <Y, X extends Y> CriteriaUpdate<T> set(SingularAttribute<? super T, Y> attribute, X value) {
        return null;
    }

    @Override
    public <Y> CriteriaUpdate<T> set(SingularAttribute<? super T, Y> attribute, Expression<? extends Y> value) {
        return null;
    }

    @Override
    public <Y, X extends Y> CriteriaUpdate<T> set(Path<Y> attribute, X value) {
        return null;
    }

    @Override
    public <Y> CriteriaUpdate<T> set(Path<Y> attribute, Expression<? extends Y> value) {
        return null;
    }

    @Override
    public CriteriaUpdate<T> set(String attributeName, Object value) {
        return null;
    }

    @Override
    public CriteriaUpdate<T> where(Expression<Boolean> restriction) {
        return null;
    }

    @Override
    public CriteriaUpdate<T> where(Predicate... restrictions) {
        return null;
    }

    @Override
    public <U> Subquery<U> subquery(Class<U> type) {
        return null;
    }

    @Override
    public Predicate getRestriction() {
        return null;
    }
}
