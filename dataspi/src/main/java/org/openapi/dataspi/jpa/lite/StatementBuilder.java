package org.openapi.dataspi.jpa.lite;

import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;

import java.util.Collection;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: StatementBuilder.java 0, 2017-sep-15 05:33 PM miguelvega $
 */
public interface StatementBuilder<E> {

    /**
     *
     * @return
     */
    Class<E> getType();

    /**
     *
     * @return
     * @param e
     * @param sqlDialect
     * @throws IllegalArgumentException
     */
    Pair<String, Collection> buildStatement(E e, SQLDialect sqlDialect) throws IllegalArgumentException;

    /**
     *
     * @param schema
     * @param e
     * @param sqlDialect
     * @throws IllegalArgumentException
     */
    Pair<String, Collection> buildStatement(String schema, E e, SQLDialect sqlDialect) throws IllegalArgumentException;
}
