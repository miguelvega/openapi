package org.openapi.dataspi;

import com.google.common.collect.Maps;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.openapi.dataspi.dbutils.EnhancedQueryRunner;
import org.openapi.lang.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Miguel A. Vega P.
 * @version $Id: DBUtilsQuery.java; Apr 23, 2016. 8:20 PM mvega $
 * @source $URL$
 */
public abstract class DBUtilsQuery {

    final protected Logger logger = LoggerFactory.getLogger(getClass());

    protected final DataStore dataStore;

    protected final List parameterList;
    protected final Map<String, Object> hints = Maps.newHashMap();
    protected String sqlStatement;
    protected boolean parameterMetaDataKnownBroken = false;
    protected int maxResult = -1;
    protected int startPosition = -1;
    protected LockModeType lockMode = LockModeType.NONE;

    protected FlushModeType flushMode;

    DBUtilsQuery(DataStore dataStore, String sqlStatement, Object... parameters) {
        this.dataStore = dataStore;
        this.sqlStatement = sqlStatement;
        this.parameterList = Arrays.asList(parameters);
    }

    public void setParameterMetaDataKnownBroken(boolean parameterMetaDataKnownBroken) {
        this.parameterMetaDataKnownBroken = parameterMetaDataKnownBroken;
    }

    protected QueryRunner getQueryRunner() {
        return new QueryRunner(parameterMetaDataKnownBroken);
    }

    protected QueryRunner getGeneratedKeysQueryRunner(Visitor<?, ?> visitor) {
        return new EnhancedQueryRunner(parameterMetaDataKnownBroken, visitor);
    }

    /**
     * Makes the query and calls {@link DataStore#poll()} method on complete
     * todo, the exception thrown is PersistenceException, this is not correct due to the JSR-338.
     *
     * @param resultSetHandler
     * @param <X>
     * @return
     * @throws PersistenceException on any SQL error, this embeds the original {@link SQLException}
     */
    protected <X> X query(ResultSetHandler<X> resultSetHandler) {
        try {

            final String statement;
            if (maxResult != -1 && startPosition != -1)
                statement = dataStore.getDialect().forMaxResults(dataStore.getDialect().forFirstResult(sqlStatement, startPosition), maxResult);
            else if (startPosition != -1)
                statement = dataStore.getDialect().forFirstResult(sqlStatement, startPosition);
            else if (maxResult != -1)
                statement = dataStore.getDialect().forMaxResults(sqlStatement, maxResult);
            else
                statement = sqlStatement;

            logger.debug("Attempting to execute the following query: \nClause:[{}] with params [{}]\n", statement, parameterList);

            return getQueryRunner().query(dataStore.getConnection(), statement, resultSetHandler, getParameterList().toArray());
        } catch (SQLException e) {
            throw new PersistenceException(e);
        } finally {
            dataStore.poll();
        }
    }

    /**
     * Makes the update and calls {@link DataStore#poll()} method on complete
     * todo, the exception thrown is PersistenceException, this is not correct due to the JSR-338.
     *
     * @return
     * @throws PersistenceException on any SQL error, this embeds the original {@link SQLException}
     */
    protected int update() {
        return update(this.sqlStatement, this.parameterList);
    }

    /**
     * @param sqlStatement
     * @param parameterList
     * @return
     */
    protected int update(String sqlStatement, List parameterList) {
        try {
            return getQueryRunner().update(dataStore.getConnection(), sqlStatement, parameterList.toArray());
        } catch (SQLException e) {
            throw new PersistenceException(e);
        } finally {
            dataStore.poll();
        }
    }

    protected <T> T update(final T t, String sqlStatement, List parameterList) {
        try {
            getGeneratedKeysQueryRunner((Visitor<Object, RuntimeException>) obj -> {
                //the PK will be received here
                try {
                    Field idColumn = getIDColumn(t.getClass());
                    AtomicBoolean accessible = new AtomicBoolean(idColumn.isAccessible());

                    idColumn.setAccessible(true);
                    try {
                        /*if(idColumn.getAnnotation(Convert.class)!=null){
                            Class<? extends AttributeConverter> attrConvType = idColumn.getAnnotation(Convert.class).converter();
                            if (attrConvType != null && AttributeConverter.class.isAssignableFrom(attrConvType)) {
                                try {
                                    AttributeConverter typeMapper = attrConvType.newInstance();

                                    setterMethod.invoke(t, typeMapper.convertToEntityAttribute(obj));
                                } catch (InstantiationException e) {
                                    throw new IllegalStateException("Unable to call getterMethod", e);
                                }
                            }
                        }else{*/
                        if (obj.getClass().isAssignableFrom(BigDecimal.class)) {
                            if (idColumn.getType() == Integer.class) {
                                idColumn.set(t, ((BigDecimal) obj).intValue());
                                return;
                            } else if (idColumn.getType() == Long.class) {
                                idColumn.set(t, ((BigDecimal) obj).longValue());
                                return;
                            } else if (idColumn.getType() == Double.class) {
                                idColumn.set(t, ((BigDecimal) obj).doubleValue());
                                return;
                            } else if (idColumn.getType() == Float.class) {
                                idColumn.set(t, ((BigDecimal) obj).floatValue());
                                return;
                            }
                        }

                        idColumn.set(t, obj);
//                        }
                    } catch (IllegalAccessException e) {
                        logger.warn("Unable to write data to field: " + t.getClass().getName() + "#" + idColumn.getName()+", type: "+idColumn.getType(), e);
                    } finally {
                        idColumn.setAccessible(accessible.get());
                    }
                } catch (NoSuchElementException x) {
                    //nothing to update
                    logger.warn("No element found", x);
                }
            }).update(dataStore.getConnection(), sqlStatement, parameterList.toArray());

            return t;
        } catch (SQLException e) {
            throw new PersistenceException(e);
        } finally {
            dataStore.poll();
        }
    }

    /**
     * Look for ID column in entity
     *
     * @param type type to examine
     * @return the {@link Field} object annotated with {@link Id}
     * @throws NoSuchElementException if no field annotated with {@link Id} has been found
     */
    protected Field getIDColumn(Class type) throws NoSuchElementException {

        for (Field field : Reflections.<Field>getDeclaredFields(type)) {
            Id col = field.getAnnotation(Id.class);
            if (col == null) continue;

            return field;
        }

        throw new NoSuchElementException("Type " + type.getName() + " has not a field annotated with @javax.persistence.Id");
    }

    /**
     * Using this method to pass it to the exeutor allows the scalability of this class
     *
     * @return
     */
    protected List getParameterList() {
        return parameterList;
    }

    /**
     * @return
     * @throws PersistenceException on any SQL error, this embeds the original {@link SQLException}
     */
//    @Override
    public int executeUpdate() {
        return update();
    }

    /**
     * @return
     */
    public int getMaxResults() {
        return maxResult;
    }

    public int getFirstResult() {
        return startPosition;
    }

    public Map<String, Object> getHints() {
        return hints;
    }

    public LockModeType getLockMode() {
        return lockMode;
    }


    public FlushModeType getFlushMode() {
        return flushMode;
    }

    public <T> T unwrap(Class<T> cls) {
        return null;
    }

    public Set<Parameter<?>> getParameters() {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public Parameter<?> getParameter(String name) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public <T> Parameter<T> getParameter(String name, Class<T> type) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public Parameter<?> getParameter(int position) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public <T> Parameter<T> getParameter(int position, Class<T> type) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public boolean isBound(Parameter<?> param) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public <T> T getParameterValue(Parameter<T> param) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public Object getParameterValue(String name) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }

    public Object getParameterValue(int position) {
        throw new UnsupportedOperationException("Method not supported for Apache DBUtils");
    }
}
