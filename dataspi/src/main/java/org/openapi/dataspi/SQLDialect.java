package org.openapi.dataspi;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Miguel A. Vega P.
 * @version $Id: SQLDialect.java; Apr 23, 2016. 7:32 PM mvega $
 * @source $URL$
 */
public interface SQLDialect {
    /**
     * Limit the maximum results to fetch rom database
     *
     * @param sqlStatement
     * @param maxResults
     * @return
     */
    String forMaxResults(String sqlStatement, int maxResults);

    /**
     * Sets the first position of the previous result to retrieve.
     *
     * @param sqlStatement
     * @param firstResult
     * @return
     */
    String forFirstResult(String sqlStatement, int firstResult);

    /**
     * This is caled when a DATE type is retrieved from database.
     * @param value
     * @return the parameter of type DATE converted into milliseconds
     * @throws SQLException
     */
    <T> long getDateInMillis(T value) throws SQLException;

    /**
     * This is caled when a TIME type is retrieved from database.
     * @param value
     * @return the parameter of type TIME converted into milliseconds
     * @throws SQLException
     */
    <T> long getTimeInMillis(T value) throws SQLException;

    /**
     * This is caled when a DATETIME, TIMESTAMP type is retrieved from database.
     * @param value
     * @return the parameter of type DATETIME, TIMESTAMP converted into milliseconds
     * @throws SQLException
     */
    <T> long getTimeStampInMillis(T value) throws SQLException;

    /**
     *
     * @param date must be either {@link Date} or {@link Calendar} type
     * @return
     * @throws IllegalArgumentException if the given object is not of the expected type.
     */
    <T> T getSQLDate(Date date) throws IllegalArgumentException;
    <T> T getSQLDate(Calendar date) throws IllegalArgumentException;

    /**
     *
     * @param time must be either {@link Date} or {@link Calendar} type
     * @return
     * @throws IllegalArgumentException if the given object is not of the expected type.
     */
    <T> T getSQLTime(Date time);
    <T> T getSQLTime(Calendar time);

    /**
     *
     * @param timeStamp must be either {@link Date} or {@link Calendar} type
     * @return
     * @throws IllegalArgumentException if the given object is not of the expected type.
     */
    <T> T getSQLTimeStamp(Date timeStamp);
    <T> T getSQLTimeStamp(Calendar timeStamp);
}