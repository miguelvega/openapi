package org.openapi.dataspi;

/**
 * @author Miguel A. Vega P.
 * @version $Id: DataStoreAdapter.java; Apr 24, 2016. 1:31 AM mvega $
 * @source $URL$
 */
public abstract class DataStoreAdapter implements DataStore {

    protected SQLDialect sqlDialect = new DefaultSQLDialect();

    /**
     * {@inheritDoc}
     *
     * @param c
     * @param <C>
     * @return
     */
    @Override
    public <C> JPAQuery<C> prepareJpaQuery(Class<C> c) {
        return new JPAQuery<C>(this, c);
    }

    /**
     * @param sqlStatement
     * @param params
     * @return
     */
    public SQLQuery prepareSqlQuery(String sqlStatement, Object... params) {
        return new SQLQuery(this, sqlStatement, params);
    }

    @Override
    public BatchQuery prepareBatchQuery(String sqlStatement) {
        return new BatchQuery(this, sqlStatement);
    }

    @Override
    public SQLDialect getDialect() {
        return sqlDialect;
    }

    @Override
    public DataStore setDialect(SQLDialect sqlDialect) {
        this.sqlDialect = sqlDialect;
        return this;
    }
}
