package org.openapi.dataspi.dbutils;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.openapi.dataspi.Visitor;

import java.sql.*;

/**
 * @author Miguel A. Vega P.
 * @version $Id: EnhancedQueryRunner.java; Sep 15, 2017 3:08 AM miguel.vega $
 */
public class EnhancedQueryRunner<S> extends QueryRunner{
    private StatementStrategy statementStrategy;

    final Visitor<Object, RuntimeException> visitor;

//    public EnhancedQueryRunner() {
//        super();
//    }

    public EnhancedQueryRunner(boolean pmdKnownBroken, Visitor<Object, RuntimeException> visitor) {
        super(pmdKnownBroken);
        this.visitor = visitor;
    }

    /*public EnhancedQueryRunner(DataSource ds) {
        super(ds);
    }

    public EnhancedQueryRunner(DataSource ds, boolean pmdKnownBroken) {
        super(ds, pmdKnownBroken);
    }*/

    /**
     * Factory method that creates and initializes a
     * <code>PreparedStatement</code> object for the given SQL.
     * <code>QueryRunner</code> methods always call this method to prepare
     * statements for them. Subclasses can override this method to provide
     * special PreparedStatement configuration if needed. This implementation
     * simply calls <code>conn.prepareStatement(sql)</code>.
     *
     * @param conn The <code>Connection</code> used to create the
     *             <code>PreparedStatement</code>
     * @param sql  The SQL statement to prepare.
     * @return An initialized <code>PreparedStatement</code>.
     * @throws java.sql.SQLException if a database access error occurs
     */
    @Override
    protected PreparedStatement prepareStatement(Connection conn, String sql)
            throws SQLException {

        if (statementStrategy == null) return conn.prepareStatement(sql);

        return conn.prepareStatement(sql, statementStrategy.getStatementStrategy());
    }

    /**
     * Execute an SQL INSERT, UPDATE, or DELETE query without replacement
     * parameters.
     *
     * @param conn The connection to use to run the query.
     * @param sql  The SQL to execute.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(Connection conn, String sql) throws SQLException {
        return this.update(conn, false, sql, (Object[]) null);
    }

    /**
     * Execute an SQL INSERT, UPDATE, or DELETE query with a single replacement
     * parameter.
     *
     * @param conn  The connection to use to run the query.
     * @param sql   The SQL to execute.
     * @param param The replacement parameter.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(Connection conn, String sql, Object param) throws SQLException {
        return this.update(conn, false, sql, new Object[]{param});
    }

    /**
     * Execute an SQL INSERT, UPDATE, or DELETE query.
     *
     * @param conn   The connection to use to run the query.
     * @param sql    The SQL to execute.
     * @param params The query replacement parameters.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(Connection conn, String sql, Object... params) throws SQLException {
        return update(conn, false, sql, params);
    }

    /**
     * Executes the given INSERT, UPDATE, or DELETE SQL statement without
     * any replacement parameters. The <code>Connection</code> is retrieved
     * from the <code>DataSource</code> set in the constructor.  This
     * <code>Connection</code> must be in auto-commit mode or the update will
     * not be saved.
     *
     * @param sql The SQL statement to execute.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(String sql) throws SQLException {
        Connection conn = this.prepareConnection();

        return this.update(conn, true, sql, (Object[]) null);
    }

    /**
     * Executes the given INSERT, UPDATE, or DELETE SQL statement with
     * a single replacement parameter.  The <code>Connection</code> is
     * retrieved from the <code>DataSource</code> set in the constructor.
     * This <code>Connection</code> must be in auto-commit mode or the
     * update will not be saved.
     *
     * @param sql   The SQL statement to execute.
     * @param param The replacement parameter.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(String sql, Object param) throws SQLException {
        Connection conn = this.prepareConnection();

        return this.update(conn, true, sql, new Object[]{param});
    }

    /**
     * Executes the given INSERT, UPDATE, or DELETE SQL statement.  The
     * <code>Connection</code> is retrieved from the <code>DataSource</code>
     * set in the constructor.  This <code>Connection</code> must be in
     * auto-commit mode or the update will not be saved.
     *
     * @param sql    The SQL statement to execute.
     * @param params Initializes the PreparedStatement's IN (i.e. '?')
     *               parameters.
     * @return The number of rows updated.
     * @throws SQLException if a database access error occurs
     */
    public int update(String sql, Object... params) throws SQLException {
        Connection conn = this.prepareConnection();

        return this.update(conn, true, sql, params);
    }

    /**
     * Calls update after checking the parameters to ensure nothing is null.
     *
     * @param conn      The connection to use for the update call.
     * @param closeConn True if the connection should be closed, false otherwise.
     * @param sql       The SQL statement to execute.
     * @param params    An array of update replacement parameters.  Each row in
     *                  this array is one set of update replacement values.
     * @return The number of rows updated.
     * @throws SQLException If there are database or parameter errors.
     */
    private int update(Connection conn, boolean closeConn, String sql, Object... params) throws SQLException {
        if (conn == null) {
            throw new SQLException("Null connection");
        }

        if (sql == null) {
            if (closeConn) {
                close(conn);
            }
            throw new SQLException("Null SQL statement");
        }

        PreparedStatement stmt = null;

        ResultSet rs = null;

        int rows = 0;

        try {
            stmt = this.prepareStatement(conn, sql);
            this.fillStatement(stmt, params);
            rows = stmt.executeUpdate();

            //retrieve the generated key or if update produced any output
            rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                //generatedKey = rs.getLong(1);
                S generatedKey = (S) rs.getObject(1);
                //pass it to the one who executed this
                if (visitor != null) {
                    visitor.visit(generatedKey);
                }
            }

        } catch (SQLException e) {
            this.rethrow(e, sql, params);

        } finally {
            close(stmt);
            if (closeConn) {
                close(conn);
            }
            DbUtils.close(rs);
        }

        return rows;
    }

    public StatementStrategy getStatementStrategy() {
        return statementStrategy;
    }

    public EnhancedQueryRunner setStatementStrategy(StatementStrategy statementStrategy) {
        this.statementStrategy = statementStrategy;
        return this;
    }

    public enum StatementStrategy {
        RETURN_GENERATED_KEYS {
            @Override
            public int getStatementStrategy() {
                return Statement.RETURN_GENERATED_KEYS;
            }
        }, CLOSE_CURRENT_RESULT {
            @Override
            public int getStatementStrategy() {
                return Statement.CLOSE_CURRENT_RESULT;
            }
        }, EXECUTE_FAILED {
            @Override
            public int getStatementStrategy() {
                return Statement.EXECUTE_FAILED;
            }
        }, KEEP_CURRENT_RESULT {
            @Override
            public int getStatementStrategy() {
                return Statement.KEEP_CURRENT_RESULT;
            }
        }, NO_GENERATED_KEYS {
            @Override
            public int getStatementStrategy() {
                return Statement.NO_GENERATED_KEYS;
            }
        }, CLOSE_ALL_RESULTS {
            @Override
            public int getStatementStrategy() {
                return Statement.CLOSE_ALL_RESULTS;
            }
        }, SUCCESS_NO_INFO {
            @Override
            public int getStatementStrategy() {
                return Statement.SUCCESS_NO_INFO;
            }
        };

        public abstract int getStatementStrategy();
    }
}
