package org.openapi.dataspi;

import org.apache.commons.dbutils.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * An implementation of DataStore, this will fetch a connection on every SQL call to database and will release it immediately on finalization.
 *
 * @author Miguel A. Vega P.
 * @version $Id: DetachableDataStore.java; Apr 23, 2016. 2:02 AM mvega $
 * @source $URL$
 */
public abstract class DetachableDataStore extends DataStoreAdapter implements DataStore {

    protected Connection connection;

    public DetachableDataStore() {
    }

    /**
     * Returns a {@link DataStore} instance using the JNDI configured in application context,
     *
     * @param jndiRef the JNDI referencing name
     * @return
     */
    public static DataStore getDataStore(String jndiRef) {
        throw new UnsupportedOperationException();
    }

    /**
     * A new instance with a data source manager of one or many connections
     *
     * @param dataSource
     * @return
     * @throws SQLException
     */
    public static DataStore getDataStore(final DataSource dataSource) {
        return new DetachableDataStore() {

            public Connection getConnection() throws SQLException {
                this.connection = dataSource.getConnection();
                return connection;
            }
        };
    }

    /**
     * A new instance which will fetch connections from the provider
     *
     * @param connectionProvider
     * @return
     */
    public static DataStore getDataStore(final Provider<Connection> connectionProvider) {
        return new DetachableDataStore() {

            public Connection getConnection() throws SQLException {
                this.connection = connectionProvider.get();
                return connection;
            }
        };
    }

    /**
     * {@inheritDoc}
     * <p>
     * Releases the connection on any execution.
     */
    public void poll() {
        DbUtils.closeQuietly(connection);
    }
}