package org.openapi.dataspi.handler.meta;

import org.openapi.dataspi.SQLDialect;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class helps to assignate a value from/to java object to/from database.
 * Useful for metadata processor
 * @author Miguel Angel Vega P.
 * @version $Id: ColumnFieldProcessor.java 0, 2017-sep-15 10:32 AM miguelvega $
 */
public interface ColumnFieldProcessor {
    /**
     * The field related to the column name
     * @return
     */
    Field getField();

    /**
     * The column name related to a java field
     * @return
     */
    String getColumnName();

    /**
     * This method is invoked before the {@link #assign(ResultSet, Object, SQLDialect)} method is invoked
     */
//    void processWillAssign();

    /**
     * Assignate the database value to a java field
     * @param resultSet data fetched from database
     * @param assignable object where to store resultset object
     * @param sqlDialect
     * @throws IllegalAccessException
     */
    void assign(ResultSet resultSet, Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException;

    /**
     * This method is invoked after the {@link #assign(ResultSet, Object, SQLDialect)} method is invoked
     */
//    void processDidAssign();
}
