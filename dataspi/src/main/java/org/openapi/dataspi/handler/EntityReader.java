package org.openapi.dataspi.handler;

import com.google.common.collect.Lists;
import com.google.common.primitives.Primitives;
import org.openapi.dataspi.SQLDialect;
import org.openapi.lang.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.openapi.lang.Annotations.getAnnotatedValueFromFieldOrReadMethod;

/**
 * @author Miguel A. Vega P.
 * @version $Id: EntityReader.java; May 07, 2016. 3:01 PM mvega $
 * @source $URL$
 * @deprecated maybe will be useful if reading an entity at once, replaced with classes related to {@link EntityCollectionHandler}
 */
@Deprecated
public class EntityReader<T> {

    final static Logger logger = LoggerFactory.getLogger(EntityReader.class);

    final Class<T> t;

    EntityReader(Class<T> t) {
        this.t = t;
    }


    /**
     * @param rs
     * @param sqlDialect
     * @param useColumnLabel
     * @return
     * @throws SQLException
     */
    protected T readEntity(ResultSet rs, SQLDialect sqlDialect, boolean useColumnLabel) throws SQLException {
        //instantiate object, use a strategy.

        final T obj = newInstance();

        ResultSetMetaData rsmd = rs.getMetaData();

        //it is possible to define a value in the statement which is not necessarily an attribute, the target
        //class just might have a setter method listening this call, in that case we need to call such that
        //methods if they aren't binded to a class attribute
        final List<String> columns = Lists.newLinkedList();

        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            columns.add(useColumnLabel ? rsmd.getColumnLabel(i) : rsmd.getColumnName(i));

        //fetch all setters
        List<Field> declaredFields = Reflections.getDeclaredFields(t);

        for (Field field : declaredFields) {
            String columnName = "";

            AtomicBoolean accessible = new AtomicBoolean(field.isAccessible());
            field.setAccessible(true);

            //skip transient annotated attributes.
            Transient transientc = getAnnotatedValueFromFieldOrReadMethod(field, Transient.class);
            if (transientc != null)
                continue;

            Column c = getAnnotatedValueFromFieldOrReadMethod(field, Column.class);
            if (c == null) {
                //Nor the field neither the method has an annotation {@link Column}
                continue;
            }
            columnName = c.name().trim();
            columnName = columnName.length() > 0 ? columnName : field.getName();

            Object value = rs.getObject(columnName);

            if (value == null) {
                continue;
            }

            try {

                Enumerated enumerated = field.getAnnotation(Enumerated.class);
                if (enumerated != null) {

                    Class<Enum> etype = (Class<Enum>) field.getType();

                    if (enumerated.value() == EnumType.STRING) {
                        try {
                            Enum anEnum = Enum.valueOf(etype, value.toString());
//                            setter.invoke(obj, anEnum);
                            field.set(obj, anEnum);
                        } catch (IllegalArgumentException x) {
                            logger.warn("Attempting to set an invalid value [{}] for a not valid Enum type [{}], skipping writing", value, etype);
                        } catch (NullPointerException x) {
                            logger.warn("Attempting to set an invalid value [{}] for a not valid Enum type [{}], skipping writing", value, etype);
                        }
                    } else {
                        int ordinal = (Integer) value;
                        EnumSet enums = EnumSet.allOf(etype);

                        int i = 0;

                        for (Object anEnum : enums) {
                            if (i++ == ordinal) {
//                                setter.invoke(obj, anEnum);
                                field.set(obj, anEnum);
                                break;
                            }
                        }
                    }

                    continue;
                }

                Temporal temporal = field.getAnnotation(Temporal.class);
                if (temporal != null) {

                    final Calendar calendar = Calendar.getInstance();

                    long dateInMillis = 0;


                    switch (temporal.value()) {
                        case DATE:
                            dateInMillis = sqlDialect.getDateInMillis(value);
                            break;
                        case TIME:
                            dateInMillis = sqlDialect.getTimeInMillis(value);
                            break;
                        case TIMESTAMP:
                            dateInMillis = sqlDialect.getTimeStampInMillis(value);
                            break;
                    }


                    calendar.setTimeInMillis(dateInMillis);

                    if (Date.class.isAssignableFrom(field.getType()))
//                        setter.invoke(obj, calendar.getTime());
                        field.set(obj, calendar.getTime());
                    else if (Calendar.class.isAssignableFrom(field.getType()))
//                        setter.invoke(obj, calendar);
                        field.set(obj, calendar);
                    else
                        logger.warn("@Temporal annotation only can be applied to java.util.Date or java.util.Calendar objects. Fied [{}] maped to column [{}] can not be initialized.",
                                field.getType(), columnName);
                    continue;
                }

                //try adecuate numbers

                /*if (Number.class.isAssignableFrom(field.getType().getClass()) || Number.class.isAssignableFrom(Primitives.wrap(field.getType()))) {
                    Number bestNumber = getBestNumber((Number) value, field.getType());
                    setter.invoke(obj, bestNumber);
                }else*/
                try {
//                    setter.invoke(obj, value);
                    field.set(obj, value);
                }catch(IllegalArgumentException x){
                    if (Number.class.isAssignableFrom(field.getType().getClass()) || Number.class.isAssignableFrom(Primitives.wrap(field.getType()))) {
                        Number bestNumber = getBestNumber((Number) value, field.getType());
//                        setter.invoke(obj, bestNumber);
                        field.set(obj, bestNumber);
                        continue;
                    }
                    throw x;
                }
            } catch (IllegalAccessException e) {
                logger.warn("IllegalAccessException, unable to instantiate column '{}#{}', caused by: {}", new Object[]{t.getSimpleName(), field.getName(), e.getMessage()});
            } /*catch (InvocationTargetException e) {
                logger.warn("InvocationTargetException, unable to instantiate column '{}#{}', caused by: {}", new Object[]{t.getSimpleName(), field.getName(), e.getMessage()});
            } */catch (IllegalArgumentException e) {
                e.printStackTrace();
                logger.warn("IllegalArgumentException, unable to instantiate column '{}#{}'. Database returned [{}], but expected type is [{}], caused by: {}",
                        new Object[]{t.getSimpleName(), field.getName(), value.getClass(), field.getType(), e.getMessage()});
            }finally {
                field.setAccessible(accessible.get());
            }
        }

        return obj;
    }


    private Number getBestNumber(Number givenValue, Class<?> requiredType) {

        if (givenValue == null || (givenValue != null && org.openapi.lang.Types.isSameAs(requiredType, givenValue.getClass()))) {
            return givenValue;
        }

        Class<?> wrap = Primitives.wrap(requiredType);

        if (BigDecimal.class.isAssignableFrom(requiredType) || BigDecimal.class.isAssignableFrom(wrap))
            return new BigDecimal(givenValue.toString());
        else if (Byte.class.isAssignableFrom(requiredType) || Byte.class.isAssignableFrom(wrap))
            return givenValue.byteValue();
        else if (Long.class.isAssignableFrom(requiredType) || Long.class.isAssignableFrom(wrap))
            return givenValue.longValue();
        else if (Integer.class.isAssignableFrom(requiredType) || Integer.class.isAssignableFrom(wrap))
            return givenValue.intValue();
        else if (Float.class.isAssignableFrom(requiredType)  || Float.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.floatValue();
        else if (Double.class.isAssignableFrom(requiredType)  || Double.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.doubleValue();
        else if (Short.class.isAssignableFrom(requiredType)   || Byte.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.shortValue();
        else if (BigInteger.class.isAssignableFrom(requiredType)  || BigInteger.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return new BigInteger(givenValue.toString());

        return givenValue;
    }

    /**
     * Creates a new instance of object
     *
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @deprecated replace this with a better one
     */
    @Deprecated
    private T newInstance() {
        try {
            return t.newInstance();
        } catch (Throwable t) {
            throw new InstantiationError("Unable to create a new instance of the given entity type");
        }
    }
}