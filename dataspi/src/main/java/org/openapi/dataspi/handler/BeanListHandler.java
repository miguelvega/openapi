package org.openapi.dataspi.handler;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: BeanListHandler.java 0, 2013-12-04 9:37 AM mvega $
 */
public class BeanListHandler<T> implements ResultSetHandler<List<T>> {

    final Class<T> type;

    public BeanListHandler(Class<T> type){
        this.type = type;
    }

    @Override
    public List<T> handle(ResultSet rs) throws SQLException {

        List<T> list = Lists.newLinkedList();

        JPAEntityHandler<T> mapper = JPAEntityHandler.getSimpleBeanMapper(type);

        while(rs.next()){
            list.add(mapper.mapResultSet(rs));
        }

        return list;
    }
}