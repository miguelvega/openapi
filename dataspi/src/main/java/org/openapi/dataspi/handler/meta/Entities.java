package org.openapi.dataspi.handler.meta;

import org.javatuples.Pair;
import org.openapi.lang.Annotations;
import org.openapi.lang.Reflections;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Optional.ofNullable;
import static org.openapi.lang.Annotations.getAnnotatedValueFromFieldOrReadMethod;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Entities.java; Sep 16, 2017 9:14 AM miguel.vega $
 */
public class Entities {
    /**
     * Returns a valid column name to use, if field has an empty annotated @Column, then returns the field name
     *
     * @param field
     * @return a pair containing the table name related to entity and the annotation found on it: COlumn, EmbeddedID, Embdded, etc
     */

    public static Pair<String, Annotation> findUsableAnnotatedColumnName(Field field) throws NoSuchElementException {

        Column column = getAnnotatedValueFromFieldOrReadMethod(field, Column.class);

        if (column == null) {
            //check if field has EmbeddedId on it
            EmbeddedId embeddedId = getAnnotatedValueFromFieldOrReadMethod(field, EmbeddedId.class);
            if (embeddedId == null) {
                //check if field has Embedded on it
                Embedded embedded = getAnnotatedValueFromFieldOrReadMethod(field, Embedded.class);
                if (embedded == null) {
                    throw new NoSuchElementException("Field: " + field.getDeclaringClass().getName() + "#" + field.getName() +
                            " does not have a @Column annotation.");
                }
                return new Pair<String, Annotation>(null, embedded);
            }
            return new Pair<String, Annotation>(null, embeddedId);
        }

        String columName = isNullOrEmpty(column.name()) ? field.getName() : column.name().trim();
        return new Pair<String, Annotation>(columName, column);
    }


    /**
     * Fetch all names and fields with a collection colection of all the fields that must be used as entity column names
     * @param type
     * @param <E>
     * @return
     */
    public static <E> java.util.Collection<Pair<String, Field>> fetchBestColumnNamesFromEntityType(Class<E> type){
        List<Field> declaredFields = Reflections.getDeclaredFields(type);

        Collection<Pair<String, Field>> collection = new ArrayList<>();

        for (Field ff : declaredFields) {
            Column columnAnnot = Annotations.getAnnotatedValueFromFieldOrReadMethod(ff, Column.class);

            collection.add(new Pair<>(
                ofNullable(columnAnnot).map(column->ofNullable(column.name()).map(name -> name.isEmpty()?ff.getName():name).get()).orElse(ff.getName()),
                ff
            ));
        };

        return collection;
    }

}
