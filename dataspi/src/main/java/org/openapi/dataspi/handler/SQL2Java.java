package org.openapi.dataspi.handler;

import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.handler.meta.ColumnFieldProcessor;
import org.openapi.dataspi.handler.meta.EntityMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: SQL2Java.java 0, 2017-sep-15 11:47 AM miguelvega $
 */
public class SQL2Java<E> {
    final static Logger logger = LoggerFactory.getLogger(EntityReader.class);

    final Class<E> entityType;

    SQL2Java(Class<E> entityType) {
        this.entityType = entityType;
    }

    public EntityMetaData<E> getEntityMetaData(){
        return new EntityMetaData<E>(entityType);
    }

    /**
     * @param rs
     * @param entityMetaData
     * @param sqlDialect
     * @param useColumnLabel
     * @return
     * @throws SQLException
     */
    protected E readEntity(ResultSet rs, EntityMetaData<E> entityMetaData, SQLDialect sqlDialect, boolean useColumnLabel) throws SQLException {
        //instantiate object, use a strategy.

        final E obj = newInstance();

        ResultSetMetaData rsmd = rs.getMetaData();

        //it is possible to define a value in the statement which is not necessarily an attribute, the target
        //class just might have a setter method listening this call, in that case we need to call such that
        //methods if they aren't binded to a class attribute
        /*final List<String> columns = Lists.newLinkedList();
        for (int i = 1; i <= rsmd.getColumnCount(); i++)
            columns.add(useColumnLabel ? rsmd.getColumnLabel(i) : rsmd.getColumnName(i));*/

        //fetch all setters
        Iterator<ColumnFieldProcessor> iterator = entityMetaData.iterator();
        while(iterator.hasNext()){

            ColumnFieldProcessor assignator = iterator.next();

            AtomicBoolean accessible = new AtomicBoolean(assignator.getField().isAccessible());
            assignator.getField().setAccessible(true);


            /*
            Object value = rs.getObject(assignator.getColumnName());

            if (value == null) {
                continue;
            }
            */

            try {
                assignator.assign(rs, obj, sqlDialect);
            } catch (IllegalAccessException e) {
                logger.warn("IllegalAccessException, unable to instantiate column '{}#{}', caused by: {}",
                        new Object[]{entityType.getSimpleName(), assignator.getField().getName(), e.getMessage()});
            }finally {
                assignator.getField().setAccessible(accessible.get());
            }
        }

        return obj;
    }

    /**
     * Creates a new instance of object
     *
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @deprecated replace this with a better one
     */
    @Deprecated
    private E newInstance() {
        try {
            return entityType.newInstance();
        } catch (Throwable t) {
            throw new InstantiationError("Unable to create a new instance of the given entity type");
        }
    }
}
