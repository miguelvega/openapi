package org.openapi.dataspi.handler;

import com.google.common.collect.Lists;
import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;
import org.openapi.lang.Reflections;
import org.openapi.lang.StringMerger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.*;

import static com.google.common.base.Optional.fromNullable;
import static com.google.common.base.Strings.isNullOrEmpty;
import static org.openapi.lang.Annotations.getAnnotatedValueFromFieldOrReadMethod;
import static org.openapi.lang.Reflections.getDeclaredFields;

/**
 * This class allows to build a SQL statement based on an annotated @Entity type
 *
 * @author Miguel A. Vega P.
 * @version $Id: SQLStatementBuilder.java; May 07, 2016. 10:37 PM mvega $
 * @source $URL$
 * @deprecated use an implementation of {@link org.openapi.dataspi.jpa.lite.StatementBuilder} class instead
 */
@Deprecated
public class SQLStatementBuilder<V> {

    public static final String PARAM_SEPARATOR = ", ";
    private boolean distinct = false;

    private static Logger logger = LoggerFactory.getLogger(SQLStatementBuilder.class);

    final SQLDialect sqlDialect;

    public SQLStatementBuilder(SQLDialect sqlDialect) {
        this.sqlDialect = sqlDialect;
    }

    /**
     * todo, what happens when none method is annotated with column
     * todo, need to provide filter, groupby, etc
     *
     * @param type
     * @return
     */
    public String buildSimpleSelect(Class<V> type) {
        return this.buildSimpleSelect(type, null);
    }

    /**
     * @param type
     * @param schema sometimes it's necessary to define a schema to work within specifically
     * @return
     */
    public String buildSimpleSelect(Class<V> type, final String schema) {

        //first get annotation
        Table table = getTableName(type);

        if (table == null) {
            throw new IllegalArgumentException(StringMerger.of("Type '").app(type.getName()).app("', must be annotated with javax.persistence.Table.").merge());
        }

        List<Field> declaredFields = getDeclaredFields(type);

        StringMerger is = StringMerger.of("SELECT ");

        is.app(distinct ? " DISTINCT " : "");

        for (Field field : declaredFields) {

            try {
                is.app(getUsableColumnName(field)).app(PARAM_SEPARATOR);
            } catch (NoSuchElementException x) {
                logger.debug("Column for field '{}' not usable to fetch or to save data in database", field.getName());
            }
        }

        is = is.removeLastToken(PARAM_SEPARATOR);

        String qualified = getQualifiedTableName(schema, type);

        is.app(" FROM ").app(qualified);

        return is.merge();
    }

    public String buildPreparedDelete(V v) {
        return buildPreparedDelete(v, null);
    }

    protected String buildPreparedDelete(V v, final String schema) {

        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

        if (table == null)
            throw new IllegalArgumentException(StringMerger.of("Type '").app(type.getName()).app("', must be annotated with javax.persistence.Table.").merge());

        String tqualifiedName = getQualifiedTableName(schema, type);

        StringMerger is = StringMerger.of("DELETE FROM ").app(tqualifiedName);

        return is.merge();
    }

    /**
     * This method builds the UPDATE statement using the '?' in the same way PreparedStatements does.
     *
     * @param v only {@link Table} annotated beans can use this method
     * @return
     */
    public Pair<String, List> buildPreparedUpdate(V v) {
        return buildPreparedUpdate(v, null);
    }

    public Pair<String, List> buildPreparedUpdate(V v, final String schema) {

        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

        if (table == null)
            throw new IllegalArgumentException(StringMerger.of("Type '").app(type.getName()).app("', must be annotated with javax.persistence.Table.").merge());

        Iterable<Field> fields = getDeclaredFields(type);

        String qualified = getQualifiedTableName(schema, type);

        StringMerger is = StringMerger.of("UPDATE ").app(qualified).app(" SET ");

        final List<Object> values = Lists.newLinkedList();

        for (Field field : fields) {

            Column col = field.getAnnotation(Column.class);
            if (col == null) continue;

            GeneratedValue pk = field.getAnnotation(GeneratedValue.class);

            try {

                values.add(javaType2Database(field, v));

                is.app(getUsableColumnName(field)).app(" = ?").app(PARAM_SEPARATOR);

            } catch (IllegalStateException e) {
                //todo
            }
            //}
        }

        is = is.removeLastToken(PARAM_SEPARATOR);

        final String qstr = is.merge();

        logger.debug("QUERY: " + qstr + ", DATA: " + values);

        return new Pair<>(qstr, values);
//        return Query.of(qstr, values.toArray());
    }

    /**
     * This method builds the UPDATE statement using the '?' in the same way PreparedStatements does.
     *
     * @param v only {@link Table} annotated beans can use this method
     * @return
     */
    public Pair<String, List> buildPreparedInsert(V v) {
        return buildPreparedInsert(v, null);
    }

    /**
     *
     * @param v
     * @param schema
     * @return
     */
    protected Pair<String, List> buildPreparedInsert(V v, final String schema) {
        Class<V> type = (Class<V>) v.getClass();

        assert isValidInput(v);

        Table table = getTableName(type);

        if (table == null)
            throw new IllegalArgumentException(StringMerger.of("Type '").app(type.getName()).app("', must be annotated with javax.persistence.Table.").merge());

//        Iterable<PropertyDescriptor> descriptors = inspectType(type);
        Iterable<Field> fields = getDeclaredFields(type);

        String qualified = getQualifiedTableName(schema, type);

        StringMerger insertSQLStatement = StringMerger.of("INSERT INTO ").app(qualified).app(" (");

        StringMerger wildcardMarks = StringMerger.of();

        final List<Object> values = Lists.newLinkedList();

        for (Field field : fields) {

//            Transient transientc = getAnnotatedValueFromFieldOrReadMethod(field, Transient.class);
//            if (transientc != null)
//                continue;

            Column col = getAnnotatedValueFromFieldOrReadMethod(field, Column.class);
            if (col == null) continue;

            GeneratedValue pk = field.getAnnotation(GeneratedValue.class);

            if (pk != null) {
                if (pk.strategy() == GenerationType.SEQUENCE) {

                    //When using a postgresql database, column might be SERIAL, which means

                    String generator = pk.generator();

                    if (!isNullOrEmpty(generator)) {
                        //only if user explicitly specifies the SEQUENCE generator, use it for insert
                        //e.g. generator = "nextval('idsequence')"
                        insertSQLStatement.app(getUsableColumnName(field)).app(PARAM_SEPARATOR);
                        wildcardMarks.app(pk.generator()).app(PARAM_SEPARATOR);
                    }
                } else if (pk.strategy() == GenerationType.TABLE) {
                    throw new UnsupportedOperationException("This feture is not implemented yet");
                } else {
                    //AUTO, IDENTITY or NULL
                    //simply do nothing, skip such this column
                }
            } else {
                try {

                    values.add(javaType2Database(field, v));

                    insertSQLStatement.app(isNullOrEmpty(col.name()) ? field.getName() : col.name()).app(PARAM_SEPARATOR);
                    wildcardMarks.app("?").app(PARAM_SEPARATOR);

                } catch (IllegalStateException e) {
                    logger.warn("Can not concrete the SQL building for field: {}#{}", field.getDeclaringClass(), field.getName(), e);
                }
            }
        }

        insertSQLStatement = insertSQLStatement.removeLastToken(PARAM_SEPARATOR);
        wildcardMarks = wildcardMarks.removeLastToken(PARAM_SEPARATOR);

        insertSQLStatement.app(") VALUES (").app(wildcardMarks.merge()).app(')');

        final String qstr = insertSQLStatement.merge();

        logger.debug("QUERY: " + qstr + ", DATA: " + values);

        return new Pair<>(qstr, values);
    }

    private String getQualifiedTableName(final String schema, Class type) {

        Table table = (Table) type.getAnnotation(Table.class);

        String tableName = isNullOrEmpty(table.name())?type.getSimpleName():table.name();

        return fromNullable(schema).transform(input -> isNullOrEmpty(input) ? "" : schema.concat(".")).or("").concat(tableName);
    }

    private boolean isValidInput(V v) {
        return v != null && v.getClass().getAnnotation(Table.class) != null;
    }

    private Table getTableName(Class<?> type) {
        Table ann = type.getAnnotation(Table.class);

        return fromNullable(ann).orNull();
    }

    /*
    public Filter buildFilterForId(V v) {
        assert v != null;

        Table e = v.getClass().getAnnotation(Table.class);
        if (e == null)
            throw new IllegalArgumentException(v.getClass().getName() + " is not a valid @Table object");

        List<Field> declaredFields = getDeclaredFields(v.getClass());

        for (Field df : declaredFields) {
            Id id = df.getAnnotation(Id.class);
            if (id != null) {
                return Filter.equal(getUsableColumnName(df), javaType2Database(df, v));
            }
        }

        throw new IllegalArgumentException(v.getClass().getName() + " does not contain a attribute annotated with @Id");
    }
    */

    /**
     * Converts the java object into a valid database type, use the dialect to achieve this conversion
     * @param field
     * @param declaringObject
     * @param parameters
     * @return if Temporal a {@link java.util.Date} or a {@link java.util.Calendar} object if not null
     */
    private Object javaType2Database(Field field, Object declaringObject, Object... parameters) {

        final boolean accessible = field.isAccessible();

        try {

            field.setAccessible(true);

            Method readMethod = Reflections.getGetterMethod(field.getName(), declaringObject.getClass());

            Object plain = field.get(declaringObject);

            if(plain == null)
                return plain;

            if(field.getAnnotation(Temporal.class)!=null || readMethod.getAnnotation(Temporal.class)!=null){
                Temporal temporal = Optional.ofNullable(field.getAnnotation(Temporal.class)).orElse(readMethod.getAnnotation(Temporal.class));
                switch(temporal.value()){
                    case DATE:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLDate((Date)plain);
                        else
                            return sqlDialect.getSQLDate((Calendar) plain);
                    case TIME:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLTime((Date) plain);
                        else
                            return sqlDialect.getSQLTime((Calendar) plain);
                    case TIMESTAMP:
                        if(Date.class.isAssignableFrom(plain.getClass()))
                            return sqlDialect.getSQLTimeStamp((Date) plain);
                        else
                            return sqlDialect.getSQLTimeStamp((Calendar) plain);
                };
            }

            if(field.getAnnotation(Enumerated.class)!=null || readMethod.getAnnotation(Enumerated.class)!=null) {
                Enumerated enu = Optional.
                        ofNullable(field.getAnnotation(Enumerated.class)).
                        orElse(readMethod.getAnnotation(Enumerated.class));

                Class<Enum> etype = (Class<Enum>) plain.getClass();

                //plain is a Enum
                if(enu.value() == EnumType.STRING){
                    return Enum.valueOf(etype, plain.toString()).name();
                }else{
                    //by default also
                    return Enum.valueOf(etype, plain.toString()).ordinal();
                }
            }

            return plain;

        } catch (IntrospectionException e) {
            throw new IllegalStateException("Method not found ["+field.getName()+"] in object of type ["+declaringObject.getClass()+"]", e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Unexcpected error", e);
        }finally {
            field.setAccessible(accessible);
        }
    }

    /**
     * Returns a valid column name to use, if field has an empty annotated @Column, then returns the field name
     *
     * @param field
     * @return
     */
    protected String getUsableColumnName(Field field) {

        Column column = getAnnotatedValueFromFieldOrReadMethod(field, Column.class);


        /*Column column = field.getAnnotation(Column.class);

        if (column == null) {
            //check if the getter method has the Column annotation on it
            try {
                Method getterMethod = Reflections.getGetterMethod(field);
                if (getterMethod != null) {
                    //todo, only accept Fields annotated with @Column, if not avoid this Field
                    column = getterMethod.getAnnotation(Column.class);
                }
            } catch (IntrospectionException e) {
                //todo, only accept Fields annotated with @Column, if not avoid this Field
            }
        }
        */

        if (column == null) {
            throw new NoSuchElementException("Field: " + field.getDeclaringClass().getName() + "#" + field.getName() +
                    " does not have a @Column annotation.");
        }

        return isNullOrEmpty(column.name()) ? field.getName() : column.name();
    }

    /**
     * Allows to pretty print the statement created
     * @return
     */
    public static String prettyPrintStatemet(Pair<String, List> pair){
        StringBuilder buffer = new StringBuilder("SQL Statement [");

        buffer.append(pair.getValue0()).append("]");

        buffer.append(" with parameters [");

        int i=0;
        for (Object o : pair.getValue1()) {
            if(o==null){
                buffer.append(o);
                continue;
            }
            buffer.append(o).append(":").append(o.getClass().getName()).append(++i<pair.getValue1().size()?", ":"");
        }

        buffer.append("]");

        return buffer.toString();
    }
}
