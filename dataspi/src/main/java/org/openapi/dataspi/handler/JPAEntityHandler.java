/*
 * gears
 *     http://www.open-logics.com
 *     Copyright (C) 2012, OpenLogics
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.openapi.dataspi.handler;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.dbutils.ResultSetHandler;
import org.openapi.lang.Reflections;
import org.openapi.lang.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: JPAEntityHandler.java 0, 2012-11-14 2:55 PM mvega $
 * @deprecated use the {@link EntityHandler<T>} class instead
 */
@Deprecated
public class JPAEntityHandler<T> implements ResultSetHandler {

    Logger logger = LoggerFactory.getLogger(JPAEntityHandler.class);
    private Visitor<T, SQLException> setHandler;
    private Class<T> requiredType;

    public JPAEntityHandler(Visitor<T, SQLException> setHandler, Class<T> requiredType) {
        this.setHandler = setHandler;
        this.requiredType = requiredType;
    }

    /**
     * Whenever need to map a simple
     *
     * @param type
     * @param <T>
     * @return
     */
    public static <T> JPAEntityHandler<T> getSimpleBeanMapper(Class<T> type) {
        return new JPAEntityHandler<T>(new Visitor<T, SQLException>() {
            @Override
            public void visit(T obj) throws SQLException {
            }
        }, type);
    }

    /**
     * @param setHandler
     * @return
     * @deprecated replaced by {@link org.apache.commons.dbutils.handlers.MapListHandler} class
     */
    @Deprecated
    public static final JPAEntityHandler buildMapListHandler(Visitor<Map<String, Object>, SQLException> setHandler) {
        return new JPAEntityHandler(setHandler, Map.class);
    }

    @Override
    public T handle(ResultSet rs) throws SQLException {
        while (rs.next()) {
            T obj = mapResultSet(rs, requiredType);
            //T obj = new BeanHandler<T>(requiredType).onStatementPopulation(rs);
            setHandler.visit(obj);
        }
        return null;
    }

    /**
     * If {@link JPAEntityHandler} has been built using the: {@link JPAEntityHandler#getSimpleBeanMapper(Class)} method, then simply use this to map result set
     * to object.
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    public T mapResultSet(ResultSet resultSet) throws SQLException {
        return this.mapResultSet(resultSet, false, requiredType);
    }

    /**
     * @param resultSet
     * @param requiredType the class to be used to map result
     * @return
     * @throws SQLException
     */
    public T mapResultSet(ResultSet resultSet, Class<T> requiredType) throws SQLException {
        return this.mapResultSet(resultSet, false, requiredType);
    }

    /**
     * @param resultSet
     * @param instantiate
     * @return
     * @throws SQLException
     */
    public T mapResultSet(ResultSet resultSet, InstantiationStrategy<T> instantiate) throws SQLException {
        return this.mapResultSet(resultSet, false, instantiate);
    }

    /**
     * @param resultSet
     * @param useColumnLabel this means that the column label will be used
     *                       instead of the columnName, some drivers support this feature
     * @param requiredType   the class to be used to map result
     * @return
     * @throws SQLException
     */
    public T mapResultSet(ResultSet resultSet, boolean useColumnLabel, Class<T> requiredType) throws SQLException {
        return this.mapResultSet(resultSet, useColumnLabel, new InstantiationStrategy<T>(requiredType));
    }

    /**
     * @param resultSet
     * @param useColumnLabel
     * @param instantiationStrategy
     * @return
     * @throws SQLException
     */
    private T mapResultSet(ResultSet resultSet, boolean useColumnLabel, InstantiationStrategy<T> instantiationStrategy) throws SQLException {
        try {
            //T obj = requiredType.newInstance();
            if (instantiationStrategy == null || instantiationStrategy.getType() == null) {
                throw new IllegalArgumentException("InstantiationStrategy can not be null neither the type to instantiate.");
            }
            ResultSetMetaData rsmd = resultSet.getMetaData();

            /*
            todo, to decide which dialect to use, can be useful to use the class of resultset
            System.err.println("a >>>>>>>>"+resultSet.getClass());
            System.err.println("b >>>>>>>>"+rsmd.getClass());
            */

            //it is possible to define a value in the statement which is not necessarily an attribute, the target
            //class just might have a setter method listening this call, in that case we need to call such that
            //methods if they aren't binded to a class attribute
            List<String> pending = Lists.newArrayList();
            for (int i = 1; i <= rsmd.getColumnCount(); i++)
                pending.add(useColumnLabel ? rsmd.getColumnLabel(i) : rsmd.getColumnName(i));


            Class requiredType = instantiationStrategy.getType();
            if (!Map.class.isAssignableFrom(requiredType)) {
                T obj = instantiationStrategy.newInstance(resultSet);
                //Adecuate RESULTS to BEAN struct

                //todo, use reflections to retrieve all SETTER methods, for now only VOID methods works
                List<Field> fields = Reflections.getDeclaredFields(requiredType);//requiredType.getDeclaredFields();

                for (Field field : fields) {
                    Method method;
                    String columnName = "";
                    try {
                        method = Reflections.getSetterMethod(field.getName(), requiredType);
                    } catch (IntrospectionException e) {
                        //logger.debug("Can't bind a result to method " + field.getName() + " of class " + requiredType.getEntityName());
                        continue;
                    }
                    Object value = null;
                    try {

                        Transient transientc = field.getAnnotation(Transient.class);
                        if (transientc != null)
                            continue;

                        Column c = field.getAnnotation(Column.class);
                        if (c != null) {
                            columnName = c.name().trim();
                        } else {
                            //check if the getter method has the COlumn annotation on it
                            try {
                                Method getterMethod = Reflections.getGetterMethod(field);
                                if (getterMethod == null) {
                                    //todo, only accept Fields annotated with @Column, if not avoid this Field
                                    continue;
                                }
                                c = getterMethod.getAnnotation(Column.class);
                                if (c == null) {
                                    continue;
                                }
                                columnName = c.name().trim();
                            } catch (IntrospectionException e) {
                                //todo, only accept Fields annotated with @Column, if not avoid this Field
                                continue;
                            }
                        }

                        columnName = columnName.length() > 0 ? columnName : field.getName();

                        //todo, the following line has been commented, and replaced with one which evaluates in a better manner the value requesting
                        //value = resultSet.getObject(columnName);

                        value = processColumn(resultSet, columnName, field.getType());

                        //check if enum type is annotated
                        Enumerated enumerated = field.getAnnotation(Enumerated.class);
                        if (enumerated != null) {
                            //todo, not implemented yet
                            if (enumerated.value() == EnumType.ORDINAL) {
                            } else {
                            }

                            Class<Enum> etype = (Class<Enum>) field.getType();

                            try {
                                method.invoke(obj, Enum.valueOf(etype, value.toString()));
                            } catch (IllegalArgumentException x) {
                                logger.error("Unable to instantiate column '{}#{}', caused by: {}", new Object[]{requiredType.getSimpleName(), field.getName(), x.getMessage()});
                            }
                            continue;
                        }

                        //todo, how should tempora must work
                        Temporal temporal = field.getAnnotation(Temporal.class);
                        if(temporal != null){
                            final Calendar calendar = Calendar.getInstance();
                            switch(temporal.value()){
                                case DATE:
                                    long time = ((java.util.Date) value).getTime();
                                    calendar.setTimeInMillis(time);
                                    method.invoke(obj, calendar.getTime());
                                    break;
                                case TIME:
                                    time = ((java.util.Date) value).getTime();
                                    calendar.setTimeInMillis(time);
                                    method.invoke(obj, calendar.getTime());
                                    break;
                                case TIMESTAMP:
                                    time = ((java.util.Date) value).getTime();
                                    calendar.setTimeInMillis(time);
                                    method.invoke(obj, calendar.getTime());
                                    break;
                            }

                            continue;
                        }

                        //todo, try removing this

                        //before invoke

                        //simply invoke method
                        method.invoke(obj, value);
                        continue;

                    } catch (IllegalArgumentException ex) {
                        ex.printStackTrace();
                        if (value == null) {
                            continue;
                        }
                        logger.debug("Type found in database is '" + value.getClass().getName() + "', but target object requires '" + field.getType().getName() + "': " + ex.getLocalizedMessage());
                        //if this is thrown the try to fix this error using the following:
                        //If is a big decimal, maybe pojo has double or float attributes
                        try {
                            if (value instanceof BigDecimal || value instanceof Number) {
                                if (Double.class.isAssignableFrom(field.getType())
                                        || double.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, ((BigDecimal) value).doubleValue());
                                    continue;
                                } else if (Float.class.isAssignableFrom(field.getType())
                                        || float.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, ((BigDecimal) value).floatValue());
                                    continue;
                                } else if (Long.class.isAssignableFrom(field.getType())
                                        || long.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, ((BigDecimal) value).longValue());
                                    continue;
                                } else {
                                    logger.warn("Tried to fix the mismatch problem, but couldn't: " + "Trying to inject an object of class " + value.getClass().getName() + " to an object of class " + field.getType());
                                }
                            } else if (value instanceof Date) {
                                Date dd = (Date) value;
                                if (Date.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, new Date(dd.getTime()));
                                    continue;
                                } else if (Timestamp.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, new Timestamp(dd.getTime()));
                                    continue;
                                } else if (Time.class.isAssignableFrom(field.getType())) {
                                    method.invoke(obj, new Time(dd.getTime()));
                                    continue;
                                }
                            }
                        } catch (IllegalArgumentException x) {
                            printIllegalArgumentException(x, field, value);
                        } catch (InvocationTargetException x) {
                            x.printStackTrace();
                        }
                        //throw new DataSourceException("Can't call method " + method.getEntityName() + " due to "+ex.getMessage(), ex);
                        logger.warn("Can't call method " + method.getName() + " due to: " + ex.getMessage() + ".");
                    } catch (InvocationTargetException ex) {
                        //throw new DataSourceException("Can't inject an object into method " + method.getEntityName(), ex);
                        logger.warn("Can't inject an object into method " + method.getName() + " due to: " + ex.getMessage());
                    } catch (SQLException ex) {
                        //removing the print of exception
                        logger.debug("Target object has a field '" + columnName + "', this was not found in query results, "
                                + "this cause that attribute remains NULL or with default value.", ex);
                    }
                }
                return obj;
            } else {
                ImmutableMap.Builder<String, Object> obj = new ImmutableMap.Builder<String, Object>();
                //Adecuate results to BEAN
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    String column = useColumnLabel ? rsmd.getColumnLabel(i) : rsmd.getColumnName(i);
                    Object value = resultSet.getObject(i);
                    obj.put(column, value);
                }
                return (T) obj.build();
            }
        } catch (IllegalAccessException ex) {
            throw new SQLException("Object of class " + instantiationStrategy.getType().getName() + " doesn't provide a valid access. It's possible be private or protected access only.", ex);
        }
    }

    /**
     * Convert a <code>ResultSet</code> column into an object.  Simple
     * implementations could just call <code>rs.getObject(index)</code> while
     * more complex implementations could perform type manipulation to match
     * the column's type to the bean property type.
     * <p/>
     * <p>
     * This implementation calls the appropriate <code>ResultSet</code> getter
     * method for the given property type to perform the type conversion.  If
     * the property type doesn't match one of the supported
     * <code>ResultSet</code> types, <code>getObject</code> is called.
     * </p>
     *
     * @param rs         The <code>ResultSet</code> currently being processed.  It is
     *                   positioned on a valid row before being passed into this method.
     * @param columnName The current column name being processed.
     * @param propType   The bean property type that this column needs to be
     *                   converted into.
     * @return The object from the <code>ResultSet</code> at the given column
     * index after optional type processing or <code>null</code> if the column
     * value was SQL NULL.
     * @throws SQLException if a database access error occurs
     */
    protected Object processColumn(ResultSet rs, String columnName, Class<?> propType)
            throws SQLException {

        if (!propType.isPrimitive() && rs.getObject(columnName) == null) {
            return null;
        }

        if (propType.equals(String.class)) {
            return rs.getString(columnName);

        } else if (
                propType.equals(Integer.TYPE) || propType.equals(Integer.class)) {
            return Integer.valueOf(rs.getInt(columnName));

        } else if (
                propType.equals(Boolean.TYPE) || propType.equals(Boolean.class)) {
            return Boolean.valueOf(rs.getBoolean(columnName));

        } else if (propType.equals(Long.TYPE) || propType.equals(Long.class)) {
            return Long.valueOf(rs.getLong(columnName));

        } else if (
                propType.equals(Double.TYPE) || propType.equals(Double.class)) {
            return Double.valueOf(rs.getDouble(columnName));

        } else if (
                propType.equals(Float.TYPE) || propType.equals(Float.class)) {
            return Float.valueOf(rs.getFloat(columnName));

        } else if (
                propType.equals(Short.TYPE) || propType.equals(Short.class)) {
            return Short.valueOf(rs.getShort(columnName));

        } else if (propType.equals(Byte.TYPE) || propType.equals(Byte.class)) {
            return Byte.valueOf(rs.getByte(columnName));

        } else if (propType.equals(Timestamp.class)) {
            return rs.getTimestamp(columnName);

        } else if (propType.equals(SQLXML.class)) {
            return rs.getSQLXML(columnName);

        } else {
            return rs.getObject(columnName);
        }

    }

    void printIllegalArgumentException(IllegalArgumentException x, Field field, Object value) {
        x.printStackTrace();
        if (x.getMessage().trim().equals("argument type mismatch")) {
            logger.warn("Tried to fix the mismatch problem, but couldn't: "
                    + "Trying to inject an object of class " + value.getClass() + " to an object of class "
                    + field.getType());
        }
    }
}