package org.openapi.dataspi.handler.meta;

import org.javatuples.Pair;
import org.openapi.dataspi.SQLDialect;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Miguel A. Vega P.
 * @version $Id: EmbeddedProcessor.java; Sep 15, 2017 10:33 PM miguel.vega $
 */
public class EmbeddedProcessor extends AbstractProcessor {

    final Collection<Pair<String, Field>> embeddedFields;

    protected EmbeddedProcessor(Field field, String resultSetColumnName) {
        super(field, resultSetColumnName);

        //get the fields from class
        this.embeddedFields = Entities.fetchBestColumnNamesFromEntityType(field.getType());
    }

    @Override
    public void assign(final ResultSet resultSet, final Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException {
        //when this is performed, need to populate all embedded fields from result set
        for (Pair<String, Field> pair : embeddedFields) {
            Field efield = pair.getValue1();
            AtomicBoolean acc = new AtomicBoolean(efield.isAccessible());
            try{
                efield.setAccessible(true);
                efield.set(assignable, resultSet.getObject(pair.getValue0()));
            } catch (IllegalAccessException e) {
                throw new IllegalAccessException("Unable to assign a value to embedded field: "+
                        field.getDeclaringClass().getName()+"#"+field.getName()+". Cause: "+e.getMessage());
            } finally {
                efield.setAccessible(acc.get());
            }
        };
    }

    public Collection<Pair<String, Field>> getEmbeddedFields() {
        return embeddedFields;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || !EmbeddedProcessor.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        EmbeddedProcessor proc = (EmbeddedProcessor) obj;
        return field.equals(proc.getField()) && embeddedFields.equals(proc.embeddedFields);
    }
}
