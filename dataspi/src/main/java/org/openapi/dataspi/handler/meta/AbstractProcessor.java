package org.openapi.dataspi.handler.meta;

import org.openapi.lang.StringMerger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: AbstractProcessor.java 0, 2017-sep-15 10:56 AM miguelvega $
 */
public abstract class AbstractProcessor implements ColumnFieldProcessor {

    final protected Logger logger = LoggerFactory.getLogger(getClass());

    protected final Field field;

    protected final String resultSetColumnName;

    protected AbstractProcessor(Field field, String resultSetColumnName) {

        if(field==null)
            throw new IllegalArgumentException("Field given must not be null");

        this.field = field;
        this.resultSetColumnName = resultSetColumnName;
    }

    @Override
    public Field getField() {
        return field;
    }

    @Override
    public String getColumnName() {
        return resultSetColumnName;
    }

    /**
     * Fetches data fetched from database via ResultSet object, this is suppossed to be the binding data to the field
     * annotated with {@link javax.persistence.Column}.
     * @param resultSet
     * @return
     * @throws SQLException
     */
    protected Object getBindingValue(ResultSet resultSet) throws SQLException {
        return resultSet.getObject(getColumnName());
    }

    /**
     * Checks if a column is primary key
     * @return
     * @see <a href="http://www.objectdb.com/java/jpa/entity/id">About PK</a>
     */
    public boolean isPrimaryKey(){
        return field.getAnnotation(Id.class)!=null ||
                field.getAnnotation(EmbeddedId.class)!=null ||
                field.getAnnotation(IdClass.class)!=null;
    }

    @Override
    public String toString() {
        return StringMerger.of("Mapper[Field: ", field.getName(), " mapped to column name: ", resultSetColumnName, "]").merge();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || !ColumnFieldProcessor.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        ColumnFieldProcessor proc = (ColumnFieldProcessor) obj;
        return field.equals(proc.getField()) && resultSetColumnName.equals(proc.getColumnName());
    }
}
