package org.openapi.dataspi.handler.meta;

import org.javatuples.Pair;
import org.openapi.lang.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Optional.ofNullable;
import static org.openapi.lang.Annotations.getAnnotatedValueFromFieldOrReadMethod;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: EntityMetaData.java 0, 2017-sep-15 10:21 AM miguelvega $
 */
public class EntityMetaData<E> {

    final Logger logger = LoggerFactory.getLogger(getClass());

    final List<ColumnFieldProcessor> metaData = new ArrayList();

    final Class<E> entityType;

    final AtomicInteger PK_INDEX = new AtomicInteger(-1);

    public EntityMetaData(Class<E> entityType) {
        this.entityType = entityType;
        discover();
    }

    /**
     * Retrieves the well formed
     * @param schema if this value is not null, then will replace the {@link Table#schema()} annotation attribute
     * @return
     */
    public String getQualifiedTableName(final String schema) {

        Table table = (Table) entityType.getAnnotation(Table.class);

        String tableName = isNullOrEmpty(table.name())?entityType.getSimpleName():table.name();
        String tableSchema = isNullOrEmpty(table.schema())?schema:table.schema();

        return ofNullable(tableSchema).map(input -> tableSchema.concat(".")).orElse("").concat(tableName);
    }

    public Class<E> getEntityType(){
        return entityType;
    }

    /**
     * @return the field that represents the primary key in table
     */
    public ColumnFieldProcessor getIdentifierField() throws NoSuchFieldException {
        if (PK_INDEX.get() == -1)
            throw new NoSuchFieldException("There is no column annotated as a primary key");

        return metaData.get(PK_INDEX.get());
    }

    /**
     * Start filling the relationship between table columns and object fields
     */
    private void discover() {

        if (metaData.size() > 0) {
            //metadata already has been dicovered
            return;
        }

        //fetch all setters
        List<Field> declaredFields = Reflections.getDeclaredFields(getGenericTypeClass());

        int count = -1;

        for (Field field : declaredFields) {

            count++;

            AbstractProcessor processor = null;

            try {
                Pair<String, Annotation> columnMap = null;

                //skip transient annotated attributes.
                Transient transientc = getAnnotatedValueFromFieldOrReadMethod(field, Transient.class);
                if (transientc != null) {
                    //don't care about transients
                    metaData.add(processor = new TransientProcessor(field));
                    continue;
                }

                try {
                    columnMap = Entities.findUsableAnnotatedColumnName(field);
                } catch (NoSuchElementException x) {
                    //not a field annotated with Column
                    continue;
                }

                Class<? extends Annotation> columnAnnotationType = columnMap.getValue1().getClass();

                if(Column.class.isAssignableFrom(columnAnnotationType)){
                    Enumerated enumerated = field.getAnnotation(Enumerated.class);
                    if (enumerated != null) {
                        if (enumerated.value() == EnumType.STRING) {
                            metaData.add(processor = new EnumeratedProcessor(field, columnMap.getValue0(), EnumType.STRING));
                        } else {
                            metaData.add(processor = new EnumeratedProcessor(field, columnMap.getValue0(), EnumType.ORDINAL));
                        }
                        continue;
                    }

                    Temporal temporal = field.getAnnotation(Temporal.class);
                    if (temporal != null) {
                        metaData.add(processor = new TemporalProcessor(field, columnMap.getValue0(), temporal.value()));
                        continue;
                    }

                    //nor temporal either enumerated, just a simple
                    metaData.add(processor = new SimpleProcessor(field, columnMap.getValue0()));
                }else if(EmbeddedId.class.isAssignableFrom(columnAnnotationType)){
                    //get the type and fetch all the fields within, they are the keys, if they are annotated with
                    //@Column, then use the value in COlumn.name, else, simply yse the fieldName
                    metaData.add(processor = new EmbeddedProcessor(field, columnMap.getValue0()));
                }else if(Embedded.class.isAssignableFrom(columnAnnotationType)){
                    metaData.add(processor = new EmbeddedProcessor(field, columnMap.getValue0()));
                }
            } finally {
                //get the recently added element and check if this is primary key column
                if (processor != null && processor.isPrimaryKey()) {
                    int old = PK_INDEX.getAndSet(count);
                    if (old != -1) {
                        Field field1 = metaData.get(old).getField();
                        logger.warn("A previous field has been declared as PRIMARY ALSO, this was: {}#{}",
                                field1.getDeclaringClass().getName(), field1.getName());
                    }
                }
            }
        }
    }

    /**
     * @return
     */
    public Iterator<ColumnFieldProcessor> iterator() {
        return metaData.iterator();
    }

    /**
     * @return
     */
    public List<ColumnFieldProcessor> list() {
        return metaData;
    }

    /**
     * Method to fetch the Type of generic type in this class
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    private Class<E> getGenericTypeClass() {
        return entityType;
    }
}
