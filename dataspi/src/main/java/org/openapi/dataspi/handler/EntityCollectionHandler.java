package org.openapi.dataspi.handler;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.ResultSetHandler;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.SQLDialect;
import org.openapi.dataspi.handler.meta.EntityMetaData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Converts data fetched from database into a java collection in a faster manner than older version.
 * @author Miguel Angel Vega P.
 * @version $Id: EntityCollectionHandler.java 0, 2017-sep-15 10:24 AM miguelvega $
 */
public class EntityCollectionHandler<E> implements ResultSetHandler<List<E>> {

    final SQLDialect sqlDialect;

    /**
     * maybe will dismiss this by using something like: {@link EntityMetaData#getGenericTypeClass()} method
     */
    @Deprecated
    final Class<E> entityType;

    /**
     * Determines if the query will use the column label or column name, this differs from RDBMS to another. By default is false.
     */
    boolean useColumnLabel;

    public EntityCollectionHandler(Class<E> entityType) {
        this(entityType, new DefaultSQLDialect());
    }

    public EntityCollectionHandler(Class<E> entityType, SQLDialect sqlDialect) {
        this.entityType = entityType;
        this.sqlDialect = sqlDialect;
    }

    public EntityCollectionHandler<E> setUseColumnLabel(boolean useColumnLabel) {
        this.useColumnLabel = useColumnLabel;
        return this;
    }

    /**
     * JDK 8 above must use this implemenation instead of sending the Type explicitly
     *
     * @return
     */
    private Class<E> getType() {
        return entityType;
    }

    @Override
    public List<E> handle(ResultSet rs) throws SQLException {
        final List<E> data = Lists.newLinkedList();

        final SQL2Java<E> sql2Java = new SQL2Java(getType());

        EntityMetaData<E> metaData = sql2Java.getEntityMetaData();

        while (rs.next()) {
            data.add(sql2Java.readEntity(rs, metaData, sqlDialect, useColumnLabel));
        }

        return data;
    }
}
