/**
 * This package contains an API for allowing make queries using beans instead of simply '?' tokens in SQL statements
 * @author Miguel A. Vega P.
 * @source $URL$
 * @version $Id: package-info.java; Apr 23, 2016. 6:06 PM mvega $
 */
package org.openapi.dataspi.handler;