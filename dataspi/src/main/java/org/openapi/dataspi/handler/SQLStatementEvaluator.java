package org.openapi.dataspi.handler;

import org.openapi.lang.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * @author Miguel A. Vega P.
 * @version $Id: SQLStatementEvaluator.java; Apr 23, 2016. 4:21 PM mvega $
 * @source $URL$
 */
class SQLStatementEvaluator<U> {

    final Logger logger = LoggerFactory.getLogger(getClass());

    protected final String sqlStatement;

    protected final U u;

    SQLStatementEvaluator(String sqlStatement, U u) {
        assert sqlStatement != null;

        this.sqlStatement = sqlStatement;
        this.u = u;
    }

    protected <V> String evaluateStatement(String schema, final List data){

        String finalStatement = sqlStatement;

        //Before threat all the complexQuery replace the ${ds_schema}
        if (!isNullOrEmpty(schema)) {
            finalStatement = finalStatement.replace("${schema}", schema);
        }

        if (u != null) {
            SQLExpressionTransformer el = SQLExpressionTransformer.buildStaticTransformer();
            //simple access
            finalStatement = el.transform(finalStatement, u);
            el = SQLExpressionTransformer.buildDynamicTransformer();
            finalStatement = el.evaluateFixedParameter(finalStatement, u, "?", new Visitor() {

                public void visit(Object obj) {
                    data.add(obj);
                }
            });
            //in the case that query is being used as the common DBUtils String and parameters, this will solve such that problem
            if (data.size() == 0 && u != null) {
                if (u.getClass().isArray()) {
                    data.addAll(Arrays.asList((V[]) u));
                } else {
                    data.add(u);
                }
            }
        }

        /*StringMerger finalQuery = StringMerger.of(finalStatement);

        if (filter != null) {

            Filter whereclause = Filter.whereClause(filter);

            org.openlogics.gears.jdbc.builder.Criteria criteria = whereclause.evaluate();
            data.addAll(criteria.getQueryValues());

            finalQuery.add(criteria.getQueryString());
        }

        if (groupBy != null) {
            finalQuery.add(groupBy.toSQLString());
        }
        if (orderBy != null) {
            finalQuery.add(orderBy.toSQLString());
        }
        //offset and limit are deprecated, but to keep compatibility, we'll persist in using them
        //if and only if limitOffset clause is missing
        if (limitOffset == null) {
                    *//*
                    if (offset != null) {
                        finalQuery.add(offset.toSQLString());
                    }
                    if (limit != null) {
                        finalQuery.add(limit.toSQLString());
                    }
                    *//*
        } else {
            finalQuery.add(limitOffset.toSQLString());
        }

        String queryString = finalQuery.build();

        */
        logger.debug("The final query string built is: {}", finalStatement);

        return finalStatement;
    }
}
