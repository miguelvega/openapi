package org.openapi.dataspi.handler.meta;

import org.openapi.dataspi.SQLDialect;

import javax.persistence.TemporalType;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: TemporalProcessor.java 0, 2017-sep-15 10:55 AM miguelvega $
 */
public class TemporalProcessor extends AbstractProcessor {

    final TemporalType temporalType;

    protected TemporalProcessor(Field field, String resultSetColumnName, TemporalType temporalType) {
        super(field, resultSetColumnName);
        this.temporalType = temporalType;
    }

    @Override
    public void assign(ResultSet resultSet, Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException {

        Object binding = getBindingValue(resultSet);

        final Calendar calendar = Calendar.getInstance();
        long dateInMillis = 0;

        try {
            switch (temporalType) {
                case DATE:
                    dateInMillis = sqlDialect.getDateInMillis(binding);
                    break;
                case TIME:
                    dateInMillis = sqlDialect.getTimeInMillis(binding);
                    break;
                case TIMESTAMP:
                    dateInMillis = sqlDialect.getTimeStampInMillis(binding);
                    break;
            }
        }catch(SQLException sqlX){
            logger.error("Unable to fetch teporal data from result set. Error ocurred while attempting to write result to "+
                    field.getDeclaringClass().getName()+"#"+field.getName(), sqlX);
            return;
        }

        calendar.setTimeInMillis(dateInMillis);
        if (Date.class.isAssignableFrom(field.getType()))
//                        setter.invoke(obj, calendar.getTime());
            field.set(assignable, calendar.getTime());
        else if (Calendar.class.isAssignableFrom(field.getType()))
//                        setter.invoke(obj, calendar);
            field.set(assignable, calendar);
        else
            logger.warn("@Temporal annotation only can be applied to java.util.Date or java.util.Calendar objects. Fied [{}] maped to column [{}#{}] can not be initialized.",
                    field.getType(), field.getDeclaringClass().getName(), resultSetColumnName);
    }
}
