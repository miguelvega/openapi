package org.openapi.dataspi.handler.meta;

import com.google.common.primitives.Primitives;
import org.openapi.dataspi.SQLDialect;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: SimpleProcessor.java 0, 2017-sep-15 10:46 AM miguelvega $
 */
public class SimpleProcessor extends AbstractProcessor{

    protected SimpleProcessor(Field field, String resultSetColumnName) {
        super(field, resultSetColumnName);
    }

    @Override
    public void assign(ResultSet resultSet, Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException {

        Object binding = getBindingValue(resultSet);

        try {
            field.set(assignable, binding);
        } catch (IllegalArgumentException x) {
            if (Number.class.isAssignableFrom(field.getType().getClass()) || Number.class.isAssignableFrom(Primitives.wrap(field.getType()))) {
                Number bestNumber = getBestNumber((Number) binding, field.getType());
                field.set(assignable, bestNumber);
                return;
            }
            throw new IllegalAccessException("Unable to assigna value to a type. Cause: "+x.getMessage());
        }
    }

    private Number getBestNumber(Number givenValue, Class<?> requiredType) {

        if (givenValue == null || (givenValue != null && org.openapi.lang.Types.isSameAs(requiredType, givenValue.getClass()))) {
            return givenValue;
        }

        Class<?> wrap = Primitives.wrap(requiredType);

        if (BigDecimal.class.isAssignableFrom(requiredType) || BigDecimal.class.isAssignableFrom(wrap))
            return new BigDecimal(givenValue.toString());
        else if (Byte.class.isAssignableFrom(requiredType) || Byte.class.isAssignableFrom(wrap))
            return givenValue.byteValue();
        else if (Long.class.isAssignableFrom(requiredType) || Long.class.isAssignableFrom(wrap))
            return givenValue.longValue();
        else if (Integer.class.isAssignableFrom(requiredType) || Integer.class.isAssignableFrom(wrap))
            return givenValue.intValue();
        else if (Float.class.isAssignableFrom(requiredType) || Float.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.floatValue();
        else if (Double.class.isAssignableFrom(requiredType) || Double.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.doubleValue();
        else if (Short.class.isAssignableFrom(requiredType) || Byte.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return givenValue.shortValue();
        else if (BigInteger.class.isAssignableFrom(requiredType) || BigInteger.class.isAssignableFrom(Primitives.wrap(requiredType)))
            return new BigInteger(givenValue.toString());

        return givenValue;
    }
}
