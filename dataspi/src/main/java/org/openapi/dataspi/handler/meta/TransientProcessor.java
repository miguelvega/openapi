package org.openapi.dataspi.handler.meta;

import org.openapi.dataspi.SQLDialect;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: TransientProcessor.java 0, 2017-sep-15 10:42 AM miguelvega $
 * @deprecated evaluate how useful is this processor, TODO
 */
@Deprecated
public class TransientProcessor extends AbstractProcessor {

    public TransientProcessor(Field field) {
        super(field, null);
    }

    @Override
    public void assign(ResultSet resultSet, Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException {
        //do nothing
    }
}
