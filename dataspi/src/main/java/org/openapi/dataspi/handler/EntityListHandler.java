package org.openapi.dataspi.handler;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.ResultSetHandler;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.SQLDialect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * This class reads entire metadata and based on such that metadata, updates field data using reflection.
 * @author Miguel A. Vega P.
 * @version $Id: EntityListHandler.java; May 07, 2016. 9:30 PM mvega $
 * @source $URL$
 * @deprecated Need another class to extract first the metadata an then execuite all jobs in one single parse. Replaced by {@link EntityCollectionHandler} class
 */
@Deprecated
public class EntityListHandler<Y> implements ResultSetHandler<List<Y>> {

    final SQLDialect sqlDialect;

    final Class<Y> entityType;

    /**
     * Determines if the query will use the column label or column name, this differs from RDBMS to another. By default is false.
     */
    boolean useColumnLabel;

    public EntityListHandler(Class<Y> entityType) {
        this(entityType, new DefaultSQLDialect());
    }

    public EntityListHandler(Class<Y> entityType, SQLDialect sqlDialect) {
        this.entityType = entityType;
        this.sqlDialect = sqlDialect;
    }


    @Override
    public List<Y> handle(ResultSet rs) throws SQLException {
        final List<Y> data = Lists.newLinkedList();

        final EntityReader<Y> reader = new EntityReader(getType());

        while (rs.next()) {
            data.add(reader.readEntity(rs, sqlDialect, useColumnLabel));
        }

        return data;
    }

    public EntityListHandler<Y> setUseColumnLabel(boolean useColumnLabel) {
        this.useColumnLabel = useColumnLabel;
        return this;
    }

    /**
     * JDK 8 above must use this implemenation instead of sending the Type explicitly
     *
     * @return
     */
    private Class<Y> getType() {
        return entityType;
        /*
        Type sooper = getClass().getGenericSuperclass();
        Type t = ((ParameterizedType)sooper).getActualTypeArguments()[ 0 ];
        try {
            return (Class<Y>) Class.forName(t.toString());
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("A several error ocurred.");
        }
        */
    }
}
