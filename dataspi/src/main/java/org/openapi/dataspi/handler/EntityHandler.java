package org.openapi.dataspi.handler;

import org.apache.commons.dbutils.ResultSetHandler;
import org.openapi.dataspi.DefaultSQLDialect;
import org.openapi.dataspi.SQLDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class is an implementation of result set handler, but this is limited to fetch only one result when handle method is called.
 * @author Miguel A. Vega P.
 * @version $Id: EntityHandler.java; May 07, 2016. 1:49 PM mvega $
 * @source $URL$
 */
public class EntityHandler<T> implements ResultSetHandler<T>{

    static Logger logger = LoggerFactory.getLogger(JPAEntityHandler.class);

    private final Class<T> t;
    final SQLDialect sqlDialect;
    /**
     * Determines if the query will use the column label or column name, this differs from RDBMS to another. By default is false.
     */
    boolean useColumnLabel = false;


    public EntityHandler(Class<T> t) {
        this(t, new DefaultSQLDialect());
    }

    /**
     *
     * @param t
     * @param sqlDialect
     * @throws IllegalArgumentException if the given type is not a valid {@link Entity} object.
     */
    public EntityHandler(Class<T> t, SQLDialect sqlDialect) {

        assert t != null;
        assert sqlDialect != null;

        validateEntityType(t);

        this.t = t;
        this.sqlDialect = sqlDialect;
    }

    private void validateEntityType(Class<T> t) throws IllegalArgumentException {
        if(t.getAnnotation(Entity.class)==null || t.getAnnotation(Table.class)==null)
            throw new IllegalArgumentException("The given type ["+t.getName()+"] is not a valid @Entity");
    }

    /**
     *
     * @param rs
     * @return
     * @throws SQLException if any error occurs while making operations
     * @throws InstantiationError if the object to map can not be instantiated
     */
    @Override
    public T handle(ResultSet rs) throws SQLException, InstantiationError {

        if(rs.next()){
            return new EntityReader<T>(t).readEntity(rs, sqlDialect, useColumnLabel);
        }

        return null;
    }

    public EntityHandler<T> setUseColumnLabel(boolean useColumnLabel) {
        this.useColumnLabel = useColumnLabel;
        return this;
    }
}
