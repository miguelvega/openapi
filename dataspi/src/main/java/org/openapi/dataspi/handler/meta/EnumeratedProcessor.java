package org.openapi.dataspi.handler.meta;

import org.openapi.dataspi.SQLDialect;

import javax.persistence.EnumType;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;

/**
 * @author Miguel Angel Vega P.
 * @version $Id: EnumeratedProcessor.java 0, 2017-sep-15 10:55 AM miguelvega $
 */
public class EnumeratedProcessor extends AbstractProcessor{

    EnumType enumType;

    protected EnumeratedProcessor(Field field, String columnName, EnumType enumType) {
        super(field, columnName);
        this.enumType = enumType;
    }

    @Override
    public void assign(ResultSet resultSet, Object assignable, SQLDialect sqlDialect) throws IllegalAccessException, SQLException {

        Object binding = getBindingValue(resultSet);

        Class<Enum> etype = (Class<Enum>) field.getType();

        switch (enumType){
            case STRING:
                Enum anEnum = Enum.valueOf(etype, binding.toString());
                field.set(assignable, anEnum);
                break;
            default:
                int databaseOrdinalValue = Integer.parseInt(String.valueOf(binding));
                EnumSet enums = EnumSet.allOf(etype);

                int i = 0;

                for (Object anEnumObj : enums) {
                    if (i++ == databaseOrdinalValue) {
                        field.set(assignable, anEnumObj);
                        break;
                    }
                }
        }
    }
}
