package org.openapi.dataspi;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Simple SQLDialect MAX_RESULTS and FIRST_RESULT are not implemented.
 * @author Miguel A. Vega P.
 * @version $Id: DefaultSQLDialect.java; May 07, 2016. 8:38 PM mvega $
 * @source $URL$
 */
public class DefaultSQLDialect implements SQLDialect {
    /**
     *
     * @param sqlStatement
     * @param maxResults
     * @return
     */
    @Override
    public String forMaxResults(String sqlStatement, int maxResults) {
        throw new UnsupportedOperationException("This feature must be implemnted by specific RDBMS dialect");
//        return sqlStatement;
    }

    @Override
    public String forFirstResult(String sqlStatement, int firstResult) {
        throw new UnsupportedOperationException("This feature must be implemnted by specific RDBMS dialect");
//        return sqlStatement;
    }

    @Override
    public long getDateInMillis(Object value) throws SQLException {
        return ((Date)value).getTime();
    }

    @Override
    public long getTimeInMillis(Object value) throws SQLException {
        return ((Time)value).getTime();
    }

    @Override
    public long getTimeStampInMillis(Object value) throws SQLException {
        return ((Timestamp)value).getTime();
    }

    @Override
    public Date getSQLDate(Calendar date) throws IllegalArgumentException {
        return new Date(date.getTimeInMillis());
    }

    @Override
    public Date getSQLDate(java.util.Date date) throws IllegalArgumentException {
        return new Date(date.getTime());
    }

    @Override
    public Time getSQLTime(Calendar time) {
        return new Time(time.getTimeInMillis());
    }

    @Override
    public Time getSQLTime(java.util.Date time) {
        return new Time(time.getTime());
    }

    @Override
    public Timestamp getSQLTimeStamp(Calendar timeStamp) {
        return new Timestamp(timeStamp.getTimeInMillis());
    }

    @Override
    public Timestamp getSQLTimeStamp(java.util.Date timeStamp) {
        return new Timestamp(timeStamp.getTime());
    }
}
