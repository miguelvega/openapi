package org.openapi.dataspi;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Visitor.java; Apr 23, 2016. 5:49 PM mvega $
 * @source $URL$
 */
public interface Visitor<T, E extends Throwable> {
    void visit(T t) throws E;
}
