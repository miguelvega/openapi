package org.openapi.lang;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Miguel Vega
 * @version $Id: Reflections.java 0, 2013-12-10 3:48 PM mvega $
 */
public class Reflections {
    static final Logger LOGGER = LoggerFactory.getLogger(Reflections.class);

    static final String IS_PREFIX = "is";
    static final String GET_PREFIX = "get";
    static final String SET_PREFIX = "set";

    /**
     * find the annotation defined in a target class with a meta annotation associated with the resulting annotation
     *
     * for example:
     *
     * @AnntationA is annotated with @AnntationB, and ObjectA is annotation with @AnnotationA,
     * findAnnotation(ObjectA.class, AnnotationB.class) returns AnnotationA.class
     *
     * @param targetObjectClass
     * @param metaAnnotationClass
     * @return
     */
    public static Class<? extends Annotation> findAnnotation(
            Class<?> targetObjectClass,
            Class<? extends Annotation> metaAnnotationClass) {

        //return null if the parameters are not valid
        if(targetObjectClass == null || metaAnnotationClass == null){
            return null;
        }

        //the resulting annotation class
        Class<? extends Annotation> resultingAnnotation = null;

        // get all the annotations defined in the class level for
        // the managed bean objects
        Annotation[] annotations = targetObjectClass.getAnnotations();

        // check all the available annotations
        for (Annotation annotation : annotations) {
            // get the annotation's annotation to check whether it contains
            // the @MetaAnnotation annotation to define the annotation
            Annotation metaAnnotationInstance = annotation.annotationType().getAnnotation(metaAnnotationClass);

            if(metaAnnotationInstance != null){
                resultingAnnotation = annotation.annotationType();
            }
        }

        //return the resuling annotation
        return resultingAnnotation;
    }

    /**
     * This method retrieves all declared fields including those which belong to
     * the super classes.
     *
     * @param type
     * @return
     */
    public static List<Field> getDeclaredFields(Class<?> type) {
        List<Field> fields = Lists.newArrayList();
        for (Class<?> c = type; c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    /**
     *
     * @param provider
     * @param field
     * @param <T>
     * @return
     * @throws ReflectionException
     */
    public static <T> T fetch(Object provider, Field field) throws ReflectionException {
        AtomicBoolean accesible = new AtomicBoolean(field.isAccessible());
        try {
            field.setAccessible(true);
            return (T) field.get(provider);
        } catch (IllegalAccessException e) {
            throw new ReflectionException(e);
        }finally {
            field.setAccessible(accesible.get());
        }
    }

    /**
     *
     * @param assignable
     * @param value
     * @param field
     * @param <T>
     * @throws ReflectionException
     */
    public static <T> void assign(Object assignable, T value, Field field) throws ReflectionException{
        AtomicBoolean accesible = new AtomicBoolean(field.isAccessible());
        try {
            field.setAccessible(true);
            field.set(assignable, value);
        } catch (IllegalAccessException e) {
            throw new ReflectionException(e);
        }finally {
            field.setAccessible(accesible.get());
        }
    }

    /**
     * Retrieves all the Property descriptors of a type
     *
     * @param type
     * @return
     */
    public static Iterable<PropertyDescriptor> inspectType(final Class<?> type) {
        return new Iterable<PropertyDescriptor>() {

            public Iterator<PropertyDescriptor> iterator() {
                try {
                    return Arrays.<PropertyDescriptor>asList(Introspector.getBeanInfo(type).getPropertyDescriptors()).iterator();
                } catch (IntrospectionException e) {
                    //throw new IllegalStateException(of("Unable to inspect given type: '").add(type).add("'").build(), e);
                    LOGGER.debug(StringMerger.of("Unable to inspect given type: '").app(type.getName()).app("'").merge(), e);
                    return Collections.<PropertyDescriptor>emptyList().iterator();
                }
            }
        };
    }

    /**
     *
     * @param field field
     * @return
     * @throws IntrospectionException
     */
    public static Method getGetterMethod(Field field) throws IntrospectionException {
        //return new PropertyDescriptor(field.getName(), field.getDeclaringClass()).getReadMethod();
        return getGetterMethod(field.getName(), field.getDeclaringClass());
    }

    /**
     * returns the GET or IS prefixed method for given field name. If  method does not exist, returns null;
     * @param fieldName
     * @param ownerType
     * @return
     * @throws IntrospectionException
     */
    public static Method getGetterMethod(String fieldName, Class ownerType) throws IntrospectionException {
//        return new PropertyDescriptor(fieldName, ownerType).getReadMethod();
        try {
            return ownerType.getDeclaredMethod(GET_PREFIX+NameGenerator.capitalize(fieldName));
        } catch (NoSuchMethodException e) {
            try {
                return ownerType.getDeclaredMethod(IS_PREFIX+NameGenerator.capitalize(fieldName));
            } catch (NoSuchMethodException e1) {
                throw new IntrospectionException(e.getMessage());
            }
        }
    }

    /**
     * A method to build a possible set method name
     * @param name the name to be used for getter
     * @param type the type to be used for getter
     * @return
     */
    public static String getSetterMethodName(String name, Class type) {
        //return getGetterMethod(fname, ftype).getName();

        assert name!=null && type!=null;

        return SET_PREFIX + NameGenerator.capitalize(name);
    }

    /**
     * Retrieves the field setter method, if and only if write methdod is public and is void type.
     * {@link PropertyDescriptor} class validates this as follows: if (!writeMethod.getReturnType().equals(void.class)) {
     * @param field
     * @return
     * @throws IntrospectionException
     */
    public static Method getSetterMethod(Field field) throws IntrospectionException {
        //return new PropertyDescriptor(field.getName(), field.getDeclaringClass()).getWriteMethod();
        return getGetterMethod(field.getName(), field.getDeclaringClass());
    }

    /**
     * Retrieves the field setter method, if and only if write methdod is public and is void type.
     * {@link PropertyDescriptor} class validates this as follows: if (!writeMethod.getReturnType().equals(void.class)) {
     * @param fname field name
     * @param ownerType field declaring class
     * @return
     * @throws IntrospectionException
     */
    public static Method getSetterMethod(String fname, Class ownerType) throws IntrospectionException {
        //return new PropertyDescriptor(fname, ownerType).getWriteMethod();
        try {
            return ownerType.getMethod(SET_PREFIX+NameGenerator.capitalize(fname));
        } catch (NoSuchMethodException e) {
            throw new IntrospectionException(e.getMessage());
        }
    }

    /**
     * A method to build a possible get/is  method name, up to the given type
     * @param name
     * @param type
     * @return
     */
    public static String getGetterMethodName(String name, Class type) {
        //return getSetterMethod(fname, ftype).getName();

        assert name!=null && type!=null;

        if (type == boolean.class || type == null) {
            return IS_PREFIX + NameGenerator.capitalize(name);
        } else {
            return GET_PREFIX + NameGenerator.capitalize(name);
        }
    }

    /**
     * @param fname
     * @param ftype
     * @return
     * @deprecated use by {@link Reflections#getGetterMethodName}
     */
    @Deprecated
    public static String getGetterName(String fname, Class ftype) {
        String getterName = ftype.isAssignableFrom(Boolean.class) || ftype.isAssignableFrom(boolean.class) ? "is" : "get";
        getterName += fname.substring(0, 1).toUpperCase();
        getterName += fname.substring(1, fname.length());
        return getterName;
    }

    /**
     * @param fieldName
     * @return
     * @deprecated replaced by {@link Reflections#getSetterMethodName}
     */
    @Deprecated
    public static String getSetterName(String fieldName) {
        String setterName = "set";
        setterName += fieldName.substring(0, 1).toUpperCase();
        setterName += fieldName.substring(1, fieldName.length());
        return setterName;
    }
}
