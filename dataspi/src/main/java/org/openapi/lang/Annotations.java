package org.openapi.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.IntrospectionException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Annotations.java; Sep 15, 2017 2:28 AM miguel.vega $
 */
public class Annotations {

    final static Logger logger = LoggerFactory.getLogger(Annotations.class);

    /**
     * Fetches the annotated value which corresponds from given field or read method
     * @param field the field used to determine the source of data
     * @return the annotation fetched from field or getter method. If annotation is not found, then returns null.
     */
    public static <A extends Annotation> A getAnnotatedValueFromFieldOrReadMethod(Field field, Class<A> annot){
        //first try with field itself
        if(field.getAnnotation(annot)!=null){
            return field.getAnnotation(annot);
        }

        //try with read method
        try {
            Method readMethod = Reflections.getGetterMethod(field);
            if(readMethod.getAnnotation(annot)!=null)
                return readMethod.getAnnotation(annot);
        } catch (IntrospectionException e) {
            logger.warn("Unable to fetch read method of field: {}#{}", field.getDeclaringClass(), field.getName());
        }

        return null;
    }
}