package org.openapi.lang;

/**
 * A MESSAGE has left being correctly structured when this exception occurs
 * @author Miguel Vega
 * @version $Id: IntegrityDataException.java 0, 8/22/14 2:54 PM, @miguel $
 */
public class TooManyAttemptsException extends Throwable {
    public TooManyAttemptsException(String s, Throwable e) {
        super(s, e);
    }
}
