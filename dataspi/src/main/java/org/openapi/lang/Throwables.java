package org.openapi.lang;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Throwables.java; May 10, 2016. 4:45 PM mvega $
 * @source $URL$
 */
public class Throwables {
public static String getFullStackTrace(Throwable throwable){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(out);

        Throwable t = throwable;
        do {
            t.printStackTrace(stream);
            stream.println();
            t = t.getCause();
        } while (t != null);

        IOUtils.closeQuietly(stream);

        try {
            return out.toString("ISO-8859-1");
        } catch (UnsupportedEncodingException e1) {
            return out.toString();
        }
    }
}