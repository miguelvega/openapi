package org.openapi.lang;

/**
 * @author Miguel A. Vega P.
 * @version $Id: ReflectionException.java; Sep 17, 2017 9:47 PM miguel.vega $
 */
public class ReflectionException extends RuntimeException{
    public ReflectionException() {
        super();
    }

    public ReflectionException(String message) {
        super(message);
    }

    public ReflectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReflectionException(Throwable cause) {
        super(cause);
    }

    protected ReflectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
