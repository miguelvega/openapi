package org.openapi.lang;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @author Miguel A. Vega P.
 * @version $Id: StringMerger.java; Apr 23, 2016. 4:41 PM mvega $
 * @source $URL$
 */
public class StringMerger {

    final List<String> parts;

    final String separator;

    Style style = Style.NONE;

    public StringMerger removeLastToken(String s) {
        for (int i = parts.size()-1; i >= 0 ; i--) {
            if(s.equals(parts.get(i))) {
                parts.remove(i);
                break;
            }
        }
        return this;
    }

    public StringMerger removeFirstToken(String s) {
        for (int i = 0; i < parts.size() ; i++) {
            if(s.equals(parts.get(i))) {
                parts.remove(i);
                break;
            }
        }
        return this;
    }

    public enum Style {
        NONE, CAPITALIZED, UPPERED, LOWERED;
    }

    StringMerger(Object... strings){
        this(null, strings);
    }

    StringMerger(String separator, Object... strings){

        this.separator = Optional.fromNullable(separator).or("");

        parts = Lists.newArrayListWithCapacity(strings.length);

        parts.addAll(Lists.newArrayList(Iterables.transform(Lists.newArrayList(strings), new Function<Object, String>() {
            @Nullable
            @Override
            public String apply(@Nullable Object input) {
                return String.valueOf(input);
            }
        })));

//        Collections.addAll(parts, strings);
    }

    public static StringMerger of(Object... srings){
        return new StringMerger(srings);
    }

    public static StringMerger withSeparator(String separator, String... strings){
        return new StringMerger(separator, strings);
    }

    public StringMerger withStyle(Style style){
        this.style = style;
        return this;
    }

    public StringMerger app(Object s){
        return this.app(String.valueOf(s));
    }

    public StringMerger app(String s){
        parts.add(s);
        return this;
    }

    public StringMerger prep(Object s){
        return this.prep(String.valueOf(s));
    }

    public StringMerger prep(String s){
        parts.add(0, s);
        return this;
    }

    public String merge(){
        return Joiner.on(separator).join(parts);
    }
}
