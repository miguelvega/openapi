package org.openapi.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class allows to perform executions avoiding some expected error. WIll try to perform a routine by a certain
 * number of times, if this assign doesn't complete successfully, an exception of type {@link TooManyAttemptsException}
 * is thrown.
 *
 * @author Miguel Vega
 * @version $Id: ContingencyAttempt.java 0, 8/22/14 9:24 PM, @miguel $
 */
public class AttemptExecutor<T, U extends Throwable> {

    final Logger LOGGER;

    final int attemptCount;

    final TimeUnit timeUnit;

    final long timeUntilNextAttempt;

    final Class<U> expectedErrorType;

    /**
     * Uses 10 seconds between each retry
     *
     * @param attemptCount max retries
     * @param <X>
     * @param <Y>
     * @return
     */
    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int attemptCount) {
        return new AttemptExecutor<X, Y>(attemptCount, TimeUnit.SECONDS, 10, null);
    }

    /**
     * Sets 10 retries by default, and the time unit used is in milliseconds
     *
     * @param timeUntilNextAttempt milliseconds between each retry
     * @param <X>
     * @param <Y>
     * @return
     */
    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(long timeUntilNextAttempt) {
        return new AttemptExecutor<X, Y>(10, TimeUnit.MILLISECONDS, timeUntilNextAttempt, null);
    }

    /**
     * Uses MILLISECONDS as time unit between retrie
     *
     * @param attemptCount         maximum retries before throwing {@link TooManyAttemptsException}
     * @param timeUntilNextAttempt time in milliseconds between each retry
     * @param <X>
     * @param <Y>
     * @return
     */
    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int attemptCount, long timeUntilNextAttempt) {
        return new AttemptExecutor<X, Y>(attemptCount, TimeUnit.MILLISECONDS, timeUntilNextAttempt, null);
    }

    /**
     * Uses 10 retries by default uses seconds as time unit
     *
     * @param expectedErrorType
     * @param <X>
     * @param <Y>
     * @return
     */

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            long timeUntilNextAttempt, Class<Y> expectedErrorType) {
        return new AttemptExecutor<X, Y>(10, TimeUnit.SECONDS, timeUntilNextAttempt, expectedErrorType);
    }

    /**
     * Uses 10 seconds by default until next retry and 10 retries
     *
     * @param expectedErrorType
     * @param <X>
     * @param <Y>
     * @return
     */
    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(Class<Y> expectedErrorType) {
        return new AttemptExecutor<X, Y>(10, TimeUnit.SECONDS, 10, expectedErrorType);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int attemptCount,
            TimeUnit timeUnit,
            int timeUntilNextAttempt) {
        return createExecutor(attemptCount, timeUnit, timeUntilNextAttempt, null);
    }

    /**
     * Craetes a new instance of executr
     *
     * @param attemptCount
     * @param timeUnit
     * @param timeUntilNextAttempt
     * @param expectedErrorType
     * @param <X>                  the object to return on success
     * @param <Y>                  the expected error type
     * @return
     */
    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int attemptCount,
            TimeUnit timeUnit,
            int timeUntilNextAttempt,
            Class<Y> expectedErrorType) {
        return new AttemptExecutor<X, Y>(attemptCount, timeUnit, timeUntilNextAttempt, expectedErrorType);
    }

    /**
     * @param attemptCount         number of retries before it throws an {@link TooManyAttemptsException}
     * @param timeUnit             the time unit used for creating lapse of times
     * @param timeUntilNextAttempt period of time in milliseconds to wait before next retry
     * @param expectedErrorType    if a possible exception can occur while excecuting a assign,
     *                             that might be declared here and the {@link AttemptExecutor#execute(Attempt)}
     *                             method will throw it if occurs
     */
    AttemptExecutor(int attemptCount, TimeUnit timeUnit, long timeUntilNextAttempt, Class<U> expectedErrorType) {
        this.attemptCount = attemptCount;
        this.LOGGER = LoggerFactory.getLogger(getClass());
        this.timeUnit = timeUnit;
        this.timeUntilNextAttempt = timeUntilNextAttempt;
        this.expectedErrorType = expectedErrorType;
    }

    public T execute(final Attempt<T> attempt) throws TooManyAttemptsException, U {
        return this.execute(attempt, "Unable to perform [" + attempt.getAttemptDescription() + "]. An unrecoverable state has been reached, contact immediately with administrator.");
    }

    /**
     * If the 'expectedErrorType' is catched, then will unleash this until the caller. If any other exception is thrown willl be ignored and retry will be
     * executed until maximum retries is reached.
     *
     * @param attempt
     * @param messageOnFailure
     * @return
     * @throws TooManyAttemptsException
     * @throws U
     */
    public T execute(final Attempt<T> attempt, final String messageOnFailure) throws TooManyAttemptsException, U {

        final AtomicInteger count = new AtomicInteger(0);

        Throwable caught = null;

        do {
            try {

                count.set(count.incrementAndGet());

                T call = attempt.doTry();

                LOGGER.info("Attempt [{}] has been completed successfully.", attempt.getAttemptDescription());

                return call;

            } catch (Throwable throwable) {

                if (expectedErrorType != null &&
                        expectedErrorType.isAssignableFrom(throwable.getClass())) {
                    throw (U) throwable;
                }

                LOGGER.warn("Attempt executor has failed while executing: {}. Next retry will be in {} {}. Error is the following:\n[{}]",
                        new Object[]{attempt.getAttemptDescription(), timeUntilNextAttempt, timeUnit, Throwables.getFullStackTrace(throwable)});

                if (count.get() == attemptCount) {
                    break;
                }

                caught = throwable;

                try {
                    timeUnit.sleep(timeUntilNextAttempt);
                } catch (InterruptedException e) {
                }
            }
        } while (true);

        throw new TooManyAttemptsException(messageOnFailure, caught);
    }
}