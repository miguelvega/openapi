package org.openapi.lang;

/**
 * A visitor implementation will be to catch anything on any event happening
 * @author Miguel A. Vega P.
 * @version $Id: Visitor.java; Apr 23, 2016. 5:49 PM mvega $
 * @source $URL$
 */
public interface Visitor<T, E extends Throwable> {
    /**
     * A simple object to notify about anything
     * @param t
     * @throws E
     */
    void visit(T t) throws E;
}