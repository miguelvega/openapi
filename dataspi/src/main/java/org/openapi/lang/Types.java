package org.openapi.lang;

/**
 * @author Miguel A. Vega P.
 * @version $Id: Types.java; May 10, 2016. 6:04 PM mvega $
 * @source $URL$
 */
public class Types {
    public static boolean isInstanceOf(Class clazz, Object obj){
        if(obj == null)return false;
        return clazz.isAssignableFrom(obj.getClass());
    }

    public static boolean isSameAs(Class classA, Class classB){
        if(classA == null || classB == null)return false;
        return classA.equals(classB);
    }
}