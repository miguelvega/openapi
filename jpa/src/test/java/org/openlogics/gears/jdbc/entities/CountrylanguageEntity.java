package org.openlogics.gears.jdbc.entities;

import javax.persistence.*;

/**
 * @author Miguel Vega
 * @version $Id: CountrylanguageEntity.java 0, December 30, 2014 11:23 PM mvega $
 */
@Entity
@Table(name = "countrylanguage", schema = "public", catalog = "world")
@IdClass(CountrylanguageEntityPK.class)
public class CountrylanguageEntity {
    private String countrycode;
    private String language;
    private boolean isofficial;
    private float percentage;

    @Id
    @Column(name = "countrycode", nullable = false, insertable = true, updatable = true, length = 3)
    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    @Id
    @Column(name = "language", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "isofficial", nullable = false, insertable = true, updatable = true)
    public boolean isIsofficial() {
        return isofficial;
    }

    public void setIsofficial(boolean isofficial) {
        this.isofficial = isofficial;
    }

    @Basic
    @Column(name = "percentage", nullable = false, insertable = true, updatable = true, precision = 8)
    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountrylanguageEntity that = (CountrylanguageEntity) o;

        if (isofficial != that.isofficial) return false;
        if (Float.compare(that.percentage, percentage) != 0) return false;
        if (countrycode != null ? !countrycode.equals(that.countrycode) : that.countrycode != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = countrycode != null ? countrycode.hashCode() : 0;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (isofficial ? 1 : 0);
        result = 31 * result + (percentage != +0.0f ? Float.floatToIntBits(percentage) : 0);
        return result;
    }
}
