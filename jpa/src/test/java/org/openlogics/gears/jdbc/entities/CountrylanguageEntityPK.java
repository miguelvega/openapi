package org.openlogics.gears.jdbc.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author Miguel Vega
 * @version $Id: CountrylanguageEntityPK.java 0, December 30, 2014 11:23 PM mvega $
 */
public class CountrylanguageEntityPK implements Serializable {
    private String countrycode;
    private String language;

    @Column(name = "countrycode", nullable = false, insertable = true, updatable = true, length = 3)
    @Id
    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    @Column(name = "language", nullable = false, insertable = true, updatable = true, length = 2147483647)
    @Id
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountrylanguageEntityPK that = (CountrylanguageEntityPK) o;

        if (countrycode != null ? !countrycode.equals(that.countrycode) : that.countrycode != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = countrycode != null ? countrycode.hashCode() : 0;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }
}
