package org.openlogics.gears.jdbc.entities;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Miguel Vega
 * @version $Id: CountryEntity.java 0, December 30, 2014 11:23 PM mvega $
 */
@Entity
@Table(name = "country", schema = "public", catalog = "world")
public class CountryEntity {
    private String code;
    private String name;
    private String continent;
    private String region;
    private float surfacearea;
    private Short indepyear;
    private int population;
    private Float lifeexpectancy;
    private BigDecimal gnp;
    private BigDecimal gnpold;
    private String localname;
    private String governmentform;
    private String headofstate;
    private String code2;

    @Id
    @Column(name = "code", nullable = false, insertable = true, updatable = true, length = 3)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "continent", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    @Basic
    @Column(name = "region", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Basic
    @Column(name = "surfacearea", nullable = false, insertable = true, updatable = true, precision = 8)
    public float getSurfacearea() {
        return surfacearea;
    }

    public void setSurfacearea(float surfacearea) {
        this.surfacearea = surfacearea;
    }

    @Basic
    @Column(name = "indepyear", nullable = true, insertable = true, updatable = true)
    public Short getIndepyear() {
        return indepyear;
    }

    public void setIndepyear(Short indepyear) {
        this.indepyear = indepyear;
    }

    @Basic
    @Column(name = "population", nullable = false, insertable = true, updatable = true)
    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Basic
    @Column(name = "lifeexpectancy", nullable = true, insertable = true, updatable = true, precision = 8)
    public Float getLifeexpectancy() {
        return lifeexpectancy;
    }

    public void setLifeexpectancy(Float lifeexpectancy) {
        this.lifeexpectancy = lifeexpectancy;
    }

    @Basic
    @Column(name = "gnp", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getGnp() {
        return gnp;
    }

    public void setGnp(BigDecimal gnp) {
        this.gnp = gnp;
    }

    @Basic
    @Column(name = "gnpold", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getGnpold() {
        return gnpold;
    }

    public void setGnpold(BigDecimal gnpold) {
        this.gnpold = gnpold;
    }

    @Basic
    @Column(name = "localname", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getLocalname() {
        return localname;
    }

    public void setLocalname(String localname) {
        this.localname = localname;
    }

    @Basic
    @Column(name = "governmentform", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getGovernmentform() {
        return governmentform;
    }

    public void setGovernmentform(String governmentform) {
        this.governmentform = governmentform;
    }

    @Basic
    @Column(name = "headofstate", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getHeadofstate() {
        return headofstate;
    }

    public void setHeadofstate(String headofstate) {
        this.headofstate = headofstate;
    }

    @Basic
    @Column(name = "code2", nullable = false, insertable = true, updatable = true, length = 2)
    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryEntity that = (CountryEntity) o;

        if (population != that.population) return false;
        if (Float.compare(that.surfacearea, surfacearea) != 0) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (code2 != null ? !code2.equals(that.code2) : that.code2 != null) return false;
        if (continent != null ? !continent.equals(that.continent) : that.continent != null) return false;
        if (gnp != null ? !gnp.equals(that.gnp) : that.gnp != null) return false;
        if (gnpold != null ? !gnpold.equals(that.gnpold) : that.gnpold != null) return false;
        if (governmentform != null ? !governmentform.equals(that.governmentform) : that.governmentform != null)
            return false;
        if (headofstate != null ? !headofstate.equals(that.headofstate) : that.headofstate != null) return false;
        if (indepyear != null ? !indepyear.equals(that.indepyear) : that.indepyear != null) return false;
        if (lifeexpectancy != null ? !lifeexpectancy.equals(that.lifeexpectancy) : that.lifeexpectancy != null)
            return false;
        if (localname != null ? !localname.equals(that.localname) : that.localname != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (region != null ? !region.equals(that.region) : that.region != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (continent != null ? continent.hashCode() : 0);
        result = 31 * result + (region != null ? region.hashCode() : 0);
        result = 31 * result + (surfacearea != +0.0f ? Float.floatToIntBits(surfacearea) : 0);
        result = 31 * result + (indepyear != null ? indepyear.hashCode() : 0);
        result = 31 * result + population;
        result = 31 * result + (lifeexpectancy != null ? lifeexpectancy.hashCode() : 0);
        result = 31 * result + (gnp != null ? gnp.hashCode() : 0);
        result = 31 * result + (gnpold != null ? gnpold.hashCode() : 0);
        result = 31 * result + (localname != null ? localname.hashCode() : 0);
        result = 31 * result + (governmentform != null ? governmentform.hashCode() : 0);
        result = 31 * result + (headofstate != null ? headofstate.hashCode() : 0);
        result = 31 * result + (code2 != null ? code2.hashCode() : 0);
        return result;
    }
}
