package org.openlogics.jpa.openjpa;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: Foo.java 0, 8/21/14 8:27 PM, @miguel $
 */
@Entity
@Table(name = "foo")
public class Foo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "foo_id")
    private Long id;
    @Column(name = "foo_fname")
    private String name;
    @Column(name = "foo_add_date")
    private Date dateAdd;
    @Column
    private BigDecimal foo_rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public BigDecimal getFoo_rate() {
        return foo_rate;
    }

    public void setFoo_rate(BigDecimal foo_rate) {
        this.foo_rate = foo_rate;
    }
}
