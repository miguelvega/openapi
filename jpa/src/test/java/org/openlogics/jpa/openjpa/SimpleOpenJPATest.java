package org.openlogics.jpa.openjpa;

import com.google.common.io.Resources;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.ResultSetHandler;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.openlogics.gears.jdbc.*;

import javax.persistence.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: SimpleOpenJPATest.java 0, 8/21/14 8:36 PM, @miguel $
 * @see http://www.informit.com/articles/article.aspx?p=1671224&seqNum=2
 * @see http://arquillian.org/guides/testing_java_persistence/
 *
 */
public class SimpleOpenJPATest {

    protected IDatabaseTester databaseTester;
    protected BasicDataSource basicDataSource;

    @Before
    public void setup(){
        try {

            basicDataSource = new BasicDataSource();
            basicDataSource.setUrl("jdbc:h2:mem:parametrostest");
            basicDataSource.setDriverClassName("org.h2.Driver");

            Connection connection = basicDataSource.getConnection();
            URL sql = getResource(getClass(), "foo.sql");
            try {
                connection.createStatement().execute(Resources.toString(sql, US_ASCII));
                connection.close();

                URL resource = getResource(getClass(), "foo.xml");
                FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
                databaseTester = new DataSourceDatabaseTester(basicDataSource);
                databaseTester.setDataSet(build);
                databaseTester.onSetup();
            } catch (SQLException x) {
                x.printStackTrace();
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    @Test
    public void testSimple() throws SQLException {

        ObjectDataStore ds = DataStoreFactory.createObjectDataStore(basicDataSource);

        /*ds.select(org.openlogics.gears.jdbc.Query.of("select * from products"), new ResultSetHandler<Object>() {
            @Override
            public Object handle(ResultSet rs) throws SQLException {
                while(rs.next())
                    System.out.println("..."+rs.getObject(1));
                return null;
            }
        });*/

        List<Foo> foos = null;

        foos = ds.getResultList(Foo.class);
        for (Foo foo : foos) {
            System.out.println("1st. NAME="+foo.getName());
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("localTest");
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        TypedQuery<Foo> query = em.createQuery("select f from Foo f", Foo.class);
        foos = query.getResultList();
        for (Foo foo : foos) {
            System.out.println("NAme="+foo.getName());
        }

        em.getTransaction().commit();
        em.close();
        factory.close();
    }
}
