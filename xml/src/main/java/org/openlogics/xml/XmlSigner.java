package org.openlogics.xml;

import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.resolver.ResourceResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: XmlSigner.java 0, 10/19/14 12:38 AM, @miguel $
 */
public class XmlSigner {

    final Xml xml;

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public XmlSigner(Xml xml) {
        this.xml = xml;
    }

    /**
     * Method used to get the KeyInfo
     *
     * @param xmlSigFactory
     * @param publicKey
     * @return KeyInfo
     */
    private KeyInfo getKeyInfo(XMLSignatureFactory xmlSigFactory, PublicKey publicKey) {
        KeyInfo keyInfo = null;
        KeyValue keyValue = null;

        KeyInfoFactory keyInfoFact = xmlSigFactory.getKeyInfoFactory();

        try {
            keyValue = keyInfoFact.newKeyValue(publicKey);
        } catch (KeyException ex) {
            ex.printStackTrace();
        }

        keyInfo = keyInfoFact.newKeyInfo(Collections.singletonList(keyValue));
        return keyInfo;
    }

    /**
     * Method used to attach a generated digital signature to the existing
     * document
     *
     * @param privateKey
     * @param certificate
     */
    public void generateEnvelopedSignature(
            PrivateKey privateKey,
            java.security.cert.Certificate certificate
    )
            throws TransformerException, IOException, SAXException {

        //Create XML Signature Factory
        XMLSignatureFactory xmlSigFactory = XMLSignatureFactory.getInstance("DOM");

        DOMSignContext domSignCtx = new DOMSignContext(privateKey, xml.getDocument().getDocumentElement());
        SignedInfo signedInfo = null;

        try {
            Reference ref = xmlSigFactory.newReference("", xmlSigFactory.newDigestMethod(DigestMethod.SHA1, null),
                    Collections.singletonList(xmlSigFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)),
                    null, null);
            signedInfo = xmlSigFactory.newSignedInfo(
                    xmlSigFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,
                            (C14NMethodParameterSpec) null),
                    xmlSigFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                    Collections.singletonList(ref));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidAlgorithmParameterException ex) {
            ex.printStackTrace();
        }
        //Pass the Public Key File Path
        KeyInfo keyInfo = getKeyInfo(xmlSigFactory, certificate.getPublicKey());
        //Create a new XML Signature
        XMLSignature xmlSignature = xmlSigFactory.newXMLSignature(signedInfo, keyInfo);

        try {
            //Sign the document
            xmlSignature.sign(domSignCtx);
        } catch (MarshalException ex) {
            throw new IllegalStateException(ex);
        } catch (XMLSignatureException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public org.apache.xml.security.signature.XMLSignature generateEnvelopedSignature2(Key key,
                                                                                      SignatureMetadata signatureMetadata,
                                                                                      ObjectContainer... objectContainers) throws XMLSecurityException{
        return this.generateEnvelopedSignature2(null, key, signatureMetadata, objectContainers);
    }
    /**
     * In order to keep preferences the signature, this method uses the apache security xml API to perform
     * XML signing, supported in a widely e.
     * @param certificate
     * @param key
     * @param signatureMetadata
     * @param objectContainers
     * @return
     * @throws XMLSecurityException
     * @throws ParserConfigurationException
     * @throws MalformedURLException
     */
    public org.apache.xml.security.signature.XMLSignature generateEnvelopedSignature2(X509Certificate certificate,
                                                                                      Key key,
                                                                                      SignatureMetadata signatureMetadata,
                                                                                      ObjectContainer... objectContainers) throws XMLSecurityException{

        assert signatureMetadata!=null;

        //ElementProxy.setDefaultPrefix(Constants.SignatureSpecNS, "ds");

        Document doc = xml.getDocument();

        //Create an XML Signature object from the document, BaseURI and
        //signature algorithm (in this case DSA)
        org.apache.xml.security.signature.XMLSignature signature =
                new org.apache.xml.security.signature.XMLSignature(doc,
                        signatureMetadata.<String>getValueOf(SignatureMetadata.Key.SIGNATURE_BASE_URI),
                        signatureMetadata.<String>getValueOf(SignatureMetadata.Key.SIGNATURE_METHOD_URI),
                        signatureMetadata.<String>getValueOf(SignatureMetadata.Key.CANONICALIZATION_METHOD_URI));


        //Append the signature element to the root element before signing because
        //this is going to be an enveloped signature.
        //This means the signature is going to be enveloped by the document.
        //Two other possible forms are enveloping where the document is inside the
        //signature and detached where they are seperate.
        //Note that they can be mixed in 1 signature with seperate references as
        //shown below.
        xml.getRoot().appendChild(signature.getElement());

        for (ResourceResolver resourceResolver : signatureMetadata.<List<ResourceResolver>>getValueOf(SignatureMetadata.Key.RESOURCE_RESOLVER)) {
//            signature.getSignedInfo().addResourceResolver(new org.apache.xml.security.samples.utils.resolver.OfflineResolver());
            signature.getSignedInfo().addResourceResolver(resourceResolver);
        }
        for (ResourceResolverSpi resourceResolverSpi : signatureMetadata.<List<ResourceResolverSpi>>getValueOf(SignatureMetadata.Key.RESOURCE_RESOLVER_SPI)) {
            signature.getSignedInfo().addResourceResolver(resourceResolverSpi);
        }

        for (ObjectContainer objectContainer : objectContainers) {

            //An autogenerated ID
            /*if (isNullOrEmpty(objectContainer.getId())){
                objectContainer.setId(randomUUID().toString());
            }*/

            signature.appendObject(objectContainer);
        }

        for (String keyName : signatureMetadata.<List<String>>getValueOf(SignatureMetadata.Key.KEY_INFO_KEY_NAMES)) {
            signature.getKeyInfo().addKeyName(keyName);
        }

        String referenceURI = signatureMetadata.getValueOf(SignatureMetadata.Key.REFERENCE_URI);

        if (!signatureMetadata.hasMetadata(SignatureMetadata.Key.TRANSFORMS)) {
            //create the transforms object for the Document/Reference
            org.apache.xml.security.transforms.Transforms transforms = new org.apache.xml.security.transforms.Transforms(doc);

            //First we have to strip away the signature element (it's not part of the
            //signature calculations). The enveloped transform can be used for this.
            transforms.addTransform(org.apache.xml.security.transforms.Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
            //Part of the signature element needs to be canonicalized. It is a kind
            //of normalizing algorithm for XML. For more information please take a
            //look at the W3C XML Digital Signature webpage.
            transforms.addTransform(org.apache.xml.security.transforms.Transforms.TRANSFORM_C14N_WITH_COMMENTS);
            //Add the above Document/Reference
            signature.addDocument(referenceURI, transforms, signatureMetadata.<String>getValueOf(SignatureMetadata.Key.DIGEST_URI));
        }else{
            signature.addDocument(referenceURI,
                    signatureMetadata.<Transforms>getValueOf(SignatureMetadata.Key.TRANSFORMS),
                    signatureMetadata.<String>getValueOf(SignatureMetadata.Key.DIGEST_URI));
        }

        /*{
            //Add in 2 external URIs. This is a detached Reference.
            //
            // When sign() is called, two network connections are made. -- well,
            // not really, as we use the OfflineResolver which acts as a proxy for
            // these two resouces ;-))
            //
            signature.addDocument("http://www.w3.org/TR/xml-stylesheet");
            signature.addDocument("http://www.nue.et-inf.uni-siegen.de/index.html");
        }*/

        if(certificate!=null){
            signature.addKeyInfo(certificate);
            signature.addKeyInfo(certificate.getPublicKey());

            signature.sign(key);
        }else{
            signature.sign(key);
        }

//        org.apache.xml.security.utils.XMLUtils.outputDOMc14nWithComments(doc, System.out);

        return signature;
    }

    public boolean verifySignature(PublicKey publicKey) throws XMLSecurityException {
        return verifySignature(publicKey, null);
    }

    public boolean verifySignature(PublicKey publicKey, String baseURI) throws XMLSecurityException {
        //retrieve the Signed Element
        NodeList signatures = xml.getDocument().getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);

        Element signatureElement = (Element) signatures.item(0);

        org.apache.xml.security.signature.XMLSignature signature = new org.apache.xml.security.signature.XMLSignature(
                signatureElement, baseURI);

        return signature.checkSignatureValue(publicKey);

    }

    public ObjectContainer buildObjectContainer() {
        return buildObjectContainer("");
    }

    public ObjectContainer buildObjectContainer(String id) {

        ObjectContainer objectContainer = new ObjectContainer(xml.getDocument());
        objectContainer.setId(id);

        return objectContainer;
    }
}
