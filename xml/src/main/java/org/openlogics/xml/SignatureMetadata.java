package org.openlogics.xml;

import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.resolver.ResourceResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;

import java.util.*;

/**
 * @author Miguel Vega
 * @version $Id: SignMetadata.java 0, 10/20/14 1:55 PM, @miguel $
 */
public class SignatureMetadata {

    final Map<Key, Object> data = new HashMap<Key, Object>();

    SignatureMetadata(){}

    public static final SignatureMetadata build(){
        return new SignatureMetadata();
    }

    public SignatureMetadata withTransforms(Transforms transforms){
        data.put(Key.TRANSFORMS, transforms);
        return this;
    }

    public SignatureMetadata withReferenceURI(String referenceURI){
        data.put(Key.REFERENCE_URI, referenceURI);
        return this;
    }
    public SignatureMetadata withDigestURI(String digestURI){
        data.put(Key.DIGEST_URI, digestURI);
        return this;
    }

    public SignatureMetadata withSignatureBaseURI(String signatureBaseURI){
        data.put(Key.SIGNATURE_BASE_URI, signatureBaseURI);
        return this;
    }

    public SignatureMetadata withSignatureMethodURI(String signedMethodURI){
        data.put(Key.SIGNATURE_METHOD_URI, signedMethodURI);
        return this;
    }
    public SignatureMetadata withCanonicalizationMethodURI(String canonizalizationMethodURI){
        data.put(Key.CANONICALIZATION_METHOD_URI, canonizalizationMethodURI);
        return this;
    }
    public SignatureMetadata withKeyInfoKeyNames(String... keyNames){
        data.put(Key.KEY_INFO_KEY_NAMES, Arrays.asList(keyNames));
        return this;
    }
    public SignatureMetadata withResourceResolvers(ResourceResolver... resourceResolver){
        data.put(Key.RESOURCE_RESOLVER, Arrays.asList(resourceResolver));
        return this;
    }
    public SignatureMetadata withResourceResolverSpis(ResourceResolverSpi... resourceResolverSpi){
        data.put(Key.RESOURCE_RESOLVER_SPI, Arrays.asList(resourceResolverSpi));
        return this;
    }

    protected final void detach(){
        Iterator<Map.Entry<Key, Object>> iterator = data.entrySet().iterator();

        while(iterator.hasNext()){
            Map.Entry<Key, Object> entry = iterator.next();
            Object o = entry.getValue();
            o = null;
            data.remove(entry.getKey());
        }

        data.clear();
    }

    protected <T> T getValueOf(Key key){
        if(data.containsKey(key))
            return (T) data.get(key);
        return (T) key.getDefaultValue();
    }

    public boolean hasMetadata(Key key) {
        return data.containsKey(key);
    }

    protected enum Key {
        REFERENCE_URI{
            @Override
            Object getDefaultValue() {
                return "";
            }
        }, DIGEST_URI{
            @Override
            Object getDefaultValue() {
                return Constants.ALGO_ID_DIGEST_SHA1;
            }
        }, TRANSFORMS{
            @Override
            Object getDefaultValue() {
                return null;
            }
        }, SIGNATURE_BASE_URI{
            @Override
            Object getDefaultValue() {
                return null;
            }
        }, SIGNATURE_METHOD_URI {
            @Override
            Object getDefaultValue() {
                return XMLSignature.ALGO_ID_SIGNATURE_RSA;
            }
        }, CANONICALIZATION_METHOD_URI {
            @Override
            Object getDefaultValue() {
                return Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS;
            }
        }, KEY_INFO_KEY_NAMES{
            @Override
            Object getDefaultValue() {
                return Collections.emptyList();
            }
        }, RESOURCE_RESOLVER{
            @Override
            Object getDefaultValue() {
                return Collections.emptyList();
            }
        }, RESOURCE_RESOLVER_SPI{
            @Override
            Object getDefaultValue() {
                return Collections.emptyList();
            }
        };

        abstract Object getDefaultValue();
    }
}
