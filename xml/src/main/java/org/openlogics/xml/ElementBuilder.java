package org.openlogics.xml;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;

/**
 * @author Miguel Vega
 * @version $Id: ElementBuilder.java 0, 12/22/13 11:37 PM miguel $
 */
public class ElementBuilder {

    private final Document document;

    private Element element;

    ElementBuilder(Document document){
        this.document = document;
    }

    public ElementBuilder withIdentity(String tagName){
        this.element = document.createElement(tagName);
        return this;
    }

    public ElementBuilder withIdentity(String tagName, NameSpace nameSpace){
        return withIdentity(new QName(nameSpace.getNamespaceURI(), tagName, nameSpace.getPrefix()));
    }

    public ElementBuilder withIdentity(QName qName){
        this.element = document.createElementNS(qName.getNamespaceURI(), qName.getLocalPart());
        element.setPrefix(qName.getPrefix());
        return this;
    }

    public ElementBuilder addIdAttribute(String name, String value){
        return addIdAttribute(null, name, value);
    }

    public ElementBuilder addAttribute(String name, String value){
        return addAttribute(null, name, value);
    }

    public ElementBuilder addAttribute(NameSpace nameSpace, String name, String value){
        return addAttribute(null, name, value, false);
    }

    public ElementBuilder addIdAttribute(NameSpace nameSpace, String name, String value){
        return addAttribute(null, name, value, true);
    }

    private ElementBuilder addAttribute(NameSpace nameSpace, String name, String value, boolean isId){
        Attr attr;

        if(nameSpace!=null)
            attr = document.createAttributeNS(nameSpace.getNamespaceURI(), name);
        else
            attr = document.createAttribute(name);

        attr.setValue(value);

        return !isId?addAttribute(attr):addIdAttribute(attr);
    }

    public ElementBuilder addAttribute(Attr attr){
        element.setAttributeNode(attr);
        return this;
    }

    /**
     * Used when looking by ID
     * @param attr
     * @return
     */
    public ElementBuilder addIdAttribute(Attr attr){
        element.setIdAttributeNode(attr, true);
        return this;
    }

    public ElementBuilder withText(String text){
        element.setTextContent(text);
        return this;
    }

    public Element build(){
        return element;
    }
}
