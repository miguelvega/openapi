package org.openlogics.xml;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.soap.*;

/**
 * Soap 1.1 instantiator sue to the factory used
 *
 * @author Miguel Vega
 * @version $Id: SOAPRequestBuilder.java 0, 9/7/14 11:05 PM, @miguel $
 */
public class SOAPDocument {

    public static final String SOAP = "soap";
    final SOAPMessage message;

    public NameSpace getSOAPNS() {
        return soapNS;
    }

    final NameSpace soapNS;

    public SOAPDocument() throws SOAPException {
        super();
        final MessageFactory messageFactory = MessageFactory.newInstance();
        message = messageFactory.createMessage();

        this.soapNS = new NameSpace(SOAPConstants.URI_NS_SOAP_1_1_ENVELOPE, SOAP);

        //change the default PREFIX
        message.getSOAPPart().getEnvelope().setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
//        message.getSOAPPart().getEnvelope().addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        message.getSOAPPart().getEnvelope().setPrefix("soap");
        message.getSOAPBody().setPrefix("soap");
        message.getSOAPPart().getEnvelope().getHeader().detachNode();

        //remove the duplicate Namespace declaration
        message.getSOAPPart().getEnvelope().removeAttribute("xmlns:SOAP-ENV");
    }

    public NameSpace getSOAPNameSpace() {
        return soapNS;
    }

    public void declareNameSpace(NameSpace nameSpace) throws SOAPException {
        message.getSOAPPart().getEnvelope().addNamespaceDeclaration(nameSpace.getPrefix(), nameSpace.getNamespaceURI());
        /*message.getSOAPPart().getEnvelope().setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI,
                nameSpace.getQualifiedName(), nameSpace.getNamespaceURI());*/
    }

    public SOAPHeader getHeader() throws SOAPException {
        return message.getSOAPPart().getEnvelope().getHeader();
    }

    public SOAPBody getBody() throws SOAPException {
        return message.getSOAPPart().getEnvelope().getBody();
    }

    public SOAPElementBuilder createHeaderElement() throws SOAPException {
        return new SOAPElementBuilder(getHeader(), message);
    }

    public SOAPElementBuilder createBodyElement() throws SOAPException {
        return new SOAPElementBuilder(getBody(), message);
    }

    public SOAPElementBuilder createElement(SOAPElement parent) throws SOAPException {
        return new SOAPElementBuilder(parent, message);
    }

    public Xml getXml() throws SOAPException {

        return new Xml(message.getSOAPPart());

        /*
        SOAPPart soapPart = message.getSOAPPart();

        message.getSOAPPart().getEnvelope().setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
        message.getSOAPPart().getEnvelope().removeAttributeNS("http://schemas.xmlsoap.org/soap/envelope/", "SOAP-ENV");
        message.getSOAPPart().getEnvelope().removeAttribute("xmlns:SOAP-ENV");
        message.getSOAPPart().getEnvelope().setPrefix("soap");
        message.getSOAPBody().setPrefix("soap");
        message.getSOAPHeader().setPrefix("soap");

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("a", "http://www.w3.org/2005/08/addressing");

        SOAPHeader soapHeader = envelope.getHeader();

        SOAPHeaderElement actionH = soapHeader.addHeaderElement(envelope.createQName("Action", "a"));
        actionH.addAttribute(envelope.createQName("mustUnderstand", "soap"), "1");
        actionH.setTextContent("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue");

        SOAPHeaderElement messageID = soapHeader.addHeaderElement(envelope.createQName("MessageID", "a"));
        messageID.setTextContent("urn:uuid:6a94eab7-efb2-4a6c-a1fb-0c6d2d84eef5");

        SOAPHeaderElement replyTo = soapHeader.addHeaderElement(envelope.createQName("ReplyTo", "a"));

        SOAPHeaderElement address = soapHeader.addHeaderElement(envelope.createQName("Address", "a"));
        address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
        replyTo.addChildElement(address);

        SOAPHeaderElement to = soapHeader.addHeaderElement(envelope.createQName("To", "a"));
        to.addAttribute(envelope.createQName("mustUnderstand", "soap"), "1");
        to.setTextContent("http://s-nal-sfv-01.sin.gobierno.bo/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure");

        return soapPart;
        */
    }

    /**
     * @param soapOperation
     * @param nameSpace
     * @return
     */
    public QName buildOperationQName(String soapOperation, NameSpace nameSpace) {
        return new QName(nameSpace.getNamespaceURI(), soapOperation, nameSpace.getPrefix());
    }

    public SOAPMessage getSOAPMessage(){
        return message;
    }
}
