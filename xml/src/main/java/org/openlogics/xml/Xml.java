package org.openlogics.xml;


import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static javax.xml.transform.OutputKeys.ENCODING;

/**
 * @author Miguel Vega
 * @version $Id: org.openlogics.xml.Xml.java 0, 12/22/13 2:12 AM miguel $
 */
public class Xml {

    //private final DocumentBuilder documentBuilder;
    private final Document document;

    static {
        //noinspection AccessOfSystemProperties
        //System.setProperty("org.apache.xml.security.ignoreLineBreaks", "true");
        try {
            org.apache.xml.security.Init.init();
        } catch (NoClassDefFoundError x) {
            System.err.println("Unable to initialize APACHE security." + x.getMessage());
        }
    }

    /**
     * @param document
     */
    public Xml(Document document) {
        this.document = document;
    }

    /**
     * Creates a new XML DOM document, binding rootTagName with the given nameSpace.
     *
     * @param foreignNode
     */
    public Xml(Node foreignNode) {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try {

            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

            this.document = documentBuilder.newDocument();

            foreignNode = document.adoptNode(foreignNode);

            document.appendChild(foreignNode);

            //settings
//            document.setXmlStandalone(true);
//            document.setXmlVersion("1.0");

        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("A serious problem happened", e);
        }
    }

    /**
     * Creates a new XML DOM document.
     *
     * @param rootTagName
     */
    public Xml(String rootTagName) {
        this(rootTagName, null);
    }

    public Xml(InputStream inputStream) throws IOException, SAXException {
        this(new InputSource(inputStream));
    }

    public Xml(Reader reader) throws IOException, SAXException {
        this(new InputSource(reader));
    }

    /**
     * Creates a new XML DOM document, binding rootTagName with the given nameSpace.
     *
     * @param rootTagName
     * @param nameSpace
     */
    public Xml(String rootTagName, NameSpace nameSpace) {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try {

            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

            this.document = documentBuilder.newDocument();

            if (nameSpace == null)
                document.appendChild(document.createElement(rootTagName));
            else {
                Element elementNS = document.createElementNS(nameSpace.getNamespaceURI(), rootTagName);
                elementNS.setPrefix(nameSpace.getPrefix());
                document.appendChild(elementNS);
            }

            //settings
//            document.setXmlStandalone(true);
//            document.setXmlVersion("1.0");

        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("A serious problem happened", e);
        }
    }

    /**
     * Creates a new instance of XML object with normalized DOM
     *
     * @param inputSource
     * @throws java.io.IOException
     * @throws org.xml.sax.SAXException
     */
    public Xml(InputSource inputSource) throws IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try {
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

            this.document = documentBuilder.parse(inputSource);
            document.getDocumentElement().normalize();

        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("A serious problem happened", e);
        }
    }

    public Document getDocument() {
        return document;
    }

    public Element getElementById(Element element, String id) {
        return (Element) document.getElementById(id);
    }

    public Xml addNameSpace(NameSpace nameSpace) {
        Attr attribute = document.createAttribute("xmlns:" + nameSpace.getNamespaceURI());
        attribute.setValue(nameSpace.getNamespaceURI());
        getRoot().setAttributeNode(attribute);
        return this;
    }

    public ElementBuilder elementBuilder() {
        return new ElementBuilder(document);
    }

    public Element getRoot() {
        return (Element) document.getFirstChild();
    }

    public void write(Transformer transformer, OutputStream outputStream) throws TransformerException {
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputStream);
        transformer.transform(source, result);
    }

    public void write(Transformer transformer, Writer writer) throws TransformerException {
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
    }

    public void write(OutputStream outputStream) throws TransformerException {
        write(outputStream, false);
    }

    public void write(OutputStream outputStream, Map<String, String> outputProperties) throws TransformerException {
        write(outputStream, false, outputProperties);
    }

    public void write(Writer writer, Map<String, String> outputProperties) throws TransformerException {
        write(writer, false, outputProperties);
    }

    public void write(Writer writer) throws TransformerException {
        write(writer, false);
    }

    /**
     * Writes using a default transformer for writing. Encoding used is UTF-8.
     *
     * @param outputStream
     * @param prettyPrint
     * @throws javax.xml.transform.TransformerException
     */
    public void write(OutputStream outputStream, boolean prettyPrint) throws TransformerException {
        write(outputStream, prettyPrint, ImmutableMap.of(ENCODING, "UTF-8"));
    }

    /**
     * Writes xml and uses provided map of xml settings
     *
     * @param outputStream
     * @param prettyPrint
     * @param outputProperties a map with the keys from the {@link javax.xml.transform.OutputKeys#} class and others supported
     * @throws TransformerException
     */
    public void write(OutputStream outputStream, boolean prettyPrint, Map<String, String> outputProperties) throws TransformerException {
        // Use a Transformer for output
        TransformerFactory tFactory = TransformerFactory.newInstance();
        final Transformer transformer = tFactory.newTransformer();

//        transformer.setOutputProperty(ENCODING, "UTF-8");
//        transformer.setOutputProperty(OMIT_XML_DECLARATION, "yes");

        if (prettyPrint) {
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        }

        if (outputProperties != null) {
            Collections2.transform(Lists.newArrayList(outputProperties), new Function<Map<String, String>, Void>() {
                @Override
                public Void apply(Map<String, String> input) {
                    Set<String> iterator = input.keySet();
                    for (String key : iterator) {
                        transformer.setOutputProperty(key, input.get(key));
                    }
                    return null;
                }
            }).iterator().next();
        }

        write(transformer, outputStream);
    }

    public void write(Writer writer, boolean prettyPrint) throws TransformerException {
        write(writer, prettyPrint, ImmutableMap.of(ENCODING, "UTF-8"));
    }

    public void write(Writer writer, boolean prettyPrint, Map<String, String> outputProperties) throws TransformerException {
        // Use a Transformer for output
        TransformerFactory tFactory = TransformerFactory.newInstance();
        final Transformer transformer = tFactory.newTransformer();

        //transformer.setOutputProperty(OMIT_XML_DECLARATION, "yes");
//        transformer.setOutputProperty(ENCODING, "UTF-8");

        if (prettyPrint) {
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        }

        if (outputProperties != null) {
            Collections2.transform(Lists.newArrayList(outputProperties), new Function<Map<String, String>, Void>() {
                @Override
                public Void apply(Map<String, String> input) {
                    Set<String> iterator = input.keySet();
                    for (String key : iterator) {
                        transformer.setOutputProperty(key, input.get(key));
                    }
                    return null;
                }
            }).iterator().next();
        }

        write(transformer, writer);
    }

    @Deprecated
    public ObjectContainer buildObjectContainer() {
        return buildObjectContainer("");
    }

    @Deprecated
    public ObjectContainer buildObjectContainer(String id) {

        ObjectContainer objectContainer = new ObjectContainer(document);
        objectContainer.setId(id);

        return objectContainer;
    }

    /**
     * Signs an XML document without verifying it with a public key nor adding data about it.
     *
     * @param key
     * @param keyNames
     * @param transforms
     * @param referenceURI
     * @param objectContainers
     * @return
     * @throws XMLSecurityException
     */
    @Deprecated
    public org.apache.xml.security.signature.XMLSignature sign(
            Key key,
            List<String> keyNames,
            Transforms transforms,
            String referenceURI,
            ObjectContainer... objectContainers) throws XMLSecurityException {
        return sign(null, key, keyNames, transforms, referenceURI, objectContainers);
    }

    /**
     * Signs an XML.
     *
     * @param certificate      adds the public key to the KeyInfo withIdentity ands validates it against PrivateKey param. If null, then simply signs using private key
     * @param key              private key to sign document
     * @param keyNames
     * @param transforms
     * @param referenceURI
     * @param objectContainers
     * @return
     * @throws XMLSecurityException
     */
    @Deprecated
    public org.apache.xml.security.signature.XMLSignature sign(X509Certificate certificate,
                                                               Key key,
                                                               List<String> keyNames,
                                                               Transforms transforms,
                                                               String referenceURI,
                                                               ObjectContainer... objectContainers) throws XMLSecurityException {

        //

        keyNames = Optional.fromNullable(keyNames).or(Collections.<String>emptyList());
        referenceURI = Optional.fromNullable(referenceURI).or("");
        /*
        transforms = Optional.fromNullable(transforms).or(new Supplier<Transforms>() {
            @Override
            public Transforms get() {
                Transforms transforms = new Transforms(document);
                try {
                    transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
                    transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);
                } catch (TransformationException e) {
                    e.printStackTrace();
                }
                return transforms;
            }
        });
        */

        SignatureAlgorithm signatureAlgorithm = new SignatureAlgorithm(
                document, org.apache.xml.security.signature.XMLSignature.ALGO_ID_SIGNATURE_RSA);

        Element canonElem = XMLUtils.createElementInSignatureSpace(document, Constants._TAG_CANONICALIZATIONMETHOD);
        canonElem.setAttributeNS(
                null, Constants._ATT_ALGORITHM,
                //Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS
                Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS
        );

        org.apache.xml.security.signature.XMLSignature signature =
                new org.apache.xml.security.signature.XMLSignature(document, null, signatureAlgorithm.getElement(), canonElem);

        getRoot().appendChild(signature.getElement());

        for (ObjectContainer objectContainer : objectContainers) {

            //An autogenerated ID
            /*if (isNullOrEmpty(objectContainer.getId())){
                objectContainer.setId(randomUUID().toString());
            }*/

            //ADD OBJECT TO SIGNATURE
            signature.appendObject(objectContainer);
        }

        for (String keyName : keyNames) {
            signature.getKeyInfo().addKeyName(keyName);
        }

        if (transforms != null)
            signature.addDocument(referenceURI, transforms, Constants.ALGO_ID_DIGEST_SHA1);

        if (certificate != null) {
            signature.addKeyInfo(certificate);

            signature.sign(key);

            X509Certificate cert = signature.getKeyInfo().getX509Certificate();

            signature.checkSignatureValue(cert.getPublicKey());
        } else {
            signature.sign(key);
        }

        return signature;
    }

}
