package org.openlogics.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author Miguel Vega
 * @version $Id: XMLUtils.java 0, 10/17/14 5:22 PM, @miguel $
 */
public class XMLUtils {
    public static String nodeToString(Node node) {
        try {
            StringWriter sw = new StringWriter();

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "no");
            t.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            t.transform(new DOMSource(node), new StreamResult(sw));

            return sw.toString();
        } catch (TransformerException x) {
            throw new IllegalStateException("Unable to read given node", x);
        }
    }

    public static Document stringToNode(String content) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            return documentBuilder.parse(new InputSource(new StringReader(content)));
        } catch (Throwable e) {
            throw new IllegalStateException("An unexpected error has occurred while parsing content", e);
        }
    }
}