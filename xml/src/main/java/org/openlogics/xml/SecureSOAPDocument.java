package org.openlogics.xml;

import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecTimestamp;
import org.apache.ws.security.message.token.BinarySecurity;
import org.apache.ws.security.message.token.X509Security;
import org.apache.ws.security.util.WSSecurityUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import java.security.cert.X509Certificate;

/**
 * @author Miguel Vega
 * @version $Id: SecureSOAPDocument.java; abr 21, 2015 02:19 PM mvega $
 */
public class SecureSOAPDocument extends SOAPDocument{
    public SecureSOAPDocument() throws SOAPException {
    }

    public void addSecurityHeader(X509Certificate certificate) throws WSSecurityException {
        SOAPPart soapPart = message.getSOAPPart();

        WSSecHeader secHeader = new WSSecHeader();
        Element securityHeader = secHeader.insertSecurityHeader(soapPart);

        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(15);
        Document env = timestamp.build(soapPart, secHeader);

        BinarySecurity bstToken = new X509Security(env);
        ((X509Security) bstToken).setX509Certificate(certificate);
        WSSecurityUtil.prependChildElement(securityHeader, bstToken.getElement());
    }

    public void addSecurityBody(X509Certificate certificate) throws WSSecurityException {
        SOAPPart soapPart = message.getSOAPPart();

        WSSecHeader secHeader = new WSSecHeader();
        Element securityHeader = secHeader.insertSecurityHeader(soapPart);

        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(15);
        Document env = timestamp.build(soapPart, secHeader);

        BinarySecurity bstToken = new X509Security(env);
        ((X509Security) bstToken).setX509Certificate(certificate);
        WSSecurityUtil.prependChildElement(securityHeader, bstToken.getElement());
    }

}
