package org.openlogics.xml;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;

/**
 * @author Miguel Vega
 * @version $Id: NameSpace.java 0, 12/22/13 11:26 PM miguel $
 */
public class NameSpace extends QName{

    public NameSpace(String localPart) {
        super(localPart);
    }

    public NameSpace(String nsUri, String prefix) {
        super(nsUri, prefix, prefix);
    }

    public NameSpace(String nsUri, String localPart, String prefix) {
        super(nsUri, localPart, prefix);
    }

    public static NameSpace of(QName qName){
        return new NameSpace(qName.getNamespaceURI(), qName.getLocalPart(), qName.getPrefix());
    }

    public static NameSpace withNamespaceURI(QName qName, String prefix){
        return new NameSpace(qName.getNamespaceURI(), prefix, prefix);
    }

    public String getQualifiedName(){
        return new StringBuilder(XMLConstants.XMLNS_ATTRIBUTE).append(":").append(getNamespaceURI()).toString();
    }
}
