package org.openlogics.xml;

import com.google.common.base.Strings;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

/**
 * @author Miguel Vega
 * @version $Id: SOAPElementBuilder.java 0, 9/8/14 12:18 AM, @miguel $
 */
public class SOAPElementBuilder {
    final SOAPElement parent;
    final SOAPMessage soapMessage;

    SOAPElement soapElement;

    SOAPElementBuilder(SOAPElement parent, SOAPMessage soapMessage) throws SOAPException {
        this.parent = parent;
        this.soapMessage = soapMessage;
    }

    public SOAPElementBuilder withLocalName(String localName) throws SOAPException {
        soapElement = parent.addChildElement(soapMessage.getSOAPPart().getEnvelope().createName(localName));
        return this;
    }

    public SOAPElementBuilder withLocalName(NameSpace ns, String localName) throws SOAPException {
        try {
            return withLocalName(ns.getPrefix(), localName);
        } catch (SOAPException x) {
            if (!x.getMessage().contains("Unable to locate namespace for prefix")) {
                throw x;
            }
            soapElement = parent.addChildElement(localName, ns.getPrefix(), ns.getNamespaceURI());
            return this;
        }
    }

    public SOAPElementBuilder withLocalName(String nsPrefix, String localName) throws SOAPException {
        soapElement = parent.addChildElement(soapMessage.getSOAPPart().getEnvelope().createQName(localName, nsPrefix));
        return this;
    }

    public SOAPElementBuilder addAttribute(String name, String value) throws SOAPException {
//        return addAttribute(soapMessage.getSOAPPart().getEnvelope().getPrefix(), name, value);
        return addAttribute((String) null, name, value);
    }

    public SOAPElementBuilder addAttribute(NameSpace ns, String name, String value) throws SOAPException {
        return addAttribute(ns.getPrefix(), name, value);
    }

    public SOAPElementBuilder addAttribute(String nsPrefix, String name, String value) throws SOAPException {
        if (Strings.isNullOrEmpty(nsPrefix))
            soapElement.addAttribute(QName.valueOf(name), value);
        else
            soapElement.addAttribute(soapMessage.getSOAPPart().getEnvelope().createQName(name, nsPrefix), value);
        return this;
    }

    public SOAPElementBuilder withText(String text) {
        soapElement.setTextContent(text);
        return this;
    }

    public SOAPElement getSoapElement() {
        return soapElement;
    }
}
