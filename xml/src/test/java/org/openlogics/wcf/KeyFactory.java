package org.openlogics.wcf;

/**
 * @author Miguel Vega
 * @version $Id: KeyFactory.java 0, 9/9/14 12:50 PM, @miguel $
 */
/*
 *  Copyright 2010 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

import org.apache.commons.io.IOUtils;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSEncryptionPart;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSSecurityEngine;
import org.apache.ws.security.components.crypto.CredentialException;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.Merlin;
import org.apache.ws.security.message.WSSecEncrypt;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.apache.ws.security.message.WSSecTimestamp;
import org.apache.ws.security.message.token.BinarySecurity;
import org.apache.ws.security.message.token.UsernameToken;
import org.apache.ws.security.message.token.X509Security;
import org.apache.ws.security.util.WSSecurityUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.openlogics.xml.Xml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.Vector;

/**
 * Manejador de certificados publicos y llaves privadas <br/>
 * <br/>
 *
 * @author Ricardo Primintela
 */
public class KeyFactory {

    public static final String ALIAS = "tigo";
    private static final Logger LOG = LoggerFactory.getLogger(KeyFactory.class);
    private static final String KEYSTORE_FILE_TYPE = "seguridad.certificados.tipoArchivo";
    private static final String PRIVATE_KEY_ALIAS = "seguridad.certificados.aliasLlavePrivada";
    private static final String PUBLIC_KEYSTORE_FILE = "seguridad.certificados.repositorioPublico";
    private static final String PUBLIC_KEYSTORE_PASSWORD = "seguridad.certificados.passwordRepositorioPublico";
    private static final String PRIVATE_KEYSTORE_FILE = "seguridad.certificados.repositorioPrivado";
    private static final String PRIVATE_KEYSTORE_PASSWORD = "seguridad.certificados.passwordRepositorioPrivado";
    private static final String PASSWD = "tigo";
    private static final String KEYSTORE_TYPE = "pkcs12";

    {
        try {

            Security.addProvider(new BouncyCastleProvider());

            keyStoreFile = new File("/media/ntfs/usr/java/mc4/sin/firmaTigo.p12");
            keyStore = KeyStore.getInstance("pkcs12", "BC");
            InputStream fis = new FileInputStream(keyStoreFile);
            keyStore.load(fis, "tigo".toCharArray());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static KeyStore keyStore;
    private static File keyStoreFile;

    public synchronized Certificate getCertificadoPublico(String alias) {
        String file = keyStoreFile.getAbsolutePath();
        String pass = PASSWD;
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
            fis = new FileInputStream(file);
            ks.load(fis, pass.toCharArray());

            return ks.getCertificate(alias);
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado publico " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public synchronized Certificate getMiCertificadoPublico() {
        return getMiCertificadoPublico(ALIAS);
    }

    public synchronized Certificate getMiCertificadoPublico(String alias) {

        String file = keyStoreFile.getAbsolutePath();
        String pass = PASSWD;
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
            fis = new FileInputStream(file);
            ks.load(fis, pass.toCharArray());
            return ks.getCertificate(alias);
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado private " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public synchronized PrivateKey getLlavePrivada(String passKey) {
        return getLlavePrivada(ALIAS, passKey);
    }

    public synchronized PrivateKey getLlavePrivada(String alias, String passKey) {
        String file = keyStoreFile.getAbsolutePath();
        String passRepo = PASSWD;
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
            fis = new FileInputStream(file);
            ks.load(fis, passRepo.toCharArray());
            return (PrivateKey) ks.getKey(alias, passKey.trim().toCharArray());
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado publico " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public String getContenidoProcesado(String content, String aliasLlavePrivada, String claveLlavePrivada, String aliasCertificadoPublico,
                                        String usuarioCertificado) throws Exception {
        LOG.debug("Procesando el siguiente contenido\n{}", content);
        StringWriter sw = new StringWriter();
        try {
            long total = System.currentTimeMillis();
            long t;

            InputStream inputStream = IOUtils.toInputStream(content);

            Document parsed = new Xml(inputStream).getDocument();

            Crypto merlinSign = getCryptoSign();
            Crypto merlinEncrypt = getCryptoEncript();

            WSSecHeader secHeader = new WSSecHeader();
            Element securityHeader = secHeader.insertSecurityHeader(parsed);

            // Paso 1: Colocar el Timestamp
            t = System.currentTimeMillis();
            WSSecTimestamp timestamp = new WSSecTimestamp();
            timestamp.setTimeToLive(15);
            Document env = timestamp.build(parsed, secHeader);
            LOG.debug("Tiempo en colocar Timestamp {} milis", System.currentTimeMillis() - t);

            // Paso 2: Colocar el BinaryToken
            t = System.currentTimeMillis();
            BinarySecurity bstToken = new X509Security(env);
            ((X509Security) bstToken).setX509Certificate((X509Certificate) getMiCertificadoPublico(aliasLlavePrivada));
            WSSecurityUtil.prependChildElement(securityHeader, bstToken.getElement());
            LOG.debug("Tiempo en colocar BinarySecurity {} milis", System.currentTimeMillis() - t);

            // Paso 3: Colocar el UsernameToken
            t = System.currentTimeMillis();
            UsernameToken ut = new UsernameToken(false, env, null);
            ut.addNonce(env);
            ut.addCreated(false, env);
            ut.setName(usuarioCertificado);
            ut.setID("UsernameToken-1"); // En duro!! En realidad no hay mucho
            // problema
            WSSecurityUtil.prependChildElement(securityHeader, ut.getElement());
            LOG.debug("Tiempo en colocar UsernameToken {} milis", System.currentTimeMillis() - t);

            Vector<WSEncryptionPart> partsSign = new Vector<WSEncryptionPart>();
            WSEncryptionPart partSign;
            partSign = new WSEncryptionPart("Body", "http://schemas.xmlsoap.org/soap/envelope/", "Content");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("Action", "http://schemas.xmlsoap.org/ws/2004/08/addressing", "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("MessageID", "http://schemas.xmlsoap.org/ws/2004/08/addressing", "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("ReplyTo", "http://schemas.xmlsoap.org/ws/2004/08/addressing", "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("To", "http://schemas.xmlsoap.org/ws/2004/08/addressing", "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart(timestamp.getId(), "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart(ut.getID(), "Element");
            partsSign.add(partSign);

            // PASO 4: Firmar
            t = System.currentTimeMillis();
            WSSecSignature signature = new WSSecSignature();
            signature.setUserInfo(aliasLlavePrivada, claveLlavePrivada);
            signature.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
            signature.setParts(partsSign);
            env = signature.build(parsed, merlinSign, secHeader);
            LOG.debug("Tiempo en Firmar {} milis", System.currentTimeMillis() - t);

            Vector<WSEncryptionPart> partsEncrypt = new Vector<WSEncryptionPart>();
            WSEncryptionPart partEncrypt;
            partEncrypt = new WSEncryptionPart("Body", "http://schemas.xmlsoap.org/soap/envelope/", "Content");
            partsEncrypt.add(partEncrypt);
            partEncrypt = new WSEncryptionPart("DatosCabecera", "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:headerdata:v1", "Element");
            partsEncrypt.add(partEncrypt);
            partEncrypt = new WSEncryptionPart(signature.getId(), "Element");
            partsEncrypt.add(partEncrypt);

            // PASO 5 Encriptar
            t = System.currentTimeMillis();
            WSSecEncrypt encrypt = new WSSecEncrypt();
            encrypt.setUserInfo(aliasCertificadoPublico);
            encrypt.setKeyIdentifierType(WSConstants.THUMBPRINT_IDENTIFIER);
            encrypt.setParts(partsEncrypt);
            env = encrypt.build(env, merlinEncrypt, secHeader);
            LOG.debug("Tiempo en Encriptar {} milis", System.currentTimeMillis() - t);

            LOG.debug("Tiempo de procesamiento total del  xml {} milis", (System.currentTimeMillis() - total));

            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(env), new StreamResult(sw));
        } catch (CredentialException e) {
            String msg = "Problemas de seguridad en los JKS";
            LOG.error(msg, e);
            throw e;
        } catch (IOException e) {
            String msg = "Algo no se pudo leer o escribir. I/O Error!!";
            LOG.error(msg, e);
            throw e;
        } catch (TransformerException e) {
            String msg = "No se pudo transformar el xml procesado";
            LOG.error(msg, e);
            throw e;
        } catch (SAXException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw e;
        } catch (Exception e) {
            String msg = "Error inesperado al procesar el documento para el envio al SIN";
            LOG.error(msg, e);
            throw e;
        }

        return sw.toString();
    }

    public String procesarSeguridadFacturar(String xml, final String claveLlavePrivada) {
        try {
            Document respuesta = procesarSeguridad(xml, claveLlavePrivada);

            NodeList elementsByTagNameNS = respuesta.getElementsByTagNameNS(
                    "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:messages:v1",
                    "facturarResponse");
            Element element = (Element) elementsByTagNameNS.item(0);

            StringWriter w = new StringWriter();

            new Xml(element).write(w, true);

            return w.toString();

            /*
            JAXBContext context = JAXBContext.newInstance(FacturarResponse.class, ArrayOfFallo.class, Fallo.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            facturarResponse = (FacturarResponse) unmarshaller.unmarshal(element);

            if (facturarResponse.getErrores() == null) {
                LOG.debug(XMLUtils.documentToPrettyString(respuesta));
                LOG.info("Factura electronica satisfactoriamente solicitada {}", facturarResponse.getETicket());
            } else {
                List<Fallo> fallos = facturarResponse.getErrores().getFallo();
                for (Fallo fallo : fallos) {
                    LOG.error("Error: {}\nDescripcion: {}", fallo.getCodigo(), fallo.getDescripcion());
                }
            }*/
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public String procesarSeguridadRecuperarFactura(String xml, final String claveLlavePrivada) {
        try {
            Document respuesta = procesarSeguridad(xml, claveLlavePrivada);

            NodeList elementsByTagNameNS = respuesta.getElementsByTagNameNS(
                    "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:messages:v1",
                    "recuperarFacturasResponse");
            Element element = (Element) elementsByTagNameNS.item(0);

            StringWriter w = new StringWriter();

            new Xml(element).write(w, true);

            return w.toString();

            /*
            JAXBContext context = JAXBContext.newInstance(RecuperarFacturasResponse.class, ArrayOfFacturaRespuesta.class, ArrayOfFallo.class, FacturaRespuesta.class, Fallo.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            recuperarFacturasResponse = (RecuperarFacturasResponse) unmarshaller.unmarshal(element);

            if (recuperarFacturasResponse.getErrores() == null) {
                LOG.debug(XMLUtils.documentToPrettyString(respuesta));
                LOG.info("Factura electronica recuperada satisfactoriamente {}", recuperarFacturasResponse.getETicket());
            } else {
                List<Fallo> fallos = recuperarFacturasResponse.getErrores().getFallo();
                for (Fallo fallo : fallos) {
                    LOG.error("Error: {}\nDescripcion: {}", fallo.getCodigo(), fallo.getDescripcion());
                }
            }
            */
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private Document procesarSeguridad(String xml, final String claveLlavePrivada) {
        try {

            LOG.info("Comenzando proceso de verificacion de respuesta de solicitud al SIN");

            //Document respuesta = new Xml(new ByteArrayInputStream(xml.getBytes())).getDocument();
            Document respuesta = new Xml(new StringReader(xml)).getDocument();

            Crypto merlinSign = getCryptoSign();
            Crypto merlinEncrypt = getCryptoEncript();

            WSSecurityEngine secEngine = new WSSecurityEngine();
            //XXX: Esto devuelve un vector deberiamos hacer algo mas?
            secEngine.processSecurityHeader(respuesta, null, new CallbackHandler() {

                @Override
                public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
                    for (int i = 0; i < callbacks.length; i++) {
                        if (callbacks[i] instanceof WSPasswordCallback) {
                            ((WSPasswordCallback) callbacks[i]).setPassword(claveLlavePrivada);
                        }
                    }
                }
            }, merlinEncrypt, merlinSign);
            return respuesta;
        } catch (IOException e) {
            String msg = "Algo no se pudo leer o escribir. I/O Error!!";
            LOG.error(msg, e);
            throw new IllegalStateException(e);
        } catch (SAXException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new IllegalStateException(e);
        } catch (CredentialException e) {
            String msg = "Problemas de seguridad en los JKS";
            LOG.error(msg, e);
            throw new IllegalStateException(e);
        } catch (Exception e) {
            String msg = "Error inesperado al procesar la respuesta encriptada del SIN";
            LOG.error(msg, e);
            throw new IllegalStateException(e);
        }
    }

    /**
     * Retorna JKS con llave y certificado privado (del BISA). Para firmar XML
     *
     * @return Crypto
     * @throws IOException
     * @throws CredentialException
     * @throws Exception
     */
    private Crypto getCryptoSign() throws CredentialException, IOException {
        Properties p = new Properties();
        p.setProperty("org.apache.ws.security.crypto.merlin.file", keyStoreFile.getAbsolutePath());
        p.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", PASSWD);
        return new Merlin(p);
    }

    /**
     * Retorna JKS con certificado publico (del SIN). Para encriptar el XML
     *
     * @return Crypto
     * @throws IOException
     * @throws CredentialException
     * @throws Exception
     */
    private Crypto getCryptoEncript() throws CredentialException, IOException {
        Properties p = new Properties();
        p.setProperty("org.apache.ws.security.crypto.merlin.file", keyStoreFile.getAbsolutePath());
        p.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", PASSWD);
        return new Merlin(p);
    }

    public String valorPorDefecto(String variable) {
        if (KEYSTORE_FILE_TYPE.equals(variable)) {
            return "JKS";
        } else if (PRIVATE_KEY_ALIAS.equals(variable)) {
            return "sec.bisa.com";
        } else if (PUBLIC_KEYSTORE_FILE.equalsIgnoreCase(variable) || PUBLIC_KEYSTORE_PASSWORD.equalsIgnoreCase(variable)
                || PRIVATE_KEYSTORE_FILE.equalsIgnoreCase(variable) || PRIVATE_KEYSTORE_PASSWORD.equalsIgnoreCase(variable)) {
            LOG.error("La propiedad <{}> no esta definida como variable de medio ambiente y se la necesita (no tiene valor por defecto)", variable);
        }
        return null;
    }

    public String[] variables() {
        return new String[]{
                KEYSTORE_FILE_TYPE,
                PRIVATE_KEY_ALIAS,
                PUBLIC_KEYSTORE_FILE,
                PUBLIC_KEYSTORE_PASSWORD,
                PRIVATE_KEYSTORE_FILE,
                PRIVATE_KEYSTORE_PASSWORD
        };
    }

}
