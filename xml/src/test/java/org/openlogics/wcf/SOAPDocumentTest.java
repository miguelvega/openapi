package org.openlogics.wcf;

import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSEncryptionPart;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.components.crypto.CredentialException;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.Merlin;
import org.apache.ws.security.message.*;
import org.apache.ws.security.message.token.UsernameToken;
import org.apache.ws.security.message.token.X509Security;
import org.apache.ws.security.util.Base64;
import org.apache.ws.security.util.WSSecurityUtil;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;
import org.openlogics.xml.NameSpace;
import org.openlogics.xml.SOAPDocument;
import org.openlogics.xml.Xml;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * @see {@link http://www.ibm.com/developerworks/library/ws-Axis2/}
 */
public class SOAPDocumentTest {

    public static final String ADDR_NS_URI = "http://schemas.xmlsoap.org/ws/2004/08/addressing";
    private KeyStore keyStore;
    private File keyStoreFile;
    private static final String PASSWD = "tigo";

    @Before
    public void init() throws NoSuchProviderException, KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());

        keyStoreFile = new File("/media/ntfs/projects/java/legacy/sin/firmaTigo.p12");

        keyStore = KeyStore.getInstance("pkcs12", "BC");
        InputStream fis = new FileInputStream(keyStoreFile);
        keyStore.load(fis, "tigo".toCharArray());

    }

    @Test
    public void testFirstApproach() throws SOAPException, CredentialException, IOException, WSSecurityException, KeyStoreException, TransformerException, SAXException {
        SOAPDocument soapDocument = new SOAPDocument();
        final NameSpace addrNS = new NameSpace(ADDR_NS_URI, "a");
        soapDocument.declareNameSpace(addrNS);

        //configure the header
        soapDocument.createHeaderElement().withLocalName(addrNS, "Action").addAttribute(soapDocument.getSOAPNS(), "mustUnderstand", "1").withText("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue");
        soapDocument.createHeaderElement().withLocalName(addrNS, "MessageID").withText("urn:uuid:3f367da8-0b45-4ae2-b1f0-8c1b2662131a");
        SOAPElement replyTo = soapDocument.createHeaderElement().withLocalName(addrNS, "ReplyTo").getSoapElement();
        soapDocument.createElement(replyTo).withLocalName(addrNS, "Address").withText("http://www.w3.org/2005/08/addressing/anonymous");
        soapDocument.createHeaderElement().withLocalName(addrNS, "To").addAttribute(soapDocument.getSOAPNS(), "mustUnderstand", "1").withText("http://s-nal-sfv-01.sin.gobierno.bo/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure");


        Xml xml = soapDocument.getXml();

        Properties p = new Properties();
        p.setProperty(Merlin.KEYSTORE_FILE, keyStoreFile.getAbsolutePath());
        p.setProperty(Merlin.KEYSTORE_PASSWORD, PASSWD);
        p.setProperty(Merlin.KEYSTORE_TYPE, "pkcs12");
        Crypto crypto =  new Merlin(p);


        WSSecHeader secHeader = new WSSecHeader();
        final Element securityHeaderElement = secHeader.insertSecurityHeader(xml.getDocument());

        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(15);
        Document tmpDoc = timestamp.build(xml.getDocument(), secHeader);


        X509Security bsToken = new X509Security(tmpDoc);
        bsToken.setX509Certificate((X509Certificate) keyStore.getCertificate("tigo"));
        WSSecurityUtil.prependChildElement(securityHeaderElement, bsToken.getElement());

        UsernameToken usernameToken = new UsernameToken(false, tmpDoc, null);//WSConstants.PASSWORD_TEXT);
        usernameToken.addNonce(tmpDoc);
        usernameToken.addCreated(false, tmpDoc);
        usernameToken.setName("tigo");
        usernameToken.setID("UsernameToken-1");
        WSSecurityUtil.prependChildElement(securityHeaderElement, usernameToken.getElement());


        List<WSEncryptionPart> partsSign = new LinkedList<WSEncryptionPart>();
        WSEncryptionPart partSign;
        partSign = new WSEncryptionPart("Body", soapDocument.getSOAPNameSpace().getNamespaceURI(), "Content");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart("Action", ADDR_NS_URI, "Element");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart("MessageID", ADDR_NS_URI, "Element");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart("ReplyTo", ADDR_NS_URI, "Element");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart("To", ADDR_NS_URI, "Element");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart(timestamp.getId(), "Element");
        partsSign.add(partSign);
        partSign = new WSEncryptionPart(usernameToken.getID(), "Element");
        partsSign.add(partSign);

        WSSecSignature signature = new WSSecSignature();
        signature.setUserInfo("tigo", "tigo");
        signature.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
        signature.setParts(partsSign);

        tmpDoc = signature.build(tmpDoc, crypto, secHeader);

        Vector<WSEncryptionPart> partsEncrypt = new Vector<WSEncryptionPart>();
        WSEncryptionPart partEncrypt;
        partEncrypt = new WSEncryptionPart("Body", soapDocument.getSOAPNameSpace().getNamespaceURI(), "Content");
        partsEncrypt.add(partEncrypt);

//        partEncrypt = new WSEncryptionPart("DatosCabecera", "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:headerdata:v1", "Element");
//        partsEncrypt.add(partEncrypt);

        partEncrypt = new WSEncryptionPart(signature.getId(), "Element");
        partsEncrypt.add(partEncrypt);

        WSSecEncrypt encrypt = new WSSecEncrypt();
        encrypt.setUserInfo("tigo");
        encrypt.setKeyIdentifierType(WSConstants.THUMBPRINT_IDENTIFIER);
        encrypt.setParts(partsEncrypt);
        tmpDoc = encrypt.build(tmpDoc, crypto, secHeader);


        DOMSource domSource = new DOMSource(tmpDoc);

        MessageFactory factory = MessageFactory.newInstance();

        SOAPMessage message = factory.createMessage();
        message.getSOAPPart().setContent(domSource);

        xml = new Xml(message.getSOAPPart());

        xml.write(System.out, true);
    }


    @Test
    public void testUserNameToken() throws SOAPException, TransformerException, WSSecurityException {
        SOAPDocument soapDocument = new SOAPDocument();
        Xml xml = soapDocument.getXml();

        final String user = "foo", pwd = "foopwd";

        //add username token
//        WSSAddUsernameToken builder = new WSSAddUsernameToken("", false);
        WSSecUsernameToken usrToken = new WSSecUsernameToken();
        WSSecHeader secHeader = new WSSecHeader();

        //wsSecHeader.setActor("test"); // Actor Not Required,
        // throws Security Header missing Exception
        secHeader.insertSecurityHeader(xml.getDocument());
        //now document soap:header contains just:
        /*<soap:Header>
        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="1"/>
        </soap:Header>*/

//        usrToken.setPasswordType(WSConstants.PASSWORD_DIGEST); //PASSWORD_TEXT);
        usrToken.setPasswordType(WSConstants.PASSWORD_TEXT);
        usrToken.setUserInfo(user, pwd);

        Document usernameTokenDoc = usrToken.build(xml.getDocument(), secHeader);

        xml = new Xml(usernameTokenDoc);

        /*
        //add an 'Id' to it
        //Element usrEle =
        //	  (Element)(doc.getElementById("UsernameToken")); //.item(0));
        //(Element)(doc.getElementsByTagNameNS( WSConstants.WSSE_NS,
        //
        String idValue = "7654";
        //usrEle.setAttribute("Id", idValue);

        // Create a Reference to the UserNameToken.
        Reference ref = new Reference(xml.getDocument());
        ref.setURI("#"+idValue);
        ref.setValueType("UsernameToken");

        SecurityTokenReference str = new SecurityTokenReference(xml.getDocument());
        str.setReference(ref);

        //adding the namespace

        WSSecurityUtil.setNamespace(str.getElement(), WSConstants.WSSE_NS, WSConstants.WSSE_PREFIX);

        DOMSource domSource = new DOMSource(usernameTokenDoc);

        MessageFactory factory = MessageFactory.newInstance();

        SOAPMessage message = factory.createMessage();
        message.getSOAPPart().setContent(domSource);

        xml = new Xml(message.getSOAPPart());
        */

        xml.write(System.out, true);
    }

    @Test
    public void testEnvelopeSignature() throws SOAPException, CredentialException, IOException, TransformerException, WSSecurityException {
        SOAPDocument soapDocument = new SOAPDocument();
        Xml xml = soapDocument.getXml();

        // WSSignEnvelope signs a SOAP envelope according to the
        // WS Specification (X509 profile) and adds the signature data
        // to the envelope.
        WSSecSignature signer = new WSSecSignature();

        String alias = "tigo";//"16c73ab6-b892-458f-abf5-2f875f74882e"
        String pwd = "tigo";

        signer.setUserInfo(alias, pwd);

        // The "build" method, creates the signed SOAP envelope.
        // It takes a SOAP Envelope as a W3C Document and adds
        // a WSS Signature header to it. The signed elements
        // depend on the signature parts that are specified by
        // the WSBaseMessage.setParts(java.util.Vector parts)
        // method. By default, SOAP Body is signed.
        // The "crypto" parameter is the object that implements
        // access to the keystore and handling of certificates.
        // A default implementation is included:
        //    org.apache.ws.security.components.crypto.Merlin
        signer.setKeyIdentifierType(WSConstants.X509_KEY_IDENTIFIER);

        Properties p = new Properties();
        p.setProperty(Merlin.KEYSTORE_FILE, keyStoreFile.getAbsolutePath());
        p.setProperty(Merlin.KEYSTORE_PASSWORD, PASSWD);
        p.setProperty(Merlin.KEYSTORE_TYPE, "pkcs12");
        Crypto crypto =  new Merlin(p);


        WSSecHeader secHeader = new WSSecHeader();
        secHeader.insertSecurityHeader(xml.getDocument());
        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(15);
        Document withSecDoc = timestamp.build(xml.getDocument(), secHeader);

        Document signedDoc = signer.build(withSecDoc, crypto, secHeader);

        DOMSource src = new DOMSource(signedDoc);
        MessageFactory mf = MessageFactory.newInstance();
        SOAPMessage soapMsg = mf.createMessage();
        soapMsg.getSOAPPart().setContent(src);

        xml = new Xml(soapMsg.getSOAPPart());

        xml.write(System.out, true);
    }

    @Test
    public void decode() throws WSSecurityException, CredentialException, IOException {
        byte[] decode = Base64.decode("FgMBAF4BAABaAwFUDyda4x6SjhWul7YNezvUr4/lC3MGi9hYybY///8iBwAAGAAvADUABQAKwBPAFMAJwAoAMgA4ABMABAEAABn/AQABAAAKAAYABAAXABgACwACAQAAIwAA");
        System.out.println(new String(decode));

        Properties p = new Properties();
        p.setProperty(Merlin.KEYSTORE_FILE, keyStoreFile.getAbsolutePath());
        p.setProperty(Merlin.KEYSTORE_PASSWORD, "tigo");

        Merlin merlin = new Merlin(p);

        System.out.println(Base64.encode(decode));

    }

    @Test
    public void testBuildMessage() throws SOAPException, TransformerException, NoSuchProviderException, KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, WSSecurityException, XMLSecurityException {
        SOAPDocument req = new SOAPDocument();
        NameSpace addrNS = new NameSpace("http://www.w3.org/2005/08/addressing", "a");
        req.declareNameSpace(addrNS);

        //configure the header
        req.createHeaderElement().withLocalName(addrNS, "Action").addAttribute(req.getSOAPNS(), "mustUnderstand", "1").withText("http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue");
        req.createHeaderElement().withLocalName(addrNS, "MessageID").withText("urn:uuid:3f367da8-0b45-4ae2-b1f0-8c1b2662131a");
        SOAPElement replyTo = req.createHeaderElement().withLocalName(addrNS, "ReplyTo").getSoapElement();
        req.createElement(replyTo).withLocalName(addrNS, "Address").withText("http://www.w3.org/2005/08/addressing/anonymous");
        req.createHeaderElement().withLocalName(addrNS, "To").addAttribute(req.getSOAPNS(), "mustUnderstand", "1").withText("http://s-nal-sfv-01.sin.gobierno.bo/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure");

        //configure the body

        NameSpace trustNS = new NameSpace("http://schemas.xmlsoap.org/ws/2005/02/trust", "t");
        SOAPElement requestSecurityToken = req.createBodyElement().withLocalName(trustNS, "RequestSecurityToken").
                addAttribute("Context", "uuid-558ef6c4-82f9-4efe-b7c5-75d25a3e69a3-1")
                .getSoapElement();
        req.createElement(requestSecurityToken).withLocalName(trustNS, "TokenType").withText("http://schemas.xmlsoap.org/ws/2005/02/sc/sct");
        req.createElement(requestSecurityToken).withLocalName(trustNS, "RequestType").withText("http://schemas.xmlsoap.org/ws/2005/02/trust/Issue");
        req.createElement(requestSecurityToken).withLocalName(trustNS, "KeySize").withText("256");

        X509Security bsToken = new X509Security(req.getXml().getDocument());
        bsToken.setX509Certificate((X509Certificate) keyStore.getCertificate("tigo"));
        Element binarySecElement = bsToken.getElement();

        req.createElement(requestSecurityToken).withLocalName(trustNS, "BinaryExchange")
                .addAttribute("ValueType", "http://schemas.xmlsoap.org/ws/2005/02/trust/tlsnego")
                .addAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary")
//                .withText("FgMBAF4BAABaAwFUDyda4x6SjhWul7YNezvUr4/lC3MGi9hYybY///8iBwAAGAAvADUABQAKwBPAFMAJwAoAMgA4ABMABAEAABn/AQABAAAKAAYABAAXABgACwACAQAAIwAA")
                .withText(binarySecElement.getTextContent())
        ;

        //req.addSecurityHeader((java.security.cert.X509Certificate) keyStore.getCertificate("tigo"));

        Xml xml = req.getXml();
        Document document = xml.getDocument();

        /*
        WSSecHeader secHeader = new WSSecHeader();
        Element securityHeader = secHeader.insertSecurityHeader(document);
        xml.getRoot().appendChild(securityHeader);

        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(15);
        Document env = timestamp.build(document, secHeader);

        BinarySecurity bstToken = new X509Security(env);
        ((X509Security) bstToken).setX509Certificate((X509Certificate) keyStore.getCertificate("tigo"));
        Element element = bstToken.getElement();
        String textContent = element.getTextContent();

        xml = new Xml(env);

        System.out.println("TEXT="+textContent);
        */

        xml.write(System.out, true);

        SecureConsumer.consume(xml);
    }
}