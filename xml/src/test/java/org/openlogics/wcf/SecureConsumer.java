package org.openlogics.wcf;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.Closeables;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openlogics.xml.Xml;

import javax.xml.soap.SOAPException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

import static com.google.common.io.ByteStreams.toByteArray;

/**
 * @author Miguel Vega
 * @version $Id: SecureConsumer.java 0, 9/9/14 10:43 AM, @miguel $
 */
public class SecureConsumer {
    public static void consume(Xml xml) throws SOAPException, TransformerException {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(1000)
                .setConnectionRequestTimeout(3000)
                .setSocketTimeout(3000)
                .build();

        CloseableHttpClient httpClient = HttpClients.custom()
//                .setSSLSocketFactory(sslsf)
                .setDefaultRequestConfig(config)
                .build();


        HttpPost post = null;
        try {
            post = new HttpPost(new URI("http://190.181.4.251/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

//        post.setHeader("SOAPAction", "urn:SIN.Recaudacion.Facturacion.WsFacturacion.Repositorio.ContratoServicio.PruebaConexion");
//        post.setHeader("Content-Type", "application/soap+xml;charset=UTF-8;action=\"urn:SIN.Recaudacion.Facturacion.WsFacturacion.Repositorio.ContratoServicio.PruebaConexion\"");
        post.setHeader("Content-Type", "application/soap+xml");
        post.setHeader("Host", "s-nal-sfv-01.sin.gobierno.bo");

        StringWriter w = new StringWriter();
        xml.write(w, true, ImmutableMap.of(OutputKeys.OMIT_XML_DECLARATION, "yes"));

        String string = "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\"><s:Header><a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action><a:MessageID>urn:uuid:f5199bdb-43e7-41c1-b7ba-d09cc992a87a</a:MessageID><a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo><a:To s:mustUnderstand=\"1\">http://s-nal-sfv-01.sin.gobierno.bo/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure</a:To></s:Header><s:Body><t:RequestSecurityToken Context=\"uuid-3f135535-7b7c-4766-a99f-1010f72d4ec3-1\" xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\"><t:TokenType>http://schemas.xmlsoap.org/ws/2005/02/sc/sct</t:TokenType><t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType><t:KeySize>256</t:KeySize><t:BinaryExchange ValueType=\" http://schemas.xmlsoap.org/ws/2005/02/trust/tlsnego\" EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">FgMBAF4BAABaAwFUE0X0ltvSXVdhR1TasGxXgexGOK02nn3kKenN5B2iEwAAGAAvADUABQAKwBPAFMAJwAoAMgA4ABMABAEAABn/AQABAAAKAAYABAAXABgACwACAQAAIwAA</t:BinaryExchange></t:RequestSecurityToken></s:Body></s:Envelope>";

        string = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:a=\"http://www.w3.org/2005/08/addressing\">\n" +
                "  <soap:Header>\n" +
                "    <a:Action soap:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>\n" +
                "    <a:MessageID>urn:uuid:3f367da8-0b45-4ae2-b1f0-8c1b2662131a</a:MessageID>\n" +
                "    <a:ReplyTo>\n" +
                "      <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>\n" +
                "    </a:ReplyTo>\n" +
                "    <a:To soap:mustUnderstand=\"1\">http://s-nal-sfv-01.sin.gobierno.bo/SIN/Recaudacion/WsFacturacion/WsFacturacionContrato.svc/secure</a:To>\n" +
                "  </soap:Header>\n" +
                "  <soap:Body>\n" +
                "    <t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\" Context=\"uuid-558ef6c4-82f9-4efe-b7c5-75d25a3e69a3-1\">\n" +
                "      <t:TokenType>http://schemas.xmlsoap.org/ws/2005/02/sc/sct</t:TokenType>\n" +
                "      <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>\n" +
                "      <t:KeySize>256</t:KeySize>\n" +
                "      <t:BinaryExchange EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\" ValueType=\"http://schemas.xmlsoap.org/ws/2005/02/trust/tlsnego\">MIIFFTCCA/2gAwIBAgIQQHAVn9Xb0++Zd0mQ2TENDzANBgkqhkiG9w0BAQUFADCByzELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMTAwLgYDVQQLEydGb3IgVGVzdCBQdXJwb3NlcyBPbmx5LiAgTm8gYXNzdXJhbmNlcy4xQjBABgNVBAsTOVRlcm1zIG9mIHVzZSBhdCBodHRwczovL3d3dy52ZXJpc2lnbi5jb20vY3BzL3Rlc3RjYSAoYykwOTEtMCsGA1UEAxMkVmVyaVNpZ24gVHJpYWwgU2VjdXJlIFNlcnZlciBDQSAtIEcyMB4XDTE0MDgxOTAwMDAwMFoXDTE0MDkxODIzNTk1OVowcjELMAkGA1UEBhMCQk8xEzARBgNVBAgTClNhbnRhIENydXoxEzARBgNVBAcUClNhbnRhIENydXoxDTALBgNVBAoUBFRJR08xDDAKBgNVBAsUA0NTRDEcMBoGA1UEAxQTYXpvZ3Vlai50aWdvLm5ldC5ibzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIfXJzdohugZef1qZIhAf1lNiylFH7QvIIYNct6P2fiDeSEsNY/LAUa6MzPeZdtbqv1Tko+qSwGqL7SI8Mk6YHbqHPYusK6AG0b8ytfaLFpzxiQ9U4d+7d+XjUY/12Kie58sQpl6wyZnt8scYcrrGDwA50m8CAZMu3q/qqw1t+XnmBYZWxsZYxhvlcOzeyFJ0KgkM4G8u+32JNNTY3ZAHFOxvj3UflP4f5afd58l7QOY3ppVOE4SNICu/d0TMNSvFJtxQWcOGrTK1JlY3e1KRIxBoCZHLjDJxBUAjha7OITqgWjYKTurmFUauyzm00i61GCK3aa0b4smknN8DN6Ewm8CAwEAAaOCAUswggFHMB4GA1UdEQQXMBWCE2F6b2d1ZWoudGlnby5uZXQuYm8wCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBaAwKwYDVR0fBCQwIjAgoB6gHIYaaHR0cDovL3NxLnN5bWNiLmNvbS9zcS5jcmwwZQYDVR0gBF4wXDBaBgpghkgBhvhFAQcVMEwwIwYIKwYBBQUHAgEWF2h0dHBzOi8vZC5zeW1jYi5jb20vY3BzMCUGCCsGAQUFBwICMBkWF2h0dHBzOi8vZC5zeW1jYi5jb20vcnBhMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAfBgNVHSMEGDAWgBQoFxOKvdaitdwGLLe2jtoQZmBu5TA2BggrBgEFBQcBAQQqMCgwJgYIKwYBBQUHMAKGGmh0dHA6Ly9zcS5zeW1jYi5jb20vc3EuY3J0MA0GCSqGSIb3DQEBBQUAA4IBAQABZGjTrpRB7QPKUyjRZRPqR5VUI4OBZlogTN8RqddWlyEbvUoECP2yV2BAlggeYHWuDv6D/bgmP7UBZdRrNUn9jt8l4MeO6BIJHJCzhknPjfXkXVKcSz9SJvTCDSul2tjhYI/AWM4tY/KH3wjuj6QN3AObumRI0ODID5ZUqhqXvM/LJb0fR6K9O788f5/X3kuj0Gloawsa5semBXY4YLzVdGxaJOwm2Ds86PY0g4SmV7ue1mmDyjznFpzZFjzMbt53vWhlL9T9hh4pwgHd34dmTgLMUDNkeoavl52PXs5aV8Unm4pQXLNlw+t0qS0h493+gWmK1i3hpVWRCRsUgaoZ</t:BinaryExchange>\n" +
                "    </t:RequestSecurityToken>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

//        string = w.toString();

        post.setEntity(new StringEntity(string, ContentType.TEXT_XML));

        System.out.println("Request completo: '" + post.getRequestLine() + "'\n" + string);

        CloseableHttpResponse response;
        try {
            response = httpClient.execute(post);
        } catch (ClientProtocolException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        try {
            HttpEntity entity = response.getEntity();
            final InputStream inputStream = entity.getContent();

            byte[] bytes = toByteArray(inputStream);

            Closeables.closeQuietly(inputStream);
            String respuesta = new String(bytes);

            System.out.println("STATUS:" + response.getStatusLine().getStatusCode() + " <<<<" + respuesta);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
