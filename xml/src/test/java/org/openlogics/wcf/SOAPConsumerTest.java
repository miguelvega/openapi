package org.openlogics.wcf;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;
import org.junit.Test;
import org.openlogics.xml.NameSpace;
import org.openlogics.xml.SOAPDocument;
import org.openlogics.xml.Xml;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @author Miguel Vega
 * @version $Id: SOAPConsumerTest.java 0, January 27, 2015 10:56 PM mvega $
 */
public class SOAPConsumerTest {
    @Test
    public void testSimpleConsume() throws SOAPException, TransformerException, IOException {
        SOAPDocument soapDocument = new SOAPDocument();

        //estabilish the target namespace
        NameSpace jaxNS = new NameSpace("http://jaxws.openlogics.org/", "jax");
        soapDocument.declareNameSpace(jaxNS);

        //populate the body
        SOAPBody body = soapDocument.getBody();

        QName qname = soapDocument.buildOperationQName("getDate", jaxNS);
        SOAPElement getDateOp = body.addChildElement(qname);

        SOAPElement dateFormat = getDateOp.addChildElement("dateFormat");
        dateFormat.setTextContent("yyyy-MM-dd HH:mm:ss.zzzz");

        Xml xml = soapDocument.getXml();

        StringWriter w = new StringWriter();
        xml.write(w, true);
        write(w.toString(), "getDate");
    }

    private void write(String data, String soapAction) throws IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        HttpRequestFactory factory = httpTransport.createRequestFactory();

        HttpContent content = new UrlEncodedContent(data);

        HttpRequest httpRequest = factory.buildPostRequest(new GenericUrl("http://127.0.0.1:9999/olws?wsdl"), content);

        httpRequest.getHeaders().set("Content-Type", ImmutableList.of("text/xml"));
        httpRequest.getHeaders().set("SOAPAction", soapAction);
        httpRequest.getHeaders().set("Connection", "Keep-Alive");

        HttpResponse httpResponse = httpRequest.execute();

        InputStream inputStream = httpResponse.getContent();

        ByteArrayOutputStream to = new ByteArrayOutputStream();
        ByteStreams.copy(inputStream, to);

        inputStream.close();

        System.out.println(to.toString());
    }
}
