package org.openlogics.xml;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Miguel Vega
 * @version $Id: SAXTest.java 0, January 08, 2015 10:43 PM mvega $
 */
public class SAXTest extends DefaultHandler{
    @Test
    public void testParsing() throws ParserConfigurationException, SAXException {
        SAXParser parser = getSAXParser();
        InputStream in = null;
        try {

            InputSource inputSource = new InputSource( in = getClass().getResourceAsStream("/acclach/problem2A.xml") );

            parser.setProperty("http://xml.org/sax/properties/lexical-handler", this);
            parser.parse(inputSource, this);

            try {
                // cleanup - so that the parser can be reused.
                parser.setProperty("http://xml.org/sax/properties/lexical-handler", nullLexicalHandler);
            } catch (Exception e){
                // Ignore.
            }

            // only release the parser for reuse if there wasn't an
            // error.  While parsers should be reusable, don't trust
            // parsers that died to clean up appropriately.
            releaseSAXParser(parser);
        } catch (IOException e) {
            throw new SAXException(e);
        }finally {
            IOUtils.closeQuietly(in);
        }
    }

    private void releaseSAXParser(SAXParser saxParser) {

    }

    SAXParser getSAXParser() throws ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        return saxParser;
    }

    /** We only need one instance of this dummy handler to set into the parsers. */
    private static final NullLexicalHandler nullLexicalHandler = new NullLexicalHandler();

    /**
     * It is illegal to set the lexical-handler property to null. To facilitate
     * discarding the heavily loaded instance of DeserializationContextImpl from
     * the SAXParser instance that is kept in the Stack maintained by XMLUtils
     * we use this class.
     */
    private static class NullLexicalHandler implements LexicalHandler {
        public void startDTD(String arg0, String arg1, String arg2) throws SAXException {}
        public void endDTD() throws SAXException {}
        public void startEntity(String arg0) throws SAXException {}
        public void endEntity(String arg0) throws SAXException {}
        public void startCDATA() throws SAXException {}
        public void endCDATA() throws SAXException {}
        public void comment(char[] arg0, int arg1, int arg2) throws SAXException {}
    }
}
