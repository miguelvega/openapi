package org.openlogics.xml;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.transforms.Transforms;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;
import org.openlogics.acclach.Mensaje1A;
import org.openlogics.acclach.XmlMessageBuilder;
import org.openlogics.security.SecurityError;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.logging.Logger;

import static java.util.UUID.randomUUID;
import static javax.xml.transform.OutputKeys.ENCODING;
import static org.apache.xml.security.transforms.Transforms.TRANSFORM_C14N_OMIT_COMMENTS;
import static org.junit.Assert.assertTrue;

public class XmlSignerTest {

    private KeyStore keyStore;
    private X509Certificate certificate;
    private PrivateKey privateKey;
    private Logger LOG = Logger.getLogger("XMLTest");

    @Before
    public void init() {
        try {
            //noinspection AccessOfSystemProperties
            System.setProperty("org.apache.xml.security.ignoreLineBreaks", "true");

            org.apache.xml.security.Init.init();

            Security.addProvider(new BouncyCastleProvider());

            keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(getClass().getResourceAsStream("/keystore.p12"), "poipoipoi".toCharArray());
            this.certificate = (X509Certificate) keyStore.getCertificate("testing");
            this.privateKey = (PrivateKey) keyStore.getKey("testing", "poipoipoi".toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEnvelopedSignatureAndVerify() throws TransformerException, ParserConfigurationException, IOException, SAXException, XMLSecurityException {
        Xml xml = new Xml("Mike");
        xml.getRoot().appendChild(xml.elementBuilder().withIdentity("name").withText("miguel vega").build());


        XmlSigner signer = new XmlSigner(xml);
        signer.generateEnvelopedSignature(privateKey, certificate);

        StringWriter writer = new StringWriter();
        xml.write(writer);

        System.err.println(writer.toString());

        //validate

        /*
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setAttribute("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = documentBuilder.parse(new InputSource(new StringReader(writer.toString())));
        //retrieve the Signed Element
        NodeList signatures = document.getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);//xml.getRoot().getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);
        Element signatureElement = (Element) signatures.item(0);

        org.apache.xml.security.signature.XMLSignature signature = new org.apache.xml.security.signature.XMLSignature(
                signatureElement, null);

        boolean condition = signature.checkSignatureValue(certificate.getPublicKey());
        Assert.assertTrue(condition);
        */

        //not needed
        signer = new XmlSigner(new Xml(new StringReader(writer.toString())));
        assertTrue(signer.verifySignature(certificate.getPublicKey()));

    }

    @Test
    public void testEnvelopedSignature2AndVerify() throws XMLSecurityException, TransformerException, IOException, SAXException, ParserConfigurationException {
        Xml xml = new Xml("Mike");
        xml.getRoot().appendChild(xml.elementBuilder().withIdentity("name").withText("miguel vega").build());

        XmlSigner signer = new XmlSigner(xml);

        Transforms transforms = new Transforms(xml.getDocument());
        transforms.addTransform(TRANSFORM_C14N_OMIT_COMMENTS);

        final String objId = randomUUID().toString();
        final String referenceURI = '#' + objId;

        ObjectContainer oc = signer.buildObjectContainer(objId);

        oc.appendChild(xml.elementBuilder().withIdentity("num_orden_originante").withText("jos�").build());

        signer.generateEnvelopedSignature2(certificate, privateKey,
                SignatureMetadata.build().withTransforms(transforms).withReferenceURI(referenceURI).withKeyInfoKeyNames("1042"),
                oc);

        StringWriter w = new StringWriter();
        xml.write(w, ImmutableMap.of(ENCODING, "ISO-8859-1"));

        //xml.write(System.err, true, ImmutableMap.of(ENCODING, "ISO-8859-1"));
        System.err.println(w.toString());

        signer = new XmlSigner(new Xml(new StringReader(w.toString())));

        assertTrue(signer.verifySignature(certificate.getPublicKey()));
    }

    @Test
    public void testDocumentSIgnature() throws XMLSecurityException, SecurityError, TransformerException {
        Mensaje1A m1a = new Mensaje1A("51510800908", null, "BO", "LPZ",
                "8585", "BO", "LPZ", "1004", "BOB", "BOB", "BOB", BigDecimal.valueOf(266d),
                "200", "50", "CCAO", "CCAD", 0, new Date(), new Date(), "J9I+UsKg00CvgUKjUUD9m4z4NGi507A9QArmAsr7FPbnipKBPKO3ncPtnxaGiGSue20ISHM36flC617PMdsINrHB6bCWjVgv3JoJ8zaqyLWgXZN2+tHLxVCkldAzT6d22IN4/68yGtBLdCGjAgVvaJLZRyLDeHnOUxRgXZSHGqxcd1FAcFJqgri5O6PFVfFatunnQpc80L56OrQwerdl6dIIh0U/HuJmUyaW4go19/NSmu8/sTUi5LH2hyQiV+DTBmKDd7r/UsbOu1IDug1X6pZK2JTxw45kw0RUiUhS7wSQED1oe22BIMZdNQI6YDnJydPT59Y12tPgU5jQBw2oQg==",
                "FAx/9LlikvYCJgfClz6twgX+jBe8m6RprGbBY9+TiBJfF26534N0Weby3RYrzqphojn2fu+xJNN23hk8qxwPiyiXHTOmEQxUq6dcbWRrGzid4RQoE2gg/9i09LBLJW+0ck8Pp2C6Agw3iEzkpWBYBbgnIQRkb/GobKmwtaRTscuLbhESz9XxSJ2Lsb8MkiJ9d1RudtR2Uf1/vwiRtAC7ZnIm3s/NiqQ2E01sxnUP15PeG0oyOIA1d5dARlUt1ROrJ70QlcuEMPzKZ7X2D2ccAKnRe2+/hR5FdQ3dmPaiGC6ErrusdPWrYdFJ81VkuYpHD/hi54XFSI4zn2o7LPIolA==",
                null, null, null, "PRIVADO", "1020115028", "EMP.PUBL.SOCIAL DE AGUA Y SANEAMIENTO", "1234543",
                "juan jos�",
//                "NINA VARGAS NIEVES JULIETA",
                null, null, null, null, null);
        m1a.setCodCamara(Integer.valueOf(3));

        //Mensaje mensaje = mensaje1Axml.armaMensaje1A(m1a);

        Xml xml = XmlMessageBuilder.build1A("8584", m1a, new KeyPair(certificate.getPublicKey(), privateKey), ImmutableMap.<String, Object>of("RECIBE_ENCRIPTADO", "Y"));

        xml.write(System.out, true, ImmutableMap.of(ENCODING, "ISO-8859-1"));
    }
}