package org.openlogics.xml;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.crypto.*;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.logging.Logger;

import static org.apache.xml.security.transforms.Transforms.TRANSFORM_C14N_OMIT_COMMENTS;
import static org.apache.xml.security.utils.Constants._TAG_KEYNAME;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: XmlTest.java 0, 12/22/13 11:31 PM miguel $
 */
public class XmlTest {

    private KeyStore keyStore;
    private X509Certificate certificate;
    private PrivateKey privateKey;
    private Logger LOG = Logger.getLogger("XMLTest");

    @Before
    public void init() {
        try {
            //noinspection AccessOfSystemProperties
            System.setProperty("org.apache.xml.security.ignoreLineBreaks", "true");

            org.apache.xml.security.Init.init();

            Security.addProvider(new BouncyCastleProvider());

            keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(getClass().getResourceAsStream("/keystore.p12"), "poipoipoi".toCharArray());
            this.certificate = (X509Certificate) keyStore.getCertificate("testing");
            this.privateKey = (PrivateKey) keyStore.getKey("testing", "poipoipoi".toCharArray());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void testDomSource() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(getClass().getResourceAsStream("/acclach/tmp.xml"));
    }

    @Test
    public void testReadXml() throws IOException, SAXException, TransformerException {
        Xml xml = new Xml(getClass().getResourceAsStream("/order.xml"));

        xml.write(System.out, ImmutableMap.of(OutputKeys.OMIT_XML_DECLARATION, "yes"));

        Element element = (Element) xml.getRoot().getElementsByTagName("Item").item(0);

        Assert.assertEquals("130046593231", element.getAttribute("number"));
    }

    @Test
    public void testBuildSimpleXmlAndSign() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, XMLSecurityException, TransformerException, ParserConfigurationException, SAXException {
        Xml xml = new Xml("raiz");

        Element root = xml.getRoot();

        root.appendChild(xml.elementBuilder().withIdentity("Child-0").withText("A simple text content").build());

        root.appendChild(xml.elementBuilder().withIdentity("Name").addAttribute("age", "28").build());

        // Load the KeyStore and get the signing key and certificate.
//        KeyStore ks = KeyStore.getInstance("JKS");

        XmlSigner signer = new XmlSigner(xml);

        ObjectContainer oc = signer.buildObjectContainer();
        oc.setId("NOT_RANDOM_ID-8584");
        oc.appendChild(xml.elementBuilder().withIdentity("Inside").withText("Miguel").build());

        ObjectContainer oc1 = signer.buildObjectContainer();
        oc1.setId(UUID.randomUUID().toString());
        oc1.appendChild(xml.elementBuilder().withIdentity("Outside").withText("Vega").build());

        //xml.sign(cert, keyEntry.getPrivateKey(), null, transforms, null);


//        ImmutableList<String> objectItems = ImmutableList.of("One");
        ImmutableList<String> objectItems = ImmutableList.of("One", "Two", "Ith");

        Transforms transforms = new Transforms(xml.getDocument());
        transforms.addTransform(TRANSFORM_C14N_OMIT_COMMENTS);

        org.apache.xml.security.signature.XMLSignature signed = xml.sign(certificate, privateKey, objectItems, transforms, null, oc, oc1);

        //xml.getRoot().appendChild(signed.getElement());

        xml = new Xml(signed.getElement());

        //xml = new Xml(signed.getElement());

        String file = "/tmp/file.xml";


        StringWriter writer = new StringWriter();
        xml.write(writer);

        //xml.write(writer, true);

        /*
        OutputStream out = new FileOutputStream(file);
        xml.write(out);
        IOUtils.closeQuietly(out);
        */

//        System.out.println(writer.toString());


        //******************************************************************** try verify signature
        Document document;

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setAttribute("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        document = documentBuilder.parse(new InputSource(new StringReader(writer.toString())));
//        document = documentBuilder.parse(new InputSource(new FileInputStream(file)));


        //retrieve the Signed Element
        NodeList signatures = document.getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);//xml.getRoot().getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);
        Element signatureElement = (Element) signatures.item(0);

        //System.out.println(">>>" + org.openlogics.xml.XMLUtils.nodeToString(signed.getElement()));
        System.out.println(org.openlogics.xml.XMLUtils.nodeToString(signatureElement));

        //NodeList keyNameNodeList = signatureElement.getElementsByTagNameNS(SignatureSpecNS, _TAG_KEYNAME);
//        Assert.assertEquals(objectItems.size(), keyNameNodeList.getLength());

        Element keyName = (Element) signatureElement.getElementsByTagNameNS(Constants.SignatureSpecNS, _TAG_KEYNAME)
                .item(0);

        /*
        <ds:KeyName>One</ds:KeyName>
        <ds:KeyName>Two</ds:KeyName>
        <ds:KeyName>Ith</ds:KeyName>
        */

        org.apache.xml.security.signature.XMLSignature signature = new org.apache.xml.security.signature.XMLSignature(
                signatureElement,
                null
        );

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(getClass().getResourceAsStream("/keystore.p12"), "poipoipoi".toCharArray());
        java.security.cert.Certificate certificate = keyStore.getCertificate("testing");

        boolean condition = signature.checkSignatureValue(certificate.getPublicKey());
        Assert.assertTrue(condition);

        /*
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        //xml.write(System.out, true);
        xml.write(out, true);

        System.out.println(out.toString());
        */
    }

    @Test
    public void testSignature() throws ParserConfigurationException, XMLSecurityException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        DocumentBuilder db = dbf.newDocumentBuilder();

        org.w3c.dom.Document doc = db.newDocument();

        doc.appendChild(doc.createComment(" Comment before "));
        Element root = doc.createElementNS("", "RootElement");

        doc.appendChild(root);
        root.appendChild(doc.createTextNode("Some simple text\n"));

        Element canonElem = XMLUtils.createElementInSignatureSpace(doc, Constants._TAG_CANONICALIZATIONMETHOD);
        canonElem.setAttributeNS(
                null, Constants._ATT_ALGORITHM, Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS
        );

        SignatureAlgorithm signatureAlgorithm =
                new SignatureAlgorithm(doc, org.apache.xml.security.signature.XMLSignature.ALGO_ID_SIGNATURE_RSA);
        org.apache.xml.security.signature.XMLSignature sig =
                new org.apache.xml.security.signature.XMLSignature(doc, null, signatureAlgorithm.getElement(), canonElem);

        root.appendChild(sig.getElement());
        doc.appendChild(doc.createComment(" Comment after "));

        Transforms transforms = new Transforms(doc);
        transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
        transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);
        sig.addDocument("", transforms, Constants.ALGO_ID_DIGEST_SHA1);

        sig.addKeyInfo(certificate);
        sig.getKeyInfo().addKeyName("ASOBAN");

        sig.sign(privateKey);

        X509Certificate cert = sig.getKeyInfo().getX509Certificate();
        sig.checkSignatureValue(cert.getPublicKey());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLUtils.outputDOMc14nWithComments(doc, bos);

        System.out.println(new String(bos.toByteArray()));
    }

    @Test
    @Ignore("can not run from mvn test")
    public void doVerify() throws Exception {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        DocumentBuilder db = dbf.newDocumentBuilder();

        org.w3c.dom.Document doc = db.parse(getClass().getResourceAsStream("/tmp.xml"));

//        XPathFactory xpf = XPathFactory.newInstance();
//        XPath xpath = xpf.newXPath();
//        xpath.setNamespaceContext(new DSNamespaceContext());

        String expression = "//ds:Signature[1]";
//        Element sigElement = (Element) xpath.evaluate(expression, doc, XPathConstants.NODE);

        Element sigElement = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);

        org.apache.xml.security.signature.XMLSignature signature = new org.apache.xml.security.signature.XMLSignature(doc.getDocumentElement(), "");
        org.apache.xml.security.keys.KeyInfo ki = signature.getKeyInfo();

        if (ki == null) {
            throw new RuntimeException("No keyinfo");
        }

        //as we didn't write the public key in XML SAMPLE, then need to provide the PUBLIC KEY manually
        //PublicKey pk = signature.getKeyInfo().getPublicKey();
        PublicKey pk = certificate.getPublicKey();

        if (pk == null) {
            throw new RuntimeException("No public key");
        }

        assertTrue(signature.checkSignatureValue(pk));
    }

    @Test
    public void testBuildSimpleXml() throws TransformerException {
        Xml xml = new Xml("root");

        Element root = xml.getRoot();

        root.appendChild(xml.elementBuilder().withIdentity("Child-0").withText("A simple text content").build());

        root.appendChild(xml.elementBuilder().withIdentity("Name").addAttribute("age", "28").build());

        root.appendChild(xml.elementBuilder().withIdentity("Code").withText(null).build());

        xml.write(System.out, true);
    }

    @Test
    @Ignore
    public void testBuildNSXml() throws TransformerException {
        NameSpace ol = new NameSpace("http://www.openlogics.com", "ol");

        //root will belong to the given namespace
        Xml xml = new Xml("root", new NameSpace("http://www.w3.org/2001/XMLSchema-instance", "xsi")).addNameSpace(ol);

        //if root shuldn't belong to namespace, simply add namespace out of the constructor

        Element root = xml.getRoot();

        root.appendChild(xml.elementBuilder().withIdentity("Child-0").withText("A simple text content").build());

        root.appendChild(xml.elementBuilder().withIdentity("Name", ol).addAttribute("age", "28").addAttribute("id", "8584").build());

        Element elementById = xml.getElementById(root, "8584");

        System.out.println(">>>>" + elementById);

        xml.write(System.out, true);
    }

    @Test
    public void testSignWithKeystore() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, UnrecoverableEntryException, KeyStoreException, IOException, CertificateException, ParserConfigurationException, SAXException, MarshalException, XMLSignatureException, TransformerException {
        // Create a DOM XMLSignatureFactory that will be used to
        // generate the enveloped signature.
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case,
        // you are signing the whole document, so a URI of "" signifies
        // that, and also specify the SHA1 digest algorithm and
        // the ENVELOPED Transform.
        Reference ref = fac.newReference
                ("", fac.newDigestMethod(DigestMethod.SHA1, null),
                        Collections.singletonList
                                (fac.newTransform
                                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                        null, null);

        // Create the SignedInfo.
        SignedInfo si = fac.newSignedInfo
                (fac.newCanonicalizationMethod
                                (CanonicalizationMethod.INCLUSIVE,
                                        (C14NMethodParameterSpec) null),
                        fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                        Collections.singletonList(ref));

        // Load the KeyStore and get the signing key and certificate.
//        KeyStore ks = KeyStore.getInstance("JKS");
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(getClass().getResourceAsStream("/keystore.p12"), "poipoipoi".toCharArray());
        KeyStore.PrivateKeyEntry keyEntry =
                (KeyStore.PrivateKeyEntry) ks.getEntry
                        ("testing", new KeyStore.PasswordProtection("poipoipoi".toCharArray()));
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        // Create the KeyInfo containing the X509Data.
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        List x509Content = new ArrayList();
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(cert);
        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));


        // Instantiate the document to be signed.
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc = dbf.newDocumentBuilder().parse(getClass().getResourceAsStream("/order.xml"));

        // Create a DOMSignContext and specify the RSA PrivateKey and
        // location of the resulting XMLSignature's parent withIdentity.
        DOMSignContext dsc = new DOMSignContext
                (keyEntry.getPrivateKey(), doc.getDocumentElement());

        // Create the XMLSignature, but don't sign it yet.
        XMLSignature signature = fac.newXMLSignature(si, ki);

        // Marshal, generate, and sign the enveloped signature.
        signature.sign(dsc);

        // Output the resulting document.
        OutputStream os = new FileOutputStream("/tmp/keystore-signedPurchaseOrder.xml");
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));


        //        Code Sample 5
        // Find Signature withIdentity.
        NodeList nl =
                doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (nl.getLength() == 0) {
            throw new IllegalStateException("Cannot find Signature withIdentity");
        }

// Create a DOMValidateContext and specify a KeySelector
// and document context.
        DOMValidateContext valContext = new DOMValidateContext
                (new X509KeySelector(), nl.item(0));

// Unmarshal the XMLSignature.
        signature = fac.unmarshalXMLSignature(valContext);

// Validate the XMLSignature.
        boolean coreValidity = signature.validate(valContext);

        //        Code Sample 6
        // Check core validation status.
        if (coreValidity == false) {
            System.err.println("Signature failed core validation");
            boolean sv = signature.getSignatureValue().validate(valContext);
            System.out.println("signature validation status: " + sv);
            if (sv == false) {
                // Check the validation status of each Reference.
                Iterator i = signature.getSignedInfo().getReferences().iterator();
                for (int j = 0; i.hasNext(); j++) {
                    boolean refValid = ((Reference) i.next()).validate(valContext);
                    System.out.println("ref[" + j + "] validity status: " + refValid);
                }
            }
        } else {
            System.out.println("Signature passed core validation");
        }

    }

    /*
    @Test
    public void testVerificar() {

        boolean valid = true;
        InputStream in = getClass().getResourceAsStream("/1A.xml");
        try {

            Xml xml = new Xml(getClass().getResourceAsStream(""));
            Document doc = xml.getDocument();


            URL url = getClass().getResource("/repositorio.jks");
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(url.openStream(), "poipoipoi".toCharArray());

            java.security.cert.Certificate cer = ks.getCertificate("pruebas.certificado");

            Map<String, java.security.cert.Certificate> pubring = new HashMap<String, java.security.cert.Certificate>();
            pubring.put("1426001", cer);
            pubring.put("1500", cer);
            pubring.put("1009", cer);

            NodeList signatures = doc.getElementsByTagNameNS(Constants.SignatureSpecNS, "Signature");
            if (signatures.getLength() < 1) {
                System.err.println("El presente mensaje no contiene firma alguna");
                return;
            }

            //LOG.info("Total firmas encontradas '{}'", signatures.getLength());
            for (int i = 0; i < signatures.getLength(); i++) {
                Element sigElement = (Element) signatures.item(i);
                OutputStream out = new WriterOutputStream();
                XMLUtils.ElementToStream(sigElement, out);
                LOG.info("Parte de la firma a vericar\n{}"+out);

                XMLSignature signature = new XMLSignature(sigElement, "");
                String keyName = signature.getKeyInfo().itemKeyName(0).getKeyName();
                Certificate certificado = pubring.get(keyName);
                if (certificado == null) {
                    String msg = "No tengo el certificado publico de uno de los firmantes...";
                    LOG.log(Level.SEVERE, msg);
                    fail(msg);
                    return;
                }

                PublicKey pk = certificado.getPublicKey();
                X509Certificate x509 = X509Certificate.getInstance(cer.getEncoded());
                Date notBefore = x509.getNotBefore();
                Date notAfter = x509.getNotAfter();

                LOG.debug("Certificado de '{}' valido desde '{}' hasta '{}'",
                        new Object[]{keyName, sdf.format(notBefore), sdf.format(notAfter)});

                try {
                    // Primero verificamos si el certificado esta caduco
                    x509.checkValidity();
                } catch (CertificateNotYetValidException e) {
                    String msg = "Certificado del participante '" + keyName + "' no es valido aún";
                    LOG.error(msg, e);
                    fail(msg);
                    return;
                } catch (CertificateExpiredException e) {
                    String msg = "Certificado del participante '" + keyName + "' ha expirado";
                    LOG.error(msg, e);
                    fail(msg);
                    return;
                }

                valid &= signature.checkSignatureValue(pk);
            }

            if (valid) {
                LOG.info("Firma digital valida!!");
                return;
            } else {
                String msg = "Firma digital INVALIDA!!";
                LOG.info(msg);
                fail(msg);
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            fail("No se pudo verificar la firma");
        } finally {
            IOUtils.closeQuietly(in);
        }
        LOG.error("El mensaje contiene una firma invalida");

    }
    */

    @Test
    public void testSignAutogeneratedKey() throws InstantiationException, IllegalAccessException, ClassNotFoundException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, KeyException, MarshalException, XMLSignatureException,
            IOException, TransformerException, ParserConfigurationException, SAXException {


        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(getClass().getResourceAsStream("/order.xml"));

        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        doc.getDocumentElement().normalize();

        String providerName = System.getProperty("jsr105Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");

        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

        DigestMethod digestMethod = fac.newDigestMethod(DigestMethod.SHA256, null);
        Transform transform = fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null);
        Reference reference = fac.newReference("", digestMethod, Collections.singletonList(transform), null, null);
        SignatureMethod signatureMethod = fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
        CanonicalizationMethod canonicalizationMethod = fac.newCanonicalizationMethod(
                CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo(canonicalizationMethod, signatureMethod, Collections.singletonList(reference));

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);

        KeyPair kp = kpg.generateKeyPair();

        KeyInfoFactory kif = fac.getKeyInfoFactory();
        KeyValue kv = kif.newKeyValue(kp.getPublic());

        // Create a KeyInfo and add the KeyValue to it
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(kv));
        DOMSignContext dsc = new DOMSignContext(kp.getPrivate(), doc.getDocumentElement());

        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();

        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        // output the resulting document
        OutputStream os;

        os = new FileOutputStream("/tmp/autogen-signedPurchaseOrder.xml");

        trans.transform(new DOMSource(doc), new StreamResult(os));
    }

    public static class X509KeySelector extends KeySelector {
        static boolean algEquals(String algURI, String algName) {
            if ((algName.equalsIgnoreCase("DSA") &&
                    algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) ||
                    (algName.equalsIgnoreCase("RSA") &&
                            algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1))) {
                return true;
            } else {
                return false;
            }
        }

        public KeySelectorResult select(KeyInfo keyInfo,
                                        KeySelector.Purpose purpose,
                                        AlgorithmMethod method,
                                        XMLCryptoContext context)
                throws KeySelectorException {
            Iterator ki = keyInfo.getContent().iterator();
            while (ki.hasNext()) {
                XMLStructure info = (XMLStructure) ki.next();
                if (!(info instanceof X509Data))
                    continue;
                X509Data x509Data = (X509Data) info;
                Iterator xi = x509Data.getContent().iterator();
                while (xi.hasNext()) {
                    Object o = xi.next();
                    if (!(o instanceof X509Certificate))
                        continue;
                    final PublicKey key = ((X509Certificate) o).getPublicKey();
                    // Make sure the algorithm is compatible
                    // with the method.
                    if (algEquals(method.getAlgorithm(), key.getAlgorithm())) {
                        return new KeySelectorResult() {
                            public Key getKey() {
                                return key;
                            }
                        };
                    }
                }
            }
            throw new KeySelectorException("No key found!");
        }
    }

    private class WriterOutputStream extends OutputStream {

        private StringBuilder sb;

        public WriterOutputStream() {
            sb = new StringBuilder();
        }

        @Override
        public void write(int b) throws IOException {
            sb.append((char) b);
        }

        @Override
        public String toString() {
            return sb.toString();
        }
    }

}
