package org.openlogics.xml;

import org.junit.Test;

import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Iterator;

public class SimpleSOAPDocumentTest {

    @Test
    public void tesSOAPMessage() throws SOAPException, IOException {
        final MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage message = messageFactory.createMessage();

        message.writeTo(System.out);
    }

    @Test
    public void createSimple() throws SOAPException, TransformerException {
        SOAPDocument req = new SOAPDocument();

        NameSpace xns = new NameSpace("http://www.w3.org/ui", "xui");
        NameSpace ans = new NameSpace("http://www.w3.org/ai", "ai");
        req.declareNameSpace(xns);
        req.declareNameSpace(ans);

        //configure the header
        req.createBodyElement().withLocalName(xns, "Action").addAttribute(ans, "mustUnderstand", "1").withText("A GUI");

        Xml xml = req.getXml();

        xml.write(System.out, true);
    }

    @Test
    public void testSOAPFault() throws SOAPException, IOException {
        MessageFactory msgFactory = MessageFactory.newInstance();
        SOAPMessage msg = msgFactory.createMessage();
        SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
        SOAPBody body = envelope.getBody();
        SOAPFault fault = body.addFault();

        fault.setFaultCode("Client");
        fault.setFaultString(
                "Message does not have necessary info");
        fault.setFaultActor("http://gizmos.com/order");

        Detail detail = fault.addDetail();

        Name entryName = envelope.createName("order", "PO",
                "http://gizmos.com/orders/");
        DetailEntry entry = detail.addDetailEntry(entryName);
        entry.addTextNode(
                "quantity element does not have a value");

        Name entryName2 = envelope.createName("confirmation",
                "PO", "http://gizmos.com/confirm");
        DetailEntry entry2 = detail.addDetailEntry(entryName2);
        entry2.addTextNode("Incomplete address: no zip code");

        msg.saveChanges();

        // Now retrieve the SOAPFault object and its contents
        //after checking to see that there is one

        if (body.hasFault()) {
            fault = body.getFault();
            String code = fault.getFaultCode();
            String string = fault.getFaultString();
            String actor = fault.getFaultActor();

            System.out.println("SOAP fault contains: ");
            System.out.println("    fault code = " + code);
            System.out.println("    fault string = " + string);
            if (actor != null) {
                System.out.println("    fault actor = " + actor);
            }

            detail = fault.getDetail();
            if (detail != null) {
                Iterator it = detail.getDetailEntries();
                while (it.hasNext()) {
                    entry = (DetailEntry) it.next();
                    String value = entry.getValue();
                    System.out.println("    Detail entry = " + value);
                }
            }
        }

        msg.writeTo(System.out);

    }
}