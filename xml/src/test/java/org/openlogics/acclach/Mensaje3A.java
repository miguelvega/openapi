package org.openlogics.acclach;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar
 * Ivonne 09/13
 * Date: 6/2/13
 * Time: 2:21 PM
 */
public class Mensaje3A implements Serializable {

    private String numOrdenAch;
    private String numOrdenOriginante;
    private String numOrdenDestinatario;
    private String codPaisDestinatario;
    private String codSucursalDestinatario;
    private String codRespuesta;
    private String tipoOrden;
    private String titularDestinatario;

    public Mensaje3A() {

    }

    public Mensaje3A(String numOrdenAch, String numOrdenOriginante, String numOrdenDestinatario, String codPaisDestinatario,
                     String codSucursalDestinatario, String codRespuesta, String tipoOrden, String titularDestinatario) {
        this.numOrdenAch = numOrdenAch;
        this.numOrdenOriginante = numOrdenOriginante;
        this.numOrdenDestinatario = numOrdenDestinatario;
        this.codPaisDestinatario = codPaisDestinatario;
        this.codSucursalDestinatario = codSucursalDestinatario;
        this.codRespuesta = codRespuesta;
        this.tipoOrden = tipoOrden;
        this.titularDestinatario = titularDestinatario;
    }

    public String getNumOrdenAch() {
        return numOrdenAch;
    }

    public void setNumOrdenAch(String numOrdenAch) {
        this.numOrdenAch = numOrdenAch;
    }

    public String getNumOrdenOriginante() {
        return numOrdenOriginante;
    }

    public void setNumOrdenOriginante(String numOrdenOriginante) {
        this.numOrdenOriginante = numOrdenOriginante;
    }

    public String getNumOrdenDestinatario() {
        return numOrdenDestinatario;
    }

    public void setNumOrdenDestinatario(String numOrdenDestinatario) {
        this.numOrdenDestinatario = numOrdenDestinatario;
    }

    public String getCodPaisDestinatario() {
        return codPaisDestinatario;
    }

    public void setCodPaisDestinatario(String codPaisDestinatario) {
        this.codPaisDestinatario = codPaisDestinatario;
    }

    public String getCodSucursalDestinatario() {
        return codSucursalDestinatario;
    }

    public void setCodSucursalDestinatario(String codSucursalDestinatario) {
        this.codSucursalDestinatario = codSucursalDestinatario;
    }

    public String getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getTipoOrden() {
        return tipoOrden;
    }

    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    public String getTitularDestinatario() {
        return titularDestinatario;
    }

    public void setTitularDestinatario(String titularDestinatario) {
        this.titularDestinatario = titularDestinatario;
    }
}

