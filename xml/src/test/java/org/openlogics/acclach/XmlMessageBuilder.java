package org.openlogics.acclach;

import com.google.common.base.Optional;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.openlogics.security.Ciphers;
import org.openlogics.security.SecurityError;
import org.openlogics.xml.SignatureMetadata;
import org.openlogics.xml.Xml;
import org.openlogics.xml.XmlSigner;
import org.w3c.dom.Element;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static java.util.UUID.randomUUID;
import static org.apache.xml.security.transforms.Transforms.TRANSFORM_C14N_OMIT_COMMENTS;

/**
 * @author Miguel Vega
 * @version $Id: XmlMessageBuilder.java 0, 1/9/14 6:18 PM miguel $
 */
public class XmlMessageBuilder {

    final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Builds an XML representing the 3A message
     *
     * @param mensaje3A
     * @return
     */
    public static Xml build3A(String codParticipante, Mensaje3A mensaje3A, PrivateKey privateKey, Map<String, Object> params) throws XMLSecurityException {
        Xml xml = new Xml("dummy");

        Element destAch = xml.elementBuilder().withIdentity("Destinatario_ACH").build();

        destAch.appendChild(xml.elementBuilder().withIdentity("num_orden_originante").withText(mensaje3A.getNumOrdenOriginante()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("num_orden_ach").withText(mensaje3A.getNumOrdenAch()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("tip_orden").withText(mensaje3A.getTipoOrden()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("cod_sucursal_destinatario").withText(mensaje3A.getCodSucursalDestinatario()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("cod_pais_destinatario").withText(mensaje3A.getCodPaisDestinatario()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("cod_respuesta").withText(mensaje3A.getCodRespuesta()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("num_orden_destinatario").withText(mensaje3A.getNumOrdenDestinatario()).build());
        destAch.appendChild(xml.elementBuilder().withIdentity("titular_destinatario").withText(mensaje3A.getTitularDestinatario()).build());


        //final String objId = codParticipante+" "+new Date();
        final String objId = randomUUID().toString();
        final String referenceURI = '#' + objId;
        XmlSigner signer = new XmlSigner(xml);
        ObjectContainer oc = signer.buildObjectContainer(objId);

        oc.appendChild(destAch);

        //XMLSignature signature = xml.sign(privateKey, ImmutableList.of(codParticipante), transforms, referenceURI, oc);

        // Transformaciones
        Transforms transforms = new Transforms(xml.getDocument());
        transforms.addTransform(TRANSFORM_C14N_OMIT_COMMENTS);

        XMLSignature signature = signer.generateEnvelopedSignature2(privateKey, SignatureMetadata.build().withTransforms(transforms).withReferenceURI(referenceURI).
                withKeyInfoKeyNames(codParticipante).
                withCanonicalizationMethodURI(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS).
                withSignatureMethodURI(XMLSignature.ALGO_ID_SIGNATURE_RSA), oc);

        return new Xml(signature.getElement());
    }

    public static Xml build1A(String codParticipante, Mensaje1A mensaje1A, KeyPair keyPair, Map<String, Object> params) throws XMLSecurityException, SecurityError {

        assert params != null && codParticipante != null && mensaje1A != null && keyPair != null;

        Xml xml = new Xml("raiz");

        Element origAch = xml.elementBuilder().withIdentity("Originante_ACH").build();

        origAch.appendChild(xml.elementBuilder().withIdentity("num_orden_originante").withText(mensaje1A.getNumOrdenOriginante()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_alfanumerico_originante").withText(mensaje1A.getCodAlfaNumOrig()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_pais_originante").withText(mensaje1A.getCodPaisOriginante()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_sucursal_originante").withText(mensaje1A.getCodSucursalOriginante()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_destinatario").withText(mensaje1A.getCodDestinatario()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_pais_destinatario").withText(mensaje1A.getCodPaisDestinatario()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_sucursal_destinatario").withText(mensaje1A.getCodSucursalDestino()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_compensador").withText(mensaje1A.getCodCompensador()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_moneda_destinatario").withText(mensaje1A.getCodMonedaDestino()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_moneda_originante").withText(mensaje1A.getCodMonedaOrigen()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_moneda").withText(mensaje1A.getCodMoneda()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("importe").withText(mensaje1A.getImporte().toPlainString()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("tipo_documento").withText(mensaje1A.getTipoDocumento()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("tip_orden").withText(mensaje1A.getTipoOrden()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_procedimiento").withText(mensaje1A.getCodProcedimiento()).build());

        String codCamara = mensaje1A.getCodCamara() != null ? mensaje1A.getCodCamara().toString() : null;
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_camara").withText(codCamara).build());

        origAch.appendChild(xml.elementBuilder().withIdentity("tip_cuenta_origen").withText(mensaje1A.getTipoCuentaOrigen()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("tip_cuenta_destino").withText(mensaje1A.getTipoCuentaDestino()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("fec_camara").withText(toACCLDate(mensaje1A.getFechaCamara())).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("fec_envio").withText(toACCLDate(mensaje1A.getFechaEnvio())).build());

        String encriptado = Optional.fromNullable((String) params.get("RECIBE_ENCRIPTADO")).or("N");

        if ("N".equalsIgnoreCase(encriptado.trim())) {

            String cuentaOrigenEncriptada = Ciphers.doEncrypt(mensaje1A.getCuentaOrigen(), keyPair.getPublic()).toString();
            origAch.appendChild(xml.elementBuilder().withIdentity("cuenta_origen").withText(cuentaOrigenEncriptada).build());

            String cuentaDestinoEncriptada = Ciphers.doEncrypt(mensaje1A.getCuentaDestino(), keyPair.getPublic()).toString();
            origAch.appendChild(xml.elementBuilder().withIdentity("cuenta_destino").withText(cuentaDestinoEncriptada).build());
        } else {
            origAch.appendChild(xml.elementBuilder().withIdentity("cuenta_origen").withText(mensaje1A.getCuentaOrigen()).build());
            origAch.appendChild(xml.elementBuilder().withIdentity("cuenta_destino").withText(mensaje1A.getCuentaDestino()).build());
        }

        origAch.appendChild(xml.elementBuilder().withIdentity("ci_nit_originante").withText(mensaje1A.getCiNitOriginante()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("titular_originante").withText(mensaje1A.getTitularOriginate()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("ci_nit_destinatario").withText(mensaje1A.getCiNitDestinatario()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("titular_destinatario").withText(mensaje1A.getTitularDestinatario()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("num_originante_antiguo").withText(mensaje1A.getNumOriginanteAntiguo()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("iso8583").withText(mensaje1A.getIso8583()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("glosa").withText(mensaje1A.getGlosa()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_sub_originante").withText(mensaje1A.getCodSubOriginante()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("cod_sub_destinatario").withText(mensaje1A.getCodSubDestinatario()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("origen_fondos").withText(mensaje1A.getOrigenFondos()).build());
        origAch.appendChild(xml.elementBuilder().withIdentity("destino_fondos").withText(mensaje1A.getDestinoFondos()).build());


        XmlSigner signer = new XmlSigner(xml);
        // Transformaciones
        Transforms transforms = new Transforms(xml.getDocument());
        transforms.addTransform(TRANSFORM_C14N_OMIT_COMMENTS);
//        transforms.addTransform(TRANSFORM_C14N11_OMIT_COMMENTS);
//        transforms.addTransform(TRANSFORM_C14N_EXCL_OMIT_COMMENTS);

        final String objId = codParticipante + " " + new Date();
        final String referenceURI = '#' + objId;

        ObjectContainer oc = signer.buildObjectContainer(objId);

        oc.appendChild(origAch);

        //xml.sign(privateKey, ImmutableList.of(codParticipante), transforms, referenceURI, oc);

        //Using SHA1
        /*
        signer.generateEnvelopedSignature2(keyPair.getPrivate(),
                SignatureMetadata.build().withTransforms(transforms).withReferenceURI(referenceURI).
                        withKeyInfoKeyNames(codParticipante).
                        withCanonicalizationMethodURI(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS).
                        withSignatureMethodURI(XMLSignature.ALGO_ID_SIGNATURE_RSA),
                oc);
        */

        //Using SHA256
        signer.generateEnvelopedSignature2(keyPair.getPrivate(),
                SignatureMetadata.build().
                        withTransforms(transforms).
                        withReferenceURI(referenceURI).
                        withKeyInfoKeyNames(codParticipante).
                        withCanonicalizationMethodURI(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS).
                        withDigestURI(MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA256).
                withSignatureMethodURI(XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA256),
                oc);

        xml.getRoot().appendChild(xml.elementBuilder().withIdentity("etc").withText(mensaje1A.getEtc()).build());

        return xml;
    }

    private static String toACCLDate(Date date) {

        if (date != null) {
            return DATE_FORMAT.format(date);
        } else {
            return null;
        }
    }
}
