package org.openlogics.acclach;


import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Created with IntelliJ IDEA.
 * User: Ariel Escobar
 * Date: 5/31/13
 * Time: 3:27 PM
 */

public class Mensaje1A implements Serializable{

    private String numOrdenOriginante;
    private String codAlfaNumOrig;
    private String codPaisOriginante;
    private String codSucursalOriginante;
    private String codDestinatario;
    private String codPaisDestinatario;
    private String codSucursalDestino;
    private String codCompensador;
    private String codMonedaDestino;
    private String codMonedaOrigen;
    private String codMoneda;
    private BigDecimal importe;
    private String tipoDocumento;
    private String tipoOrden;
    private String codProcedimiento;
    private String tipoCuentaOrigen;
    private String tipoCuentaDestino;
    private Integer codCamara;
    private Date fechaCamara;
    private Date fechaEnvio;
    private String cuentaOrigen;
    private String cuentaDestino;
    private String ciNitOriginante;
    private String titularOriginate;
    private String ciNitDestinatario;
    private String titularDestinatario;
    private String numOriginanteAntiguo;
    private String iso8583;
    private String glosa;
    private String codSubOriginante;
    private String codSubDestinatario;
    private String origenFondos;
    private String destinoFondos;
    private String etc;


    public Mensaje1A() {
    }

    public Mensaje1A(String numOrdenOriginante, String codAlfaNumOrig, String codPaisOriginante, String codSucursalOriginante, String codDestinatario,
                     String codPaisDestinatario, String codSucursalDestino, String codCompensador, String codMonedaDestino, String codMonedaOrigen,
                     String codMoneda, BigDecimal importe, String tipoOrden, String codProcedimiento, String tipoCuentaOrigen,
                     String tipoCuentaDestino, Integer codCamara, Date fechaCamara, Date fechaEnvio, String cuentaOrigen, String cuentaDestino,
                     String numOriginanteAntiguo, String iso8583, String glosa, String tipoDocumento,
                     String ciNitOriginante, String titularOriginate, String ciNitDestinatario, String titularDestinatario,
                     String codSubOriginante, String codSubDestinatario, String origenFondos, String destinoFondos, String etc) {
        this.numOrdenOriginante = numOrdenOriginante;
        this.codAlfaNumOrig = codAlfaNumOrig;
        this.codPaisOriginante = codPaisOriginante;
        this.codSucursalOriginante = codSucursalOriginante;
        this.codDestinatario = codDestinatario;
        this.codPaisDestinatario = codPaisDestinatario;
        this.codSucursalDestino = codSucursalDestino;
        this.codCompensador = codCompensador;
        this.codMonedaDestino = codMonedaDestino;
        this.codMonedaOrigen = codMonedaOrigen;
        this.codMoneda = codMoneda;
        this.importe = importe;
        this.tipoOrden = tipoOrden;
        this.codProcedimiento = codProcedimiento;
        this.tipoCuentaOrigen = tipoCuentaOrigen;
        this.tipoCuentaDestino = tipoCuentaDestino;
        this.codCamara = codCamara;
        this.fechaCamara = fechaCamara;
        this.fechaEnvio = fechaEnvio;
        this.cuentaOrigen = cuentaOrigen;
        this.cuentaDestino = cuentaDestino;
        this.numOriginanteAntiguo = numOriginanteAntiguo;
        this.iso8583 = iso8583;
        this.glosa = glosa;
        this.tipoDocumento = tipoDocumento;
        this.ciNitOriginante = ciNitOriginante;
        this.titularOriginate = titularOriginate;
        this.ciNitDestinatario = ciNitDestinatario;
        this.titularDestinatario = titularDestinatario;
        this.codSubOriginante = codSubOriginante;
        this.codSubDestinatario = codSubDestinatario;
        this.origenFondos = origenFondos;
        this.destinoFondos = destinoFondos;
        this.etc = etc;
    }
    @XmlElement(required=true)
    public String getNumOrdenOriginante() {
        return numOrdenOriginante;
    }

    public void setNumOrdenOriginante(String numOrdenOriginante) {
        this.numOrdenOriginante = numOrdenOriginante;
    }

    public String getCodAlfaNumOrig() {
        return codAlfaNumOrig;
    }

    public void setCodAlfaNumOrig(String codAlfaNumOrig) {
        this.codAlfaNumOrig = codAlfaNumOrig;
    }

    @XmlElement(required=true)
    public String getCodPaisOriginante() {
        return codPaisOriginante;
    }

    public void setCodPaisOriginante(String codPaisOriginante) {
        this.codPaisOriginante = codPaisOriginante;
    }

    @XmlElement(required=true)
    public String getCodSucursalOriginante() {
        return codSucursalOriginante;
    }

    public void setCodSucursalOriginante(String codSucursalOriginante) {
        this.codSucursalOriginante = codSucursalOriginante;
    }

    @XmlElement(required=true)
    public String getCodDestinatario() {
        return codDestinatario;
    }

    public void setCodDestinatario(String codDestinatario) {
        this.codDestinatario = codDestinatario;
    }

    @XmlElement(required=true)
    public String getCodPaisDestinatario() {
        return codPaisDestinatario;
    }

    public void setCodPaisDestinatario(String codPaisDestinatario) {
        this.codPaisDestinatario = codPaisDestinatario;
    }

    //@XmlElement(required=true)
    public String getCodSucursalDestino() {
        return codSucursalDestino;
    }

    public void setCodSucursalDestino(String codSucursalDestino) {
        this.codSucursalDestino = codSucursalDestino;
    }

    @XmlElement(required=true)
    public String getCodCompensador() {
        return codCompensador;
    }

    public void setCodCompensador(String codCompensador) {
        this.codCompensador = codCompensador;
    }

    public String getCodMonedaDestino() {
        return codMonedaDestino;
    }

    public void setCodMonedaDestino(String codMonedaDestino) {
        this.codMonedaDestino = codMonedaDestino;
    }

    public String getCodMonedaOrigen() {
        return codMonedaOrigen;
    }

    public void setCodMonedaOrigen(String codMonedaOrigen) {
        this.codMonedaOrigen = codMonedaOrigen;
    }

    @XmlElement(required=true)
    public String getCodMoneda() {
        return codMoneda;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    @XmlElement(required=true)
    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @XmlElement(required=true)
    public String getTipoOrden() {
        return tipoOrden;
    }

    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    @XmlElement(required=true)
    public String getCodProcedimiento() {
        return codProcedimiento;
    }

    public void setCodProcedimiento(String codProcedimiento) {
        this.codProcedimiento = codProcedimiento;
    }

    @XmlElement(required=true)
    public String getTipoCuentaOrigen() {
        return tipoCuentaOrigen;
    }

    public void setTipoCuentaOrigen(String tipoCuentaOrigen) {
        this.tipoCuentaOrigen = tipoCuentaOrigen;
    }

    @XmlElement(required=true)
    public String getTipoCuentaDestino() {
        return tipoCuentaDestino;
    }

    public void setTipoCuentaDestino(String tipoCuentaDestino) {
        this.tipoCuentaDestino = tipoCuentaDestino;
    }

    @XmlElement(required=true)
    public Integer getCodCamara() {
        return codCamara;
    }

    public void setCodCamara(Integer codCamara) {
        this.codCamara = codCamara;
    }

    @XmlElement(required=true)
    public Date getFechaCamara() {
        return fechaCamara;
    }

    public void setFechaCamara(Date fechaCamara) {
        this.fechaCamara = fechaCamara;
    }

    @XmlElement(required=true)
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @XmlElement(required=true)
    public String getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(String cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    @XmlElement(required=true)
    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public String getNumOriginanteAntiguo() {
        return numOriginanteAntiguo;
    }

    public void setNumOriginanteAntiguo(String numOriginanteAntiguo) {
        this.numOriginanteAntiguo = numOriginanteAntiguo;
    }

    public String getIso8583() {
        return iso8583;
    }

    public void setIso8583(String iso8583) {
        this.iso8583 = iso8583;
    }

    @XmlElement(required=true)
    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    @XmlElement(required=true)
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @XmlElement(required=true)
    public String getCiNitOriginante() {
        return ciNitOriginante;
    }

    public void setCiNitOriginante(String ciNitOriginante) {
        this.ciNitOriginante = ciNitOriginante;
    }

    @XmlElement(required=true)
    public String getTitularOriginate() {
        return titularOriginate;
    }

    public void setTitularOriginate(String titularOriginate) {
        this.titularOriginate = titularOriginate;
    }

    @XmlElement(required=true)
    public String getCiNitDestinatario() {
        return ciNitDestinatario;
    }

    public void setCiNitDestinatario(String ciNitDestinatario) {
        this.ciNitDestinatario = ciNitDestinatario;
    }

    @XmlElement(required=true)
    public String getTitularDestinatario() {
        return titularDestinatario;
    }

    public void setTitularDestinatario(String titularDestinatario) {
        this.titularDestinatario = titularDestinatario;
    }

    @XmlElement(required=true)
    public String getCodSubDestinatario() {
        return codSubDestinatario;
    }

    public void setCodSubDestinatario(String codSubDestinatario) {
        this.codSubDestinatario = codSubDestinatario;
    }

    @XmlElement(required=true)
    public String getCodSubOriginante() {
        return codSubOriginante;
    }

    public void setCodSubOriginante(String codSubOriginante) {
        this.codSubOriginante = codSubOriginante;
    }

    @XmlElement(required=true)
    public String getOrigenFondos() {
        return origenFondos;
    }

    public void setOrigenFondos(String origenFondos) {
        this.origenFondos = origenFondos;
    }

    @XmlElement(required=true)
    public String getDestinoFondos() {
        return destinoFondos;
    }

    public void setDestinoFondos(String destinoFondos) {
        this.destinoFondos = destinoFondos;
    }

    @XmlElement(required=true)
    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }

}
