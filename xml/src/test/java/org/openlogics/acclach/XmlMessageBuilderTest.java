package org.openlogics.acclach;

import com.google.common.collect.ImmutableMap;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.utils.Constants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlogics.security.SecurityError;
import org.openlogics.xml.Xml;
import org.openlogics.xml.XmlSigner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;

import static org.apache.xml.security.utils.Constants.SignatureSpecNS;
import static org.apache.xml.security.utils.Constants._TAG_KEYNAME;

/**
 * @author Miguel Vega
 * @version $Id: XmlMessageBuilderTest.java 0, 1/9/14 7:03 PM miguel $
 */
public class XmlMessageBuilderTest {

    private KeyStore keyStore;
    private X509Certificate certificate;
    private PrivateKey privateKey;

    @Before
    public void init() throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {

        Security.addProvider(new BouncyCastleProvider());

        /*loadKeyStore("/banco-desa.p12", "poipoipoi");
        this.certificate = (X509Certificate) this.keyStore.getCertificate("banco desa");
        this.privateKey = (PrivateKey) this.keyStore.getKey("banco desa", "poipoipoi".toCharArray());*/

        loadKeyStore("/ach-bisa.jks", "bisabisa");
        this.certificate = (X509Certificate) this.keyStore.getCertificate("ach");
        this.privateKey = (PrivateKey) this.keyStore.getKey("1009", "c3rt4ch4achq".toCharArray());
    }

    private void loadKeyStore(String keyStore, String password) {
        try {
            this.keyStore = KeyStore.getInstance(keyStore.toLowerCase().trim().endsWith(".p12")?"PKCS12":"JKS");

//            keyStore.load(getClass().getResourceAsStream("/keystore.p12"), "poipoipoi".toCharArray());
//            this.certificate = (X509Certificate) keyStore.getCertificate("testing");
//            this.privateKey = (PrivateKey) keyStore.getKey("testing", "poipoipoi".toCharArray());

            this.keyStore.load(getClass().getResourceAsStream(keyStore), password.toCharArray());

            /*
            PEMReader reader = new PEMReader(new InputStreamReader(new FileInputStream("assets/certificates/fassil.pem")),
                    new PasswordFinder() {
                        @Override
                        public char[] getPassword() {
                            return "passwordfassil".toCharArray();
                        }
                    });

            KeyPair keyPair = (KeyPair) reader.readObject();
            */
            //this.privateKey = (PrivateKey) keyStore.getKey("testing", "poipoipoi".toCharArray());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testValidate1A() throws KeyStoreException, IOException, SAXException, TransformerException, XMLSecurityException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {

        loadKeyStore("/pubring.jks", "qwerty1");
//        loadKeyStore("/banco-desa.p12", "poipoipoi");

        Enumeration<String> aliases = keyStore.aliases();
        while (aliases.hasMoreElements()) {
            System.out.println("..."+aliases.nextElement());
        }

        System.out.println(">>>>>"+keyStore.getProvider().getInfo());

        this.certificate = (X509Certificate) keyStore.getCertificate("1004");

        Xml xml = new Xml(getClass().getResourceAsStream("/acclach/1aBCB.xml"));
        //xml.write(System.out, true);

        XmlSigner signer = new XmlSigner(xml);

        Assert.assertTrue(signer.verifySignature(certificate.getPublicKey()));
    }

    @Test
    public void testBuild1A() throws Exception, SecurityError {

        Mensaje1A m1a = new Mensaje1A("51510800908", null, "BO", "LPZ",
                "8585", "BO", "LPZ", "1004", "BOB", "BOB", "BOB", BigDecimal.valueOf(266d),
                "200", "50", "CCAO", "CCAD", 0, new Date(), new Date(), "J9I+UsKg00CvgUKjUUD9m4z4NGi507A9QArmAsr7FPbnipKBPKO3ncPtnxaGiGSue20ISHM36flC617PMdsINrHB6bCWjVgv3JoJ8zaqyLWgXZN2+tHLxVCkldAzT6d22IN4/68yGtBLdCGjAgVvaJLZRyLDeHnOUxRgXZSHGqxcd1FAcFJqgri5O6PFVfFatunnQpc80L56OrQwerdl6dIIh0U/HuJmUyaW4go19/NSmu8/sTUi5LH2hyQiV+DTBmKDd7r/UsbOu1IDug1X6pZK2JTxw45kw0RUiUhS7wSQED1oe22BIMZdNQI6YDnJydPT59Y12tPgU5jQBw2oQg==",
                "FAx/9LlikvYCJgfClz6twgX+jBe8m6RprGbBY9+TiBJfF26534N0Weby3RYrzqphojn2fu+xJNN23hk8qxwPiyiXHTOmEQxUq6dcbWRrGzid4RQoE2gg/9i09LBLJW+0ck8Pp2C6Agw3iEzkpWBYBbgnIQRkb/GobKmwtaRTscuLbhESz9XxSJ2Lsb8MkiJ9d1RudtR2Uf1/vwiRtAC7ZnIm3s/NiqQ2E01sxnUP15PeG0oyOIA1d5dARlUt1ROrJ70QlcuEMPzKZ7X2D2ccAKnRe2+/hR5FdQ3dmPaiGC6ErrusdPWrYdFJ81VkuYpHD/hi54XFSI4zn2o7LPIolA==",
                null, null, null, "PRIVADO", "1020115028", "EMP.PUBL.SOCIAL DE AGUA Y SANEAMIENTO", "1234543", "NINA VARGAS NIEVES JULIETA",
                null, null, null, null, null);
        m1a.setCodCamara(Integer.valueOf(3));

        //Mensaje mensaje = mensaje1Axml.armaMensaje1A(m1a);

        Xml xml = XmlMessageBuilder.build1A("8584", m1a,
                new KeyPair(certificate.getPublicKey(), privateKey),
                ImmutableMap.<String, Object>of("RECIBE_ENCRIPTADO", "Y"));

        System.out.println("********************************* "+certificate.getSigAlgName());
        xml.write(System.out, true);

        StringWriter w = new StringWriter();
        xml.write(w);
/*
        System.out.println("-----------------------------------------");
        System.out.println(w.toString());
*/

        //check signature
        Document documento1A = null;
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setAttribute("http://xml.org/sax/features/external-general-entities", false);
            documentBuilderFactory.setNamespaceAware(true);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            documento1A = documentBuilder.parse(new InputSource(new StringReader(w.toString())));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        NodeList signatures = documento1A.getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE);
        Element signatureElement = (Element) signatures.item(0);
        NodeList keyNameNodeList = signatureElement.getElementsByTagNameNS(SignatureSpecNS, _TAG_KEYNAME);
        if (keyNameNodeList.getLength() == 0) {
            throw new IllegalStateException("Elemento firmado no encontrado");
        }

        String keyName = keyNameNodeList.item(0).getTextContent();

        org.apache.xml.security.signature.XMLSignature signature = new org.apache.xml.security.signature.XMLSignature(signatureElement, null);

        //get the "<ds:Object Id="1001 - 1/2/2014 1:02:33 PM">" like element
        ObjectContainer objectItem = signature.getObjectItem(0);
        Element element = objectItem.getElement();

        boolean signatureNoVerificada = !signature.checkSignatureValue(certificate.getPublicKey());

        Assert.assertFalse(signatureNoVerificada);

        if (signatureNoVerificada) {
            throw new IllegalStateException("Firma con la llave llamada '" + keyName + "' no se ha podido verificar. " +
                    (Element) element.getFirstChild());
        }

        /*StringSelection stringSelection = new StringSelection(new String(w.toString()));
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);*/
    }

    @Test
    public void testBuild3A() throws Exception {
        Mensaje3A m = new Mensaje3A();
        m.setTitularDestinatario("PRUEBA ACH");
        m.setTipoOrden("210");
        m.setNumOrdenOriginante("4761624835545282307");
        m.setNumOrdenDestinatario("9203160214906802468");
        m.setCodPaisDestinatario("BO");
        m.setCodRespuesta("0000");
        m.setCodSucursalDestinatario("SCZ");
        m.setNumOrdenAch("18903683147");

        Xml xml = XmlMessageBuilder.build3A("1014", m, privateKey, null);

        System.out.println("*********************************************");
        xml.write(System.out, true);

        StringWriter writer = new StringWriter();
        xml.write(writer);

        XmlSigner signer = new XmlSigner(new Xml(new StringReader(writer.toString())));
        Assert.assertTrue(signer.verifySignature(certificate.getPublicKey()));
    }
}
