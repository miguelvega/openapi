package jaxws;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * @author Miguel Vega
 * @version $Id: SimpleServerImpl.java 0, 2013-11-08 11:06 AM mvega $
 */

@WebService(endpointInterface = "jaxws.SimpleServer")

public class SimpleServerImpl implements SimpleServer {

    private Logger LOGGER = Logger.getLogger(SimpleServerImpl.class.getName());

    public static void main(String[] args) {
        Endpoint.publish("http://127.0.0.1:9999/olws", new SimpleServerImpl());
    }

    @Override
    public String getDate(String format) {

        LOGGER.info("Receiving date format: " + format);

        DateFormat fmt = new SimpleDateFormat(format);

        return fmt.format(new Date());

    }
}
