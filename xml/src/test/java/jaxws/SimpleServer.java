package jaxws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Miguel Vega
 * @version $Id: SimpleServer.java 0, 2013-11-08 11:00 AM mvega $
 */
@WebService(targetNamespace = "http://jaxws.openlogics.org/", name = "SOAPServer")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface SimpleServer {

    @WebResult(name = "return")
    @WebMethod(operationName = "getDate", action = "getDate")
    String getDate(@WebParam(name = "dateFormat") String format);
}